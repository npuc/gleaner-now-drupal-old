<?php # local settings.php 

if (isset($_SERVER['HTTP_HOST'])) {

  switch ($_SERVER['HTTP_HOST']) {
    case 'gleanernew.dev:8888':
    case 'gleanernow.aimsolutions.co':
    case 'test.dev.gleanernow.com':
    case 'gleanernow.digibee.io':
      // Label the current env
      $current_env = 'dev';

      // do something on dev
      // Enable email rerouting.
      $conf['reroute_email_enable'] = 1;
      // Space, comma, or semicolon-delimited list of email addresses to pass
      // through. Every destination email address which is not on this list will be
      // rerouted to the first address on the list.
      $conf['reroute_email_address'] = "brent.hardinge@nw.npuc.org";

      $conf['stage_file_proxy_origin'] = 'http://gleanernow.com';
      $conf['stage_file_proxy_origin_dir'] = 'sites/gleanernow.com/files';
      break;

    case 'test.gleanernow.com':
      // Label the current env
      $current_env = 'test';

      // do something on staging
      $conf['reroute_email_enable'] = 0;
      break;

    case 'www.gleanernow.com':
      // forward www traffic.
      header('HTTP/1.0 301 Moved Permanently');
      header('Location: http://gleanernow.com'. $_SERVER['REQUEST_URI']);
    break;

    case 'www.gleanernow.com':
    case 'gleanernow.com':
      // Label the current env
      $current_env = 'live';

      // do something on prod
      $conf['reroute_email_enable'] = 0;
      break;
  }

}

// Make sure Drush keeps working. 
// Modified from function drush_verify_cli()
$cli = (php_sapi_name() == 'cli');
// PASSWORD-PROTECT NON-PRODUCTION SITES (i.e. staging/dev)
if (!$cli && (isset($current_env) && ($current_env = 'test' || $current_env = 'dev'))) {
  // $username = 'gleaner';
  // $password = 'now';
  // if (!(isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER']==$username && $_SERVER['PHP_AUTH_PW']==$password))) {
  //   header('WWW-Authenticate: Basic realm="This site is protected. Only enter if you are invited!"');
  //   header('HTTP/1.0 401 Unauthorized');
  //   // Fallback message when the user presses cancel / escape
  //   echo 'Access denied';
  //   exit;
  // }
}
