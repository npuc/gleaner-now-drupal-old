Your SOLR core access details for gleanernow.com site are as follows:

  Solr host ........: 127.0.0.1
  Solr port ........: 8099
  Solr path ........: /solr/d41d8cd98f00b204e9800998ecf8427e.gleanernow.com.o5097260284

It has been auto-configured to work with latest version
of search_api_solr module, but you need to add the module to
your site codebase before you will be able to use Solr.

To learn more please make sure to check the module docs at:

https://drupal.org/project/search_api_solr
