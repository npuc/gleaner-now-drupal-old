CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Credits


INTRODUCTION
------------

This module provides Instagram handler for Video Embed Field.
Users can add Instagram videos to their site by pasting 
the video's URL(like a "http://instagram.com/p/proUdFHWCY/")
into a video embed field.
In the setting users can set width and height options of the video.
This module also can get thumbnail image from Instagram website
for using it as a teaser image.

Requirements
============

- Video Embed Field
  https://drupal.org/project/video_embed_field


INSTALLATION
------------

1. Install and enable the module as usual.
2. Navigate to "admin/config/media/vef_video_styles" 
and configure some video style.


CREDITS
-------
* Yauheni Impalitau (impol)
