(function ($) {

  Drupal.PageActions = function($element, items) {
    var self = this;

    self.element = $element;
    self.form = self.element.closest('form');

    self.items = $('<ul class="items inline"></ul>');
    self.element.prepend(self.items);

    for (var i in items) {
      initItem(items[i]);
    }
    Drupal.attachBehaviors(self.items);

    function initItem(item) {
      item.target = $(item.selector);
      if (item.target.length > 0) {
        item.id = 'page-actions-item-' + item.target.attr('id');
        switch (item.type) {
          case 'toggle':
            item.labelText = item.target.siblings('label').text();
            if (!item.on) {
              item.on = item.labelText;
            }
            if (!item.off) {
              item.off = item.labelText;
            }

            var wrapper = $('<li></li>');
            item.element = $('<a href="#" class="toggle"></a>');

            item.target.is(':checked') ? item.element.text(item.on).addClass('checked') : item.element.text(item.off);

            item.element.click(function(e) {
              e.preventDefault();

              item.target.trigger('click').trigger('formUpdated').is(':checked') ? item.element.text(item.on).addClass('checked') : item.element.text(item.off).removeClass('checked');
            });
            wrapper.append(item.element);
            self.items.append(wrapper);
            break;
        }
      }
      return true;
    }
  }

  Drupal.behaviors.pageActions = {
    attach: function(context, settings) {
      $('#page-actions', context).once('page-actions', function() {
        var items = settings.pageActions.items || new Array();
        Drupal.PageActions = new Drupal.PageActions($(this), items);
        if (settings.pageActions.style.adjustMarginBottom) {
          // Adjust margin-bottom for body.
          $('body').css('margin-bottom', $(this).height());
        }
      });
    }
  };

})(jQuery);
