<?php 


$options['sites'] = array (
  0 => 'gleanernow.digibee.io',
  1 => 'gleanernow.com',
);
$options['profiles'] = array (
  0 => 'minimal',
  1 => 'standard',
);
$options['packages'] = array (
  'base' => 
  array (
    'modules' => 
    array (
      'locale' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/locale/locale.module',
        'basename' => 'locale.module',
        'name' => 'locale',
        'info' => 
        array (
          'name' => 'Locale',
          'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'locale.test',
          ),
          'configure' => 'admin/config/regional/language',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.35',
      ),
      'overlay' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/overlay/overlay.module',
        'basename' => 'overlay.module',
        'name' => 'overlay',
        'info' => 
        array (
          'name' => 'Overlay',
          'description' => 'Displays the Drupal administration interface in an overlay.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'aggregator' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/aggregator/aggregator.module',
        'basename' => 'aggregator.module',
        'name' => 'aggregator',
        'info' => 
        array (
          'name' => 'Aggregator',
          'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'aggregator.test',
          ),
          'configure' => 'admin/config/services/aggregator/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'aggregator.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.35',
      ),
      'statistics' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/statistics/statistics.module',
        'basename' => 'statistics.module',
        'name' => 'statistics',
        'info' => 
        array (
          'name' => 'Statistics',
          'description' => 'Logs access statistics for your site.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'statistics.test',
          ),
          'configure' => 'admin/config/system/statistics',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.35',
      ),
      'taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/taxonomy/taxonomy.module',
        'basename' => 'taxonomy.module',
        'name' => 'taxonomy',
        'info' => 
        array (
          'name' => 'Taxonomy',
          'description' => 'Enables the categorization of content.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'options',
          ),
          'files' => 
          array (
            0 => 'taxonomy.module',
            1 => 'taxonomy.test',
          ),
          'configure' => 'admin/structure/taxonomy',
          'php' => '5.2.4',
        ),
        'schema_version' => '7011',
        'version' => '7.35',
      ),
      'comment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/comment/comment.module',
        'basename' => 'comment.module',
        'name' => 'comment',
        'info' => 
        array (
          'name' => 'Comment',
          'description' => 'Allows users to comment on and discuss published content.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'text',
          ),
          'files' => 
          array (
            0 => 'comment.module',
            1 => 'comment.test',
          ),
          'configure' => 'admin/content/comment',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'comment.css',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'version' => '7.35',
      ),
      'menu' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/menu/menu.module',
        'basename' => 'menu.module',
        'name' => 'menu',
        'info' => 
        array (
          'name' => 'Menu',
          'description' => 'Allows administrators to customize the site navigation menu.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'menu.test',
          ),
          'configure' => 'admin/structure/menu',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.35',
      ),
      'trigger' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/trigger/trigger.module',
        'basename' => 'trigger.module',
        'name' => 'trigger',
        'info' => 
        array (
          'name' => 'Trigger',
          'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'trigger.test',
          ),
          'configure' => 'admin/structure/trigger',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.35',
      ),
      'search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/search/search.module',
        'basename' => 'search.module',
        'name' => 'search',
        'info' => 
        array (
          'name' => 'Search',
          'description' => 'Enables site-wide keyword searching.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search.extender.inc',
            1 => 'search.test',
          ),
          'configure' => 'admin/config/search/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'search.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.35',
      ),
      'blog' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/blog/blog.module',
        'basename' => 'blog.module',
        'name' => 'blog',
        'info' => 
        array (
          'name' => 'Blog',
          'description' => 'Enables multi-user blogs.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'blog.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'shortcut' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/shortcut/shortcut.module',
        'basename' => 'shortcut.module',
        'name' => 'shortcut',
        'info' => 
        array (
          'name' => 'Shortcut',
          'description' => 'Allows users to manage customizable lists of shortcut links.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'shortcut.test',
          ),
          'configure' => 'admin/config/user-interface/shortcut',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'image' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/image/image.module',
        'basename' => 'image.module',
        'name' => 'image',
        'info' => 
        array (
          'name' => 'Image',
          'description' => 'Provides image manipulation tools.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'files' => 
          array (
            0 => 'image.test',
          ),
          'configure' => 'admin/config/media/image-styles',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.35',
      ),
      'tracker' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/tracker/tracker.module',
        'basename' => 'tracker.module',
        'name' => 'tracker',
        'info' => 
        array (
          'name' => 'Tracker',
          'description' => 'Enables tracking of recent content for users.',
          'dependencies' => 
          array (
            0 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tracker.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.35',
      ),
      'path' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/path/path.module',
        'basename' => 'path.module',
        'name' => 'path',
        'info' => 
        array (
          'name' => 'Path',
          'description' => 'Allows users to rename URLs.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'path.test',
          ),
          'configure' => 'admin/config/search/path',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'filter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/filter/filter.module',
        'basename' => 'filter.module',
        'name' => 'filter',
        'info' => 
        array (
          'name' => 'Filter',
          'description' => 'Filters content in preparation for display.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filter.test',
          ),
          'required' => true,
          'configure' => 'admin/config/content/formats',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'version' => '7.35',
      ),
      'dblog' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/dblog/dblog.module',
        'basename' => 'dblog.module',
        'name' => 'dblog',
        'info' => 
        array (
          'name' => 'Database logging',
          'description' => 'Logs and records system events to the database.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'dblog.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.35',
      ),
      'user' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/user/user.module',
        'basename' => 'user.module',
        'name' => 'user',
        'info' => 
        array (
          'name' => 'User',
          'description' => 'Manages the user registration and login system.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'user.module',
            1 => 'user.test',
          ),
          'required' => true,
          'configure' => 'admin/config/people',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'user.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7018',
        'version' => '7.35',
      ),
      'panels_content_cache' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/panels_content_cache/panels_content_cache.module',
        'basename' => 'panels_content_cache.module',
        'name' => 'panels_content_cache',
        'info' => 
        array (
          'name' => 'Panels Content Cache',
          'core' => '7.x',
          'package' => 'Panels',
          'description' => 'A content based caching plugin for panels. Allows panel caches to be expired based on content creation / updates.',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'version' => '7.x-1.0',
          'project' => 'panels_content_cache',
          'datestamp' => '1367328019',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'robotstxt' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/robotstxt/robotstxt.module',
        'basename' => 'robotstxt.module',
        'name' => 'robotstxt',
        'info' => 
        array (
          'name' => 'robots.txt',
          'description' => 'Generates the robots.txt file dynamically and gives you the chance to edit it, on a per-site basis, from the web UI.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'robotstxt.module',
            1 => 'robotstxt.admin.inc',
            2 => 'robotstxt.install',
          ),
          'configure' => 'admin/config/search/robotstxt',
          'version' => '7.x-1.3',
          'project' => 'robotstxt',
          'datestamp' => '1419385385',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.3',
      ),
      'views_taxonomy_edge' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/taxonomy_edge/views_taxonomy_edge/views_taxonomy_edge.module',
        'basename' => 'views_taxonomy_edge.module',
        'name' => 'views_taxonomy_edge',
        'info' => 
        array (
          'name' => 'Views Taxonomy Edge',
          'description' => 'Views for taxonomies using Taxonomy Edge',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'taxonomy_edge',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'handlers/views_handler_argument_term_edge_node_tid_depth.inc',
            1 => 'handlers/views_handler_argument_term_edge_node_tid_depth_modifier.inc',
            2 => 'handlers/views_handler_filter_term_edge_node_tid_depth.inc',
            3 => 'handlers/views_join_term_edge.inc',
            4 => 'handlers/views_handler_sort_term_edge_hierarchy.inc',
          ),
          'version' => '7.x-1.9',
          'project' => 'taxonomy_edge',
          'datestamp' => '1409109835',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.9',
      ),
      'taxonomy_edge' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/taxonomy_edge/taxonomy_edge.module',
        'basename' => 'taxonomy_edge.module',
        'name' => 'taxonomy_edge',
        'info' => 
        array (
          'name' => 'Taxonomy Edge',
          'description' => 'Edge lists for taxonomies',
          'package' => 'Taxonomy',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'configure' => 'admin/structure/taxonomy/edge',
          'files' => 
          array (
            0 => 'tests/tree.test',
            1 => 'tests/unit.test',
          ),
          'version' => '7.x-1.9',
          'project' => 'taxonomy_edge',
          'datestamp' => '1409109835',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.9',
      ),
      'redis' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/redis/redis.module',
        'basename' => 'redis.module',
        'name' => 'redis',
        'info' => 
        array (
          'dependencies' => 
          array (
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'blockcache_alter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/blockcache_alter/blockcache_alter.module',
        'basename' => 'blockcache_alter.module',
        'name' => 'blockcache_alter',
        'info' => 
        array (
          'name' => 'Block Cache Alter',
          'description' => 'Alter the cache settings per block.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'configure' => 'admin/config/development/performance/blockcache_alter',
          'version' => '7.x-1.1-beta1+0-dev',
          'project' => 'blockcache_alter',
          'datestamp' => '1415445181',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.1-beta1+0-dev',
      ),
      'expire' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/expire/expire.module',
        'basename' => 'expire.module',
        'name' => 'expire',
        'info' => 
        array (
          'name' => 'Cache Expiration',
          'description' => 'Logic for expiring cached pages.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'configure' => 'admin/config/system/expire',
          'files' => 
          array (
            0 => 'includes/expire.api.inc',
            1 => 'includes/expire.comment.inc',
            2 => 'includes/expire.domain.inc',
            3 => 'includes/expire.interface.inc',
            4 => 'includes/expire.menu_link.inc',
            5 => 'includes/expire.node.inc',
            6 => 'includes/expire.user.inc',
            7 => 'includes/expire.votingapi.inc',
            8 => 'includes/expire.file.inc',
          ),
          'version' => '7.x-2.0-rc4',
          'project' => 'expire',
          'datestamp' => '1414746830',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0-rc4',
      ),
      'views404' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/views404/views404.module',
        'basename' => 'views404.module',
        'name' => 'views404',
        'info' => 
        array (
          'name' => 'Views 404',
          'description' => 'Return 404 if the path doesn\'t match the view path.',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'views404',
          'datestamp' => '1418590460',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'readonlymode' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/readonlymode/readonlymode.module',
        'basename' => 'readonlymode.module',
        'name' => 'readonlymode',
        'info' => 
        array (
          'name' => 'Read Only Mode',
          'description' => 'This module will lock your site for any form postings.',
          'core' => '7.x',
          'package' => 'Administration',
          'configure' => 'admin/config/development/maintenance',
          'files' => 
          array (
            0 => 'readonlymode.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'readonlymode',
          'datestamp' => '1402574630',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.2',
      ),
      'css_emimage' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/css_emimage/css_emimage.module',
        'basename' => 'css_emimage.module',
        'name' => 'css_emimage',
        'info' => 
        array (
          'name' => 'CSS Embedded Images',
          'description' => 'Replaces image URLs in CSS files with embedded images when CSS optimization is enabled.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'configure' => 'admin/config/development/performance',
          'version' => '7.x-1.3+3-dev',
          'project' => 'css_emimage',
          'datestamp' => '1380568223',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3+3-dev',
      ),
      'flood_control' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/flood_control/flood_control.module',
        'basename' => 'flood_control.module',
        'name' => 'flood_control',
        'info' => 
        array (
          'name' => 'Flood control',
          'description' => 'Interface for hidden flood control options.',
          'core' => '7.x',
          'configure' => 'admin/config/system/flood-control',
          'version' => '7.x-1.x-dev',
          'project' => 'flood_control',
          'datestamp' => '1380579747',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'views_accelerator' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/views_accelerator/views_accelerator.module',
        'basename' => 'views_accelerator.module',
        'name' => 'views_accelerator',
        'info' => 
        array (
          'name' => 'Views Accelerator',
          'description' => 'Performance booster for views that are receptive to render optimisations.',
          'core' => '7.x',
          'configure' => 'admin/config/system/views-accelerator',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views/plugins/views_plugin_cache_none_accelerated.inc',
            1 => 'views/plugins/views_plugin_cache_none_debug.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'views_accelerator',
          'datestamp' => '1415076828',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'views_content_cache' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/views_content_cache/views_content_cache.module',
        'basename' => 'views_content_cache.module',
        'name' => 'views_content_cache',
        'info' => 
        array (
          'name' => 'Views Content Cache',
          'description' => 'Provides a views cache plugin based on content type changes.',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'plugins/views_content_cache/base.inc',
            1 => 'plugins/views_content_cache/comment.inc',
            2 => 'plugins/views_content_cache/node.inc',
            3 => 'plugins/views_content_cache/node_only.inc',
            4 => 'plugins/views_content_cache/og.inc',
            5 => 'plugins/views_content_cache/votingapi.inc',
            6 => 'views/views_content_cache_plugin_cache.inc',
            7 => 'tests/views_content_cache.test',
          ),
          'version' => '7.x-3.0-alpha3',
          'project' => 'views_content_cache',
          'datestamp' => '1383658110',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0-alpha3',
      ),
      'filefield_nginx_progress' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/filefield_nginx_progress/filefield_nginx_progress.module',
        'basename' => 'filefield_nginx_progress.module',
        'name' => 'filefield_nginx_progress',
        'info' => 
        array (
          'name' => 'FileField Nginx Progress',
          'description' => 'Adds upload progress functionality for Nginx',
          'files' => 
          array (
            0 => 'filefield_nginx_progress.install',
            1 => 'filefield_nginx_progress.module',
          ),
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'php' => '5.2',
          'version' => '7.x-2.3+1-dev',
          'project' => 'filefield_nginx_progress',
          'datestamp' => '1380579093',
        ),
        'schema_version' => '7100',
        'version' => '7.x-2.3+1-dev',
      ),
      'session_expire' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/session_expire/session_expire.module',
        'basename' => 'session_expire.module',
        'name' => 'session_expire',
        'info' => 
        array (
          'name' => 'Session Expire',
          'description' => 'Expires rows from the session table older than a certain time.',
          'core' => '7.x',
          'configure' => 'admin/config/system/session_expire',
          'files' => 
          array (
            0 => 'session_expire.module',
          ),
          'version' => '7.x-1.0-alpha1+7-dev',
          'project' => 'session_expire',
          'datestamp' => '1417209266',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha1+7-dev',
      ),
      'admin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/admin/admin.module',
        'basename' => 'admin.module',
        'name' => 'admin',
        'info' => 
        array (
          'name' => 'Admin',
          'description' => 'UI helpers for Drupal admins and managers.',
          'package' => 'Administration',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'admin.admin.inc',
            1 => 'admin.install',
            2 => 'admin.module',
            3 => 'includes/admin.devel.inc',
            4 => 'includes/admin.theme.inc',
            5 => 'theme/admin-panes.tpl.php',
            6 => 'theme/admin-toolbar.tpl.php',
            7 => 'theme/theme.inc',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'admin',
          'datestamp' => '1292541646',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta3',
      ),
      'config_perms' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/config_perms/config_perms.module',
        'basename' => 'config_perms.module',
        'name' => 'config_perms',
        'info' => 
        array (
          'name' => 'Custom Permissions',
          'description' => 'Allows additional permissions to be created and managed through an administration form.
<br /><small>Machine name: config_perms</small>',
          'core' => '7.x',
          'package' => 'Permissions',
          'configure' => 'admin/people/custom_permissions',
          'version' => '7.x-2.0+33-dev',
          'project' => 'config_perms',
          'datestamp' => '1412099050',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.0+33-dev',
      ),
      'views_cache_bully' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/views_cache_bully/views_cache_bully.module',
        'basename' => 'views_cache_bully.module',
        'name' => 'views_cache_bully',
        'info' => 
        array (
          'name' => 'Views Cache Bully',
          'description' => 'Forcibily cache all views, come hell or high water.',
          'core' => '7.x',
          'package' => 'views',
          'files' => 
          array (
            0 => 'views_cache_bully.views.inc',
          ),
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => '7.x-3.1',
          'project' => 'views_cache_bully',
          'datestamp' => '1389886710',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.1',
      ),
      'variable_clean' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/variable_clean/variable_clean.module',
        'basename' => 'variable_clean.module',
        'name' => 'variable_clean',
        'info' => 
        array (
          'name' => 'Variable Cleanup',
          'description' => 'Allows you to remove variables not currently used.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'variable_clean.module',
            1 => 'variable_clean.test',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'variable_clean',
          'datestamp' => '1382151358',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'esi_panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/esi/modules/esi_panels/esi_panels.module',
        'basename' => 'esi_panels.module',
        'name' => 'esi_panels',
        'info' => 
        array (
          'name' => 'ESI - Panels',
          'description' => 'Deliver panel-panes via ESI.',
          'core' => '7.x',
          'package' => 'Caching',
          'dependencies' => 
          array (
            0 => 'panels',
            1 => 'page_manager',
            2 => 'esi',
          ),
          'version' => '7.x-3.0-alpha1+16-dev',
          'project' => 'esi',
          'datestamp' => '1380577290',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0-alpha1+16-dev',
      ),
      'esi_context' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/esi/modules/esi_context/esi_context.module',
        'basename' => 'esi_context.module',
        'name' => 'esi_context',
        'info' => 
        array (
          'name' => 'ESI - Context integration',
          'description' => 'Deliver context-controlled blocks via ESI.',
          'core' => '7.x',
          'package' => 'Caching',
          'dependencies' => 
          array (
            0 => 'context',
            1 => 'esi',
            2 => 'esi_block',
          ),
          'version' => '7.x-3.0-alpha1+16-dev',
          'project' => 'esi',
          'datestamp' => '1380577290',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0-alpha1+16-dev',
      ),
      'esi_block' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/esi/modules/esi_block/esi_block.module',
        'basename' => 'esi_block.module',
        'name' => 'esi_block',
        'info' => 
        array (
          'name' => 'ESI - Block',
          'description' => 'Deliver Drupal blocks via ESI.',
          'core' => '7.x',
          'package' => 'Caching',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'esi',
          ),
          'files' => 
          array (
            0 => 'esi_block.test',
          ),
          'version' => '7.x-3.0-alpha1+16-dev',
          'project' => 'esi',
          'datestamp' => '1380577290',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0-alpha1+16-dev',
      ),
      'esi' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/esi/esi.module',
        'basename' => 'esi.module',
        'name' => 'esi',
        'info' => 
        array (
          'name' => 'ESI - Edge Side Includes',
          'description' => 'Allow Drupal components to be delivered via ESI (Edge-Side Includes).  Requires an ESI-capable proxy.',
          'package' => 'Caching',
          'recommends' => 
          array (
            0 => 'varnish',
          ),
          'core' => '7.x',
          'version' => '7.x-3.0-alpha1+16-dev',
          'project' => 'esi',
          'datestamp' => '1380577290',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-3.0-alpha1+16-dev',
      ),
      'display_cache' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/display_cache/display_cache.module',
        'basename' => 'display_cache.module',
        'name' => 'display_cache',
        'info' => 
        array (
          'name' => 'Display Cache',
          'description' => 'Provides views and panels plugins to display rendered entities from cache.',
          'package' => 'Performance',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.0',
          'project' => 'display_cache',
          'datestamp' => '1399555728',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'securesite' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/securesite/securesite.module',
        'basename' => 'securesite.module',
        'name' => 'securesite',
        'info' => 
        array (
          'name' => 'Secure Site',
          'description' => 'Enables HTTP Auth security or an HTML form to restrict site access.',
          'core' => '7.x',
          'configure' => 'admin/config/system/securesite',
          'files' => 
          array (
            0 => 'securesite.test',
            1 => 'securesite.inc',
          ),
          'version' => '7.x-2.0-beta2+4-dev',
          'project' => 'securesite',
          'datestamp' => '1396946353',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6200',
        'version' => '7.x-2.0-beta2+4-dev',
      ),
      'fpa' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/fpa/fpa.module',
        'basename' => 'fpa.module',
        'name' => 'fpa',
        'info' => 
        array (
          'name' => 'Fast Permissions Administration',
          'description' => 'Fast filtering on permissions administration form.',
          'core' => '7.x',
          'package' => 'Administration',
          'version' => '7.x-2.6',
          'project' => 'fpa',
          'datestamp' => '1408744435',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.6',
      ),
      'purge' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/purge/purge.module',
        'basename' => 'purge.module',
        'name' => 'purge',
        'info' => 
        array (
          'name' => 'Purge',
          'description' => 'Purge clears urls from reverse proxy caches like Varnish and Squid by issuing HTTP PURGE requests.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'expire',
          ),
          'files ' => 
          array (
            0 => 'purge.module',
            1 => 'purge.inc',
          ),
          'configure' => 'admin/config/development/performance/purge',
          'version' => '7.x-1.6',
          'project' => 'purge',
          'datestamp' => '1358998406',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.6',
      ),
      'login_security' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/login_security/login_security.module',
        'basename' => 'login_security.module',
        'name' => 'login_security',
        'info' => 
        array (
          'name' => 'Login Security',
          'description' => 'Enable security options in the login flow of the site.',
          'files' => 
          array (
            0 => 'login_security.test',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/people/login_security',
          'version' => '7.x-1.9',
          'project' => 'login_security',
          'datestamp' => '1392987818',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.9',
      ),
      'site_verify' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/site_verify/site_verify.module',
        'basename' => 'site_verify.module',
        'name' => 'site_verify',
        'info' => 
        array (
          'name' => 'Site Verification',
          'description' => 'Verifies ownership of a site for use with search engines.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'site_verify.module',
            1 => 'site_verify.admin.inc',
            2 => 'site_verify.install',
            3 => 'site_verify.test',
          ),
          'configure' => 'admin/config/search/verifications',
          'version' => '7.x-1.1',
          'project' => 'site_verify',
          'datestamp' => '1395656959',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '3',
        'version' => '7.x-1.1',
      ),
      'httprl' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/httprl/httprl.module',
        'basename' => 'httprl.module',
        'name' => 'httprl',
        'info' => 
        array (
          'name' => 'HTTP Parallel Request Library',
          'description' => 'Send http requests out in parallel in a blocking or non-blocking manner.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'version' => '7.x-1.14',
          'project' => 'httprl',
          'datestamp' => '1388542110',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'speedy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/speedy/speedy.module',
        'basename' => 'speedy.module',
        'name' => 'speedy',
        'info' => 
        array (
          'name' => 'Speedy',
          'description' => 'Tools to improve the front end performance of your site.',
          'core' => '7.x',
          'configure' => 'admin/config/development/performance',
          'version' => '7.x-1.18',
          'project' => 'speedy',
          'datestamp' => '1431027186',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.18',
      ),
      'vars' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/vars/vars.module',
        'basename' => 'vars.module',
        'name' => 'vars',
        'info' => 
        array (
          'name' => 'Variable API',
          'description' => 'Implements an API to handle persistent variables.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'vars.classes.inc',
            1 => 'tests/vars.test',
          ),
          'version' => '7.x-2.0-alpha10',
          'project' => 'vars',
          'datestamp' => '1318969538',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7204',
        'version' => '7.x-2.0-alpha10',
      ),
      'cdn' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/cdn/cdn.module',
        'basename' => 'cdn.module',
        'name' => 'cdn',
        'info' => 
        array (
          'name' => 'CDN',
          'description' => 'Integrates your site with a CDN, through altering file URLs.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'configure' => 'admin/config/development/cdn',
          'files' => 
          array (
            0 => 'cdn.test',
          ),
          'version' => '7.x-2.6+3-dev',
          'project' => 'cdn',
          'datestamp' => '1392895444',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7210',
        'version' => '7.x-2.6+3-dev',
      ),
      'advagg_css_cdn' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_css_cdn/advagg_css_cdn.module',
        'basename' => 'advagg_css_cdn.module',
        'name' => 'advagg_css_cdn',
        'info' => 
        array (
          'name' => 'AdvAgg CDN CSS',
          'description' => 'Use a shared CDN for CSS libraries, Google Libraries API currently.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'advagg_bundler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_bundler/advagg_bundler.module',
        'basename' => 'advagg_bundler.module',
        'name' => 'advagg_bundler',
        'info' => 
        array (
          'name' => 'AdvAgg Bundler',
          'description' => 'Provides intelligent bundling of CSS and JS files by grouping files that belong together.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'configure' => 'admin/config/development/performance/advagg/bundler',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'advagg_js_compress' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_js_compress/advagg_js_compress.module',
        'basename' => 'advagg_js_compress.module',
        'name' => 'advagg_js_compress',
        'info' => 
        array (
          'name' => 'AdvAgg Compress Javascript',
          'description' => 'Compress Javascript with a 3rd party compressor, JSMin+ currently.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'configure' => 'admin/config/development/performance/advagg/js-compress',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.7',
      ),
      'advagg_js_cdn' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_js_cdn/advagg_js_cdn.module',
        'basename' => 'advagg_js_cdn.module',
        'name' => 'advagg_js_cdn',
        'info' => 
        array (
          'name' => 'AdvAgg CDN Javascript',
          'description' => 'Use a shared CDN for javascript libraries, Google Libraries API currently.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'advagg_validator' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_validator/advagg_validator.module',
        'basename' => 'advagg_validator.module',
        'name' => 'advagg_validator',
        'info' => 
        array (
          'name' => 'AdvAgg CSS/JS Validator',
          'description' => 'Validate the CSS & JS files used in Aggregation for syntax errors.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'configure' => 'admin/config/development/performance/advagg/validator',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'advagg_css_compress' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_css_compress/advagg_css_compress.module',
        'basename' => 'advagg_css_compress.module',
        'name' => 'advagg_css_compress',
        'info' => 
        array (
          'name' => 'AdvAgg Compress CSS',
          'description' => 'Compress CSS with a 3rd party compressor, YUI currently.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'configure' => 'admin/config/development/performance/advagg/css-compress',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.7',
      ),
      'advagg_mod' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg_mod/advagg_mod.module',
        'basename' => 'advagg_mod.module',
        'name' => 'advagg_mod',
        'info' => 
        array (
          'name' => 'AdvAgg Modifier',
          'description' => 'Allows one to alter the CSS and JS array.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'advagg',
          ),
          'configure' => 'admin/config/development/performance/advagg/mod',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'advagg' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/advagg/advagg.module',
        'basename' => 'advagg.module',
        'name' => 'advagg',
        'info' => 
        array (
          'name' => 'Advanced CSS/JS Aggregation',
          'description' => 'Aggregates multiple CSS/JS files in a way that prevents 404 from happening when accessing a CSS or JS file.',
          'package' => 'Advanced CSS/JS Aggregation',
          'core' => '7.x',
          'configure' => 'admin/config/development/performance/advagg',
          'version' => '7.x-2.7',
          'project' => 'advagg',
          'datestamp' => '1402608232',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7205',
        'version' => '7.x-2.7',
      ),
      'force_password_change' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/force_password_change/force_password_change.module',
        'basename' => 'force_password_change.module',
        'name' => 'force_password_change',
        'info' => 
        array (
          'name' => 'Force Password Change',
          'description' => 'Allows administrators to force users to change their password',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'force_password_change.install',
            1 => 'force_password_change.module',
            2 => 'force_password_change.pages.inc',
          ),
          'version' => '7.x-1.0-rc2',
          'project' => 'force_password_change',
          'datestamp' => '1296546103',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-rc2',
      ),
      'entitycache' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/entitycache/entitycache.module',
        'basename' => 'entitycache.module',
        'name' => 'entitycache',
        'info' => 
        array (
          'name' => 'Entity cache',
          'description' => 'Provides caching for core entities including nodes and taxonomy terms.',
          'core' => '7.x',
          'package' => 'Performance and scalability',
          'files' => 
          array (
            0 => 'entitycache.module',
            1 => 'entitycache.comment.inc',
            2 => 'entitycache.taxonomy.inc',
            3 => 'entitycache.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'entitycache',
          'datestamp' => '1383216926',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.2',
      ),
      'reroute_email' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/reroute_email/reroute_email.module',
        'basename' => 'reroute_email.module',
        'name' => 'reroute_email',
        'info' => 
        array (
          'name' => 'Reroute emails',
          'description' => 'Reroutes emails send from the site to a predefined email. Useful for test sites.',
          'package' => 'Development',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'reroute_email.test',
          ),
          'configure' => 'admin/config/development/reroute_email',
          'version' => '7.x-1.2',
          'project' => 'reroute_email',
          'datestamp' => '1414831432',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'print_mail' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_mail/print_mail.module',
        'basename' => 'print_mail.module',
        'name' => 'print_mail',
        'info' => 
        array (
          'name' => 'Send by email',
          'description' => 'Provides the capability to send the web page by email',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print',
          ),
          'configure' => 'admin/config/user-interface/print/email',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => NULL,
      ),
      'print_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_ui/print_ui.module',
        'basename' => 'print_ui.module',
        'name' => 'print_ui',
        'info' => 
        array (
          'name' => 'Printer-friendly pages UI',
          'description' => 'Manages the printer-friendly versions link display conditions. Without this module, those links are not displayed.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print',
          ),
          'configure' => 'admin/config/user-interface/print/ui',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_epub_phpepub' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_epub/lib_handlers/print_epub_phpepub/print_epub_phpepub.module',
        'basename' => 'print_epub_phpepub.module',
        'name' => 'print_epub_phpepub',
        'info' => 
        array (
          'name' => 'PHPePub library handler',
          'description' => 'EPUB generation library using PHPePub.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print_epub',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_epub' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_epub/print_epub.module',
        'basename' => 'print_epub.module',
        'name' => 'print_epub',
        'info' => 
        array (
          'name' => 'EPUB version',
          'description' => 'Adds the capability to export pages as EPUB. Requires an EPUB library and the respective handler module.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print',
          ),
          'configure' => 'admin/config/user-interface/print/epub',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => NULL,
      ),
      'print_pdf_dompdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_pdf/lib_handlers/print_pdf_dompdf/print_pdf_dompdf.module',
        'basename' => 'print_pdf_dompdf.module',
        'name' => 'print_pdf_dompdf',
        'info' => 
        array (
          'name' => 'dompdf library handler',
          'description' => 'PDF generation library using dompdf.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print_pdf',
          ),
          'configure' => 'admin/config/user-interface/print/pdf/dompdf',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_pdf_mpdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_pdf/lib_handlers/print_pdf_mpdf/print_pdf_mpdf.module',
        'basename' => 'print_pdf_mpdf.module',
        'name' => 'print_pdf_mpdf',
        'info' => 
        array (
          'name' => 'mPDF library handler',
          'description' => 'PDF generation library using mPDF.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print_pdf',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_pdf_tcpdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_pdf/lib_handlers/print_pdf_tcpdf/print_pdf_tcpdf.module',
        'basename' => 'print_pdf_tcpdf.module',
        'name' => 'print_pdf_tcpdf',
        'info' => 
        array (
          'name' => 'TCPDF library handler',
          'description' => 'PDF generation library using TCPDF.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print_pdf',
          ),
          'files' => 
          array (
            0 => 'print_pdf_tcpdf.class.inc',
          ),
          'configure' => 'admin/config/user-interface/print/pdf/tcpdf',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_pdf_wkhtmltopdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_pdf/lib_handlers/print_pdf_wkhtmltopdf/print_pdf_wkhtmltopdf.module',
        'basename' => 'print_pdf_wkhtmltopdf.module',
        'name' => 'print_pdf_wkhtmltopdf',
        'info' => 
        array (
          'name' => 'wkhtmltopdf library handler',
          'description' => 'PDF generation library using wkhtmltopdf.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print_pdf',
          ),
          'configure' => 'admin/config/user-interface/print/pdf/wkhtmltopdf',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_pdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print_pdf/print_pdf.module',
        'basename' => 'print_pdf.module',
        'name' => 'print_pdf',
        'info' => 
        array (
          'name' => 'PDF version',
          'description' => 'Adds the capability to export pages as PDF. Requires a PDF library and the respective handler module.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'dependencies' => 
          array (
            0 => 'print',
          ),
          'configure' => 'admin/config/user-interface/print/pdf',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7205',
        'version' => NULL,
      ),
      'print' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/print/print.module',
        'basename' => 'print.module',
        'name' => 'print',
        'info' => 
        array (
          'name' => 'Printer-friendly pages',
          'description' => 'Generates a printer-friendly version of Drupal pages.',
          'core' => '7.x',
          'package' => 'Printer, email and PDF versions',
          'files' => 
          array (
            0 => 'print_join_page_counter.inc',
          ),
          'dependencies' => 
          array (
            0 => 'node',
          ),
          'configure' => 'admin/config/user-interface/print',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => NULL,
      ),
      'js_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/js/tests/js_test.module',
        'basename' => 'js_test.module',
        'name' => 'js_test',
        'info' => 
        array (
          'name' => 'JavaScript callback handler tests',
          'description' => 'Tests for the JS module.',
          'package' => 'Performance',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'js_test.test',
          ),
          'version' => '7.x-1.0+5-dev',
          'project' => 'js',
          'datestamp' => '1394174606',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0+5-dev',
      ),
      'js' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/js/js.module',
        'basename' => 'js.module',
        'name' => 'js',
        'info' => 
        array (
          'name' => 'JavaScript callback handler',
          'description' => 'A high-performance JavaScript callback handler for 3rd party modules.',
          'package' => 'Performance',
          'core' => '7.x',
          'configure' => 'admin/config/system/js',
          'version' => '7.x-1.0+5-dev',
          'project' => 'js',
          'datestamp' => '1394174606',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0+5-dev',
      ),
      'boost_crawler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/boost/boost_crawler/boost_crawler.module',
        'basename' => 'boost_crawler.module',
        'name' => 'boost_crawler',
        'info' => 
        array (
          'name' => 'Boost Crawler',
          'description' => 'Minimal crawler to regenerate the cache as pages are expired.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'boost_crawler.module',
          ),
          'recommends' => 
          array (
            0 => 'boost',
          ),
          'dependencies' => 
          array (
            0 => 'httprl',
            1 => 'expire',
          ),
          'configure' => 'admin/config/system/boost/crawler',
          'version' => '7.x-1.0',
          'project' => 'boost',
          'datestamp' => '1399056528',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'boost' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/boost/boost.module',
        'basename' => 'boost.module',
        'name' => 'boost',
        'info' => 
        array (
          'name' => 'Boost',
          'description' => 'Caches generated output as a static file to be served directly from the webserver.',
          'package' => 'Performance and scalability',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'boost.module',
            1 => 'boost.admin.inc',
            2 => 'boost.blocks.inc',
          ),
          'recommends' => 
          array (
            0 => 'expire',
          ),
          'configure' => 'admin/config/system/boost',
          'version' => '7.x-1.0',
          'project' => 'boost',
          'datestamp' => '1399056528',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'textile' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/textile/textile.module',
        'basename' => 'textile.module',
        'name' => 'textile',
        'info' => 
        array (
          'name' => 'Textile',
          'description' => 'Allows content to be submitted using Textile, a simple, plain text syntax that is filtered into valid XHTML.',
          'package' => 'Input filters',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'vars (> 1.0)',
          ),
          'version' => '7.x-2.0-rc11',
          'project' => 'textile',
          'datestamp' => '1319231138',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-rc11',
      ),
      'nocurrent_pass' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/o_contrib_seven/nocurrent_pass/nocurrent_pass.module',
        'basename' => 'nocurrent_pass.module',
        'name' => 'nocurrent_pass',
        'info' => 
        array (
          'name' => 'No Current Password',
          'description' => 'Make the "current password" requirement on the user edit form optional.',
          'package' => 'Other',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'nocurrent_pass',
          'datestamp' => '1328692247',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'rdf' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/rdf/rdf.module',
        'basename' => 'rdf.module',
        'name' => 'rdf',
        'info' => 
        array (
          'name' => 'RDF',
          'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rdf.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'poll' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/poll/poll.module',
        'basename' => 'poll.module',
        'name' => 'poll',
        'info' => 
        array (
          'name' => 'Poll',
          'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'poll.test',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'poll.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.35',
      ),
      'translation' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/translation/translation.module',
        'basename' => 'translation.module',
        'name' => 'translation',
        'info' => 
        array (
          'name' => 'Content translation',
          'description' => 'Allows content to be translated into different languages.',
          'dependencies' => 
          array (
            0 => 'locale',
          ),
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'translation.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'file' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/file/file.module',
        'basename' => 'file.module',
        'name' => 'file',
        'info' => 
        array (
          'name' => 'File',
          'description' => 'Defines a file field type.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'tests/file.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'field_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field_ui/field_ui.module',
        'basename' => 'field_ui.module',
        'name' => 'field_ui',
        'info' => 
        array (
          'name' => 'Field UI',
          'description' => 'User interface for the Field API.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_ui.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'block' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/block/block.module',
        'basename' => 'block.module',
        'name' => 'block',
        'info' => 
        array (
          'name' => 'Block',
          'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'block.test',
          ),
          'configure' => 'admin/structure/block',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'version' => '7.35',
      ),
      'update' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/update/update.module',
        'basename' => 'update.module',
        'name' => 'update',
        'info' => 
        array (
          'name' => 'Update manager',
          'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
          'version' => '7.35',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'update.test',
          ),
          'configure' => 'admin/reports/updates/settings',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.35',
      ),
      'book' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/book/book.module',
        'basename' => 'book.module',
        'name' => 'book',
        'info' => 
        array (
          'name' => 'Book',
          'description' => 'Allows users to create and organize related content in an outline.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'book.test',
          ),
          'configure' => 'admin/content/book/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'book.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'forum' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/forum/forum.module',
        'basename' => 'forum.module',
        'name' => 'forum',
        'info' => 
        array (
          'name' => 'Forum',
          'description' => 'Provides discussion forums.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'forum.test',
          ),
          'configure' => 'admin/structure/forum',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'forum.css',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7012',
        'version' => '7.35',
      ),
      'toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/toolbar/toolbar.module',
        'basename' => 'toolbar.module',
        'name' => 'toolbar',
        'info' => 
        array (
          'name' => 'Toolbar',
          'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.35',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'help' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/help/help.module',
        'basename' => 'help.module',
        'name' => 'help',
        'info' => 
        array (
          'name' => 'Help',
          'description' => 'Manages the display of online help.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'help.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/simpletest/simpletest.module',
        'basename' => 'simpletest.module',
        'name' => 'simpletest',
        'info' => 
        array (
          'name' => 'Testing',
          'description' => 'Provides a framework for unit and functional testing.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'simpletest.test',
            1 => 'drupal_web_test_case.php',
            2 => 'tests/actions.test',
            3 => 'tests/ajax.test',
            4 => 'tests/batch.test',
            5 => 'tests/bootstrap.test',
            6 => 'tests/cache.test',
            7 => 'tests/common.test',
            8 => 'tests/database_test.test',
            9 => 'tests/entity_crud.test',
            10 => 'tests/entity_crud_hook_test.test',
            11 => 'tests/entity_query.test',
            12 => 'tests/error.test',
            13 => 'tests/file.test',
            14 => 'tests/filetransfer.test',
            15 => 'tests/form.test',
            16 => 'tests/graph.test',
            17 => 'tests/image.test',
            18 => 'tests/lock.test',
            19 => 'tests/mail.test',
            20 => 'tests/menu.test',
            21 => 'tests/module.test',
            22 => 'tests/pager.test',
            23 => 'tests/password.test',
            24 => 'tests/path.test',
            25 => 'tests/registry.test',
            26 => 'tests/schema.test',
            27 => 'tests/session.test',
            28 => 'tests/tablesort.test',
            29 => 'tests/theme.test',
            30 => 'tests/unicode.test',
            31 => 'tests/update.test',
            32 => 'tests/xmlrpc.test',
            33 => 'tests/upgrade/upgrade.test',
            34 => 'tests/upgrade/upgrade.comment.test',
            35 => 'tests/upgrade/upgrade.filter.test',
            36 => 'tests/upgrade/upgrade.forum.test',
            37 => 'tests/upgrade/upgrade.locale.test',
            38 => 'tests/upgrade/upgrade.menu.test',
            39 => 'tests/upgrade/upgrade.node.test',
            40 => 'tests/upgrade/upgrade.taxonomy.test',
            41 => 'tests/upgrade/upgrade.trigger.test',
            42 => 'tests/upgrade/upgrade.translatable.test',
            43 => 'tests/upgrade/upgrade.upload.test',
            44 => 'tests/upgrade/upgrade.user.test',
            45 => 'tests/upgrade/update.aggregator.test',
            46 => 'tests/upgrade/update.trigger.test',
            47 => 'tests/upgrade/update.field.test',
            48 => 'tests/upgrade/update.user.test',
          ),
          'configure' => 'admin/config/development/testing/settings',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'syslog' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/syslog/syslog.module',
        'basename' => 'syslog.module',
        'name' => 'syslog',
        'info' => 
        array (
          'name' => 'Syslog',
          'description' => 'Logs and records system events to syslog.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'syslog.test',
          ),
          'configure' => 'admin/config/development/logging',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'contact' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/contact/contact.module',
        'basename' => 'contact.module',
        'name' => 'contact',
        'info' => 
        array (
          'name' => 'Contact',
          'description' => 'Enables the use of both personal and site-wide contact forms.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contact.test',
          ),
          'configure' => 'admin/structure/contact',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.35',
      ),
      'dashboard' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/dashboard/dashboard.module',
        'basename' => 'dashboard.module',
        'name' => 'dashboard',
        'info' => 
        array (
          'name' => 'Dashboard',
          'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.35',
          'files' => 
          array (
            0 => 'dashboard.test',
          ),
          'dependencies' => 
          array (
            0 => 'block',
          ),
          'configure' => 'admin/dashboard/customize',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'contextual' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/contextual/contextual.module',
        'basename' => 'contextual.module',
        'name' => 'contextual',
        'info' => 
        array (
          'name' => 'Contextual links',
          'description' => 'Provides contextual links to perform actions related to elements on a page.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contextual.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'node' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/node/node.module',
        'basename' => 'node.module',
        'name' => 'node',
        'info' => 
        array (
          'name' => 'Node',
          'description' => 'Allows content to be submitted to the site and displayed on pages.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'node.module',
            1 => 'node.test',
          ),
          'required' => true,
          'configure' => 'admin/structure/types',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'node.css',
            ),
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7014',
        'version' => '7.35',
      ),
      'system' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/system/system.module',
        'basename' => 'system.module',
        'name' => 'system',
        'info' => 
        array (
          'name' => 'System',
          'description' => 'Handles general site configuration for administrators.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'system.archiver.inc',
            1 => 'system.mail.inc',
            2 => 'system.queue.inc',
            3 => 'system.tar.inc',
            4 => 'system.updater.inc',
            5 => 'system.test',
          ),
          'required' => true,
          'configure' => 'admin/config/system',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7079',
        'version' => '7.35',
      ),
      'openid' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/openid/openid.module',
        'basename' => 'openid.module',
        'name' => 'openid',
        'info' => 
        array (
          'name' => 'OpenID',
          'description' => 'Allows users to log into your site using OpenID.',
          'version' => '7.35',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'openid.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.35',
      ),
      'php' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/php/php.module',
        'basename' => 'php.module',
        'name' => 'php',
        'info' => 
        array (
          'name' => 'PHP filter',
          'description' => 'Allows embedded PHP code/snippets to be evaluated.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'php.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'text' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/modules/text/text.module',
        'basename' => 'text.module',
        'name' => 'text',
        'info' => 
        array (
          'name' => 'Text',
          'description' => 'Defines simple text field types.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'text.test',
          ),
          'required' => true,
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.35',
      ),
      'field_sql_storage' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/modules/field_sql_storage/field_sql_storage.module',
        'basename' => 'field_sql_storage.module',
        'name' => 'field_sql_storage',
        'info' => 
        array (
          'name' => 'Field SQL storage',
          'description' => 'Stores field data in an SQL database.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_sql_storage.test',
          ),
          'required' => true,
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.35',
      ),
      'number' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/modules/number/number.module',
        'basename' => 'number.module',
        'name' => 'number',
        'info' => 
        array (
          'name' => 'Number',
          'description' => 'Defines numeric field types.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'number.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'list' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/modules/list/list.module',
        'basename' => 'list.module',
        'name' => 'list',
        'info' => 
        array (
          'name' => 'List',
          'description' => 'Defines list field types. Use with Options to create selection lists.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'tests/list.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.35',
      ),
      'options' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/modules/options/options.module',
        'basename' => 'options.module',
        'name' => 'options',
        'info' => 
        array (
          'name' => 'Options',
          'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'options.test',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.35',
      ),
      'field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/field/field.module',
        'basename' => 'field.module',
        'name' => 'field',
        'info' => 
        array (
          'name' => 'Field',
          'description' => 'Field API to add fields to entities like nodes and users.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field.module',
            1 => 'field.attach.inc',
            2 => 'field.info.class.inc',
            3 => 'tests/field.test',
          ),
          'dependencies' => 
          array (
            0 => 'field_sql_storage',
          ),
          'required' => true,
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'theme/field.css',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.35',
      ),
      'color' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/modules/color/color.module',
        'basename' => 'color.module',
        'name' => 'color',
        'info' => 
        array (
          'name' => 'Color',
          'description' => 'Allows administrators to change the color scheme of compatible themes.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'color.test',
          ),
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.35',
      ),
      'views_infinite_scroll' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_infinite_scroll/views_infinite_scroll.module',
        'basename' => 'views_infinite_scroll.module',
        'name' => 'views_infinite_scroll',
        'info' => 
        array (
          'name' => 'Views Infinite Scroll',
          'description' => 'Provides an Infinite Scrolling pager for Views',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_infinite_scroll.module',
            1 => 'views_infinite_scroll.views.inc',
            2 => 'views_plugin_pager_infinite_scroll.inc',
            3 => 'drush/views_infinite_scroll.drush.inc',
            4 => 'theme/views_infinite_scroll_theme.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'views_infinite_scroll',
          'datestamp' => '1335182196',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'colorbox' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/colorbox/colorbox.module',
        'basename' => 'colorbox.module',
        'name' => 'colorbox',
        'info' => 
        array (
          'name' => 'Colorbox',
          'description' => 'A light-weight, customizable lightbox plugin for jQuery 1.4.3+.',
          'dependencies' => 
          array (
            0 => 'libraries (>=2.x)',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/media/colorbox',
          'files' => 
          array (
            0 => 'views/colorbox_handler_field_colorbox.inc',
          ),
          'version' => '7.x-2.9',
          'project' => 'colorbox',
          'datestamp' => '1434934994',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.9',
      ),
      'field_default_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_default_token/field_default_token.module',
        'basename' => 'field_default_token.module',
        'name' => 'field_default_token',
        'info' => 
        array (
          'name' => 'Field default token',
          'description' => 'Enables to use tokens as field default values.',
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'field_default_token',
          'datestamp' => '1390208305',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.2',
      ),
      'geocoder' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geocoder/geocoder.module',
        'basename' => 'geocoder.module',
        'name' => 'geocoder',
        'info' => 
        array (
          'name' => 'Geocoder',
          'description' => 'An API and widget to geocode various known data into other GIS data types.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
            1 => 'ctools',
          ),
          'version' => '7.x-1.2',
          'project' => 'geocoder',
          'datestamp' => '1346083034',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'date_facets' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date_facets/date_facets.module',
        'basename' => 'date_facets.module',
        'name' => 'date_facets',
        'info' => 
        array (
          'name' => 'Date Facets',
          'description' => 'Provides date range facets that are similar to implementations in major search engines.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'lib/Drupal/Apachesolr/Facetapi/QueryType/DateRangeQueryType.php',
            1 => 'lib/Drupal/Facetapi/Widget/DateRangeWidget.php',
            2 => 'lib/Drupal/Search/Facetapi/QueryType/DateRangeQueryType.php',
            3 => 'lib/Drupal/SearchApi/Facetapi/QueryType/DateRangeQueryType.php',
          ),
          'version' => '7.x-1.0-beta2+6-dev',
          'project' => 'date_facets',
          'datestamp' => '1382813050',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2+6-dev',
      ),
      'eva' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/eva/eva.module',
        'basename' => 'eva.module',
        'name' => 'eva',
        'info' => 
        array (
          'name' => 'Eva',
          'description' => 'Provides a Views display type that can be attached to entities.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'package' => 'Views',
          'files' => 
          array (
            0 => 'eva_plugin_display_entity.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'eva',
          'datestamp' => '1343701935',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'rules_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules_forms/rules_forms.module',
        'basename' => 'rules_forms.module',
        'name' => 'rules_forms',
        'info' => 
        array (
          'name' => 'Rules Forms Support',
          'description' => 'Provides events, conditions and actions for rule-based form customization.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/rules_forms.ui.inc',
            1 => 'rules_forms.test',
          ),
          'package' => 'Rules',
          'version' => '7.x-1.0-rc2',
          'project' => 'rules_forms',
          'datestamp' => '1329947457',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc2',
      ),
      'calendar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/calendar/calendar.module',
        'basename' => 'calendar.module',
        'name' => 'calendar',
        'info' => 
        array (
          'name' => 'Calendar',
          'description' => 'Views plugin to display views containing dates as Calendars.',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'date_api',
            2 => 'date_views',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/calendar_multiday.css',
            ),
          ),
          'files' => 
          array (
            0 => 'calendar.install',
            1 => 'calendar.module',
            2 => 'includes/calendar.views.inc',
            3 => 'includes/calendar_plugin_style.inc',
            4 => 'includes/calendar_plugin_row.inc',
            5 => 'includes/calendar.views_template.inc',
            6 => 'theme/theme.inc',
            7 => 'theme/calendar-style.tpl.php',
          ),
          'version' => '7.x-3.5',
          'project' => 'calendar',
          'datestamp' => '1413299943',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-3.5',
      ),
      'module_filter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/module_filter/module_filter.module',
        'basename' => 'module_filter.module',
        'name' => 'module_filter',
        'info' => 
        array (
          'name' => 'Module filter',
          'description' => 'Filter the modules list.',
          'core' => '7.x',
          'package' => 'Administration',
          'files' => 
          array (
            0 => 'module_filter.install',
            1 => 'module_filter.js',
            2 => 'module_filter.module',
            3 => 'module_filter.admin.inc',
            4 => 'module_filter.theme.inc',
            5 => 'css/module_filter.css',
            6 => 'css/module_filter_tab.css',
            7 => 'js/module_filter.js',
            8 => 'js/module_filter_tab.js',
          ),
          'configure' => 'admin/config/user-interface/modulefilter',
          'version' => '7.x-2.0+9-dev',
          'project' => 'module_filter',
          'datestamp' => '1426208586',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0+9-dev',
      ),
      'modernizr' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/modernizr/modernizr.module',
        'basename' => 'modernizr.module',
        'name' => 'modernizr',
        'info' => 
        array (
          'name' => 'Modernizr',
          'description' => 'Modernizr integration for Drupal. Extends the library by providing two APIs: (1) for test management (2) for exposing Modernizr.load() to Drupal',
          'core' => '7.x',
          'package' => 'Frontend',
          'configure' => 'admin/config/development/modernizr',
          'files' => 
          array (
            0 => 'modernizr.module',
            1 => 'modernizr.install',
            2 => 'modernizr.admin.inc',
            3 => 'modernizr.args.inc',
            4 => 'tests/modernizr.test',
          ),
          'version' => '7.x-3.4',
          'project' => 'modernizr',
          'datestamp' => '1420636982',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.4',
      ),
      'better_exposed_filters' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/better_exposed_filters/better_exposed_filters.module',
        'basename' => 'better_exposed_filters.module',
        'name' => 'better_exposed_filters',
        'info' => 
        array (
          'name' => 'Better Exposed Filters',
          'description' => 'Allow the use of checkboxes or radio buttons for exposed Views filters',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'better_exposed_filters_exposed_form_plugin.inc',
            1 => 'tests/better_exposed_filters.test',
          ),
          'version' => '7.x-3.0',
          'project' => 'better_exposed_filters',
          'datestamp' => '1418734694',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0',
      ),
      'field_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_permissions/field_permissions.module',
        'basename' => 'field_permissions.module',
        'name' => 'field_permissions',
        'info' => 
        array (
          'name' => 'Field Permissions',
          'description' => 'Set field-level permissions to create, update or view fields.',
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_permissions.module',
            1 => 'field_permissions.admin.inc',
            2 => 'field_permissions.test',
          ),
          'configure' => 'admin/reports/fields/permissions',
          'version' => '7.x-1.0-beta2',
          'project' => 'field_permissions',
          'datestamp' => '1327510549',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-beta2',
      ),
      'livefyre' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/livefyre/livefyre.module',
        'basename' => 'livefyre.module',
        'name' => 'livefyre',
        'info' => 
        array (
          'name' => 'Livefyre Comments',
          'description' => 'Embed Livefyre comments via javascript on your Drupal site.',
          'core' => '7.x',
          'version' => '7.x-1.3',
          'project' => 'livefyre',
          'datestamp' => '1360872158',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'addthis_displays' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addthis/addthis_displays/addthis_displays.module',
        'basename' => 'addthis_displays.module',
        'name' => 'addthis_displays',
        'info' => 
        array (
          'name' => 'AddThis Displays',
          'description' => 'Adds the AddThis display types to render several basic types of AddThis buttons.',
          'core' => '7.x',
          'package' => 'Sharing',
          'dependencies' => 
          array (
            0 => 'addthis',
          ),
          'files' => 
          array (
            0 => 'addthis_displays.field.inc',
          ),
          'version' => '7.x-4.0-alpha4',
          'project' => 'addthis',
          'datestamp' => '1390384409',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-4.0-alpha4',
      ),
      'addthis' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addthis/addthis.module',
        'basename' => 'addthis.module',
        'name' => 'addthis',
        'info' => 
        array (
          'name' => 'AddThis',
          'description' => 'AddThis.com provides an easy way to share your content across the web.',
          'core' => '7.x',
          'package' => 'Sharing',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'addthis.test',
            1 => 'classes/AddThis.php',
            2 => 'classes/AddThisJson.php',
          ),
          'configure' => 'admin/config/user-interface/addthis',
          'version' => '7.x-4.0-alpha4',
          'project' => 'addthis',
          'datestamp' => '1390384409',
          'php' => '5.2.4',
        ),
        'schema_version' => '7401',
        'version' => '7.x-4.0-alpha4',
      ),
      'geofield_map' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geofield/modules/geofield_map/geofield_map.module',
        'basename' => 'geofield_map.module',
        'name' => 'geofield_map',
        'info' => 
        array (
          'name' => 'Geofield Map',
          'description' => 'Provides a basic mapping interface for Geofield.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geofield',
          ),
          'files' => 
          array (
            0 => 'includes/geofield_map.views.inc',
            1 => 'includes/geofield_map_plugin_style_map.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'geofield',
          'datestamp' => '1372735859',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'geofield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geofield/geofield.module',
        'basename' => 'geofield.module',
        'name' => 'geofield',
        'info' => 
        array (
          'name' => 'Geofield',
          'description' => 'Stores geographic and location data (points, lines, and polygons).',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'geofield.module',
            1 => 'geofield.install',
            2 => 'geofield.widgets.inc',
            3 => 'geofield.formatters.inc',
            4 => 'geofield.openlayers.inc',
            5 => 'geofield.feeds.inc',
            6 => 'geofield.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'geofield',
          'datestamp' => '1372735859',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.2',
      ),
      'honeypot' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/honeypot/honeypot.module',
        'basename' => 'honeypot.module',
        'name' => 'honeypot',
        'info' => 
        array (
          'name' => 'Honeypot',
          'description' => 'Mitigates spam form submissions using the honeypot method.',
          'core' => '7.x',
          'configure' => 'admin/config/content/honeypot',
          'package' => 'Spam control',
          'files' => 
          array (
            0 => 'honeypot.test',
          ),
          'version' => '7.x-1.21',
          'project' => 'honeypot',
          'datestamp' => '1441334340',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.x-1.21',
      ),
      'webform' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/webform/webform.module',
        'basename' => 'webform.module',
        'name' => 'webform',
        'info' => 
        array (
          'name' => 'Webform',
          'description' => 'Enables the creation of forms and questionnaires.',
          'core' => '7.x',
          'package' => 'Webform',
          'configure' => 'admin/config/content/webform',
          'php' => '5.3',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'files' => 
          array (
            0 => 'includes/exporters/webform_exporter_delimited.inc',
            1 => 'includes/exporters/webform_exporter_excel_delimited.inc',
            2 => 'includes/exporters/webform_exporter_excel_xlsx.inc',
            3 => 'includes/exporters/webform_exporter.inc',
            4 => 'views/webform_handler_field_form_body.inc',
            5 => 'views/webform_handler_field_is_draft.inc',
            6 => 'views/webform_handler_field_node_link_edit.inc',
            7 => 'views/webform_handler_field_node_link_results.inc',
            8 => 'views/webform_handler_field_submission_count.inc',
            9 => 'views/webform_handler_field_submission_data.inc',
            10 => 'views/webform_handler_field_submission_link.inc',
            11 => 'views/webform_handler_field_webform_status.inc',
            12 => 'views/webform_handler_filter_is_draft.inc',
            13 => 'views/webform_handler_filter_submission_data.inc',
            14 => 'views/webform_handler_filter_webform_status.inc',
            15 => 'views/webform_handler_area_result_pager.inc',
            16 => 'views/webform_plugin_row_submission_view.inc',
            17 => 'views/webform_handler_relationship_submission_data.inc',
            18 => 'views/webform.views.inc',
            19 => 'tests/components.test',
            20 => 'tests/conditionals.test',
            21 => 'tests/permissions.test',
            22 => 'tests/submission.test',
            23 => 'tests/webform.test',
          ),
          'version' => '7.x-4.5',
          'project' => 'webform',
          'datestamp' => '1426611481',
        ),
        'schema_version' => '7422',
        'version' => '7.x-4.5',
      ),
      'search_api_facetapi' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/contrib/search_api_facetapi/search_api_facetapi.module',
        'basename' => 'search_api_facetapi.module',
        'name' => 'search_api_facetapi',
        'info' => 
        array (
          'name' => 'Search facets',
          'description' => 'Integrate the Search API with the Facet API to provide facetted searches.',
          'dependencies' => 
          array (
            0 => 'search_api',
            1 => 'facetapi',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'plugins/facetapi/adapter.inc',
            1 => 'plugins/facetapi/query_type_term.inc',
            2 => 'plugins/facetapi/query_type_date.inc',
          ),
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'search_api_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/contrib/search_api_views/search_api_views.module',
        'basename' => 'search_api_views.module',
        'name' => 'search_api_views',
        'info' => 
        array (
          'name' => 'Search views',
          'description' => 'Integrates the Search API with Views, enabling users to create views with searches as filters or arguments.',
          'dependencies' => 
          array (
            0 => 'search_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/display_facet_block.inc',
            1 => 'includes/handler_argument.inc',
            2 => 'includes/handler_argument_fulltext.inc',
            3 => 'includes/handler_argument_more_like_this.inc',
            4 => 'includes/handler_argument_string.inc',
            5 => 'includes/handler_argument_date.inc',
            6 => 'includes/handler_argument_taxonomy_term.inc',
            7 => 'includes/handler_filter.inc',
            8 => 'includes/handler_filter_boolean.inc',
            9 => 'includes/handler_filter_date.inc',
            10 => 'includes/handler_filter_entity.inc',
            11 => 'includes/handler_filter_fulltext.inc',
            12 => 'includes/handler_filter_language.inc',
            13 => 'includes/handler_filter_options.inc',
            14 => 'includes/handler_filter_taxonomy_term.inc',
            15 => 'includes/handler_filter_text.inc',
            16 => 'includes/handler_filter_user.inc',
            17 => 'includes/handler_sort.inc',
            18 => 'includes/plugin_cache.inc',
            19 => 'includes/query.inc',
          ),
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.14',
      ),
      'search_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/search_api.module',
        'basename' => 'search_api.module',
        'name' => 'search_api',
        'info' => 
        array (
          'name' => 'Search API',
          'description' => 'Provides a generic API for modules offering search capabilites.',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'search_api.test',
            1 => 'includes/callback.inc',
            2 => 'includes/callback_add_aggregation.inc',
            3 => 'includes/callback_add_hierarchy.inc',
            4 => 'includes/callback_add_url.inc',
            5 => 'includes/callback_add_viewed_entity.inc',
            6 => 'includes/callback_bundle_filter.inc',
            7 => 'includes/callback_comment_access.inc',
            8 => 'includes/callback_language_control.inc',
            9 => 'includes/callback_node_access.inc',
            10 => 'includes/callback_node_status.inc',
            11 => 'includes/callback_role_filter.inc',
            12 => 'includes/datasource.inc',
            13 => 'includes/datasource_entity.inc',
            14 => 'includes/datasource_external.inc',
            15 => 'includes/exception.inc',
            16 => 'includes/index_entity.inc',
            17 => 'includes/processor.inc',
            18 => 'includes/processor_highlight.inc',
            19 => 'includes/processor_html_filter.inc',
            20 => 'includes/processor_ignore_case.inc',
            21 => 'includes/processor_stopwords.inc',
            22 => 'includes/processor_tokenizer.inc',
            23 => 'includes/processor_transliteration.inc',
            24 => 'includes/query.inc',
            25 => 'includes/server_entity.inc',
            26 => 'includes/service.inc',
          ),
          'configure' => 'admin/config/search/search_api',
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => '7117',
        'version' => '7.x-1.14',
      ),
      'email_registration' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/email_registration/email_registration.module',
        'basename' => 'email_registration.module',
        'name' => 'email_registration',
        'info' => 
        array (
          'name' => 'Email Registration',
          'description' => 'Allows users to register with an e-mail address as their username.',
          'files' => 
          array (
            0 => 'email_registration.test',
          ),
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'email_registration',
          'datestamp' => '1398265775',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'shs' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/shs/shs.module',
        'basename' => 'shs.module',
        'name' => 'shs',
        'info' => 
        array (
          'name' => 'Simple hierarchical select',
          'core' => '7.x',
          'description' => 'Creates a simple hierarchical select widget for taxonomy fields.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'files' => 
          array (
            0 => 'includes/handlers/shs_handler_filter_term_node_tid.inc',
            1 => 'includes/handlers/shs_handler_filter_term_node_tid_depth.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'shs',
          'datestamp' => '1363529732',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.6',
      ),
      'conditional_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/conditional_fields/conditional_fields.module',
        'basename' => 'conditional_fields.module',
        'name' => 'conditional_fields',
        'info' => 
        array (
          'name' => 'Conditional Fields',
          'description' => 'Define dependencies between fields based on their states and values.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'configure' => 'admin/structure/dependencies',
          'version' => '7.x-3.0-alpha1',
          'project' => 'conditional_fields',
          'datestamp' => '1384798705',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-3.0-alpha1',
      ),
      'borealis_ri' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis_ri/borealis_ri.module',
        'basename' => 'borealis_ri.module',
        'name' => 'borealis_ri',
        'info' => 
        array (
          'name' => 'Borealis Responsive Images',
          'description' => 'Responsive Image Suite to create responsive image sets for any Image preset',
          'core' => '7.x',
          'package' => 'Borealis',
          'dependencies' => 
          array (
            0 => 'borealis',
            1 => 'field_formatter_settings',
            2 => 'image',
          ),
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-2.2',
      ),
      'borealis_sb' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis_sb/borealis_sb.module',
        'basename' => 'borealis_sb.module',
        'name' => 'borealis_sb',
        'info' => 
        array (
          'name' => 'Borealis Semantic Blocks',
          'description' => 'Allows blocks to use semantic templates',
          'core' => '7.x',
          'package' => 'Borealis',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'borealis',
            2 => 'ctools',
          ),
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-2.2',
      ),
      'borealis' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis.module',
        'basename' => 'borealis.module',
        'name' => 'borealis',
        'info' => 
        array (
          'name' => 'Borealis',
          'description' => 'Shared items between the Borealis submodules',
          'core' => '7.x',
          'package' => 'Borealis',
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'filefield_sources' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_sources/filefield_sources.module',
        'basename' => 'filefield_sources.module',
        'name' => 'filefield_sources',
        'info' => 
        array (
          'name' => 'File Field Sources',
          'description' => 'Extends File fields to allow referencing of existing files, remote files, and server files.',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.9',
          'project' => 'filefield_sources',
          'datestamp' => '1383155428',
          'php' => '5.2.4',
        ),
        'schema_version' => '6001',
        'version' => '7.x-1.9',
      ),
      'entity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity/entity.module',
        'basename' => 'entity.module',
        'name' => 'entity',
        'info' => 
        array (
          'name' => 'Entity API',
          'description' => 'Enables modules to work with any entity type and to provide entities.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity.features.inc',
            1 => 'entity.i18n.inc',
            2 => 'entity.info.inc',
            3 => 'entity.rules.inc',
            4 => 'entity.test',
            5 => 'includes/entity.inc',
            6 => 'includes/entity.controller.inc',
            7 => 'includes/entity.ui.inc',
            8 => 'includes/entity.wrapper.inc',
            9 => 'views/entity.views.inc',
            10 => 'views/handlers/entity_views_field_handler_helper.inc',
            11 => 'views/handlers/entity_views_handler_area_entity.inc',
            12 => 'views/handlers/entity_views_handler_field_boolean.inc',
            13 => 'views/handlers/entity_views_handler_field_date.inc',
            14 => 'views/handlers/entity_views_handler_field_duration.inc',
            15 => 'views/handlers/entity_views_handler_field_entity.inc',
            16 => 'views/handlers/entity_views_handler_field_field.inc',
            17 => 'views/handlers/entity_views_handler_field_numeric.inc',
            18 => 'views/handlers/entity_views_handler_field_options.inc',
            19 => 'views/handlers/entity_views_handler_field_text.inc',
            20 => 'views/handlers/entity_views_handler_field_uri.inc',
            21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
            22 => 'views/handlers/entity_views_handler_relationship.inc',
            23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'entity',
          'datestamp' => '1424876582',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.x-1.6',
      ),
      'entity_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity/entity_token.module',
        'basename' => 'entity_token.module',
        'name' => 'entity_token',
        'info' => 
        array (
          'name' => 'Entity tokens',
          'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_token.tokens.inc',
            1 => 'entity_token.module',
          ),
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.6',
          'project' => 'entity',
          'datestamp' => '1424876582',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.6',
      ),
      'session_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/session_api/session_api.module',
        'basename' => 'session_api.module',
        'name' => 'session_api',
        'info' => 
        array (
          'name' => 'Session API',
          'description' => 'Provides an interface for storing session information.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'session_api.module',
            1 => 'session_api.admin.inc',
            2 => 'session_api.install',
            3 => 'session_api.test',
          ),
          'configure' => 'admin/config/development/session-api',
          'version' => '7.x-1.0-rc1',
          'project' => 'session_api',
          'datestamp' => '1354234727',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc1',
      ),
      'maxlength' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/maxlength/maxlength.module',
        'basename' => 'maxlength.module',
        'name' => 'maxlength',
        'info' => 
        array (
          'name' => 'Maxlength',
          'description' => 'Limit the number of characters in textfields and textareas and shows the amount of characters left.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'maxlength.test',
          ),
          'version' => '7.x-3.0',
          'project' => 'maxlength',
          'datestamp' => '1413456249',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7302',
        'version' => '7.x-3.0',
      ),
      'views_bulk_operations' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_bulk_operations/views_bulk_operations.module',
        'basename' => 'views_bulk_operations.module',
        'name' => 'views_bulk_operations',
        'info' => 
        array (
          'name' => 'Views Bulk Operations',
          'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'views',
          ),
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2.9',
          'files' => 
          array (
            0 => 'plugins/operation_types/base.class.php',
            1 => 'views/views_bulk_operations_handler_field_operations.inc',
          ),
          'version' => '7.x-3.2',
          'project' => 'views_bulk_operations',
          'datestamp' => '1387798183',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'actions_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_bulk_operations/actions_permissions.module',
        'basename' => 'actions_permissions.module',
        'name' => 'actions_permissions',
        'info' => 
        array (
          'name' => 'Actions permissions (VBO)',
          'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-3.2',
          'project' => 'views_bulk_operations',
          'datestamp' => '1387798183',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'libraries' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/libraries/libraries.module',
        'basename' => 'libraries.module',
        'name' => 'libraries',
        'info' => 
        array (
          'name' => 'Libraries',
          'description' => 'Allows version-dependent and shared usage of external libraries.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'system (>=7.11)',
          ),
          'files' => 
          array (
            0 => 'tests/libraries.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'libraries',
          'datestamp' => '1391965716',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'views_rss_media' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss_media/views_rss_media.module',
        'basename' => 'views_rss_media.module',
        'name' => 'views_rss_media',
        'info' => 
        array (
          'name' => 'Views RSS: Media (MRSS) Elements',
          'description' => 'Provides Media (MRSS) element set for Views RSS module',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
            1 => 'views_rss_core',
          ),
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'views_rss_media',
          'datestamp' => '1413385143',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'transliteration' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/transliteration/transliteration.module',
        'basename' => 'transliteration.module',
        'name' => 'transliteration',
        'info' => 
        array (
          'name' => 'Transliteration',
          'description' => 'Converts non-latin text to US-ASCII and sanitizes file names.',
          'core' => '7.x',
          'configure' => 'admin/config/media/file-system',
          'version' => '7.x-3.2',
          'project' => 'transliteration',
          'datestamp' => '1395079444',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.2',
      ),
      'menu_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_views/menu_views.module',
        'basename' => 'menu_views.module',
        'name' => 'menu_views',
        'info' => 
        array (
          'name' => 'Menu Views',
          'description' => 'Allows menu items to render views instead of links. This is useful for creating mega menus.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'menu',
            1 => 'views',
          ),
          'version' => '7.x-2.2',
          'project' => 'menu_views',
          'datestamp' => '1371136255',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'magic_dev' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/magic/magic_dev/magic_dev.module',
        'basename' => 'magic_dev.module',
        'name' => 'magic_dev',
        'info' => 
        array (
          'name' => 'Magic Development',
          'description' => 'Development options for Magic',
          'core' => '7.x',
          'package' => 'Frontend',
          'dependencies' => 
          array (
            0 => 'magic',
          ),
          'version' => '7.x-2.2',
          'project' => 'magic',
          'datestamp' => '1423685005',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'magic' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/magic/magic.module',
        'basename' => 'magic.module',
        'name' => 'magic',
        'info' => 
        array (
          'name' => 'Magic',
          'description' => 'Perform many essential frontend tasks with JavaScript, CSS, etc.',
          'core' => '7.x',
          'package' => 'Frontend',
          'testing_api' => '2.x',
          'files' => 
          array (
            0 => 'magic.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'magic',
          'datestamp' => '1423685005',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'jquery_update' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/jquery_update/jquery_update.module',
        'basename' => 'jquery_update.module',
        'name' => 'jquery_update',
        'info' => 
        array (
          'name' => 'jQuery Update',
          'description' => 'Update jQuery and jQuery UI to a more recent version.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'jquery_update.module',
            1 => 'jquery_update.install',
          ),
          'configure' => 'admin/config/development/jquery_update',
          'version' => '7.x-2.5',
          'project' => 'jquery_update',
          'datestamp' => '1422221882',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.5',
      ),
      'seckit' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/seckit/seckit.module',
        'basename' => 'seckit.module',
        'name' => 'seckit',
        'info' => 
        array (
          'name' => 'Security Kit',
          'description' => 'Enhance security of your Drupal website.',
          'package' => 'Security',
          'core' => '7.x',
          'configure' => 'admin/config/system/seckit',
          'files' => 
          array (
            0 => 'seckit.install',
            1 => 'seckit.module',
            2 => 'seckit.test',
            3 => 'includes/seckit.form.inc',
          ),
          'version' => '7.x-1.9',
          'project' => 'seckit',
          'datestamp' => '1399337028',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.9',
      ),
      'search_api_index_status' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_index_status/search_api_index_status.module',
        'basename' => 'search_api_index_status.module',
        'name' => 'search_api_index_status',
        'info' => 
        array (
          'name' => 'Search API index status',
          'description' => 'Adds a page that shows the status of each Search API index.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta1',
          'project' => 'search_api_index_status',
          'datestamp' => '1377861449',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'filefield_paths' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_paths/filefield_paths.module',
        'basename' => 'filefield_paths.module',
        'name' => 'filefield_paths',
        'info' => 
        array (
          'name' => 'File (Field) Paths',
          'description' => 'Adds improved Token based file sorting and renaming functionalities.',
          'dependencies' => 
          array (
            0 => 'token',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filefield_paths.install',
            1 => 'filefield_paths.module',
            2 => 'modules/features.inc',
            3 => 'modules/file.inc',
            4 => 'modules/filefield_paths.drush.inc',
            5 => 'modules/filefield_paths.inc',
            6 => 'modules/image.inc',
            7 => 'modules/token.inc',
            8 => 'modules/video.inc',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'filefield_paths',
          'datestamp' => '1366871711',
          'php' => '5.2.4',
        ),
        'schema_version' => '7107',
        'version' => '7.x-1.0-beta4',
      ),
      'views_load_more' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_load_more/views_load_more.module',
        'basename' => 'views_load_more.module',
        'name' => 'views_load_more',
        'info' => 
        array (
          'name' => 'Views Load More',
          'description' => 'A pager plugin for views. Gives users the option to add a \'more\' button to a view and have the results appended to existing results being displayed.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_load_more.views.inc',
            1 => 'views_plugin_pager_load_more.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'views_load_more',
          'datestamp' => '1412631229',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'better_formats' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/better_formats/better_formats.module',
        'basename' => 'better_formats.module',
        'name' => 'better_formats',
        'info' => 
        array (
          'name' => 'Better Formats',
          'description' => 'Enhances the core input format system by managing input format defaults and settings.',
          'core' => '7.x',
          'configure' => 'admin/config/content/formats',
          'version' => '7.x-1.0-beta1',
          'project' => 'better_formats',
          'datestamp' => '1343262404',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'themekey_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_example/themekey_example.module',
        'basename' => 'themekey_example.module',
        'name' => 'themekey_example',
        'info' => 
        array (
          'name' => 'ThemeKey Example',
          'description' => 'Implements parts of the ThemeKey API as an example for Developers.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'Example modules',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_redirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_redirect/themekey_redirect.module',
        'basename' => 'themekey_redirect.module',
        'name' => 'themekey_redirect',
        'info' => 
        array (
          'name' => 'ThemeKey Redirect',
          'description' => 'Provides an additional rule chain to define rules to redirect the user.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/redirects',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.3',
      ),
      'themekey_css' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_css/themekey_css.module',
        'basename' => 'themekey_css.module',
        'name' => 'themekey_css',
        'info' => 
        array (
          'name' => 'ThemeKey CSS (Experimental)',
          'description' => 'Define rules to dynamically add CSS files to a page. Experimental!',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/css',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/tests/themekey_simpletest.module',
        'basename' => 'themekey_simpletest.module',
        'name' => 'themekey_simpletest',
        'info' => 
        array (
          'name' => 'ThemeKey Testing',
          'description' => 'Just a fake module for testing ThemeKey plugins.',
          'core' => '7.x',
          'package' => 'Development',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_compat' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_compat.module',
        'basename' => 'themekey_compat.module',
        'name' => 'themekey_compat',
        'info' => 
        array (
          'name' => 'ThemeKey Compatibility',
          'description' => 'Optionally disable the theme switching capability of core and additional modules. Their theme switching capabilities will become part of the ThemeKey rule chain instead and therefor configurable.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/compat',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_ui.module',
        'basename' => 'themekey_ui.module',
        'name' => 'themekey_ui',
        'info' => 
        array (
          'name' => 'ThemeKey UI',
          'description' => 'Integrates ThemeKey with Drupal administration forms.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/ui',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.3',
      ),
      'themekey' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey.module',
        'basename' => 'themekey.module',
        'name' => 'themekey',
        'info' => 
        array (
          'name' => 'ThemeKey',
          'description' => 'Map themes to Drupal paths or object properties.',
          'core' => '7.x',
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings',
          'dependencies' => 
          array (
            0 => 'system (>=7.22)',
          ),
          'files' => 
          array (
            0 => 'ThemeKeyEntityFieldQuery.php',
            1 => 'modules/themekey_browser_detection.php',
            2 => 'tests/themekey.test',
            3 => 'tests/ThemekeyDrupalPropertiesTestCase.test',
            4 => 'tests/ThemekeyModulePluginsTestCase.test',
            5 => 'tests/ThemekeyNodePropertiesTestCase.test',
            6 => 'tests/ThemekeyRuleChainTestCase.test',
            7 => 'tests/ThemekeySystemPropertiesTestCase.test',
            8 => 'tests/ThemekeyUITestCase.test',
          ),
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.3',
      ),
      'themekey_user_profile' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_user_profile.module',
        'basename' => 'themekey_user_profile.module',
        'name' => 'themekey_user_profile',
        'info' => 
        array (
          'name' => 'ThemeKey User Profile',
          'description' => 'Allows users to select their own theme for this site. Replaces the corresponding feature that existed in Drupal 6 Core.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
            1 => 'themekey_ui',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/ui',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_features.module',
        'basename' => 'themekey_features.module',
        'name' => 'themekey_features',
        'info' => 
        array (
          'name' => 'ThemeKey Features (Beta)',
          'description' => 'Export single rules with Features.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
            1 => 'features',
          ),
          'package' => 'ThemeKey',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_debug' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_debug.module',
        'basename' => 'themekey_debug.module',
        'name' => 'themekey_debug',
        'info' => 
        array (
          'name' => 'ThemeKey Debug',
          'description' => 'Debug ThemeKey, see which rules are applied and the current values of all properties.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/debug',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'elements' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/elements/elements.module',
        'basename' => 'elements.module',
        'name' => 'elements',
        'info' => 
        array (
          'name' => 'Elements',
          'description' => 'Provides a library of Form API elements.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'elements.module',
            1 => 'elements.theme.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'elements',
          'datestamp' => '1370667652',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'node_convert' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/node_convert/node_convert.module',
        'basename' => 'node_convert.module',
        'name' => 'node_convert',
        'info' => 
        array (
          'name' => 'Node Convert',
          'description' => 'Converts one or more nodes between different node types.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'node_convert.install',
            1 => 'node_convert.module',
            2 => 'node_convert.rules.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'node_convert',
          'datestamp' => '1402559034',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.2',
      ),
      'token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/token/token.module',
        'basename' => 'token.module',
        'name' => 'token',
        'info' => 
        array (
          'name' => 'Token',
          'description' => 'Provides a user interface for the Token API and some missing core tokens.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'token.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'token',
          'datestamp' => '1361665026',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.5',
      ),
      'auto_entitylabel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/auto_entitylabel/auto_entitylabel.module',
        'basename' => 'auto_entitylabel.module',
        'name' => 'auto_entitylabel',
        'info' => 
        array (
          'name' => 'Automatic Entity Labels',
          'description' => 'Allows hiding of entity label fields and automatic label creation.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.3',
          'project' => 'auto_entitylabel',
          'datestamp' => '1419756181',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'workbench' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workbench/workbench.module',
        'basename' => 'workbench.module',
        'name' => 'workbench',
        'info' => 
        array (
          'name' => 'Workbench',
          'description' => 'Workbench editorial suite.',
          'package' => 'Workbench',
          'core' => '7.x',
          'configure' => 'admin/config/workbench/settings',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => '7.x-1.2',
          'project' => 'workbench',
          'datestamp' => '1358534592',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'search_api_solr' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_solr/search_api_solr.module',
        'basename' => 'search_api_solr.module',
        'name' => 'search_api_solr',
        'info' => 
        array (
          'name' => 'Solr search',
          'description' => 'Offers an implementation of the Search API that uses an Apache Solr server for indexing content.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/document.inc',
            1 => 'includes/service.inc',
            2 => 'includes/solr_connection.inc',
            3 => 'includes/solr_connection.interface.inc',
            4 => 'includes/solr_field.inc',
            5 => 'includes/spellcheck.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'search_api_solr',
          'datestamp' => '1410186051',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.6',
      ),
      'name' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/name/name.module',
        'basename' => 'name.module',
        'name' => 'name',
        'info' => 
        array (
          'name' => 'Name Field',
          'description' => 'Defines a persons name field type.',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'package' => 'Fields',
          'version' => '7.x-1.9',
          'core' => '7.x',
          'recommends' => 
          array (
            0 => 'namedb',
          ),
          'configure' => 'admin/config/regional/name',
          'files' => 
          array (
            0 => 'tests/name.test',
            1 => 'includes/name_handler_filter_name_fulltext.inc',
            2 => 'name.migrate.inc',
          ),
          'project' => 'name',
          'datestamp' => '1368191420',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.9',
      ),
      'fences' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/fences/fences.module',
        'basename' => 'fences.module',
        'name' => 'fences',
        'info' => 
        array (
          'name' => 'Fences',
          'description' => 'Configurable field wrappers',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'configure' => 'admin/config/content/fences',
          'version' => '7.x-1.0',
          'project' => 'fences',
          'datestamp' => '1335373578',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.0',
      ),
      'file_entity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/file_entity/file_entity.module',
        'basename' => 'file_entity.module',
        'name' => 'file_entity',
        'info' => 
        array (
          'name' => 'File Entity',
          'description' => 'Extends Drupal file entities to be fieldable and viewable.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'file',
            2 => 'ctools',
            3 => 'system (>=7.9)',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'files' => 
          array (
            0 => 'views/views_handler_argument_file_type.inc',
            1 => 'views/views_handler_field_file_rendered.inc',
            2 => 'views/views_handler_field_file_type.inc',
            3 => 'views/views_handler_filter_file_type.inc',
            4 => 'views/views_handler_filter_schema_type.inc',
            5 => 'views/views_handler_field_file_filename.inc',
            6 => 'views/views_handler_field_file_link.inc',
            7 => 'views/views_handler_field_file_link_edit.inc',
            8 => 'views/views_handler_field_file_link_delete.inc',
            9 => 'views/views_handler_field_file_link_download.inc',
            10 => 'views/views_handler_field_file_link_usage.inc',
            11 => 'views/views_plugin_row_file_rss.inc',
            12 => 'views/views_plugin_row_file_view.inc',
            13 => 'file_entity.test',
          ),
          'configure' => 'admin/config/media/file-settings',
          'version' => '7.x-2.0-beta2',
          'project' => 'file_entity',
          'datestamp' => '1436896443',
          'php' => '5.2.4',
        ),
        'schema_version' => '7216',
        'version' => '7.x-2.0-beta2',
      ),
      'features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/features/features.module',
        'basename' => 'features.module',
        'name' => 'features',
        'info' => 
        array (
          'name' => 'Features',
          'description' => 'Provides feature management for Drupal.',
          'core' => '7.x',
          'package' => 'Features',
          'files' => 
          array (
            0 => 'tests/features.test',
          ),
          'configure' => 'admin/structure/features/settings',
          'version' => '7.x-2.4',
          'project' => 'features',
          'datestamp' => '1425501344',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'version' => '7.x-2.4',
      ),
      'pushbullet' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/pushbullet/pushbullet.module',
        'basename' => 'pushbullet.module',
        'name' => 'pushbullet',
        'info' => 
        array (
          'name' => 'Pushbullet API',
          'description' => 'Provide Pushbullet integration.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/Pushbullet.class.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'pushbullet',
          'datestamp' => '1428403438',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'chosen' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/chosen/chosen.module',
        'basename' => 'chosen.module',
        'name' => 'chosen',
        'info' => 
        array (
          'name' => 'Chosen',
          'description' => 'Makes select elements more user-friendly using <a href="http://harvesthq.github.com/chosen/">Chosen</a>.',
          'package' => 'User interface',
          'configure' => 'admin/config/user-interface/chosen',
          'core' => '7.x',
          'version' => '7.x-2.0-beta4',
          'project' => 'chosen',
          'datestamp' => '1394256505',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => '7.x-2.0-beta4',
      ),
      'elysia_cron' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/elysia_cron/elysia_cron.module',
        'basename' => 'elysia_cron.module',
        'name' => 'elysia_cron',
        'info' => 
        array (
          'name' => 'Elysia Cron',
          'description' => 'Extended cron support with crontab-like scheduling and other features.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'elysia_cron_update.php',
            1 => 'elysia_drupalconv.php',
          ),
          'configure' => 'admin/config/system/cron',
          'version' => '7.x-2.1',
          'project' => 'elysia_cron',
          'datestamp' => '1331658045',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '1',
        'version' => '7.x-2.1',
      ),
      'views_wookmark' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_wookmark/views_wookmark.module',
        'basename' => 'views_wookmark.module',
        'name' => 'views_wookmark',
        'info' => 
        array (
          'name' => 'Views Wookmark',
          'description' => 'Exposes a views style plugin to render items on a fluid grid.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'libraries',
          ),
          'files' => 
          array (
            0 => 'views_wookmark.module',
            1 => 'theme/theme.inc',
            2 => 'views/views_wookmark.views.inc',
            3 => 'views/views_wookmark_plugin_style.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'views_wookmark',
          'datestamp' => '1380681754',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'shield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/shield/shield.module',
        'basename' => 'shield.module',
        'name' => 'shield',
        'info' => 
        array (
          'name' => 'PHP Authentication shield',
          'description' => 'Creates a general shield for the site',
          'core' => '7.x',
          'configure' => 'admin/config/system/shield',
          'version' => '7.x-1.2',
          'project' => 'shield',
          'datestamp' => '1335436884',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'addressfield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addressfield/addressfield.module',
        'basename' => 'addressfield.module',
        'name' => 'addressfield',
        'info' => 
        array (
          'name' => 'Address Field',
          'description' => 'Manage a flexible address field, implementing the xNAL standard.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'addressfield.migrate.inc',
            1 => 'views/addressfield_views_handler_field_country.inc',
            2 => 'views/addressfield_views_handler_filter_country.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'addressfield',
          'datestamp' => '1421426885',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0',
      ),
      'views_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views_export/views_export.module',
        'basename' => 'views_export.module',
        'name' => 'views_export',
        'info' => 
        array (
          'dependencies' => 
          array (
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'views_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views_ui.module',
        'basename' => 'views_ui.module',
        'name' => 'views_ui',
        'info' => 
        array (
          'name' => 'Views UI',
          'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
          'package' => 'Views',
          'core' => '7.x',
          'configure' => 'admin/structure/views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_ui.module',
            1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
          ),
          'version' => '7.x-3.10',
          'project' => 'views',
          'datestamp' => '1423648085',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.10',
      ),
      'views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views.module',
        'basename' => 'views.module',
        'name' => 'views',
        'info' => 
        array (
          'name' => 'Views',
          'description' => 'Create customized lists and queries from your database.',
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/views.css',
            ),
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'handlers/views_handler_area.inc',
            1 => 'handlers/views_handler_area_messages.inc',
            2 => 'handlers/views_handler_area_result.inc',
            3 => 'handlers/views_handler_area_text.inc',
            4 => 'handlers/views_handler_area_text_custom.inc',
            5 => 'handlers/views_handler_area_view.inc',
            6 => 'handlers/views_handler_argument.inc',
            7 => 'handlers/views_handler_argument_date.inc',
            8 => 'handlers/views_handler_argument_formula.inc',
            9 => 'handlers/views_handler_argument_many_to_one.inc',
            10 => 'handlers/views_handler_argument_null.inc',
            11 => 'handlers/views_handler_argument_numeric.inc',
            12 => 'handlers/views_handler_argument_string.inc',
            13 => 'handlers/views_handler_argument_group_by_numeric.inc',
            14 => 'handlers/views_handler_field.inc',
            15 => 'handlers/views_handler_field_counter.inc',
            16 => 'handlers/views_handler_field_boolean.inc',
            17 => 'handlers/views_handler_field_contextual_links.inc',
            18 => 'handlers/views_handler_field_custom.inc',
            19 => 'handlers/views_handler_field_date.inc',
            20 => 'handlers/views_handler_field_entity.inc',
            21 => 'handlers/views_handler_field_markup.inc',
            22 => 'handlers/views_handler_field_math.inc',
            23 => 'handlers/views_handler_field_numeric.inc',
            24 => 'handlers/views_handler_field_prerender_list.inc',
            25 => 'handlers/views_handler_field_time_interval.inc',
            26 => 'handlers/views_handler_field_serialized.inc',
            27 => 'handlers/views_handler_field_machine_name.inc',
            28 => 'handlers/views_handler_field_url.inc',
            29 => 'handlers/views_handler_filter.inc',
            30 => 'handlers/views_handler_filter_boolean_operator.inc',
            31 => 'handlers/views_handler_filter_boolean_operator_string.inc',
            32 => 'handlers/views_handler_filter_combine.inc',
            33 => 'handlers/views_handler_filter_date.inc',
            34 => 'handlers/views_handler_filter_equality.inc',
            35 => 'handlers/views_handler_filter_entity_bundle.inc',
            36 => 'handlers/views_handler_filter_group_by_numeric.inc',
            37 => 'handlers/views_handler_filter_in_operator.inc',
            38 => 'handlers/views_handler_filter_many_to_one.inc',
            39 => 'handlers/views_handler_filter_numeric.inc',
            40 => 'handlers/views_handler_filter_string.inc',
            41 => 'handlers/views_handler_filter_fields_compare.inc',
            42 => 'handlers/views_handler_relationship.inc',
            43 => 'handlers/views_handler_relationship_groupwise_max.inc',
            44 => 'handlers/views_handler_sort.inc',
            45 => 'handlers/views_handler_sort_date.inc',
            46 => 'handlers/views_handler_sort_formula.inc',
            47 => 'handlers/views_handler_sort_group_by_numeric.inc',
            48 => 'handlers/views_handler_sort_menu_hierarchy.inc',
            49 => 'handlers/views_handler_sort_random.inc',
            50 => 'includes/base.inc',
            51 => 'includes/handlers.inc',
            52 => 'includes/plugins.inc',
            53 => 'includes/view.inc',
            54 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
            55 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
            56 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
            57 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
            58 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
            59 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
            60 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
            61 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
            62 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
            63 => 'modules/book/views_plugin_argument_default_book_root.inc',
            64 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
            65 => 'modules/comment/views_handler_field_comment.inc',
            66 => 'modules/comment/views_handler_field_comment_depth.inc',
            67 => 'modules/comment/views_handler_field_comment_link.inc',
            68 => 'modules/comment/views_handler_field_comment_link_approve.inc',
            69 => 'modules/comment/views_handler_field_comment_link_delete.inc',
            70 => 'modules/comment/views_handler_field_comment_link_edit.inc',
            71 => 'modules/comment/views_handler_field_comment_link_reply.inc',
            72 => 'modules/comment/views_handler_field_comment_node_link.inc',
            73 => 'modules/comment/views_handler_field_comment_username.inc',
            74 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
            75 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
            76 => 'modules/comment/views_handler_field_node_comment.inc',
            77 => 'modules/comment/views_handler_field_node_new_comments.inc',
            78 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
            79 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
            80 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
            81 => 'modules/comment/views_handler_filter_node_comment.inc',
            82 => 'modules/comment/views_handler_sort_comment_thread.inc',
            83 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
            84 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
            85 => 'modules/comment/views_plugin_row_comment_rss.inc',
            86 => 'modules/comment/views_plugin_row_comment_view.inc',
            87 => 'modules/contact/views_handler_field_contact_link.inc',
            88 => 'modules/field/views_handler_field_field.inc',
            89 => 'modules/field/views_handler_relationship_entity_reverse.inc',
            90 => 'modules/field/views_handler_argument_field_list.inc',
            91 => 'modules/field/views_handler_filter_field_list_boolean.inc',
            92 => 'modules/field/views_handler_argument_field_list_string.inc',
            93 => 'modules/field/views_handler_filter_field_list.inc',
            94 => 'modules/filter/views_handler_field_filter_format_name.inc',
            95 => 'modules/locale/views_handler_field_node_language.inc',
            96 => 'modules/locale/views_handler_filter_node_language.inc',
            97 => 'modules/locale/views_handler_argument_locale_group.inc',
            98 => 'modules/locale/views_handler_argument_locale_language.inc',
            99 => 'modules/locale/views_handler_field_locale_group.inc',
            100 => 'modules/locale/views_handler_field_locale_language.inc',
            101 => 'modules/locale/views_handler_field_locale_link_edit.inc',
            102 => 'modules/locale/views_handler_filter_locale_group.inc',
            103 => 'modules/locale/views_handler_filter_locale_language.inc',
            104 => 'modules/locale/views_handler_filter_locale_version.inc',
            105 => 'modules/node/views_handler_argument_dates_various.inc',
            106 => 'modules/node/views_handler_argument_node_language.inc',
            107 => 'modules/node/views_handler_argument_node_nid.inc',
            108 => 'modules/node/views_handler_argument_node_type.inc',
            109 => 'modules/node/views_handler_argument_node_vid.inc',
            110 => 'modules/node/views_handler_argument_node_uid_revision.inc',
            111 => 'modules/node/views_handler_field_history_user_timestamp.inc',
            112 => 'modules/node/views_handler_field_node.inc',
            113 => 'modules/node/views_handler_field_node_link.inc',
            114 => 'modules/node/views_handler_field_node_link_delete.inc',
            115 => 'modules/node/views_handler_field_node_link_edit.inc',
            116 => 'modules/node/views_handler_field_node_revision.inc',
            117 => 'modules/node/views_handler_field_node_revision_link.inc',
            118 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
            119 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
            120 => 'modules/node/views_handler_field_node_path.inc',
            121 => 'modules/node/views_handler_field_node_type.inc',
            122 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
            123 => 'modules/node/views_handler_filter_node_access.inc',
            124 => 'modules/node/views_handler_filter_node_status.inc',
            125 => 'modules/node/views_handler_filter_node_type.inc',
            126 => 'modules/node/views_handler_filter_node_uid_revision.inc',
            127 => 'modules/node/views_plugin_argument_default_node.inc',
            128 => 'modules/node/views_plugin_argument_validate_node.inc',
            129 => 'modules/node/views_plugin_row_node_rss.inc',
            130 => 'modules/node/views_plugin_row_node_view.inc',
            131 => 'modules/profile/views_handler_field_profile_date.inc',
            132 => 'modules/profile/views_handler_field_profile_list.inc',
            133 => 'modules/profile/views_handler_filter_profile_selection.inc',
            134 => 'modules/search/views_handler_argument_search.inc',
            135 => 'modules/search/views_handler_field_search_score.inc',
            136 => 'modules/search/views_handler_filter_search.inc',
            137 => 'modules/search/views_handler_sort_search_score.inc',
            138 => 'modules/search/views_plugin_row_search_view.inc',
            139 => 'modules/statistics/views_handler_field_accesslog_path.inc',
            140 => 'modules/system/views_handler_argument_file_fid.inc',
            141 => 'modules/system/views_handler_field_file.inc',
            142 => 'modules/system/views_handler_field_file_extension.inc',
            143 => 'modules/system/views_handler_field_file_filemime.inc',
            144 => 'modules/system/views_handler_field_file_uri.inc',
            145 => 'modules/system/views_handler_field_file_status.inc',
            146 => 'modules/system/views_handler_filter_file_status.inc',
            147 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
            148 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
            149 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
            150 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
            151 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
            152 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
            153 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
            154 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
            155 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
            156 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
            157 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
            158 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
            159 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
            160 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
            161 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
            162 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
            163 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
            164 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
            165 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
            166 => 'modules/system/views_handler_filter_system_type.inc',
            167 => 'modules/translation/views_handler_argument_node_tnid.inc',
            168 => 'modules/translation/views_handler_field_node_link_translate.inc',
            169 => 'modules/translation/views_handler_field_node_translation_link.inc',
            170 => 'modules/translation/views_handler_filter_node_tnid.inc',
            171 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
            172 => 'modules/translation/views_handler_relationship_translation.inc',
            173 => 'modules/user/views_handler_argument_user_uid.inc',
            174 => 'modules/user/views_handler_argument_users_roles_rid.inc',
            175 => 'modules/user/views_handler_field_user.inc',
            176 => 'modules/user/views_handler_field_user_language.inc',
            177 => 'modules/user/views_handler_field_user_link.inc',
            178 => 'modules/user/views_handler_field_user_link_cancel.inc',
            179 => 'modules/user/views_handler_field_user_link_edit.inc',
            180 => 'modules/user/views_handler_field_user_mail.inc',
            181 => 'modules/user/views_handler_field_user_name.inc',
            182 => 'modules/user/views_handler_field_user_permissions.inc',
            183 => 'modules/user/views_handler_field_user_picture.inc',
            184 => 'modules/user/views_handler_field_user_roles.inc',
            185 => 'modules/user/views_handler_filter_user_current.inc',
            186 => 'modules/user/views_handler_filter_user_name.inc',
            187 => 'modules/user/views_handler_filter_user_permissions.inc',
            188 => 'modules/user/views_handler_filter_user_roles.inc',
            189 => 'modules/user/views_plugin_argument_default_current_user.inc',
            190 => 'modules/user/views_plugin_argument_default_user.inc',
            191 => 'modules/user/views_plugin_argument_validate_user.inc',
            192 => 'modules/user/views_plugin_row_user_view.inc',
            193 => 'plugins/views_plugin_access.inc',
            194 => 'plugins/views_plugin_access_none.inc',
            195 => 'plugins/views_plugin_access_perm.inc',
            196 => 'plugins/views_plugin_access_role.inc',
            197 => 'plugins/views_plugin_argument_default.inc',
            198 => 'plugins/views_plugin_argument_default_php.inc',
            199 => 'plugins/views_plugin_argument_default_fixed.inc',
            200 => 'plugins/views_plugin_argument_default_raw.inc',
            201 => 'plugins/views_plugin_argument_validate.inc',
            202 => 'plugins/views_plugin_argument_validate_numeric.inc',
            203 => 'plugins/views_plugin_argument_validate_php.inc',
            204 => 'plugins/views_plugin_cache.inc',
            205 => 'plugins/views_plugin_cache_none.inc',
            206 => 'plugins/views_plugin_cache_time.inc',
            207 => 'plugins/views_plugin_display.inc',
            208 => 'plugins/views_plugin_display_attachment.inc',
            209 => 'plugins/views_plugin_display_block.inc',
            210 => 'plugins/views_plugin_display_default.inc',
            211 => 'plugins/views_plugin_display_embed.inc',
            212 => 'plugins/views_plugin_display_extender.inc',
            213 => 'plugins/views_plugin_display_feed.inc',
            214 => 'plugins/views_plugin_display_page.inc',
            215 => 'plugins/views_plugin_exposed_form_basic.inc',
            216 => 'plugins/views_plugin_exposed_form.inc',
            217 => 'plugins/views_plugin_exposed_form_input_required.inc',
            218 => 'plugins/views_plugin_localization_core.inc',
            219 => 'plugins/views_plugin_localization.inc',
            220 => 'plugins/views_plugin_localization_none.inc',
            221 => 'plugins/views_plugin_pager.inc',
            222 => 'plugins/views_plugin_pager_full.inc',
            223 => 'plugins/views_plugin_pager_mini.inc',
            224 => 'plugins/views_plugin_pager_none.inc',
            225 => 'plugins/views_plugin_pager_some.inc',
            226 => 'plugins/views_plugin_query.inc',
            227 => 'plugins/views_plugin_query_default.inc',
            228 => 'plugins/views_plugin_row.inc',
            229 => 'plugins/views_plugin_row_fields.inc',
            230 => 'plugins/views_plugin_row_rss_fields.inc',
            231 => 'plugins/views_plugin_style.inc',
            232 => 'plugins/views_plugin_style_default.inc',
            233 => 'plugins/views_plugin_style_grid.inc',
            234 => 'plugins/views_plugin_style_list.inc',
            235 => 'plugins/views_plugin_style_jump_menu.inc',
            236 => 'plugins/views_plugin_style_mapping.inc',
            237 => 'plugins/views_plugin_style_rss.inc',
            238 => 'plugins/views_plugin_style_summary.inc',
            239 => 'plugins/views_plugin_style_summary_jump_menu.inc',
            240 => 'plugins/views_plugin_style_summary_unformatted.inc',
            241 => 'plugins/views_plugin_style_table.inc',
            242 => 'tests/handlers/views_handlers.test',
            243 => 'tests/handlers/views_handler_area_text.test',
            244 => 'tests/handlers/views_handler_argument_null.test',
            245 => 'tests/handlers/views_handler_argument_string.test',
            246 => 'tests/handlers/views_handler_field.test',
            247 => 'tests/handlers/views_handler_field_boolean.test',
            248 => 'tests/handlers/views_handler_field_custom.test',
            249 => 'tests/handlers/views_handler_field_counter.test',
            250 => 'tests/handlers/views_handler_field_date.test',
            251 => 'tests/handlers/views_handler_field_file_extension.test',
            252 => 'tests/handlers/views_handler_field_file_size.test',
            253 => 'tests/handlers/views_handler_field_math.test',
            254 => 'tests/handlers/views_handler_field_url.test',
            255 => 'tests/handlers/views_handler_field_xss.test',
            256 => 'tests/handlers/views_handler_filter_combine.test',
            257 => 'tests/handlers/views_handler_filter_date.test',
            258 => 'tests/handlers/views_handler_filter_equality.test',
            259 => 'tests/handlers/views_handler_filter_in_operator.test',
            260 => 'tests/handlers/views_handler_filter_numeric.test',
            261 => 'tests/handlers/views_handler_filter_string.test',
            262 => 'tests/handlers/views_handler_sort_random.test',
            263 => 'tests/handlers/views_handler_sort_date.test',
            264 => 'tests/handlers/views_handler_sort.test',
            265 => 'tests/test_handlers/views_test_area_access.inc',
            266 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
            267 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
            268 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
            269 => 'tests/plugins/views_plugin_display.test',
            270 => 'tests/styles/views_plugin_style_jump_menu.test',
            271 => 'tests/styles/views_plugin_style.test',
            272 => 'tests/styles/views_plugin_style_base.test',
            273 => 'tests/styles/views_plugin_style_mapping.test',
            274 => 'tests/styles/views_plugin_style_unformatted.test',
            275 => 'tests/views_access.test',
            276 => 'tests/views_analyze.test',
            277 => 'tests/views_basic.test',
            278 => 'tests/views_argument_default.test',
            279 => 'tests/views_argument_validator.test',
            280 => 'tests/views_exposed_form.test',
            281 => 'tests/field/views_fieldapi.test',
            282 => 'tests/views_glossary.test',
            283 => 'tests/views_groupby.test',
            284 => 'tests/views_handlers.test',
            285 => 'tests/views_module.test',
            286 => 'tests/views_pager.test',
            287 => 'tests/views_plugin_localization_test.inc',
            288 => 'tests/views_translatable.test',
            289 => 'tests/views_query.test',
            290 => 'tests/views_upgrade.test',
            291 => 'tests/views_test.views_default.inc',
            292 => 'tests/comment/views_handler_argument_comment_user_uid.test',
            293 => 'tests/comment/views_handler_filter_comment_user_uid.test',
            294 => 'tests/node/views_node_revision_relations.test',
            295 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
            296 => 'tests/user/views_handler_field_user_name.test',
            297 => 'tests/user/views_user_argument_default.test',
            298 => 'tests/user/views_user_argument_validate.test',
            299 => 'tests/user/views_user.test',
            300 => 'tests/views_cache.test',
            301 => 'tests/views_view.test',
            302 => 'tests/views_ui.test',
          ),
          'version' => '7.x-3.10',
          'project' => 'views',
          'datestamp' => '1423648085',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.10',
      ),
      'node_view_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/node_view_permissions/node_view_permissions.module',
        'basename' => 'node_view_permissions.module',
        'name' => 'node_view_permissions',
        'info' => 
        array (
          'name' => 'Node view permissions',
          'description' => 'Enables permissions "View own content" and "View any content" for each content type.',
          'package' => 'Access control',
          'configure' => 'admin/config/content/node-view-permissions',
          'core' => '7.x',
          'version' => '7.x-1.5',
          'project' => 'node_view_permissions',
          'datestamp' => '1404800028',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'password_policy_password_tab' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/password_policy/contrib/password_tab/password_policy_password_tab.module',
        'basename' => 'password_policy_password_tab.module',
        'name' => 'password_policy_password_tab',
        'info' => 
        array (
          'name' => 'Password change tab',
          'description' => 'Implements a separate password change tab.',
          'package' => 'Other',
          'dependencies' => 
          array (
            0 => 'password_policy',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'password_policy_password_tab.module',
            1 => 'password_policy_password_tab.install',
            2 => 'password_policy_password_tab.admin.inc',
            3 => 'password_policy_password_tab.pages.inc',
            4 => 'password_policy_password_tab.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'password_policy',
          'datestamp' => '1376497420',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'password_policy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/password_policy/password_policy.module',
        'basename' => 'password_policy.module',
        'name' => 'password_policy',
        'info' => 
        array (
          'name' => 'Password policy',
          'description' => 'The password policy module allows you to enforce a specific level of password complexity for the user passwords on the system.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'password_policy.module',
            1 => 'password_policy.install',
            2 => 'password_policy.admin.inc',
            3 => 'password_policy.theme.inc',
            4 => 'tests/password_policy.test',
            5 => 'tests/password_policy_expiration.test',
          ),
          'configure' => 'admin/config/people/password_policy',
          'version' => '7.x-1.5',
          'project' => 'password_policy',
          'datestamp' => '1376497420',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.5',
      ),
      'paymentform' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentform/paymentform.module',
        'basename' => 'paymentform.module',
        'name' => 'paymentform',
        'info' => 
        array (
          'name' => 'Payment Form Field',
          'description' => 'Provides a field to add payment forms to entities.',
          'dependencies' => 
          array (
            0 => 'currency_api',
            1 => 'payment',
          ),
          'package' => 'Payment',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/paymentform.test',
            1 => 'tests/PaymentformWebTestCase.test',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'paymentmethodbasic' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentmethodbasic/paymentmethodbasic.module',
        'basename' => 'paymentmethodbasic.module',
        'name' => 'paymentmethodbasic',
        'info' => 
        array (
          'name' => 'Basic Payment Method',
          'description' => 'A \'dumb\' payment method type that always successfully executes payments, but never actually transfers money. It can be useful for <em>collect on delivery</em>, for instance.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'payment',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'paymentmethodbasic.module',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'paymentreference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentreference/paymentreference.module',
        'basename' => 'paymentreference.module',
        'name' => 'paymentreference',
        'info' => 
        array (
          'name' => 'Payment Reference Field',
          'description' => 'Provides a field to add payments to entities.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'currency_api',
            1 => 'field',
            2 => 'payment',
          ),
          'package' => 'Payment',
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.14',
      ),
      'payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/payment.module',
        'basename' => 'payment.module',
        'name' => 'payment',
        'info' => 
        array (
          'name' => 'Payment',
          'description' => 'Allows payments to be made using any of the available payment methods.',
          'core' => '7.x',
          'configure' => 'admin/config/services/payment',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'payment.classes.inc',
            1 => 'payment.generate.inc',
            2 => 'views/PaymentViewsHandlerAccessOwnPaymentOverview.inc',
            3 => 'views/PaymentViewsHandlerFieldPaymentMethodControllerTitle.inc',
            4 => 'views/PaymentViewsHandlerFieldPaymentMethodControllerDescription.inc',
            5 => 'views/PaymentViewsHandlerFieldPaymentMethodEnabledMachineName.inc',
            6 => 'views/PaymentViewsHandlerFieldPaymentMethodOperations.inc',
            7 => 'views/PaymentViewsHandlerFieldPaymentStatusTitle.inc',
            8 => 'views/PaymentViewsHandlerFieldPaymentOperations.inc',
            9 => 'views/PaymentViewsHandlerFieldTranslatableString.inc',
            10 => 'views/PaymentViewsHandlerFilterPaymentMethodControllerTitle.inc',
            11 => 'views/PaymentViewsHandlerFilterPaymentMethodTitle.inc',
            12 => 'views/PaymentViewsHandlerFilterPaymentStatusItemStatus.inc',
            13 => 'tests/PaymentWebTestCase.test',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => '7105',
        'version' => '7.x-1.14',
      ),
      'strongarm' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/strongarm/strongarm.module',
        'basename' => 'strongarm.module',
        'name' => 'strongarm',
        'info' => 
        array (
          'name' => 'Strongarm',
          'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'strongarm.admin.inc',
            1 => 'strongarm.install',
            2 => 'strongarm.module',
          ),
          'version' => '7.x-2.0',
          'project' => 'strongarm',
          'datestamp' => '1339604214',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0',
      ),
      'commerce_discount_date' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/modules/commerce_discount_date/commerce_discount_date.module',
        'basename' => 'commerce_discount_date.module',
        'name' => 'commerce_discount_date',
        'info' => 
        array (
          'name' => 'Commerce Discount Date',
          'description' => 'Provides date fields for the Commerce discount entity.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_discount',
            1 => 'date',
            2 => 'date_popup',
          ),
          'files' => 
          array (
            0 => 'commerce_discount_date.test',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_discount_usage' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/modules/commerce_discount_usage/commerce_discount_usage.module',
        'basename' => 'commerce_discount_usage.module',
        'name' => 'commerce_discount_usage',
        'info' => 
        array (
          'name' => 'Commerce Discount Usage',
          'description' => 'Provide usage tracking and limits for Commerce Discounts',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_discount',
          ),
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_discount_usage_handler_field_commerce_discount_analytics.inc',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_discount' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/commerce_discount.module',
        'basename' => 'commerce_discount.module',
        'name' => 'commerce_discount',
        'info' => 
        array (
          'name' => 'Commerce Discount',
          'description' => 'Provides common functionality and a UI for managing discounts.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'number',
            1 => 'commerce',
            2 => 'commerce_price',
            3 => 'commerce_line_item',
            4 => 'commerce_product_reference',
            5 => 'entity',
            6 => 'entityreference',
            7 => 'inline_entity_form (>=1.5)',
            8 => 'views',
            9 => 'inline_conditions',
          ),
          'files' => 
          array (
            0 => 'commerce_discount.info.inc',
            1 => 'includes/commerce_discount.admin.inc',
            2 => 'includes/commerce_discount.class.inc',
            3 => 'includes/commerce_discount_offer.class.inc',
            4 => 'includes/commerce_discount_offer.inline_entity_form.inc',
            5 => 'includes/commerce_discount.controller.inc',
            6 => 'includes/views/commerce_discount.views.inc',
            7 => 'includes/views/handlers/commerce_discount_handler_field_operations_dropbutton.inc',
            8 => 'commerce_discount.test',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_custom_product' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_custom_product/commerce_custom_product.module',
        'basename' => 'commerce_custom_product.module',
        'name' => 'commerce_custom_product',
        'info' => 
        array (
          'name' => 'Customizable Products',
          'description' => 'Adds features to support the creation of customizable products.',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_line_item',
            1 => 'commerce_line_item_ui',
            2 => 'commerce_product',
            3 => 'commerce_product_reference',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta2',
          'project' => 'commerce_custom_product',
          'datestamp' => '1347049622',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'custom_meta' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/custom_meta/custom_meta.module',
        'basename' => 'custom_meta.module',
        'name' => 'custom_meta',
        'info' => 
        array (
          'name' => 'Custom Meta',
          'description' => 'Adds user defined custom meta tags.',
          'package' => 'Third Party Integration',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'custom_meta.module',
          ),
          'version' => '7.x-1.4',
          'project' => 'custom_meta',
          'datestamp' => '1405783728',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'navbar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/navbar/navbar.module',
        'basename' => 'navbar.module',
        'name' => 'navbar',
        'info' => 
        array (
          'name' => 'Mobile Friendly Navigation Toolbar',
          'description' => 'A very simple mobile friendly toolbar that lets you switch between frontend and backend.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'libraries (>=2.0)',
          ),
          'version' => '7.x-1.5',
          'project' => 'navbar',
          'datestamp' => '1419363481',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.5',
      ),
      'commerce_product_attributes' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_product_attributes/commerce_product_attributes.module',
        'basename' => 'commerce_product_attributes.module',
        'name' => 'commerce_product_attributes',
        'info' => 
        array (
          'name' => 'Product Attributes',
          'description' => 'Enables the display of product attributes in the shopping cart view.',
          'package' => 'Commerce Product Enhancements',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_product_attributes_handler_field_attributes.inc',
          ),
          'version' => '7.x-1.0-beta3',
          'project' => 'commerce_product_attributes',
          'datestamp' => '1316355701',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta3',
      ),
      'file_entity_inline' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/file_entity_inline/file_entity_inline.module',
        'basename' => 'file_entity_inline.module',
        'name' => 'file_entity_inline',
        'info' => 
        array (
          'name' => 'File entity inline',
          'description' => 'Makes field entities editable within other entities.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file_entity',
            1 => 'field',
            2 => 'ctools',
          ),
          'configure' => 'admin/structure/types',
          'version' => '7.x-1.0-beta1',
          'project' => 'file_entity_inline',
          'datestamp' => '1348910169',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'menu_position' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_position/menu_position.module',
        'basename' => 'menu_position.module',
        'name' => 'menu_position',
        'info' => 
        array (
          'name' => 'Menu position',
          'description' => 'Customize menu position of nodes depending on their content type, associated terms and others conditions.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'menu (>7.11)',
          ),
          'files' => 
          array (
            0 => 'menu_position.module',
            1 => 'menu_position.admin.inc',
            2 => 'menu_position.install',
            3 => 'menu_position.node_type.inc',
          ),
          'configure' => 'admin/structure/menu-position',
          'version' => '7.x-1.1',
          'project' => 'menu_position',
          'datestamp' => '1329911144',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.1',
      ),
      'html5_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/html5_tools/html5_tools.module',
        'basename' => 'html5_tools.module',
        'name' => 'html5_tools',
        'info' => 
        array (
          'name' => 'HTML5 Tools',
          'description' => 'Provides a set of tools to allow sites to be built using HTML5.',
          'core' => '7.x',
          'php' => '5',
          'package' => 'Markup',
          'dependencies' => 
          array (
            0 => 'elements',
            1 => 'field',
          ),
          'configure' => 'admin/config/development/html5-tools',
          'version' => '7.x-1.2',
          'project' => 'html5_tools',
          'datestamp' => '1336411555',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'computed_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/computed_field/computed_field.module',
        'basename' => 'computed_field.module',
        'name' => 'computed_field',
        'info' => 
        array (
          'name' => 'Computed Field',
          'description' => 'Defines a field type that allows values to be "computed" via PHP code.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'computed_field.install',
            1 => 'computed_field.module',
          ),
          'version' => '7.x-1.0',
          'project' => 'computed_field',
          'datestamp' => '1386094705',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'stage_file_proxy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/stage_file_proxy/stage_file_proxy.module',
        'basename' => 'stage_file_proxy.module',
        'name' => 'stage_file_proxy',
        'info' => 
        array (
          'name' => 'Stage File Proxy',
          'description' => 'Proxies files from production server so you don\'t have to transfer them manually',
          'core' => '7.x',
          'configure' => 'admin/config/system/stage_file_proxy',
          'version' => '7.x-1.7',
          'project' => 'stage_file_proxy',
          'datestamp' => '1428604383',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'imagecache_coloractions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/coloractions/imagecache_coloractions.module',
        'basename' => 'imagecache_coloractions.module',
        'name' => 'imagecache_coloractions',
        'info' => 
        array (
          'name' => 'Imagecache Color Actions',
          'description' => 'Provides image effects color-shifting, invert colors, brightness, posterize and alpha transparency effects. Also provides a change image format effect.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'files' => 
          array (
            0 => 'imagecache_coloractions.install',
            1 => 'imagecache_coloractions.module',
            2 => 'transparency.inc',
            3 => 'tests/green.imagecache_preset.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_styles_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_styles_admin/image_styles_admin.module',
        'basename' => 'image_styles_admin.module',
        'name' => 'image_styles_admin',
        'info' => 
        array (
          'name' => 'Image styles admin',
          'description' => 'Provides additional administrative functionality to duplicate, export or import image styles.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_effects_text_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text_test/image_effects_text_test.module',
        'basename' => 'image_effects_text_test.module',
        'name' => 'image_effects_text_test',
        'info' => 
        array (
          'name' => 'Image Effects Text test',
          'description' => 'Defines image styles that test the text effect.',
          'core' => '7.x',
          'package' => 'Media',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'image_effects_text',
            2 => 'imagecache_canvasactions',
            3 => 'system_stream_wrapper',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_effects_text' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text.module',
        'basename' => 'image_effects_text.module',
        'name' => 'image_effects_text',
        'info' => 
        array (
          'name' => 'Image Effects Text',
          'description' => 'Provides an image effect to overlay text captions on images.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'imagecache_actions',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_autorotate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/autorotate/imagecache_autorotate.module',
        'basename' => 'imagecache_autorotate.module',
        'name' => 'imagecache_autorotate',
        'info' => 
        array (
          'name' => 'Imagecache Autorotate',
          'description' => 'Provides an image effect to autorotate an image based on EXIF data.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_customactions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/customactions/imagecache_customactions.module',
        'basename' => 'imagecache_customactions.module',
        'name' => 'imagecache_customactions',
        'info' => 
        array (
          'name' => 'Imagecache Custom Actions',
          'description' => 'Provides the custom and subroutine image effects.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.5',
      ),
      'imagecache_testsuite' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/tests/imagecache_testsuite.module',
        'basename' => 'imagecache_testsuite.module',
        'name' => 'imagecache_testsuite',
        'info' => 
        array (
          'name' => 'Imagecache_actions Test Suite',
          'description' => 'Displays a collection of demo image styles.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'system_stream_wrapper',
          ),
          'features' => 
          array (
            'image' => 
            array (
              0 => 'corners_combo',
            ),
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_canvasactions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/canvasactions/imagecache_canvasactions.module',
        'basename' => 'imagecache_canvasactions.module',
        'name' => 'imagecache_canvasactions',
        'info' => 
        array (
          'name' => 'Imagecache Canvas Actions',
          'description' => 'Provides image effects for manipulating image canvases: define canvas, image mask, watermark, underlay background image, rounded corners, composite source to image and resize by percent effect. Also provides an aspect switcher (portrait/landscape).',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/imagecache_actions.module',
        'basename' => 'imagecache_actions.module',
        'name' => 'imagecache_actions',
        'info' => 
        array (
          'name' => 'Imagecache Actions',
          'description' => 'Provides utility code for a number of additional image effects that can be found in the sub modules.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.5',
      ),
      'current_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi/contrib/current_search/current_search.module',
        'basename' => 'current_search.module',
        'name' => 'current_search',
        'info' => 
        array (
          'name' => 'Current Search Blocks',
          'description' => 'Provides an interface for creating blocks containing information about the current search.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/current_search/item.inc',
            1 => 'plugins/current_search/item_active.inc',
            2 => 'plugins/current_search/item_group.inc',
            3 => 'plugins/current_search/item_text.inc',
            4 => 'tests/current_search.test',
          ),
          'configure' => 'admin/config/search/current_search',
          'version' => '7.x-1.5',
          'project' => 'facetapi',
          'datestamp' => '1405685332',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.5',
      ),
      'facetapi' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi/facetapi.module',
        'basename' => 'facetapi.module',
        'name' => 'facetapi',
        'info' => 
        array (
          'name' => 'Facet API',
          'description' => 'An abstracted facet API that can be used by various search backends.',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/facetapi/adapter.inc',
            1 => 'plugins/facetapi/dependency.inc',
            2 => 'plugins/facetapi/dependency_bundle.inc',
            3 => 'plugins/facetapi/dependency_role.inc',
            4 => 'plugins/facetapi/empty_behavior.inc',
            5 => 'plugins/facetapi/empty_behavior_text.inc',
            6 => 'plugins/facetapi/filter.inc',
            7 => 'plugins/facetapi/query_type.inc',
            8 => 'plugins/facetapi/url_processor.inc',
            9 => 'plugins/facetapi/url_processor_standard.inc',
            10 => 'plugins/facetapi/widget.inc',
            11 => 'plugins/facetapi/widget_links.inc',
            12 => 'tests/facetapi_test.plugins.inc',
            13 => 'tests/facetapi.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'facetapi',
          'datestamp' => '1405685332',
          'php' => '5.2.4',
        ),
        'schema_version' => '7103',
        'version' => '7.x-1.5',
      ),
      'backup_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/backup_migrate/backup_migrate.module',
        'basename' => 'backup_migrate.module',
        'name' => 'backup_migrate',
        'info' => 
        array (
          'name' => 'Backup and Migrate',
          'description' => 'Backup or migrate the Drupal Database quickly and without unnecessary data.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'backup_migrate.module',
            1 => 'backup_migrate.install',
            2 => 'includes/destinations.inc',
            3 => 'includes/profiles.inc',
            4 => 'includes/schedules.inc',
          ),
          'configure' => 'admin/config/system/backup_migrate',
          'version' => '7.x-2.8',
          'project' => 'backup_migrate',
          'datestamp' => '1383686305',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7205',
        'version' => '7.x-2.8',
      ),
      'custom_pub' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/custom_pub/custom_pub.module',
        'basename' => 'custom_pub.module',
        'name' => 'custom_pub',
        'info' => 
        array (
          'name' => 'Custom Publishing Options',
          'description' => 'Adds the ability to add Custom publishing options to the node Add/Edit forms.',
          'core' => '7.x',
          'package' => 'Other',
          'suggests' => 
          array (
            0 => 'override_node_options',
            1 => 'views',
          ),
          'version' => '7.x-1.3',
          'project' => 'custom_pub',
          'datestamp' => '1370879152',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.3',
      ),
      'taxonomy_display' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/taxonomy_display/taxonomy_display.module',
        'basename' => 'taxonomy_display.module',
        'name' => 'taxonomy_display',
        'info' => 
        array (
          'name' => 'Taxonomy display',
          'description' => 'Modify the display of taxonomy term pages per vocabulary.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'taxonomy_display.module',
            1 => 'classes/associated_display.inc',
            2 => 'classes/breadcrumb_display.inc',
            3 => 'classes/handler_form.inc',
            4 => 'classes/term_display.inc',
            5 => 'handlers/term/core.inc',
            6 => 'handlers/term/hidden.inc',
            7 => 'handlers/associated/core.inc',
            8 => 'handlers/associated/hidden.inc',
            9 => 'handlers/associated/views.inc',
            10 => 'handlers/breadcrumb/core.inc',
            11 => 'handlers/breadcrumb/hidden.inc',
            12 => 'handlers/breadcrumb/ignore.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'taxonomy_display',
          'datestamp' => '1351568833',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.x-1.1',
      ),
      'responsive_favicons' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/responsive_favicons/responsive_favicons.module',
        'basename' => 'responsive_favicons.module',
        'name' => 'responsive_favicons',
        'info' => 
        array (
          'name' => 'Responsive Favicons',
          'description' => 'Add responsive favicons to your site based on the code from http://realfavicongenerator.net/',
          'core' => '7.x',
          'configure' => 'admin/config/user-interface/responsive_favicons',
          'version' => '7.x-1.2',
          'project' => 'responsive_favicons',
          'datestamp' => '1442890144',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.2',
      ),
      'devel_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel_generate/devel_generate.module',
        'basename' => 'devel_generate.module',
        'name' => 'devel_generate',
        'info' => 
        array (
          'name' => 'Devel generate',
          'description' => 'Generate dummy users, nodes, and taxonomy terms.',
          'package' => 'Development',
          'core' => '7.x',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'configure' => 'admin/config/development/generate',
          'files' => 
          array (
            0 => 'devel_generate.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel.module',
        'basename' => 'devel.module',
        'name' => 'devel',
        'info' => 
        array (
          'name' => 'Devel',
          'description' => 'Various blocks, pages, and functions for developers.',
          'package' => 'Development',
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'files' => 
          array (
            0 => 'devel.test',
            1 => 'devel.mail.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7006',
        'version' => '7.x-1.5',
      ),
      'devel_node_access' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel_node_access.module',
        'basename' => 'devel_node_access.module',
        'name' => 'devel_node_access',
        'info' => 
        array (
          'name' => 'Devel node access',
          'description' => 'Developer blocks and page illustrating relevant node_access records.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'telephone' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/telephone/telephone.module',
        'basename' => 'telephone.module',
        'name' => 'telephone',
        'info' => 
        array (
          'name' => 'Telephone',
          'description' => 'Defines a field type for telephone numbers.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'telephone.migrate.inc',
          ),
          'version' => '7.x-1.0-alpha1',
          'project' => 'telephone',
          'datestamp' => '1389736105',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha1',
      ),
      'workflow_extensions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow_extensions/workflow_extensions.module',
        'basename' => 'workflow_extensions.module',
        'name' => 'workflow_extensions',
        'info' => 
        array (
          'name' => 'Workflow extensions',
          'description' => 'Various UI improvements for the Workflow module. In particular the replacement of the workflow radio buttons by either a drop-down or more intuitive single-action buttons with configurable labels.',
          'core' => '7.x',
          'package' => 'Workflow',
          'configure' => 'admin/config/workflow/workflow_extensions',
          'files' => 
          array (
            0 => 'views/workflow_extensions_handler_field_state_change_form.inc',
            1 => 'views/workflow_extensions_handler_field_workflow_comment_link_edit.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'workflow_extensions',
          'datestamp' => '1374541185',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'xmlsitemap_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_custom/xmlsitemap_custom.module',
        'basename' => 'xmlsitemap_custom.module',
        'name' => 'xmlsitemap_custom',
        'info' => 
        array (
          'name' => 'XML sitemap custom',
          'description' => 'Adds user configurable links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_custom.module',
            1 => 'xmlsitemap_custom.admin.inc',
            2 => 'xmlsitemap_custom.install',
            3 => 'xmlsitemap_custom.test',
          ),
          'configure' => 'admin/config/search/xmlsitemap/custom',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_i18n/xmlsitemap_i18n.module',
        'basename' => 'xmlsitemap_i18n.module',
        'name' => 'xmlsitemap_i18n',
        'info' => 
        array (
          'name' => 'XML sitemap internationalization',
          'description' => 'Enables multilingual XML sitemaps.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'i18n',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_i18n.module',
            1 => 'xmlsitemap_i18n.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_taxonomy/xmlsitemap_taxonomy.module',
        'basename' => 'xmlsitemap_taxonomy.module',
        'name' => 'xmlsitemap_taxonomy',
        'info' => 
        array (
          'name' => 'XML sitemap taxonomy',
          'description' => 'Add taxonomy term links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'taxonomy',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_taxonomy.module',
            1 => 'xmlsitemap_taxonomy.install',
            2 => 'xmlsitemap_taxonomy.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_menu' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_menu/xmlsitemap_menu.module',
        'basename' => 'xmlsitemap_menu.module',
        'name' => 'xmlsitemap_menu',
        'info' => 
        array (
          'name' => 'XML sitemap menu',
          'description' => 'Adds menu item links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'menu',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_menu.module',
            1 => 'xmlsitemap_menu.install',
            2 => 'xmlsitemap_menu.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6201',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_node' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_node/xmlsitemap_node.module',
        'basename' => 'xmlsitemap_node.module',
        'name' => 'xmlsitemap_node',
        'info' => 
        array (
          'name' => 'XML sitemap node',
          'description' => 'Adds content links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_node.module',
            1 => 'xmlsitemap_node.install',
            2 => 'xmlsitemap_node.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6201',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_user' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_user/xmlsitemap_user.module',
        'basename' => 'xmlsitemap_user.module',
        'name' => 'xmlsitemap_user',
        'info' => 
        array (
          'name' => 'XML sitemap user',
          'description' => 'Adds user profile links to the sitemap.',
          'package' => 'XML sitemap',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'xmlsitemap_user.module',
            1 => 'xmlsitemap_user.install',
            2 => 'xmlsitemap_user.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_engines' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_engines/xmlsitemap_engines.module',
        'basename' => 'xmlsitemap_engines.module',
        'name' => 'xmlsitemap_engines',
        'info' => 
        array (
          'name' => 'XML sitemap engines',
          'description' => 'Submit the sitemap to search engines.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_engines.module',
            1 => 'xmlsitemap_engines.admin.inc',
            2 => 'xmlsitemap_engines.install',
            3 => 'tests/xmlsitemap_engines.test',
          ),
          'recommends' => 
          array (
            0 => 'site_verify',
          ),
          'configure' => 'admin/config/search/xmlsitemap/engines',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6202',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap.module',
        'basename' => 'xmlsitemap.module',
        'name' => 'xmlsitemap',
        'info' => 
        array (
          'name' => 'XML sitemap',
          'description' => 'Creates an XML sitemap conforming to the <a href="http://sitemaps.org/">sitemaps.org protocol</a>.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'xmlsitemap.module',
            1 => 'xmlsitemap.inc',
            2 => 'xmlsitemap.admin.inc',
            3 => 'xmlsitemap.drush.inc',
            4 => 'xmlsitemap.generate.inc',
            5 => 'xmlsitemap.xmlsitemap.inc',
            6 => 'xmlsitemap.pages.inc',
            7 => 'xmlsitemap.install',
            8 => 'xmlsitemap.test',
          ),
          'recommends' => 
          array (
            0 => 'robotstxt',
          ),
          'configure' => 'admin/config/search/xmlsitemap',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => '7.x-2.2',
      ),
      'masquerade' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/masquerade/masquerade.module',
        'basename' => 'masquerade.module',
        'name' => 'masquerade',
        'info' => 
        array (
          'name' => 'Masquerade',
          'description' => 'This module allows permitted users to masquerade as other users.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'masquerade.test',
          ),
          'configure' => 'admin/config/people/masquerade',
          'version' => '7.x-1.0-rc7',
          'project' => 'masquerade',
          'datestamp' => '1394333639',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-rc7',
      ),
      'corresponding_node_references' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/cnr/corresponding_node_references.module',
        'basename' => 'corresponding_node_references.module',
        'name' => 'corresponding_node_references',
        'info' => 
        array (
          'name' => 'Corresponding Node References',
          'description' => 'Syncs the node reference between two node types which have a nodereference to each other.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'node_reference',
            1 => 'ctools',
          ),
          'version' => '7.x-4.22',
          'project' => 'cnr',
          'datestamp' => '1321480838',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-4.22',
      ),
      'views_litepager' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_litepager/views_litepager.module',
        'basename' => 'views_litepager.module',
        'name' => 'views_litepager',
        'info' => 
        array (
          'name' => 'Views Litepager',
          'description' => 'Provide a pager plugin for Views that doesn\'t require an expensive count query',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'core' => '7.x',
          'package' => 'Views',
          'files' => 
          array (
            0 => 'views_litepager_plugin_pager_lite.inc',
          ),
          'version' => '7.x-3.0',
          'project' => 'views_litepager',
          'datestamp' => '1335573365',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0',
      ),
      'mailchimp_activity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_activity/mailchimp_activity.module',
        'basename' => 'mailchimp_activity.module',
        'name' => 'mailchimp_activity',
        'info' => 
        array (
          'name' => 'MailChimp Activity',
          'description' => 'View activity for an email address associated with any entity.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'mailchimp_activity.entity.inc',
            1 => 'mailchimp_activity.ui_controller.inc',
          ),
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.12',
      ),
      'mailchimp_campaign' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_campaign/mailchimp_campaign.module',
        'basename' => 'mailchimp_campaign.module',
        'name' => 'mailchimp_campaign',
        'info' => 
        array (
          'name' => 'MailChimp Campaigns',
          'description' => 'Create, send and import MailChimp campaigns.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp_lists',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'mailchimp_campaign.admin.inc',
            1 => 'mailchimp_campaign.entity.inc',
          ),
          'configure' => 'admin/config/services/mailchimp/campaigns',
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.12',
      ),
      'mailchimp_lists' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_lists/mailchimp_lists.module',
        'basename' => 'mailchimp_lists.module',
        'name' => 'mailchimp_lists',
        'info' => 
        array (
          'name' => 'MailChimp Lists',
          'description' => 'Manage and integrate MailChimp lists.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp',
            1 => 'entity',
            2 => 'entity_token',
          ),
          'configure' => 'admin/config/services/mailchimp/lists',
          'files' => 
          array (
            0 => 'lib/mailchimp_lists.entity.inc',
            1 => 'tests/mailchimp_lists.test',
          ),
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7206',
        'version' => '7.x-2.12',
      ),
      'mailchimp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/mailchimp.module',
        'basename' => 'mailchimp.module',
        'name' => 'mailchimp',
        'info' => 
        array (
          'name' => 'MailChimp',
          'description' => 'MailChimp email service integration.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/mailchimp.inc',
            1 => 'tests/mailchimp_tests.inc',
          ),
          'dependencies' => 
          array (
            0 => 'libraries (>=2)',
          ),
          'configure' => 'admin/config/services/mailchimp',
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.12',
      ),
      'semantic_ds' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/semantic_ds/semantic_ds.module',
        'basename' => 'semantic_ds.module',
        'name' => 'semantic_ds',
        'info' => 
        array (
          'name' => 'Semantic Display Suite',
          'description' => 'Semantic field options for Display Suite',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'semantic_ds',
          'datestamp' => '1380627143',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'node_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/node_reference/node_reference.module',
        'basename' => 'node_reference.module',
        'name' => 'node_reference',
        'info' => 
        array (
          'name' => 'Node Reference',
          'description' => 'Defines a field type for referencing one node from another.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'references',
            2 => 'options',
          ),
          'files' => 
          array (
            0 => 'node_reference.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.1',
      ),
      'user_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/user_reference/user_reference.module',
        'basename' => 'user_reference.module',
        'name' => 'user_reference',
        'info' => 
        array (
          'name' => 'User Reference',
          'description' => 'Defines a field type for referencing a user from a node.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'references',
            2 => 'options',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'references' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/references.module',
        'basename' => 'references.module',
        'name' => 'references',
        'info' => 
        array (
          'name' => 'References',
          'description' => 'Defines common base features for the various reference field types.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'views/references_handler_relationship.inc',
            1 => 'views/references_handler_argument.inc',
            2 => 'views/references_plugin_display.inc',
            3 => 'views/references_plugin_style.inc',
            4 => 'views/references_plugin_row_fields.inc',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'video_embed_facebook' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_facebook/video_embed_facebook.module',
        'basename' => 'video_embed_facebook.module',
        'name' => 'video_embed_facebook',
        'info' => 
        array (
          'name' => 'Video Embed Facebook',
          'description' => 'Provides Facebook handler for Video Embed Fields.  This module also serves as an example of how to add handlers to video embed field.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'dependencies' => 
          array (
            0 => 'video_embed_field',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta8',
      ),
      'video_embed_brightcove' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_brightcove/video_embed_brightcove.module',
        'basename' => 'video_embed_brightcove.module',
        'name' => 'video_embed_brightcove',
        'info' => 
        array (
          'name' => 'Video Embed Brightcove',
          'description' => 'Provides Brightcove handler for Video Embed Fields.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'dependencies' => 
          array (
            0 => 'video_embed_field',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta8',
      ),
      'video_embed_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_field.module',
        'basename' => 'video_embed_field.module',
        'name' => 'video_embed_field',
        'info' => 
        array (
          'name' => 'Video Embed Field',
          'description' => 'Expose a field type for embedding videos from youtube or vimeo.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'files' => 
          array (
            0 => 'video_embed_field.migrate.inc',
            1 => 'views/handlers/views_embed_field_views_handler_field_thumbnail_path.inc',
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'image',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => '7008',
        'version' => '7.x-2.0-beta8',
      ),
      'rules_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_i18n/rules_i18n.module',
        'basename' => 'rules_i18n.module',
        'name' => 'rules_i18n',
        'info' => 
        array (
          'name' => 'Rules translation',
          'description' => 'Allows translating rules.',
          'dependencies' => 
          array (
            0 => 'rules',
            1 => 'i18n_string',
          ),
          'package' => 'Multilingual - Internationalization',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_i18n.i18n.inc',
            1 => 'rules_i18n.rules.inc',
            2 => 'rules_i18n.test',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.9',
      ),
      'rules_scheduler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_scheduler/rules_scheduler.module',
        'basename' => 'rules_scheduler.module',
        'name' => 'rules_scheduler',
        'info' => 
        array (
          'name' => 'Rules Scheduler',
          'description' => 'Schedule the execution of Rules components using actions.',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_scheduler.admin.inc',
            1 => 'rules_scheduler.module',
            2 => 'rules_scheduler.install',
            3 => 'rules_scheduler.rules.inc',
            4 => 'rules_scheduler.test',
            5 => 'includes/rules_scheduler.handler.inc',
            6 => 'includes/rules_scheduler.views_default.inc',
            7 => 'includes/rules_scheduler.views.inc',
            8 => 'includes/rules_scheduler_views_filter.inc',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => '7204',
        'version' => '7.x-2.9',
      ),
      'rules_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_admin/rules_admin.module',
        'basename' => 'rules_admin.module',
        'name' => 'rules_admin',
        'info' => 
        array (
          'name' => 'Rules UI',
          'description' => 'Administrative interface for managing rules.',
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_admin.module',
            1 => 'rules_admin.inc',
          ),
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.9',
      ),
      'rules' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules.module',
        'basename' => 'rules.module',
        'name' => 'rules',
        'info' => 
        array (
          'name' => 'Rules',
          'description' => 'React on events and conditionally evaluate actions.',
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules.features.inc',
            1 => 'tests/rules.test',
            2 => 'includes/faces.inc',
            3 => 'includes/rules.core.inc',
            4 => 'includes/rules.event.inc',
            5 => 'includes/rules.processor.inc',
            6 => 'includes/rules.plugins.inc',
            7 => 'includes/rules.state.inc',
            8 => 'includes/rules.dispatcher.inc',
            9 => 'modules/node.eval.inc',
            10 => 'modules/php.eval.inc',
            11 => 'modules/rules_core.eval.inc',
            12 => 'modules/system.eval.inc',
            13 => 'ui/ui.controller.inc',
            14 => 'ui/ui.core.inc',
            15 => 'ui/ui.data.inc',
            16 => 'ui/ui.plugins.inc',
          ),
          'dependencies' => 
          array (
            0 => 'entity_token',
            1 => 'entity',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => '7214',
        'version' => '7.x-2.9',
      ),
      'date_repeat_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.module',
        'basename' => 'date_repeat_field.module',
        'name' => 'date_repeat_field',
        'info' => 
        array (
          'name' => 'Date Repeat Field',
          'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
            2 => 'date_repeat',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date_repeat_field.css',
            ),
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_popup' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_popup/date_popup.module',
        'basename' => 'date_popup.module',
        'name' => 'date_popup',
        'info' => 
        array (
          'name' => 'Date Popup',
          'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/date_popup',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'themes/datepicker.1.7.css',
            ),
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_context' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_context/date_context.module',
        'basename' => 'date_context.module',
        'name' => 'date_context',
        'info' => 
        array (
          'name' => 'Date Context',
          'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'context',
          ),
          'files' => 
          array (
            0 => 'date_context.module',
            1 => 'plugins/date_context_date_condition.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_all_day' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_all_day/date_all_day.module',
        'basename' => 'date_all_day.module',
        'name' => 'date_all_day',
        'info' => 
        array (
          'name' => 'Date All Day',
          'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_migrate_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
        'basename' => 'date_migrate_example.module',
        'name' => 'date_migrate_example',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'date_repeat',
            2 => 'date_repeat_field',
            3 => 'features',
            4 => 'migrate',
          ),
          'description' => 'Examples of migrating with the Date module',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-date_migrate_example-body',
              1 => 'node-date_migrate_example-field_date',
              2 => 'node-date_migrate_example-field_date_range',
              3 => 'node-date_migrate_example-field_date_repeat',
              4 => 'node-date_migrate_example-field_datestamp',
              5 => 'node-date_migrate_example-field_datestamp_range',
              6 => 'node-date_migrate_example-field_datetime',
              7 => 'node-date_migrate_example-field_datetime_range',
            ),
            'node' => 
            array (
              0 => 'date_migrate_example',
            ),
          ),
          'files' => 
          array (
            0 => 'date_migrate_example.migrate.inc',
          ),
          'name' => 'Date Migration Example',
          'package' => 'Features',
          'project' => 'date',
          'version' => '7.x-2.8',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_tools/date_tools.module',
        'basename' => 'date_tools.module',
        'name' => 'date_tools',
        'info' => 
        array (
          'name' => 'Date Tools',
          'description' => 'Tools to import and auto-create dates and calendars.',
          'dependencies' => 
          array (
            0 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/tools',
          'files' => 
          array (
            0 => 'tests/date_tools.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_api/date_api.module',
        'basename' => 'date_api.module',
        'name' => 'date_api',
        'info' => 
        array (
          'name' => 'Date API',
          'description' => 'A Date API that can be used by other modules.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date.css',
            ),
          ),
          'files' => 
          array (
            0 => 'date_api.module',
            1 => 'date_api_sql.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.8',
      ),
      'date_repeat' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
        'basename' => 'date_repeat.module',
        'name' => 'date_repeat',
        'info' => 
        array (
          'name' => 'Date Repeat API',
          'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_repeat.test',
            1 => 'tests/date_repeat_form.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_views/date_views.module',
        'basename' => 'date_views.module',
        'name' => 'date_views',
        'info' => 
        array (
          'name' => 'Date Views',
          'description' => 'Views integration for date fields and date functionality.',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'includes/date_views_argument_handler.inc',
            1 => 'includes/date_views_argument_handler_simple.inc',
            2 => 'includes/date_views_filter_handler.inc',
            3 => 'includes/date_views_filter_handler_simple.inc',
            4 => 'includes/date_views.views.inc',
            5 => 'includes/date_views_plugin_pager.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date.module',
        'basename' => 'date.module',
        'name' => 'date',
        'info' => 
        array (
          'name' => 'Date',
          'description' => 'Makes date/time fields available.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'date.migrate.inc',
            1 => 'tests/date_api.test',
            2 => 'tests/date.test',
            3 => 'tests/date_field.test',
            4 => 'tests/date_migrate.test',
            5 => 'tests/date_validation.test',
            6 => 'tests/date_timezone.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => '7004',
        'version' => '7.x-2.8',
      ),
      'nodequeue_service' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/addons/nodequeue_service/nodequeue_service.module',
        'basename' => 'nodequeue_service.module',
        'name' => 'nodequeue_service',
        'info' => 
        array (
          'name' => 'Nodequeue Service',
          'description' => 'Provides a nodequeue service.',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'services',
            1 => 'nodequeue',
          ),
          'files' => 
          array (
            0 => 'nodequeue_service.inc',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta1',
      ),
      'smartqueue' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/smartqueue.module',
        'basename' => 'smartqueue.module',
        'name' => 'smartqueue',
        'info' => 
        array (
          'name' => 'Smartqueue taxonomy',
          'description' => 'Creates a node queue for each taxonomy vocabulary',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'nodequeue',
            1 => 'taxonomy',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.0-beta1',
      ),
      'nodequeue' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/nodequeue.module',
        'basename' => 'nodequeue.module',
        'name' => 'nodequeue',
        'info' => 
        array (
          'name' => 'Nodequeue',
          'description' => 'Create queues which can contain nodes in arbitrary order',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'configure' => 'admin/structure/nodequeue/settings',
          'files' => 
          array (
            0 => 'includes/views/nodequeue.views.inc',
            1 => 'includes/nodequeue.actions.inc',
            2 => 'includes/views/nodequeue_handler_argument_subqueue_qid.inc',
            3 => 'includes/views/nodequeue_handler_field_all_queues.inc',
            4 => 'includes/views/nodequeue_handler_field_all_subqueues.inc',
            5 => 'includes/views/nodequeue_handler_field_links.inc',
            6 => 'includes/views/nodequeue_handler_field_queue_tab.inc',
            7 => 'includes/views/nodequeue_handler_filter_in_queue.inc',
            8 => 'includes/views/nodequeue_handler_relationship_nodequeue.inc',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0-beta1',
      ),
      'nodequeue_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/nodequeue_generate.module',
        'basename' => 'nodequeue_generate.module',
        'name' => 'nodequeue_generate',
        'info' => 
        array (
          'name' => 'Nodequeue generate',
          'description' => 'Bulk assign nodes into queues for quickly populating a site.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'nodequeue',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'nodequeue_generate.module',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta1',
      ),
      'content_lock_timeout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/content_lock/modules/content_lock_timeout/content_lock_timeout.module',
        'basename' => 'content_lock_timeout.module',
        'name' => 'content_lock_timeout',
        'info' => 
        array (
          'name' => 'Content locking (edit lock) timeouts',
          'description' => 'Provides mechanisms for automatically unlocking nodes that have been locked for a certain length of time.',
          'core' => '7.x',
          'project' => 'content_lock',
          'dependencies' => 
          array (
            0 => 'content_lock',
          ),
          'files' => 
          array (
            0 => 'content_lock_timeout.install',
            1 => 'content_lock_timeout.module',
          ),
          'configure' => 'admin/settings/content_lock',
          'version' => '7.x-2.0',
          'datestamp' => '1410528829',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0',
      ),
      'content_lock' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/content_lock/content_lock.module',
        'basename' => 'content_lock.module',
        'name' => 'content_lock',
        'info' => 
        array (
          'name' => 'Content locking (edit lock)',
          'description' => 'Prevents multiple users from trying to edit a single node simultaneously to prevent edit conflicts.',
          'core' => '7.x',
          'project' => 'content_lock',
          'dependencies' => 
          array (
            0 => 'filter',
            1 => 'node',
          ),
          'files' => 
          array (
            0 => 'content_lock.install',
            1 => 'content_lock.module',
            2 => 'content_lock.admin.inc',
            3 => 'views/content_lock.views',
            4 => 'views/views_handler_field_is_locked.inc',
            5 => 'views/views_handler_filter_is_locked.inc',
            6 => 'views/views_handler_sort_is_locked.inc',
          ),
          'configure' => 'admin/settings/content_lock',
          'version' => '7.x-2.0',
          'datestamp' => '1410528829',
          'php' => '5.2.4',
        ),
        'schema_version' => '6200',
        'version' => '7.x-2.0',
      ),
      'ember_support' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ember_support/ember_support.module',
        'basename' => 'ember_support.module',
        'name' => 'ember_support',
        'info' => 
        array (
          'name' => 'Ember Support',
          'description' => 'Additional functionality and unified interfaces for the Ember administration theme.',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/ember-contextual.css',
              1 => 'css/ember-ctools.css',
              2 => 'css/ember-media.css',
              3 => 'css/ember-modal.css',
              4 => 'css/ember-panels-ipe.css',
              5 => 'css/ember-purr.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'js/ember-support.js',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'ember_support',
          'datestamp' => '1421949706',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'ds_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_forms/ds_forms.module',
        'basename' => 'ds_forms.module',
        'name' => 'ds_forms',
        'info' => 
        array (
          'name' => 'Display Suite Forms',
          'description' => 'Manage the layout of forms in Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_ui/ds_ui.module',
        'basename' => 'ds_ui.module',
        'name' => 'ds_ui',
        'info' => 
        array (
          'name' => 'Display Suite UI',
          'description' => 'User interface for managing fields, view modes and classes.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_format' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_format/ds_format.module',
        'basename' => 'ds_format.module',
        'name' => 'ds_format',
        'info' => 
        array (
          'name' => 'Display Suite Format',
          'description' => 'Provides the Display Suite Code format filter.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_devel/ds_devel.module',
        'basename' => 'ds_devel.module',
        'name' => 'ds_devel',
        'info' => 
        array (
          'name' => 'Display Suite Devel',
          'description' => 'Development functionality for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
            1 => 'devel',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_search/ds_search.module',
        'basename' => 'ds_search.module',
        'name' => 'ds_search',
        'info' => 
        array (
          'name' => 'Display Suite Search',
          'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/search',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_extras' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_extras/ds_extras.module',
        'basename' => 'ds_extras.module',
        'name' => 'ds_extras',
        'info' => 
        array (
          'name' => 'Display Suite Extras',
          'description' => 'Contains additional features for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.7',
      ),
      'ds' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/ds.module',
        'basename' => 'ds.module',
        'name' => 'ds',
        'info' => 
        array (
          'name' => 'Display Suite',
          'description' => 'Extend the display options for every entity type.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'views/views_plugin_ds_entity_view.inc',
            1 => 'views/views_plugin_ds_fields_view.inc',
            2 => 'tests/ds.base.test',
            3 => 'tests/ds.search.test',
            4 => 'tests/ds.entities.test',
            5 => 'tests/ds.exportables.test',
            6 => 'tests/ds.views.test',
            7 => 'tests/ds.forms.test',
          ),
          'configure' => 'admin/structure/ds',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.7',
      ),
      'commerce_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_features/commerce_features.module',
        'basename' => 'commerce_features.module',
        'name' => 'commerce_features',
        'info' => 
        array (
          'name' => 'Commerce Features',
          'description' => 'Features integration for Drupal Commerce module',
          'package' => 'Features',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'commerce',
          ),
          'version' => '7.x-1.0',
          'project' => 'commerce_features',
          'datestamp' => '1385479047',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'redirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/redirect/redirect.module',
        'basename' => 'redirect.module',
        'name' => 'redirect',
        'info' => 
        array (
          'name' => 'Redirect',
          'description' => 'Allows users to redirect from old URLs to new URLs.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'redirect.controller.inc',
            1 => 'redirect.test',
            2 => 'views/redirect.views.inc',
            3 => 'views/redirect_handler_filter_redirect_type.inc',
            4 => 'views/redirect_handler_field_redirect_source.inc',
            5 => 'views/redirect_handler_field_redirect_redirect.inc',
            6 => 'views/redirect_handler_field_redirect_operations.inc',
            7 => 'views/redirect_handler_field_redirect_link_edit.inc',
            8 => 'views/redirect_handler_field_redirect_link_delete.inc',
          ),
          'configure' => 'admin/config/search/redirect/settings',
          'version' => '7.x-1.0-rc3',
          'project' => 'redirect',
          'datestamp' => '1436393342',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc3',
      ),
      'panels_mini' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_mini/panels_mini.module',
        'basename' => 'panels_mini.module',
        'name' => 'panels_mini',
        'info' => 
        array (
          'name' => 'Mini panels',
          'description' => 'Create mini panels that can be used as blocks by Drupal and panes by other panel modules.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/export_ui/panels_mini_ui.class.php',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'panels_ipe' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_ipe/panels_ipe.module',
        'basename' => 'panels_ipe.module',
        'name' => 'panels_ipe',
        'info' => 
        array (
          'name' => 'Panels In-Place Editor',
          'description' => 'Provide a UI for managing some Panels directly on the frontend, instead of having to use the backend.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'core' => '7.x',
          'configure' => 'admin/structure/panels',
          'files' => 
          array (
            0 => 'panels_ipe.module',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'i18n_panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/i18n_panels/i18n_panels.module',
        'basename' => 'i18n_panels.module',
        'name' => 'i18n_panels',
        'info' => 
        array (
          'name' => 'Panels translation',
          'description' => 'Supports translatable panels items.',
          'dependencies' => 
          array (
            0 => 'i18n',
            1 => 'panels',
            2 => 'i18n_string',
            3 => 'i18n_translation',
          ),
          'package' => 'Multilingual - Internationalization',
          'core' => '7.x',
          'version' => '7.x-3.5',
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'panels_node' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_node/panels_node.module',
        'basename' => 'panels_node.module',
        'name' => 'panels_node',
        'info' => 
        array (
          'name' => 'Panel nodes',
          'description' => 'Create nodes that are divided into areas with selectable content.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'configure' => 'admin/structure/panels',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'panels_node.module',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => '6001',
        'version' => '7.x-3.5',
      ),
      'panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels.module',
        'basename' => 'panels.module',
        'name' => 'panels',
        'info' => 
        array (
          'name' => 'Panels',
          'description' => 'Core Panels display functions; provides no external UI, at least one other Panels module should be enabled.',
          'core' => '7.x',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'configure' => 'admin/structure/panels',
          'dependencies' => 
          array (
            0 => 'ctools (>1.5)',
          ),
          'files' => 
          array (
            0 => 'panels.module',
            1 => 'includes/common.inc',
            2 => 'includes/legacy.inc',
            3 => 'includes/plugins.inc',
            4 => 'plugins/views/panels_views_plugin_row_fields.inc',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => '7303',
        'version' => '7.x-3.5',
      ),
      'blockify' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/blockify/blockify.module',
        'basename' => 'blockify.module',
        'name' => 'blockify',
        'info' => 
        array (
          'name' => 'Blockify',
          'description' => 'Exposes a number of core Drupal elements as blocks.',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'blockify',
          'datestamp' => '1303344135',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'breakpoints' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/breakpoints/breakpoints.module',
        'basename' => 'breakpoints.module',
        'name' => 'breakpoints',
        'info' => 
        array (
          'name' => 'Breakpoints',
          'description' => 'Manage breakpoints',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'breakpoints.module',
            1 => 'breakpoints.test',
          ),
          'configure' => 'admin/config/media/breakpoints',
          'version' => '7.x-1.3',
          'project' => 'breakpoints',
          'datestamp' => '1407507528',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.3',
      ),
      'globalredirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/globalredirect/globalredirect.module',
        'basename' => 'globalredirect.module',
        'name' => 'globalredirect',
        'info' => 
        array (
          'name' => 'Global Redirect',
          'description' => 'Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.',
          'package' => 'Path management',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'globalredirect.test',
          ),
          'configure' => 'admin/config/system/globalredirect',
          'version' => '7.x-1.5',
          'project' => 'globalredirect',
          'datestamp' => '1339748779',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'version' => '7.x-1.5',
      ),
      'mimemail_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_example/mimemail_example.module',
        'basename' => 'mimemail_example.module',
        'name' => 'mimemail_example',
        'info' => 
        array (
          'name' => 'Mime Mail Example',
          'description' => 'Example of how to use the Mime Mail module.',
          'dependencies' => 
          array (
            0 => 'mimemail',
          ),
          'package' => 'Example modules',
          'core' => '7.x',
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail_action' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_action/mimemail_action.module',
        'basename' => 'mimemail_action.module',
        'name' => 'mimemail_action',
        'info' => 
        array (
          'name' => 'Mime Mail Action',
          'description' => 'Provide actions for Mime Mail.',
          'package' => 'Mail',
          'dependencies' => 
          array (
            0 => 'mimemail',
            1 => 'trigger',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail_compress' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_compress/mimemail_compress.module',
        'basename' => 'mimemail_compress.module',
        'name' => 'mimemail_compress',
        'info' => 
        array (
          'name' => 'Mime Mail CSS Compressor',
          'description' => 'Converts CSS to inline styles in an HTML message. (Requires the PHP DOM extension.)',
          'package' => 'Mail',
          'dependencies' => 
          array (
            0 => 'mimemail',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mimemail_compress.inc',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/mimemail.module',
        'basename' => 'mimemail.module',
        'name' => 'mimemail',
        'info' => 
        array (
          'name' => 'Mime Mail',
          'description' => 'Send MIME-encoded emails with embedded images and attachments.',
          'dependencies' => 
          array (
            0 => 'mailsystem',
            1 => 'system (>=7.24)',
          ),
          'package' => 'Mail',
          'core' => '7.x',
          'configure' => 'admin/config/system/mimemail',
          'files' => 
          array (
            0 => 'includes/mimemail.mail.inc',
            1 => 'tests/mimemail.test',
            2 => 'tests/mimemail_rules.test',
            3 => 'tests/mimemail_compress.test',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.0-beta4',
      ),
      'facetapi_pretty_paths' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi_pretty_paths/facetapi_pretty_paths.module',
        'basename' => 'facetapi_pretty_paths.module',
        'name' => 'facetapi_pretty_paths',
        'info' => 
        array (
          'name' => 'Facet API Pretty Paths',
          'core' => '7.x',
          'description' => 'Enables pretty paths for searches with Facet API.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'files' => 
          array (
            0 => 'plugins/facetapi/url_processor_pretty_paths.inc',
            1 => 'plugins/base_path_provider/facetapi_pretty_paths_base_path_provider.inc',
            2 => 'plugins/base_path_provider/facetapi_pretty_paths_adapter_base_path_provider.inc',
            3 => 'plugins/base_path_provider/facetapi_pretty_paths_default_base_path_provider.inc',
            4 => 'plugins/coders/facetapi_pretty_paths_coder_default.inc',
            5 => 'plugins/coders/facetapi_pretty_paths_coder_taxonomy.inc',
            6 => 'plugins/coders/facetapi_pretty_paths_coder_taxonomy_pathauto.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'facetapi_pretty_paths',
          'datestamp' => '1422829982',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license_role' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/modules/commerce_license_role/commerce_license_role.module',
        'basename' => 'commerce_license_role.module',
        'name' => 'commerce_license_role',
        'info' => 
        array (
          'name' => 'Commerce License Role',
          'description' => 'Provides a license type for selling roles.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/modules/commerce_license_example/commerce_license_example.module',
        'basename' => 'commerce_license_example.module',
        'name' => 'commerce_license_example',
        'info' => 
        array (
          'name' => 'Commerce License Example',
          'description' => 'Provides an example license type for testing and development.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
            1 => 'inline_entity_form',
            2 => 'advancedqueue',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/commerce_license.module',
        'basename' => 'commerce_license.module',
        'name' => 'commerce_license',
        'info' => 
        array (
          'name' => 'Commerce License',
          'description' => 'Provides a framework for selling access to local or remote resources.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'configure' => 'admin/commerce/config/license',
          'dependencies' => 
          array (
            0 => 'commerce_product',
            1 => 'commerce_product_reference',
            2 => 'commerce_line_item',
            3 => 'commerce_payment',
            4 => 'ctools',
            5 => 'entity',
            6 => 'entity_bundle_plugin',
            7 => 'entityreference',
            8 => 'list',
            9 => 'number',
            10 => 'views_bulk_operations',
          ),
          'files' => 
          array (
            0 => 'commerce_license.info.inc',
            1 => 'includes/commerce_license.controller.inc',
            2 => 'includes/commerce_license.inline_entity_form.inc',
            3 => 'includes/plugins/license_type/base.inc',
            4 => 'includes/views/commerce_license.views_controller.inc',
            5 => 'includes/views/handlers/commerce_license_handler_field_access_details.inc',
            6 => 'includes/views/plugins/commerce_license_plugin_access_sync.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.3',
      ),
      'pathauto' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/pathauto/pathauto.module',
        'basename' => 'pathauto.module',
        'name' => 'pathauto',
        'info' => 
        array (
          'name' => 'Pathauto',
          'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
          'dependencies' => 
          array (
            0 => 'path',
            1 => 'token',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'pathauto.test',
          ),
          'configure' => 'admin/config/search/path/patterns',
          'recommends' => 
          array (
            0 => 'redirect',
          ),
          'version' => '7.x-1.2',
          'project' => 'pathauto',
          'datestamp' => '1344525185',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.x-1.2',
      ),
      'ctools_ajax_sample' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
        'basename' => 'ctools_ajax_sample.module',
        'name' => 'ctools_ajax_sample',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) AJAX Example',
          'description' => 'Shows how to use the power of Chaos AJAX.',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'core' => '7.x',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_access_ruleset' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
        'basename' => 'ctools_access_ruleset.module',
        'name' => 'ctools_access_ruleset',
        'info' => 
        array (
          'name' => 'Custom rulesets',
          'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_custom_content' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
        'basename' => 'ctools_custom_content.module',
        'name' => 'ctools_custom_content',
        'info' => 
        array (
          'name' => 'Custom content panes',
          'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'term_depth' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/term_depth/term_depth.module',
        'basename' => 'term_depth.module',
        'name' => 'term_depth',
        'info' => 
        array (
          'name' => 'Term Depth access',
          'description' => 'Controls access to context based upon term depth',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'bulk_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/bulk_export/bulk_export.module',
        'basename' => 'bulk_export.module',
        'name' => 'bulk_export',
        'info' => 
        array (
          'name' => 'Bulk Export',
          'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'views_content' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/views_content/views_content.module',
        'basename' => 'views_content.module',
        'name' => 'views_content',
        'info' => 
        array (
          'name' => 'Views content panes',
          'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'core' => '7.x',
          'version' => '7.x-1.7',
          'files' => 
          array (
            0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
            1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
            2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_plugin_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
        'basename' => 'ctools_plugin_example.module',
        'name' => 'ctools_plugin_example',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) Plugin Example',
          'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'panels',
            2 => 'page_manager',
            3 => 'advanced_help',
          ),
          'core' => '7.x',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'stylizer' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/stylizer/stylizer.module',
        'basename' => 'stylizer.module',
        'name' => 'stylizer',
        'info' => 
        array (
          'name' => 'Stylizer',
          'description' => 'Create custom styles for applications such as Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'color',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'page_manager' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/page_manager/page_manager.module',
        'basename' => 'page_manager.module',
        'name' => 'page_manager',
        'info' => 
        array (
          'name' => 'Page manager',
          'description' => 'Provides a UI and API to manage pages within the site.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools.module',
        'basename' => 'ctools.module',
        'name' => 'ctools',
        'info' => 
        array (
          'name' => 'Chaos tools',
          'description' => 'A library of helpful tools by Merlin of Chaos.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'files' => 
          array (
            0 => 'includes/context.inc',
            1 => 'includes/css-cache.inc',
            2 => 'includes/math-expr.inc',
            3 => 'includes/stylizer.inc',
            4 => 'tests/css_cache.test',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.7',
      ),
      'payment_commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment_commerce/payment_commerce.module',
        'basename' => 'payment_commerce.module',
        'name' => 'payment_commerce',
        'info' => 
        array (
          'name' => 'Payment for Drupal Commerce',
          'description' => 'Allows Drupal Commerce orders to be paid using Payment.',
          'configure' => 'admin/config/services/payment/payment_commerce',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_payment',
            1 => 'payment (>=1.12)',
          ),
          'test_dependencies' => 
          array (
            0 => 'views',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'tests/PaymentCommerceCheckoutWebTestCase.test',
            1 => 'tests/PaymentCommerceDeleteOrderWebTestCase.test',
          ),
          'version' => '7.x-1.7',
          'project' => 'payment_commerce',
          'datestamp' => '1414333129',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'facetapi_bonus' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi_bonus/facetapi_bonus.module',
        'basename' => 'facetapi_bonus.module',
        'name' => 'facetapi_bonus',
        'info' => 
        array (
          'name' => 'Facet API Bonus',
          'package' => 'Search Toolkit',
          'description' => 'Additions to facetapi',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'files' => 
          array (
            0 => 'plugins/facetapi/dependency_facet.inc',
            1 => 'plugins/facetapi/filter_rewrite_items.inc',
            2 => 'plugins/facetapi/filter_exclude_specified_items.inc',
            3 => 'plugins/facetapi/filter_narrow_results.inc',
            4 => 'plugins/facetapi/filter_show_if_minimum_items.inc',
            5 => 'plugins/facetapi/filter_show_deepest_level_items.inc',
            6 => 'plugins/facetapi/current_search/current_search_reset_filters_link.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'facetapi_bonus',
          'datestamp' => '1338012356',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'user_import' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/user_import/user_import.module',
        'basename' => 'user_import.module',
        'name' => 'user_import',
        'info' => 
        array (
          'name' => 'User Import',
          'description' => 'Import users into Drupal from a CSV file.',
          'core' => '7.x',
          'package' => 'Deployment',
          'files' => 
          array (
            0 => 'user_import.module',
            1 => 'user_import.install',
            2 => 'user_import.admin.inc',
            3 => 'user_import.import.inc',
            4 => 'user_import.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'user_import',
          'datestamp' => '1376260694',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.1',
      ),
      'commerce_node_checkout_expire' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_node_checkout/commerce_node_checkout_expire/commerce_node_checkout_expire.module',
        'basename' => 'commerce_node_checkout_expire.module',
        'name' => 'commerce_node_checkout_expire',
        'info' => 
        array (
          'name' => 'Commerce Node Checkout Expire',
          'description' => 'Automatic expiration of nodes published via purchase',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
            1 => 'commerce_node_checkout',
            2 => 'interval',
            3 => 'date',
            4 => 'date_popup',
          ),
          'package' => 'Commerce (contrib)',
          'files' => 
          array (
            0 => 'commerce_node_checkout_expire.test',
            1 => 'includes/views/handlers/commerce_node_checkout_expire_field_node_relist_form.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_node_checkout',
          'datestamp' => '1400256828',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'commerce_node_checkout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_node_checkout/commerce_node_checkout.module',
        'basename' => 'commerce_node_checkout.module',
        'name' => 'commerce_node_checkout',
        'info' => 
        array (
          'name' => 'Commerce Node Checkout',
          'description' => 'Allows users to pay to publish nodes.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_cart',
            2 => 'commerce_checkout',
            3 => 'commerce_price',
            4 => 'commerce_product_reference',
            5 => 'commerce_product_ui',
            6 => 'entityreference',
            7 => 'rules',
          ),
          'files' => 
          array (
            0 => 'commerce_node_checkout.test',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_node_checkout',
          'datestamp' => '1400256828',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'link' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/link/link.module',
        'basename' => 'link.module',
        'name' => 'link',
        'info' => 
        array (
          'name' => 'Link',
          'description' => 'Defines simple link field types.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'link.module',
            1 => 'link.migrate.inc',
            2 => 'tests/link.test',
            3 => 'tests/link.attribute.test',
            4 => 'tests/link.crud.test',
            5 => 'tests/link.crud_browser.test',
            6 => 'tests/link.token.test',
            7 => 'tests/link.validate.test',
            8 => 'views/link_views_handler_argument_target.inc',
            9 => 'views/link_views_handler_filter_protocol.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'link',
          'datestamp' => '1413924830',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.3',
      ),
      'imagecache_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_token/imagecache_token.module',
        'basename' => 'imagecache_token.module',
        'name' => 'imagecache_token',
        'info' => 
        array (
          'name' => 'Imagecache Token',
          'description' => 'Provides additional image tokens for use with ImageCache',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'token',
          ),
          'package' => 'Image',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'imagecache_token',
          'datestamp' => '1414529932',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'inline_entity_form' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/inline_entity_form/inline_entity_form.module',
        'basename' => 'inline_entity_form.module',
        'name' => 'inline_entity_form',
        'info' => 
        array (
          'name' => 'Inline Entity Form',
          'description' => 'Provides a widget for inline management (creation, modification, removal) of referenced entities. ',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'system (>7.14)',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/entity.inline_entity_form.inc',
            1 => 'includes/node.inline_entity_form.inc',
            2 => 'includes/taxonomy_term.inline_entity_form.inc',
            3 => 'includes/commerce_product.inline_entity_form.inc',
            4 => 'includes/commerce_line_item.inline_entity_form.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'inline_entity_form',
          'datestamp' => '1389971831',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'navigation404' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/navigation404/navigation404.module',
        'basename' => 'navigation404.module',
        'name' => 'navigation404',
        'info' => 
        array (
          'name' => '404 Navigation',
          'description' => 'On a 404 page, ensure navigation links are displayed properly.',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'navigation404',
          'datestamp' => '1294419091',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'flexslider_picture' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/picture/flexslider_picture/flexslider_picture.module',
        'basename' => 'flexslider_picture.module',
        'name' => 'flexslider_picture',
        'info' => 
        array (
          'name' => 'FlexSlider Picture',
          'description' => 'Integrates the Picture module with the FlexSlider module for a truly responsive slider.',
          'package' => 'Picture',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'picture',
            1 => 'flexslider (2.x)',
            2 => 'flexslider_fields (2.x)',
          ),
          'version' => '7.x-1.2',
          'project' => 'picture',
          'datestamp' => '1383130545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'picture' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/picture/picture.module',
        'basename' => 'picture.module',
        'name' => 'picture',
        'info' => 
        array (
          'name' => 'Picture',
          'description' => 'Picture element',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'breakpoints',
          ),
          'configure' => 'admin/config/media/picture',
          'package' => 'Picture',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'picture_wysiwyg.css',
            ),
          ),
          'version' => '7.x-1.2',
          'project' => 'picture',
          'datestamp' => '1383130545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'views_accordion' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_accordion/views_accordion.module',
        'basename' => 'views_accordion.module',
        'name' => 'views_accordion',
        'info' => 
        array (
          'name' => 'Views Accordion',
          'description' => 'Provides an accordion views display plugin.',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_accordion_style_plugin.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'views_accordion',
          'datestamp' => '1422545700',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'commerce_paypal_wps' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/wps/commerce_paypal_wps.module',
        'basename' => 'commerce_paypal_wps.module',
        'name' => 'commerce_paypal_wps',
        'info' => 
        array (
          'name' => 'PayPal WPS',
          'description' => 'Implements PayPal Website Payments Standard in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal_wpp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/wpp/commerce_paypal_wpp.module',
        'basename' => 'commerce_paypal_wpp.module',
        'name' => 'commerce_paypal_wpp',
        'info' => 
        array (
          'name' => 'PayPal WPP',
          'description' => 'Implements PayPal Website Payments Pro in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal_ec' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/ec/commerce_paypal_ec.module',
        'basename' => 'commerce_paypal_ec.module',
        'name' => 'commerce_paypal_ec',
        'info' => 
        array (
          'name' => 'PayPal Express Checkout',
          'description' => 'Implements PayPal Express Checkout in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_payflow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/payflow/commerce_payflow.module',
        'basename' => 'commerce_payflow.module',
        'name' => 'commerce_payflow',
        'info' => 
        array (
          'name' => 'PayPal Payments Advanced and Payflow Link',
          'description' => 'Implements PayPal Payments Advanced (U.S. only) and Payflow Link Hosted Checkout pages and Transparent Redirect.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/commerce_paypal.module',
        'basename' => 'commerce_paypal.module',
        'name' => 'commerce_paypal',
        'info' => 
        array (
          'name' => 'PayPal',
          'description' => 'Implements PayPal payment services for use with Drupal Commerce.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'autocomplete_deluxe' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/autocomplete_deluxe/autocomplete_deluxe.module',
        'basename' => 'autocomplete_deluxe.module',
        'name' => 'autocomplete_deluxe',
        'info' => 
        array (
          'name' => 'Autocomplete Deluxe',
          'description' => 'Enhanced autocomplete using Jquery UI autocomplete.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'autocomplete_deluxe.module',
          ),
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'autocomplete_deluxe',
          'datestamp' => '1375695669',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta3',
      ),
      'googleanalytics' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/google_analytics/googleanalytics.module',
        'basename' => 'googleanalytics.module',
        'name' => 'googleanalytics',
        'info' => 
        array (
          'name' => 'Google Analytics',
          'description' => 'Allows your site to be tracked by Google Analytics by adding a Javascript tracking code to every page.',
          'core' => '7.x',
          'package' => 'Statistics',
          'configure' => 'admin/config/system/googleanalytics',
          'files' => 
          array (
            0 => 'googleanalytics.test',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'version' => '7.x-1.4',
          'project' => 'google_analytics',
          'datestamp' => '1382021586',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7007',
        'version' => '7.x-1.4',
      ),
      'path_breadcrumbs_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs_i18n/path_breadcrumbs_i18n.module',
        'basename' => 'path_breadcrumbs_i18n.module',
        'name' => 'path_breadcrumbs_i18n',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs translation',
          'description' => 'Allows translating of breadcrumbs.',
          'core' => '7.x',
          'package' => 'Multilingual - Internationalization',
          'files' => 
          array (
            0 => 'path_breadcrumbs_i18n.inc',
          ),
          'dependencies' => 
          array (
            0 => 'path_breadcrumbs',
            1 => 'i18n_string',
            2 => 'i18n',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'path_breadcrumbs_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs_ui/path_breadcrumbs_ui.module',
        'basename' => 'path_breadcrumbs_ui.module',
        'name' => 'path_breadcrumbs_ui',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs UI',
          'description' => 'User interface for Path Breadcrumbs module',
          'core' => '7.x',
          'package' => 'Path Breadcrumbs',
          'configure' => 'admin/structure/path-breadcrumbs',
          'dependencies' => 
          array (
            0 => 'path_breadcrumbs',
          ),
          'files' => 
          array (
            0 => 'includes/path_breadcrumbs_ui.autocomplete.inc',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => '7302',
        'version' => '7.x-3.2',
      ),
      'path_breadcrumbs' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs.module',
        'basename' => 'path_breadcrumbs.module',
        'name' => 'path_breadcrumbs',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs',
          'description' => 'Allow to create custom breadcrumbs for all pages on the site using contexts.',
          'core' => '7.x',
          'package' => 'Path Breadcrumbs',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'entity',
            2 => 'entity_token',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => '7304',
        'version' => '7.x-3.2',
      ),
      'views_responsive_grid' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_responsive_grid/views_responsive_grid.module',
        'basename' => 'views_responsive_grid.module',
        'name' => 'views_responsive_grid',
        'info' => 
        array (
          'name' => 'Views Responsive Grid',
          'description' => 'Views plugin for displaying views content in a responsive grid.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_responsive_grid_plugin_style_responsive_grid.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'views_responsive_grid',
          'datestamp' => '1365191423',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'date_ical' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date_ical/date_ical.module',
        'basename' => 'date_ical.module',
        'name' => 'date_ical',
        'info' => 
        array (
          'name' => 'Date iCal',
          'description' => 'Enables export of iCal feeds using Views, and import of iCal feeds using Feeds.',
          'package' => 'Date/Time',
          'php' => '5.3',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views (>=7.x-3.5)',
            1 => 'date_views',
            2 => 'entity',
            3 => 'libraries (>=7.x-2.0)',
            4 => 'date',
            5 => 'date_api',
          ),
          'files' => 
          array (
            0 => 'includes/date_ical_plugin_row_ical_entity.inc',
            1 => 'includes/date_ical_plugin_row_ical_fields.inc',
            2 => 'includes/date_ical_plugin_style_ical_feed.inc',
            3 => 'includes/DateiCalFeedsParser.inc',
          ),
          'version' => '7.x-3.3',
          'project' => 'date_ical',
          'datestamp' => '1412727230',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.3',
      ),
      'scheduler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/scheduler/scheduler.module',
        'basename' => 'scheduler.module',
        'name' => 'scheduler',
        'info' => 
        array (
          'name' => 'Scheduler',
          'description' => 'This module allows nodes to be published and unpublished on specified dates and time.',
          'core' => '7.x',
          'configure' => 'admin/config/content/scheduler',
          'files' => 
          array (
            0 => 'scheduler.install',
            1 => 'scheduler.module',
            2 => 'scheduler.test',
            3 => 'scheduler.views.inc',
            4 => 'scheduler_handler_field_scheduler_countdown.inc',
            5 => 'tests/scheduler_api.test',
          ),
          'test_dependencies' => 
          array (
            0 => 'date',
          ),
          'version' => '7.x-1.3',
          'project' => 'scheduler',
          'datestamp' => '1415728082',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.3',
      ),
      'filefield_sources_plupload' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_sources_plupload/filefield_sources_plupload.module',
        'basename' => 'filefield_sources_plupload.module',
        'name' => 'filefield_sources_plupload',
        'info' => 
        array (
          'name' => 'FileField Sources Plupload',
          'description' => 'Extends File fields to allow multiple file uploads.',
          'dependencies' => 
          array (
            0 => 'filefield_sources',
            1 => 'plupload',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.1',
          'project' => 'filefield_sources_plupload',
          'datestamp' => '1377635510',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'commerce_product_pricing_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_pricing/commerce_product_pricing_ui.module',
        'basename' => 'commerce_product_pricing_ui.module',
        'name' => 'commerce_product_pricing_ui',
        'info' => 
        array (
          'name' => 'Product Pricing UI',
          'description' => 'Exposes a UI for managing product pricing rules and pre-calculation settings.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'rules_admin',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_price',
            4 => 'commerce_product_pricing',
            5 => 'commerce_product_reference',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/product-pricing',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_product_pricing' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_pricing/commerce_product_pricing.module',
        'basename' => 'commerce_product_pricing.module',
        'name' => 'commerce_product_pricing',
        'info' => 
        array (
          'name' => 'Product Pricing',
          'description' => 'Enables Rules based product sell price calculation for dynamic product pricing.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
            2 => 'commerce_product_reference',
            3 => 'entity',
            4 => 'rules',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_price' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/price/commerce_price.module',
        'basename' => 'commerce_price.module',
        'name' => 'commerce_price',
        'info' => 
        array (
          'name' => 'Price',
          'description' => 'Defines the price field and a price alteration system.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'commerce_price.rules.inc',
            1 => 'includes/views/handlers/commerce_price_handler_field_commerce_price.inc',
            2 => 'includes/views/handlers/commerce_price_handler_filter_commerce_price_amount.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_order_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/order/commerce_order_ui.module',
        'basename' => 'commerce_order_ui.module',
        'name' => 'commerce_order_ui',
        'info' => 
        array (
          'name' => 'Order UI',
          'description' => 'Exposes a default UI for Orders through order edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_line_item',
            4 => 'commerce_order',
            5 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/order',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_order_ui_handler_area_view_order_form.inc',
            1 => 'tests/commerce_order_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_order' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/order/commerce_order.module',
        'basename' => 'commerce_order.module',
        'name' => 'commerce_order',
        'info' => 
        array (
          'name' => 'Order',
          'description' => 'Defines the Order entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_customer',
            2 => 'commerce_line_item',
            3 => 'commerce_price',
            4 => 'entity',
            5 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_order.controller.inc',
            1 => 'includes/views/handlers/commerce_order_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_order_handler_area_order_total.inc',
            3 => 'includes/views/handlers/commerce_order_handler_argument_order_order_id.inc',
            4 => 'includes/views/handlers/commerce_order_handler_field_order.inc',
            5 => 'includes/views/handlers/commerce_order_handler_field_order_status.inc',
            6 => 'includes/views/handlers/commerce_order_handler_field_order_state.inc',
            7 => 'includes/views/handlers/commerce_order_handler_field_order_type.inc',
            8 => 'includes/views/handlers/commerce_order_handler_field_order_link.inc',
            9 => 'includes/views/handlers/commerce_order_handler_field_order_link_delete.inc',
            10 => 'includes/views/handlers/commerce_order_handler_field_order_link_edit.inc',
            11 => 'includes/views/handlers/commerce_order_handler_field_order_mail.inc',
            12 => 'includes/views/handlers/commerce_order_handler_field_order_operations.inc',
            13 => 'includes/views/handlers/commerce_order_handler_filter_order_status.inc',
            14 => 'includes/views/handlers/commerce_order_handler_filter_order_state.inc',
            15 => 'includes/views/handlers/commerce_order_handler_filter_order_type.inc',
            16 => 'includes/views/handlers/commerce_order_plugin_argument_validate_user.inc',
            17 => 'tests/commerce_order.rules.test',
            18 => 'tests/commerce_order.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7109',
        'version' => '7.x-1.11',
      ),
      'commerce_payment_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/modules/commerce_payment_example.module',
        'basename' => 'commerce_payment_example.module',
        'name' => 'commerce_payment_example',
        'info' => 
        array (
          'name' => 'Payment Method Example',
          'description' => 'Provides an example payment method for testing and development.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_payment',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/commerce_payment.module',
        'basename' => 'commerce_payment.module',
        'name' => 'commerce_payment',
        'info' => 
        array (
          'name' => 'Payment',
          'description' => 'Implement core payment features for Drupal commerce.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_order',
            2 => 'entity',
            3 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'commerce_payment.rules.inc',
            1 => 'includes/commerce_payment_transaction.controller.inc',
            2 => 'includes/views/handlers/commerce_payment_handler_area_totals.inc',
            3 => 'includes/views/handlers/commerce_payment_handler_field_amount.inc',
            4 => 'includes/views/handlers/commerce_payment_handler_field_currency_code.inc',
            5 => 'includes/views/handlers/commerce_payment_handler_field_message.inc',
            6 => 'includes/views/handlers/commerce_payment_handler_field_payment_method.inc',
            7 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_link.inc',
            8 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_link_delete.inc',
            9 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_operations.inc',
            10 => 'includes/views/handlers/commerce_payment_handler_field_status.inc',
            11 => 'includes/views/handlers/commerce_payment_handler_filter_payment_method.inc',
            12 => 'includes/views/handlers/commerce_payment_handler_filter_payment_transaction_status.inc',
            13 => 'includes/views/handlers/commerce_payment_handler_filter_currency_code.inc',
            14 => 'includes/views/handlers/commerce_payment_handler_field_balance.inc',
            15 => 'tests/commerce_payment.rules.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_payment_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/commerce_payment_ui.module',
        'basename' => 'commerce_payment_ui.module',
        'name' => 'commerce_payment_ui',
        'info' => 
        array (
          'name' => 'Payment UI',
          'description' => 'Exposes a default UI for payment method configuration and payment transaction administration.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'rules_admin',
            1 => 'commerce',
            2 => 'commerce_order',
            3 => 'commerce_order_ui',
            4 => 'commerce_payment',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/payment-methods',
          'files' => 
          array (
            0 => 'tests/commerce_payment_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_tax' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/tax/commerce_tax.module',
        'basename' => 'commerce_tax.module',
        'name' => 'commerce_tax',
        'info' => 
        array (
          'name' => 'Tax',
          'description' => 'Define tax rates and configure tax rules for applicability and display.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'commerce_price',
            3 => 'commerce_product_pricing',
            4 => 'entity',
            5 => 'rules',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_tax_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/tax/commerce_tax_ui.module',
        'basename' => 'commerce_tax_ui.module',
        'name' => 'commerce_tax_ui',
        'info' => 
        array (
          'name' => 'Tax UI',
          'description' => 'Provides a UI for creating simple tax types and rates.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_tax',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/taxes',
          'files' => 
          array (
            0 => 'tests/commerce_tax_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.11',
      ),
      'commerce_checkout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/checkout/commerce_checkout.module',
        'basename' => 'commerce_checkout.module',
        'name' => 'commerce_checkout',
        'info' => 
        array (
          'name' => 'Checkout',
          'description' => 'Enable checkout as a multi-step form with customizable checkout pages.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_order',
            3 => 'entity',
            4 => 'rules',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/checkout',
          'files' => 
          array (
            0 => 'tests/commerce_checkout.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7103',
        'version' => '7.x-1.11',
      ),
      'commerce_product_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product/commerce_product_ui.module',
        'basename' => 'commerce_product_ui.module',
        'name' => 'commerce_product_ui',
        'info' => 
        array (
          'name' => 'Product UI',
          'description' => 'Exposes a default UI for Products through product edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_product',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/products/types',
          'files' => 
          array (
            0 => 'tests/commerce_product_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_product' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product/commerce_product.module',
        'basename' => 'commerce_product.module',
        'name' => 'commerce_product',
        'info' => 
        array (
          'name' => 'Product',
          'description' => 'Defines the Product entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_product.controller.inc',
            1 => 'includes/views/handlers/commerce_product_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_product_handler_argument_product_id.inc',
            3 => 'includes/views/handlers/commerce_product_handler_field_product.inc',
            4 => 'includes/views/handlers/commerce_product_handler_field_product_type.inc',
            5 => 'includes/views/handlers/commerce_product_handler_field_product_link.inc',
            6 => 'includes/views/handlers/commerce_product_handler_field_product_link_delete.inc',
            7 => 'includes/views/handlers/commerce_product_handler_field_product_link_edit.inc',
            8 => 'includes/views/handlers/commerce_product_handler_field_product_operations.inc',
            9 => 'includes/views/handlers/commerce_product_handler_filter_product_type.inc',
            10 => 'includes/commerce_product.translation_handler.inc',
            11 => 'tests/commerce_product.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.11',
      ),
      'commerce_line_item_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/line_item/commerce_line_item_ui.module',
        'basename' => 'commerce_line_item_ui.module',
        'name' => 'commerce_line_item_ui',
        'info' => 
        array (
          'name' => 'Line Item UI',
          'description' => 'Exposes a default UI for Line Items through line item type forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_line_item',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/line-items',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_line_item' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/line_item/commerce_line_item.module',
        'basename' => 'commerce_line_item.module',
        'name' => 'commerce_line_item',
        'info' => 
        array (
          'name' => 'Line Item',
          'description' => 'Defines the Line Item entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
            2 => 'entity',
            3 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_line_item.controller.inc',
            1 => 'includes/views/handlers/commerce_line_item_handler_area_line_item_summary.inc',
            2 => 'includes/views/handlers/commerce_line_item_handler_argument_line_item_line_item_id.inc',
            3 => 'includes/views/handlers/commerce_line_item_handler_field_line_item_title.inc',
            4 => 'includes/views/handlers/commerce_line_item_handler_field_line_item_type.inc',
            5 => 'includes/views/handlers/commerce_line_item_handler_filter_line_item_type.inc',
            6 => 'includes/views/handlers/commerce_line_item_handler_field_edit_quantity.inc',
            7 => 'includes/views/handlers/commerce_line_item_handler_field_edit_delete.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.11',
      ),
      'commerce_customer_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/customer/commerce_customer_ui.module',
        'basename' => 'commerce_customer_ui.module',
        'name' => 'commerce_customer_ui',
        'info' => 
        array (
          'name' => 'Customer UI',
          'description' => 'Exposes a default UI for Customers through profile edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_customer',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/customer-profiles/types',
          'files' => 
          array (
            0 => 'tests/commerce_customer_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_customer' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/customer/commerce_customer.module',
        'basename' => 'commerce_customer.module',
        'name' => 'commerce_customer',
        'info' => 
        array (
          'name' => 'Customer',
          'description' => 'Defines the Customer entity with Address Field integration.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'addressfield',
            1 => 'commerce',
            2 => 'entity',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_customer_profile.controller.inc',
            1 => 'includes/views/handlers/commerce_customer_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile.inc',
            3 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link.inc',
            4 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link_delete.inc',
            5 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link_edit.inc',
            6 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_type.inc',
            7 => 'includes/views/handlers/commerce_customer_handler_filter_customer_profile_type.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_product_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_reference/commerce_product_reference.module',
        'basename' => 'commerce_product_reference.module',
        'name' => 'commerce_product_reference',
        'info' => 
        array (
          'name' => 'Product Reference',
          'description' => 'Defines a product reference field and default display formatters.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'commerce_price',
            3 => 'commerce_product',
            4 => 'entity',
            5 => 'options',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_product_reference_handler_filter_node_is_product_display.inc',
            1 => 'includes/views/handlers/commerce_product_reference_handler_filter_node_type.inc',
            2 => 'includes/views/handlers/commerce_product_reference_handler_filter_product_line_item_type.inc',
            3 => 'tests/commerce_product_reference.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_cart' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/cart/commerce_cart.module',
        'basename' => 'commerce_cart.module',
        'name' => 'commerce_cart',
        'info' => 
        array (
          'name' => 'Cart',
          'description' => 'Implements the shopping cart system and add to cart features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_checkout',
            2 => 'commerce_line_item',
            3 => 'commerce_order',
            4 => 'commerce_product',
            5 => 'commerce_product_pricing',
            6 => 'commerce_product_reference',
            7 => 'entity',
            8 => 'rules',
            9 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_cart_handler_field_add_to_cart_form.inc',
            1 => 'includes/views/handlers/commerce_cart_plugin_argument_default_current_cart_order_id.inc',
            2 => 'includes/views/handlers/commerce_cart_handler_area_empty_text.inc',
            3 => 'tests/commerce_cart.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/commerce_ui.module',
        'basename' => 'commerce_ui.module',
        'name' => 'commerce_ui',
        'info' => 
        array (
          'name' => 'Commerce UI',
          'description' => 'Defines menu items common to the various Drupal Commerce UI modules.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/commerce.module',
        'basename' => 'commerce.module',
        'name' => 'commerce',
        'info' => 
        array (
          'name' => 'Commerce',
          'description' => 'Defines features and functions common to the Commerce modules. Must be enabled to uninstall other Commerce modules.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'system',
            1 => 'entity',
            2 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/commerce_base.test',
            1 => 'includes/commerce.controller.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_no_payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_no_payment/commerce_no_payment.module',
        'basename' => 'commerce_no_payment.module',
        'name' => 'commerce_no_payment',
        'info' => 
        array (
          'name' => 'No Payment',
          'description' => 'Upgrades No Payment 7.x-1.x to <a href="http://drupal.org/project/payment">Payment\'s</a> Basic payment method.',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'payment_commerce',
            1 => 'paymentmethodbasic',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/CommerceNoPaymentUpgrade.test',
          ),
          'version' => '7.x-2.0',
          'project' => 'commerce_no_payment',
          'datestamp' => '1364753711',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.0',
      ),
      'imce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imce/imce.module',
        'basename' => 'imce.module',
        'name' => 'imce',
        'info' => 
        array (
          'name' => 'IMCE',
          'description' => 'An image/file uploader and browser supporting personal directories and user quota.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/imce',
          'version' => '7.x-1.9',
          'project' => 'imce',
          'datestamp' => '1400275428',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.9',
      ),
      'term_merge' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/term_merge/term_merge.module',
        'basename' => 'term_merge.module',
        'name' => 'term_merge',
        'info' => 
        array (
          'name' => 'Term Merge',
          'description' => 'This module allows you to merge multiple terms into one, while updating all fields referring to those terms to refer to the replacement term instead.',
          'package' => 'Taxonomy',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'term_merge.test',
          ),
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'entity',
          ),
          'version' => '7.x-1.2',
          'project' => 'term_merge',
          'datestamp' => '1421194982',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'menu_attributes' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_attributes/menu_attributes.module',
        'basename' => 'menu_attributes.module',
        'name' => 'menu_attributes',
        'info' => 
        array (
          'name' => 'Menu attributes',
          'description' => 'Allows administrators to specify custom attributes for menu items.',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/structure/menu/settings',
          'files' => 
          array (
            0 => 'menu_attributes.test',
          ),
          'version' => '7.x-1.0-rc3',
          'project' => 'menu_attributes',
          'datestamp' => '1413756231',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-rc3',
      ),
      'workflow_search_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_search_api/workflow_search_api.module',
        'basename' => 'workflow_search_api.module',
        'name' => 'workflow_search_api',
        'info' => 
        array (
          'name' => 'Workflow Search API',
          'description' => 'Adds workflow state information to Search API index',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'entity',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_revert' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_revert/workflow_revert.module',
        'basename' => 'workflow_revert.module',
        'name' => 'workflow_revert',
        'info' => 
        array (
          'name' => 'Workflow Revert',
          'description' => 'Adds an \'Revert\' link to the first workflow history row.',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_access' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_access/workflow_access.module',
        'basename' => 'workflow_access.module',
        'name' => 'workflow_access',
        'info' => 
        array (
          'name' => 'Workflow access',
          'description' => 'Content access control based on workflows and roles.',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_views/workflow_views.module',
        'basename' => 'workflow_views.module',
        'name' => 'workflow_views',
        'info' => 
        array (
          'name' => 'Workflow views',
          'description' => 'Provides views integration for workflows.',
          'package' => 'Workflow',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'includes/workflow_views.views.inc',
            1 => 'includes/workflow_views.views_default.inc',
            2 => 'includes/workflow_views_handler_filter_sid.inc',
            3 => 'includes/workflow_views_handler_field_sid.inc',
            4 => 'includes/workflow_views_handler_field_username.inc',
            5 => 'includes/workflow_views_handler_argument_state.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_rules' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_rules/workflow_rules.module',
        'basename' => 'workflow_rules.module',
        'name' => 'workflow_rules',
        'info' => 
        array (
          'name' => 'Workflow rules',
          'description' => 'Provides rules integration for workflows.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'rules',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_cleanup' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_cleanup/workflow_cleanup.module',
        'basename' => 'workflow_cleanup.module',
        'name' => 'workflow_cleanup',
        'info' => 
        array (
          'name' => 'Workflow Clean Up',
          'description' => 'Cleans up Workflow cruft',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_admin_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_admin_ui/workflow_admin_ui.module',
        'basename' => 'workflow_admin_ui.module',
        'name' => 'workflow_admin_ui',
        'info' => 
        array (
          'name' => 'Workflow UI',
          'description' => 'Provides administrative UI for workflow.',
          'package' => 'Workflow',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/workflow/workflow',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'workflow_admin_ui.css',
            ),
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.2',
      ),
      'workflow_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_actions/workflow_actions.module',
        'basename' => 'workflow_actions.module',
        'name' => 'workflow_actions',
        'info' => 
        array (
          'name' => 'Workflow actions and triggers',
          'description' => 'Provides actions and triggers for workflows.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'trigger',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_vbo' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_vbo/workflow_vbo.module',
        'basename' => 'workflow_vbo.module',
        'name' => 'workflow_vbo',
        'info' => 
        array (
          'name' => 'Workflow VBO',
          'description' => 'Provides workflow actions for VBO.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'views_bulk_operations',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow.module',
        'basename' => 'workflow.module',
        'name' => 'workflow',
        'info' => 
        array (
          'name' => 'Workflow',
          'description' => 'Allows the creation and assignment of arbitrary workflows to node types.',
          'package' => 'Workflow',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'workflow.pages.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.2',
      ),
      'field_group' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_group/field_group.module',
        'basename' => 'field_group.module',
        'name' => 'field_group',
        'info' => 
        array (
          'name' => 'Field Group',
          'description' => 'Provides the ability to group your fields on both form and display.',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'ctools',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/field_group.ui.test',
            1 => 'tests/field_group.display.test',
          ),
          'version' => '7.x-1.4',
          'project' => 'field_group',
          'datestamp' => '1401918529',
          'php' => '5.2.4',
        ),
        'schema_version' => '7007',
        'version' => '7.x-1.4',
      ),
      'geophp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geophp/geophp.module',
        'basename' => 'geophp.module',
        'name' => 'geophp',
        'info' => 
        array (
          'name' => 'geoPHP',
          'description' => 'Wraps the geoPHP library: advanced geometry operations in PHP',
          'core' => '7.x',
          'version' => '7.x-1.7',
          'project' => 'geophp',
          'datestamp' => '1352084822',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'adminrole' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/adminrole/adminrole.module',
        'basename' => 'adminrole.module',
        'name' => 'adminrole',
        'info' => 
        array (
          'name' => 'Admin Role',
          'description' => 'Automatically assign all permissions to an admin role.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'adminrole',
          'datestamp' => '1351352467',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'customer_profile_type_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/customer_profile_type_ui/customer_profile_type_ui.module',
        'basename' => 'customer_profile_type_ui.module',
        'name' => 'customer_profile_type_ui',
        'info' => 
        array (
          'name' => 'Customer Profile Type UI',
          'description' => 'Provides the ability to dynamically add new customer profile types for the commerce module. Also, defines new rules events and actions to disable customer profile panes.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_customer_ui',
          ),
          'files' => 
          array (
            0 => 'customer_profile_type_ui.module',
            1 => 'includes/customer_profile_type_ui.ui.inc',
            2 => 'includes/customer_profile_type_ui.checkout_pane.inc',
            3 => 'customer_profile_type_ui.rules.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'customer_profile_type_ui',
          'datestamp' => '1359936335',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'mailsystem' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailsystem/mailsystem.module',
        'basename' => 'mailsystem.module',
        'name' => 'mailsystem',
        'info' => 
        array (
          'package' => 'Mail',
          'name' => 'Mail System',
          'description' => 'Provides a user interface for per-module and site-wide mail_system selection.',
          'php' => '5.0',
          'core' => '7.x',
          'configure' => 'admin/config/system/mailsystem',
          'dependencies' => 
          array (
            0 => 'filter',
          ),
          'version' => '7.x-2.34',
          'project' => 'mailsystem',
          'datestamp' => '1334082653',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.34',
      ),
      'views_rss_dc' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/modules/views_rss_dc/views_rss_dc.module',
        'basename' => 'views_rss_dc.module',
        'name' => 'views_rss_dc',
        'info' => 
        array (
          'name' => 'Views RSS: DC Elements',
          'description' => 'Provides Dublin Core Metadata element set for Views RSS',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
          ),
          'core' => '7.x',
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-rc4',
      ),
      'views_rss_core' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/modules/views_rss_core/views_rss_core.module',
        'basename' => 'views_rss_core.module',
        'name' => 'views_rss_core',
        'info' => 
        array (
          'name' => 'Views RSS: Core Elements',
          'description' => 'Provides core XML channel and item elements for Views RSS.',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
          ),
          'core' => '7.x',
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0-rc4',
      ),
      'views_rss' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/views_rss.module',
        'basename' => 'views_rss.module',
        'name' => 'views_rss',
        'info' => 
        array (
          'name' => 'Views RSS',
          'description' => 'Provides a views plugin that allows fields to be included in RSS feeds created using the Views module.',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'views/views_rss_plugin_style_fields.inc',
            1 => 'views/views_rss_handler_field_user_mail.inc',
            2 => 'views/views_rss_handler_field_term_node_tid.inc',
          ),
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-rc4',
      ),
      'views_data_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_data_export/views_data_export.module',
        'basename' => 'views_data_export.module',
        'name' => 'views_data_export',
        'info' => 
        array (
          'name' => 'Views Data Export',
          'description' => 'Plugin to export views data into various file formats',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'plugins/views_data_export_plugin_display_export.inc',
            1 => 'plugins/views_data_export_plugin_style_export.inc',
            2 => 'plugins/views_data_export_plugin_style_export_csv.inc',
            3 => 'plugins/views_data_export_plugin_style_export_xml.inc',
            4 => 'tests/base.test',
            5 => 'tests/csv_export.test',
            6 => 'tests/doc_export.test',
            7 => 'tests/txt_export.test',
            8 => 'tests/xls_export.test',
            9 => 'tests/xml_export.test',
          ),
          'version' => '7.x-3.0-beta8',
          'project' => 'views_data_export',
          'datestamp' => '1409118835',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.0-beta8',
      ),
      'page_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/page_actions/page_actions.module',
        'basename' => 'page_actions.module',
        'name' => 'page_actions',
        'info' => 
        array (
          'name' => 'Page actions',
          'description' => 'Provides a unified method for making a page\'s form actions more accessible.',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'page_actions',
          'datestamp' => '1406912929',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'email' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/email/email.module',
        'basename' => 'email.module',
        'name' => 'email',
        'info' => 
        array (
          'name' => 'Email',
          'description' => 'Defines an email field type.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'email.migrate.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'email',
          'datestamp' => '1397134155',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'entityreference_behavior_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entityreference/examples/entityreference_behavior_example/entityreference_behavior_example.module',
        'basename' => 'entityreference_behavior_example.module',
        'name' => 'entityreference_behavior_example',
        'info' => 
        array (
          'name' => 'Entity Reference Behavior Example',
          'description' => 'Provides some example code for implementing Entity Reference behaviors.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entityreference',
          ),
          'version' => '7.x-1.1',
          'project' => 'entityreference',
          'datestamp' => '1384973110',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'entityreference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entityreference/entityreference.module',
        'basename' => 'entityreference.module',
        'name' => 'entityreference',
        'info' => 
        array (
          'name' => 'Entity Reference',
          'description' => 'Provides a field that can reference other entities.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'entityreference.migrate.inc',
            1 => 'plugins/selection/abstract.inc',
            2 => 'plugins/selection/views.inc',
            3 => 'plugins/behavior/abstract.inc',
            4 => 'views/entityreference_plugin_display.inc',
            5 => 'views/entityreference_plugin_style.inc',
            6 => 'views/entityreference_plugin_row_fields.inc',
            7 => 'tests/entityreference.handlers.test',
            8 => 'tests/entityreference.taxonomy.test',
            9 => 'tests/entityreference.admin.test',
            10 => 'tests/entityreference.feeds.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'entityreference',
          'datestamp' => '1384973110',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.1',
      ),
      'inline_conditions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/inline_conditions/inline_conditions.module',
        'basename' => 'inline_conditions.module',
        'name' => 'inline_conditions',
        'info' => 
        array (
          'name' => 'Inline Conditions',
          'description' => 'Manage frequently used conditions through an attached field.',
          'package' => 'Rules',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'inline_conditions',
          'datestamp' => '1405675129',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'metatag_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_devel/metatag_devel.module',
        'basename' => 'metatag_devel.module',
        'name' => 'metatag_devel',
        'info' => 
        array (
          'name' => 'Metatag:Devel',
          'description' => 'Provides development / debugging functionality for the Metatag module. Integrates with Devel Generate.',
          'package' => 'Development',
          'core' => '7.x',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_context' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_context/metatag_context.module',
        'basename' => 'metatag_context.module',
        'name' => 'metatag_context',
        'info' => 
        array (
          'name' => 'Metatag: Context',
          'description' => 'Assigned Metatag using Context definitions, allowing them to be assigned by path and other criteria.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'context',
            1 => 'metatag',
          ),
          'files' => 
          array (
            0 => 'metatag_context.test',
          ),
          'configure' => 'admin/config/search/metatags/context',
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_views/metatag_views.module',
        'basename' => 'metatag_views.module',
        'name' => 'metatag_views',
        'info' => 
        array (
          'name' => 'Metatag: Views',
          'description' => 'Provides Metatag integration within the Views interface.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'metatag_views_plugin_display_extender_metatags.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_facebook' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_facebook/metatag_facebook.module',
        'basename' => 'metatag_facebook.module',
        'name' => 'metatag_facebook',
        'info' => 
        array (
          'name' => 'Metatag: Facebook',
          'description' => 'Provides support for Facebook\'s custom meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_google_plus' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_google_plus/metatag_google_plus.module',
        'basename' => 'metatag_google_plus.module',
        'name' => 'metatag_google_plus',
        'info' => 
        array (
          'name' => 'Metatag: Google+',
          'description' => 'Provides support for Google+ \'itemscope\' meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'files' => 
          array (
            0 => 'metatag_google_plus.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_panels/metatag_panels.module',
        'basename' => 'metatag_panels.module',
        'name' => 'metatag_panels',
        'info' => 
        array (
          'name' => 'Metatag: Panels',
          'description' => 'Provides Metatag integration within the Panels interface.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'metatag',
            2 => 'panels',
            3 => 'token',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_twitter_cards' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_twitter_cards/metatag_twitter_cards.module',
        'basename' => 'metatag_twitter_cards.module',
        'name' => 'metatag_twitter_cards',
        'info' => 
        array (
          'name' => 'Metatag: Twitter Cards',
          'description' => 'Provides support for Twitter\'s Card meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_opengraph' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_opengraph/metatag_opengraph.module',
        'basename' => 'metatag_opengraph.module',
        'name' => 'metatag_opengraph',
        'info' => 
        array (
          'name' => 'Metatag:OpenGraph',
          'description' => 'Provides support for Open Graph Protocol meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.4',
      ),
      'metatag_dc' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_dc/metatag_dc.module',
        'basename' => 'metatag_dc.module',
        'name' => 'metatag_dc',
        'info' => 
        array (
          'name' => 'Metatag: Dublin Core',
          'description' => 'Provides the fifteen <a href="http://dublincore.org/documents/dces/">Dublin Core Metadata Element Set 1.1</a> meta tags from the <a href="http://dublincore.org/">Dublin Core Metadata Institute</a>.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag.module',
        'basename' => 'metatag.module',
        'name' => 'metatag',
        'info' => 
        array (
          'name' => 'Metatag',
          'description' => 'Adds support and an API to implement meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'system (>=7.28)',
            1 => 'ctools',
            2 => 'token',
          ),
          'configure' => 'admin/config/search/metatags',
          'files' => 
          array (
            0 => 'metatag.inc',
            1 => 'metatag.migrate.inc',
            2 => 'metatag.test',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => '7035',
        'version' => '7.x-1.4',
      ),
      'diff' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/diff/diff.module',
        'basename' => 'diff.module',
        'name' => 'diff',
        'info' => 
        array (
          'name' => 'Diff',
          'description' => 'Show differences between content revisions.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'DiffEngine.php',
          ),
          'version' => '7.x-3.2',
          'project' => 'diff',
          'datestamp' => '1352784357',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7305',
        'version' => '7.x-3.2',
      ),
      'ckeditor' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ckeditor/ckeditor.module',
        'basename' => 'ckeditor.module',
        'name' => 'ckeditor',
        'info' => 
        array (
          'name' => 'CKEditor',
          'description' => 'Enables CKEditor (WYSIWYG HTML editor) for use instead of plain text fields.',
          'core' => '7.x',
          'package' => 'User interface',
          'configure' => 'admin/config/content/ckeditor',
          'version' => '7.x-1.16',
          'project' => 'ckeditor',
          'datestamp' => '1413311935',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.x-1.16',
      ),
      'flag_bookmark' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag_bookmark/flag_bookmark.module',
        'basename' => 'flag_bookmark.module',
        'name' => 'flag_bookmark',
        'info' => 
        array (
          'name' => 'Flag Bookmark',
          'description' => 'Provides an example bookmark flag and supporting views.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'flag',
          ),
          'package' => 'Flags',
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.6',
      ),
      'flag' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag.module',
        'basename' => 'flag.module',
        'name' => 'flag',
        'info' => 
        array (
          'name' => 'Flag',
          'description' => 'Create customized flags that users can set on entities.',
          'core' => '7.x',
          'package' => 'Flags',
          'configure' => 'admin/structure/flags',
          'test_dependencies' => 
          array (
            0 => 'token',
            1 => 'rules',
          ),
          'files' => 
          array (
            0 => 'includes/flag/flag_flag.inc',
            1 => 'includes/flag/flag_entity.inc',
            2 => 'includes/flag/flag_node.inc',
            3 => 'includes/flag/flag_comment.inc',
            4 => 'includes/flag/flag_user.inc',
            5 => 'includes/flag.cookie_storage.inc',
            6 => 'includes/flag.entity.inc',
            7 => 'flag.rules.inc',
            8 => 'includes/views/flag_handler_argument_entity_id.inc',
            9 => 'includes/views/flag_handler_field_ops.inc',
            10 => 'includes/views/flag_handler_field_flagged.inc',
            11 => 'includes/views/flag_handler_filter_flagged.inc',
            12 => 'includes/views/flag_handler_sort_flagged.inc',
            13 => 'includes/views/flag_handler_relationships.inc',
            14 => 'includes/views/flag_plugin_argument_validate_flaggability.inc',
            15 => 'tests/flag.test',
          ),
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7306',
        'version' => '7.x-3.6',
      ),
      'flag_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag_actions.module',
        'basename' => 'flag_actions.module',
        'name' => 'flag_actions',
        'info' => 
        array (
          'name' => 'Flag actions',
          'description' => 'Execute actions on Flag events.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'flag',
          ),
          'package' => 'Flags',
          'configure' => 'admin/structure/flags/actions',
          'files' => 
          array (
            0 => 'flag.install',
            1 => 'flag_actions.module',
          ),
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'php' => '5.2.4',
        ),
        'schema_version' => '6200',
        'version' => '7.x-3.6',
      ),
      'userreference_url' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/userreference_url/userreference_url.module',
        'basename' => 'userreference_url.module',
        'name' => 'userreference_url',
        'info' => 
        array (
          'name' => 'User Reference URL Widget',
          'description' => 'Adds an additional widget to the User Reference field that prepopulates a reference by the URL.',
          'dependencies' => 
          array (
            0 => 'user_reference',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'userreference_url',
          'datestamp' => '1334698318',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'simplehtmldom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/simplehtmldom/simplehtmldom.module',
        'basename' => 'simplehtmldom.module',
        'name' => 'simplehtmldom',
        'info' => 
        array (
          'name' => 'simplehtmldom API',
          'description' => 'A wrapper module around the simplehtmldom PHP library at sourceforge.',
          'package' => 'Filter',
          'core' => '7.x',
          'version' => '7.x-1.12',
          'project' => 'simplehtmldom',
          'datestamp' => '1307968619',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.12',
      ),
      'missing_module' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/missing_module/missing_module.module',
        'basename' => 'missing_module.module',
        'name' => 'missing_module',
        'info' => 
        array (
          'name' => 'Missing Module',
          'description' => 'Find modules turned on in the database that aren\'t in file system.',
          'package' => 'Performance',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'missing_module',
          'datestamp' => '1375756337',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'mandrill_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_test/mandrill_test.module',
        'basename' => 'mandrill_test.module',
        'name' => 'mandrill_test',
        'info' => 
        array (
          'name' => 'Mandrill Test',
          'description' => 'Used to test Mandrill API hooks. DO NOT ENABLE UNLESS YOU KNOW WHAT YOU ARE DOING. This module is for the purpose of testing Mandrill only.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'dependencies' => 
          array (
            0 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'tests/mandrill_hooks.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_reports' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_reports/mandrill_reports.module',
        'basename' => 'mandrill_reports.module',
        'name' => 'mandrill_reports',
        'info' => 
        array (
          'name' => 'Mandrill Reports',
          'description' => 'Providing reporting on activity through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'dependencies' => 
          array (
            0 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'tests/mandrill_reports.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_activity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_activity/mandrill_activity.module',
        'basename' => 'mandrill_activity.module',
        'name' => 'mandrill_activity',
        'info' => 
        array (
          'name' => 'Mandrill Activity',
          'description' => 'View activity for an email address associated with any entity.',
          'package' => 'Mandrill',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mandrill',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill_activity.entity.inc',
            1 => 'lib/mandrill_activity.ui_controller.inc',
            2 => 'tests/mandrill_activity.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_template' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_template/mandrill_template.module',
        'basename' => 'mandrill_template.module',
        'name' => 'mandrill_template',
        'info' => 
        array (
          'name' => 'Mandrill Template',
          'description' => 'Use Mandrill templates for messages sent through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'configure' => 'admin/config/services/mandrill/templates',
          'dependencies' => 
          array (
            0 => 'entity (>=1.0)',
            1 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill_template_map.entity.inc',
            1 => 'lib/mandrill_template_map.ui_controller.inc',
            2 => 'tests/mandrill_template.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.1',
      ),
      'mandrill' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/mandrill.module',
        'basename' => 'mandrill.module',
        'name' => 'mandrill',
        'info' => 
        array (
          'name' => 'Mandrill',
          'description' => 'Allow for site emails to be sent through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'configure' => 'admin/config/services/mandrill',
          'dependencies' => 
          array (
            0 => 'mailsystem (>=2.x)',
            1 => 'libraries (>=2)',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill.mail.inc',
            1 => 'lib/mandrill.inc',
            2 => 'tests/includes/mandrill_test.inc',
            3 => 'tests/mandrill.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.x-2.1',
      ),
      'references_dialog' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references_dialog/references_dialog.module',
        'basename' => 'references_dialog.module',
        'name' => 'references_dialog',
        'info' => 
        array (
          'name' => 'References dialog',
          'description' => 'Enhances references fields by adding dialogs for adding and creating entities.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'views/references_dialog_plugin_display.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'references_dialog',
          'datestamp' => '1405042428',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'commerce_fees' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_fees/commerce_fees.module',
        'basename' => 'commerce_fees.module',
        'name' => 'commerce_fees',
        'info' => 
        array (
          'name' => 'Commerce Fees',
          'description' => 'Implement a system for add custom fees to an order.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'configure' => 'admin/commerce/config/fees',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'rules',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_fees',
          'datestamp' => '1391006907',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'search404' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search404/search404.module',
        'basename' => 'search404.module',
        'name' => 'search404',
        'info' => 
        array (
          'name' => 'Search 404',
          'description' => 'Automatically search for the keywords in URLs that result in 404 errors and show results instead of Page-Not-Found. ',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search404.module',
            1 => 'search404.install',
          ),
          'configure' => 'admin/config/search/search404',
          'version' => '7.x-1.3',
          'project' => 'search404',
          'datestamp' => '1370809553',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'smartcrop' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/smartcrop/smartcrop.module',
        'basename' => 'smartcrop.module',
        'name' => 'smartcrop',
        'info' => 
        array (
          'name' => 'SmartCrop',
          'description' => 'Crops low-entropy parts of the image.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'package' => 'Image',
          'files' => 
          array (
            0 => 'imageapi_gd.inc',
            1 => 'smartcrop.install',
            2 => 'smartcrop.module',
            3 => 'smartcrop.effects.inc',
            4 => 'tests/smartcrop.test',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'smartcrop',
          'datestamp' => '1291750843',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'geocoder_autocomplete' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geocoder_autocomplete/geocoder_autocomplete.module',
        'basename' => 'geocoder_autocomplete.module',
        'name' => 'geocoder_autocomplete',
        'info' => 
        array (
          'name' => 'Geocoder Autocomplete',
          'description' => 'Provides a text field widget with autocomplete via the Google Geocoding API.',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'geocoder_autocomplete',
          'datestamp' => '1380580681',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'select_or_other' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/select_or_other/select_or_other.module',
        'basename' => 'select_or_other.module',
        'name' => 'select_or_other',
        'info' => 
        array (
          'name' => 'Select (or other)',
          'description' => 'Provides a select box form element with additional option \'Other\' to give a textfield.',
          'core' => '7.x',
          'package' => 'Fields',
          'version' => '7.x-2.20',
          'project' => 'select_or_other',
          'datestamp' => '1378331245',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.20',
      ),
      'entity_bundle_plugin_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity_bundle_plugin/tests/entity_bundle_plugin_test.module',
        'basename' => 'entity_bundle_plugin_test.module',
        'name' => 'entity_bundle_plugin_test',
        'info' => 
        array (
          'name' => 'Entity Bundle Plugin Test',
          'description' => 'Test support and example module for Entity Bundle Plugin.',
          'package' => 'Entity',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity_bundle_plugin',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'entity_bundle_plugin',
          'datestamp' => '1372669557',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'entity_bundle_plugin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity_bundle_plugin/entity_bundle_plugin.module',
        'basename' => 'entity_bundle_plugin.module',
        'name' => 'entity_bundle_plugin',
        'info' => 
        array (
          'name' => 'Entity Bundle Plugin',
          'description' => 'Provide supports for entity types which bundles are plugin classes.',
          'package' => 'Entity',
          'core' => '7.x',
          'php' => '5.3',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'tests/entity_bundle_plugin_merge.test',
            1 => 'entity_bundle_plugin.controller.inc',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'entity_bundle_plugin',
          'datestamp' => '1372669557',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'commerce_rules_extra' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_rules_extra/commerce_rules_extra.module',
        'basename' => 'commerce_rules_extra.module',
        'name' => 'commerce_rules_extra',
        'info' => 
        array (
          'name' => 'Commerce Rules Extra',
          'description' => 'Extra Commerce Rules.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_line_item',
            1 => 'taxonomy',
            2 => 'rules',
          ),
          'version' => '7.x-2.1',
          'project' => 'commerce_rules_extra',
          'datestamp' => '1417509188',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'search_api_attachments_field_collections' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_attachments/contrib/search_api_attachments_field_collections/search_api_attachments_field_collections.module',
        'basename' => 'search_api_attachments_field_collections.module',
        'name' => 'search_api_attachments_field_collections',
        'info' => 
        array (
          'name' => 'Search API Attachments Field Collections',
          'description' => 'Extends the attachments indexing to file fields in the referenced field collection',
          'dependencies' => 
          array (
            0 => 'field_collection',
            1 => 'search_api_attachments',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/callback_attachments_field_collections_settings.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'search_api_attachments',
          'datestamp' => '1402272545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'search_api_attachments' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_attachments/search_api_attachments.module',
        'basename' => 'search_api_attachments.module',
        'name' => 'search_api_attachments',
        'info' => 
        array (
          'name' => 'Search API attachments',
          'description' => 'Provides a generic API for modules offering search capabilites.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'configure' => 'admin/config/search/search_api/attachments',
          'files' => 
          array (
            0 => 'includes/callback_attachments_settings.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'search_api_attachments',
          'datestamp' => '1402272545',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.4',
      ),
      'environment_indicator_variable' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/environment_indicator/environment_indicator_variable/environment_indicator_variable.module',
        'basename' => 'environment_indicator_variable.module',
        'name' => 'environment_indicator_variable',
        'info' => 
        array (
          'name' => 'Environment indicator variables',
          'description' => 'Adds a variable a realm for environment.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'variable_realm (7.x-2.x)',
            1 => 'variable_store (7.x-2.x)',
            2 => 'environment_indicator',
          ),
          'files' => 
          array (
            0 => 'class/EnvironmentVariableRealmController.php',
          ),
          'version' => '7.x-2.5',
          'project' => 'environment_indicator',
          'datestamp' => '1403432932',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.5',
      ),
      'environment_indicator' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/environment_indicator/environment_indicator.module',
        'basename' => 'environment_indicator.module',
        'name' => 'environment_indicator',
        'info' => 
        array (
          'name' => 'Environment indicator',
          'description' => 'Adds a color indicator for the different environments.',
          'core' => '7.x',
          'configure' => 'admin/config/development/environment-indicator',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'scripts' => 
          array (
            0 => 'tinycon.min.js',
            1 => 'environment_indicator.js',
            2 => 'color.js',
          ),
          'files' => 
          array (
            0 => 'plugins/export_ui/ctools_export_ui_environment_indicator.class.php',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'environment_indicator.css',
            ),
          ),
          'version' => '7.x-2.5',
          'project' => 'environment_indicator',
          'datestamp' => '1403432932',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.5',
      ),
      'rules_conditional' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules_conditional/rules_conditional.module',
        'basename' => 'rules_conditional.module',
        'name' => 'rules_conditional',
        'info' => 
        array (
          'name' => 'Conditional Rules',
          'description' => 'Provides conditional control elements within action containers.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'package' => 'Rules',
          'files' => 
          array (
            0 => 'includes/rules_conditional.core.inc',
            1 => 'includes/rules_conditional.plugin.inc',
            2 => 'includes/rules_conditional.ui.inc',
            3 => 'tests/rules_conditional.test',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'rules_conditional',
          'datestamp' => '1352752992',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'rules_batch' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/rules_batch/rules_batch.module',
        'basename' => 'rules_batch.module',
        'name' => 'rules_batch',
        'info' => 
        array (
          'name' => 'Rules Batch',
          'description' => 'Provides a batched rule loop component to rules.',
          'package' => 'Rules',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules (2.x)',
          ),
          'files' => 
          array (
            0 => 'rules_batch.rules.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'rules_batch',
          'datestamp' => '1355145533',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'exif_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/exif_custom/exif_custom.module',
        'basename' => 'exif_custom.module',
        'name' => 'exif_custom',
        'info' => 
        array (
          'name' => 'Custom EXIF mapping',
          'description' => 'Save EXIF data to custom fields',
          'package' => 'Image',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file_entity',
          ),
          'version' => '7.x-1.14',
          'project' => 'exif_custom',
          'datestamp' => '1390318106',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.14',
      ),
      'tagged_text' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/tagged_text/tagged_text.module',
        'basename' => 'tagged_text.module',
        'name' => 'tagged_text',
        'info' => 
        array (
          'name' => 'Tagged text',
          'description' => 'Generates tagged text files from content.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'includes/handlers/TaggedTextFieldHandler.inc',
            1 => 'includes/handlers/TaggedTextFieldHandlerMultiple.inc',
            2 => 'includes/handlers/TaggedTextFieldHandlerSingle.inc',
            3 => 'includes/handlers/TaggedTextFieldHandlerText.inc',
            4 => 'includes/handlers/TaggedTextFieldHandlerImage.inc',
            5 => 'includes/handlers/TaggedTextFieldHandlerReferenceNode.inc',
            6 => 'includes/views/tagged_text_handler_field_node_tagged_text_link.inc',
            7 => 'includes/views/tagged_text_plugin_display_archive.inc',
            8 => 'includes/views/tagged_text_plugin_style_archive.inc',
            9 => 'includes/views/tagged_text_plugin_row_node_view.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => NULL,
      ),
      'wordcounter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/wordcounter/wordcounter.module',
        'basename' => 'wordcounter.module',
        'name' => 'wordcounter',
        'info' => 
        array (
          'name' => 'Word Counter',
          'description' => 'Monitor number of words used in an entity and record the value.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => NULL,
      ),
      'gleaner_import' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_import/gleaner_import.module',
        'basename' => 'gleaner_import.module',
        'name' => 'gleaner_import',
        'info' => 
        array (
          'name' => 'Gleaner import',
          'description' => 'Import Gleaner articles.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'aiocalendar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/aiocalendar/aiocalendar.module',
        'basename' => 'aiocalendar.module',
        'name' => 'aiocalendar',
        'info' => 
        array (
          'name' => 'All-in-One Calendar',
          'description' => 'All-in-one calendar',
          'core' => '7.x',
          'package' => 'All-in-One',
          'version' => '7.x-1.0-alpha2',
          'project' => 'aiocalendar',
          'dependencies' => 
          array (
            0 => 'date_popup',
            1 => 'ds',
            2 => 'filefield_sources',
            3 => 'gleaner_features',
            4 => 'image',
            5 => 'taxonomy',
            6 => 'token',
          ),
          'datestamp' => '1383156310',
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'strongarm:strongarm:1',
              1 => 'views:views_default:3.0',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'field_date',
              1 => 'field_event_location',
              2 => 'field_event_type_event',
            ),
            'field_instance' => 
            array (
              0 => 'node-calendar_event-body',
              1 => 'node-calendar_event-field_date',
              2 => 'node-calendar_event-field_event_location',
              3 => 'node-calendar_event-field_event_type_event',
              4 => 'node-calendar_event-field_image',
            ),
            'node' => 
            array (
              0 => 'calendar_event',
            ),
            'taxonomy' => 
            array (
              0 => 'event_type',
            ),
            'user_permission' => 
            array (
              0 => 'create calendar_event content',
              1 => 'delete any calendar_event content',
              2 => 'delete own calendar_event content',
              3 => 'edit any calendar_event content',
              4 => 'edit own calendar_event content',
            ),
            'variable' => 
            array (
              0 => 'additional_settings__active_tab_calendar_event',
              1 => 'comment_anonymous_calendar_event',
              2 => 'comment_calendar_event',
              3 => 'comment_default_mode_calendar_event',
              4 => 'comment_default_per_page_calendar_event',
              5 => 'comment_form_location_calendar_event',
              6 => 'comment_preview_calendar_event',
              7 => 'comment_subject_field_calendar_event',
              8 => 'date_format_full',
              9 => 'field_bundle_settings_node__calendar_event',
              10 => 'menu_options_calendar_event',
              11 => 'menu_parent_calendar_event',
              12 => 'node_options_calendar_event',
              13 => 'node_preview_calendar_event',
              14 => 'node_submitted_calendar_event',
            ),
            'views_view' => 
            array (
              0 => 'events_calendar',
            ),
          ),
          'features_exclude' => 
          array (
            'taxonomy' => 
            array (
              'events' => 'events',
            ),
            'dependencies' => 
            array (
              'calendar' => 'calendar',
              'date' => 'date',
              'date_ical' => 'date_ical',
              'date_views' => 'date_views',
              'better_exposed_filters' => 'better_exposed_filters',
              'views' => 'views',
            ),
          ),
          'files' => 
          array (
            0 => 'aiocalendar.module',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha2',
      ),
      'gleaner_license' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_license/gleaner_license.module',
        'basename' => 'gleaner_license.module',
        'name' => 'gleaner_license',
        'info' => 
        array (
          'name' => 'Gleaner License',
          'description' => 'Provices a simple license type for Gleaner subscriptions.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
          ),
          'files' => 
          array (
            0 => 'plugins/license_type/GleanerLicense.class.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/print_export/print_export.module',
        'basename' => 'print_export.module',
        'name' => 'print_export',
        'info' => 
        array (
          'name' => 'Print export',
          'description' => 'Export tagged text and image files from content.',
          'core' => '7.x',
          'configure' => 'admin/config/content/print-export',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'image_archive' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/image_archive/image_archive.module',
        'basename' => 'image_archive.module',
        'name' => 'image_archive',
        'info' => 
        array (
          'name' => 'Image archive',
          'description' => 'Provides an archived download of all images tied to an entity.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'files' => 
          array (
            0 => 'views/image_archive_handler_field_image_archive_link.inc',
            1 => 'views/image_archive_handler_field_image_archive_link_node.inc',
            2 => 'views/image_archive_plugin_display_archive.inc',
            3 => 'views/image_archive_plugin_style_archive.inc',
            4 => 'views/image_archive_plugin_row_node_view.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_search/gleaner_search.module',
        'basename' => 'gleaner_search.module',
        'name' => 'gleaner_search',
        'info' => 
        array (
          'name' => 'Gleaner Search',
          'description' => 'Gleaner search features',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.0',
          'project' => 'gleaner_search',
          'dependencies' => 
          array (
            0 => 'better_exposed_filters',
            1 => 'current_search',
            2 => 'date_facets',
            3 => 'ds_extras',
            4 => 'elysia_cron',
            5 => 'entity',
            6 => 'facetapi',
            7 => 'facetapi_bonus',
            8 => 'facetapi_pretty_paths',
            9 => 'search_api',
            10 => 'search_api_facetapi',
            11 => 'search_api_index_status',
            12 => 'search_api_solr',
            13 => 'search_api_views',
            14 => 'views',
            15 => 'workflow_search_api',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'current_search:current_search:1',
              1 => 'ds:ds:1',
              2 => 'ds_extras:ds_extras:1',
              3 => 'elysia_cron:default_elysia_cron_rules:1',
              4 => 'facetapi:facetapi_defaults:1',
              5 => 'views:views_default:3.0',
            ),
            'current_search' => 
            array (
              0 => 'columns',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|search-page|default',
            ),
            'ds_fields' => 
            array (
              0 => 'gn_search',
              1 => 'gn_search_author',
              2 => 'gn_search_current',
              3 => 'gn_search_date_published',
              4 => 'gn_search_issue',
              5 => 'gn_search_news_section',
              6 => 'gn_search_node_type',
              7 => 'gn_search_section',
              8 => 'gn_search_tags',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|search-page|default',
            ),
            'ds_vd' => 
            array (
              0 => 'search-page',
            ),
            'ds_view_modes' => 
            array (
              0 => 'search_result',
            ),
            'elysia_cron' => 
            array (
              0 => 'search_api_cron',
              1 => 'search_api_solr_cron',
            ),
            'facetapi' => 
            array (
              0 => 'search_api@node_search::field_section',
              1 => 'search_api@node_search:block:field_news_section',
              2 => 'search_api@node_search:block:field_section',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'search_api_index' => 
            array (
              0 => 'node_search',
            ),
            'search_api_server' => 
            array (
              0 => 'solr_search',
            ),
            'views_view' => 
            array (
              0 => 'columns',
              1 => 'search',
              2 => 'search_more_like_this',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
              'ds' => 'ds',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'gn_commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gn_commerce/gn_commerce.module',
        'basename' => 'gn_commerce.module',
        'name' => 'gn_commerce',
        'info' => 
        array (
          'name' => 'Gleaner Commerce',
          'description' => 'Gleaner Commerce features for ads and subscriptions.',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.0-alpha17',
          'project' => 'gn_commerce',
          'dependencies' => 
          array (
            0 => 'commerce_custom_product',
            1 => 'commerce_discount',
            2 => 'commerce_features',
            3 => 'commerce_fees',
            4 => 'commerce_payment_example',
            5 => 'commerce_payment_ui',
            6 => 'commerce_product_attributes',
            7 => 'commerce_product_pricing_ui',
            8 => 'commerce_rules_extra',
            9 => 'commerce_wordcounter',
            10 => 'customer_profile_type_ui',
            11 => 'field_group',
            12 => 'file',
            13 => 'gleaner_features',
            14 => 'gleaner_license',
            15 => 'inline_entity_form',
            16 => 'scheduler',
          ),
          'features' => 
          array (
            'commerce_discount' => 
            array (
              0 => 'discount_npuc_member_discount',
            ),
            'commerce_line_item_type' => 
            array (
              0 => 'custom_ad',
              1 => 'gn_classified',
              2 => 'gn_email_ad',
            ),
            'commerce_product_type' => 
            array (
              0 => 'commerce_node_checkout',
              1 => 'product',
            ),
            'ctools' => 
            array (
              0 => 'ds:ds:1',
              1 => 'ds_extras:ds_extras:1',
              2 => 'field_group:field_group:1',
              3 => 'strongarm:strongarm:1',
              4 => 'views:views_default:3.0',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|classified-page|default',
              1 => 'node|gn_classified_ad|default',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|classified-page|default',
              1 => 'node|gn_classified_ad|default',
            ),
            'ds_vd' => 
            array (
              0 => 'classified-page',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'commerce_customer_address',
              1 => 'commerce_display_path',
              2 => 'commerce_license',
              3 => 'commerce_license_duration',
              4 => 'commerce_license_type',
              5 => 'commerce_price',
              6 => 'commerce_product',
              7 => 'commerce_total',
              8 => 'commerce_unit_price',
              9 => 'field_available_issues',
              10 => 'field_category',
              11 => 'field_design_fee',
              12 => 'field_design_fee2',
              13 => 'field_node_order',
              14 => 'field_online_early',
              15 => 'field_product',
              16 => 'field_special_requests',
              17 => 'field_upload_ad',
              18 => 'field_word_count',
              19 => 'field_word_counter',
            ),
            'field_group' => 
            array (
              0 => 'group_price|node|gn_classified_ad|form',
            ),
            'field_instance' => 
            array (
              0 => 'commerce_customer_profile-gn_mail_add-commerce_customer_address',
              1 => 'commerce_line_item-custom_ad-commerce_display_path',
              2 => 'commerce_line_item-custom_ad-commerce_product',
              3 => 'commerce_line_item-custom_ad-commerce_total',
              4 => 'commerce_line_item-custom_ad-commerce_unit_price',
              5 => 'commerce_line_item-custom_ad-field_available_issues',
              6 => 'commerce_line_item-custom_ad-field_design_fee2',
              7 => 'commerce_line_item-gn_classified-commerce_display_path',
              8 => 'commerce_line_item-gn_classified-commerce_product',
              9 => 'commerce_line_item-gn_classified-commerce_total',
              10 => 'commerce_line_item-gn_classified-commerce_unit_price',
              11 => 'commerce_line_item-gn_email_ad-commerce_display_path',
              12 => 'commerce_line_item-gn_email_ad-commerce_product',
              13 => 'commerce_line_item-gn_email_ad-commerce_total',
              14 => 'commerce_line_item-gn_email_ad-commerce_unit_price',
              15 => 'commerce_line_item-gn_online_ad-commerce_display_path',
              16 => 'commerce_line_item-gn_online_ad-commerce_product',
              17 => 'commerce_line_item-gn_online_ad-commerce_total',
              18 => 'commerce_line_item-gn_online_ad-commerce_unit_price',
              19 => 'commerce_line_item-gn_print_ad-commerce_display_path',
              20 => 'commerce_line_item-gn_print_ad-commerce_product',
              21 => 'commerce_line_item-gn_print_ad-commerce_total',
              22 => 'commerce_line_item-gn_print_ad-commerce_unit_price',
              23 => 'commerce_line_item-gn_print_ad-field_available_issues',
              24 => 'commerce_line_item-gn_print_ad-field_design_fee2',
              25 => 'commerce_line_item-gn_subscription-commerce_display_path',
              26 => 'commerce_line_item-gn_subscription-commerce_product',
              27 => 'commerce_line_item-gn_subscription-commerce_total',
              28 => 'commerce_line_item-gn_subscription-commerce_unit_price',
              29 => 'commerce_product-commerce_node_checkout-commerce_price',
              30 => 'commerce_product-product-commerce_price',
              31 => 'commerce_product-subscription-commerce_price',
              32 => 'node-gn_classified_ad-body',
              33 => 'node-gn_classified_ad-field_available_issues',
              34 => 'node-gn_classified_ad-field_category',
              35 => 'node-gn_classified_ad-field_node_order',
              36 => 'node-gn_classified_ad-field_online_early',
              37 => 'node-gn_classified_ad-field_special_requests',
              38 => 'node-gn_classified_ad-field_word_counter',
              39 => 'node-gn_email_ad-body',
              40 => 'node-gn_email_ad-field_product',
              41 => 'node-gn_online_ad-body',
              42 => 'node-gn_online_ad-field_product',
              43 => 'node-gn_print_ad-body',
              44 => 'node-gn_print_ad-field_product',
              45 => 'node-gn_subscription-body',
              46 => 'node-gn_subscription-field_product',
            ),
            'node' => 
            array (
              0 => 'gn_classified_ad',
            ),
            'rules_config' => 
            array (
              0 => 'rules_add_order_reference',
              1 => 'rules_check_billing_state',
              2 => 'rules_classified_delete_old',
              3 => 'rules_classified_evaluate_last_issue',
              4 => 'rules_classified_publish_check_current_issue',
              5 => 'rules_classified_straight_to_cart',
              6 => 'rules_design_fees',
              7 => 'rules_early_check_publish',
              8 => 'rules_early_check_schedule',
              9 => 'rules_gleaner_online_early_fee',
              10 => 'rules_gn_assign_issue_print_content',
              11 => 'rules_gn_assign_issue_print_content_list_item',
              12 => 'rules_gn_check_node_for_early_fee',
              13 => 'rules_gn_classified_quantity',
              14 => 'rules_gn_combine_classified',
              15 => 'rules_gn_combine_print',
              16 => 'rules_gn_commerce_hide_mailing',
              17 => 'rules_gn_commerce_node_checkout_reference',
              18 => 'rules_gn_commerce_online_early_check',
              19 => 'rules_gn_commerce_publish_classified',
              20 => 'rules_gn_subscription_pane',
              21 => 'rules_issue_based_ads',
              22 => 'rules_worksheets_send_to_print',
            ),
            'variable' => 
            array (
              0 => 'auto_entitylabel_node_gn_classified_ad',
              1 => 'auto_entitylabel_pattern_node_gn_classified_ad',
              2 => 'auto_entitylabel_php_node_gn_classified_ad',
              3 => 'field_bundle_settings_node__gn_classified_ad',
              4 => 'menu_options_gn_classified_ad',
              5 => 'menu_parent_gn_classified_ad',
              6 => 'node_options_gn_classified_ad',
              7 => 'node_preview_gn_classified_ad',
              8 => 'node_submitted_gn_classified_ad',
            ),
            'views_view' => 
            array (
              0 => 'classified',
              1 => 'classified_admin',
              2 => 'classified_customer',
              3 => 'upcoming_issues',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
              'number' => 'number',
              'commerce' => 'commerce',
              'commerce_checkout' => 'commerce_checkout',
              'commerce_line_item' => 'commerce_line_item',
              'commerce_node_checkout' => 'commerce_node_checkout',
              'commerce_order' => 'commerce_order',
              'commerce_price' => 'commerce_price',
              'commerce_product_reference' => 'commerce_product_reference',
              'entity' => 'entity',
              'entityreference' => 'entityreference',
              'features' => 'features',
              'options' => 'options',
              'rules' => 'rules',
              'taxonomy' => 'taxonomy',
              'wordcounter' => 'wordcounter',
              'views' => 'views',
            ),
            'field_base' => 
            array (
              'body' => 'body',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha17',
      ),
      'gleaner_copy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_copy/gleaner_copy.module',
        'basename' => 'gleaner_copy.module',
        'name' => 'gleaner_copy',
        'info' => 
        array (
          'name' => 'Gleaner copy',
          'description' => 'Provides a copy button above the Print Body field that copies various field values and inserts them in the Print Body field.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_features/gleaner_features.module',
        'basename' => 'gleaner_features.module',
        'name' => 'gleaner_features',
        'info' => 
        array (
          'name' => 'Gleaner Features',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.12',
          'project' => 'gleaner_features',
          'dependencies' => 
          array (
            0 => 'addressfield',
            1 => 'addthis',
            2 => 'better_exposed_filters',
            3 => 'calendar',
            4 => 'ckeditor',
            5 => 'computed_field',
            6 => 'custom_pub',
            7 => 'date_ical',
            8 => 'ds_extras',
            9 => 'elysia_cron',
            10 => 'email',
            11 => 'environment_indicator',
            12 => 'eva',
            13 => 'facetapi',
            14 => 'features',
            15 => 'field_default_token',
            16 => 'field_group',
            17 => 'file_entity_inline',
            18 => 'filefield_paths',
            19 => 'filefield_sources_plupload',
            20 => 'geocoder',
            21 => 'geocoder_autocomplete',
            22 => 'geofield',
            23 => 'gleaner_copy',
            24 => 'gleaner_custom',
            25 => 'image_archive',
            26 => 'link',
            27 => 'list',
            28 => 'mailchimp_lists',
            29 => 'mandrill_template',
            30 => 'menu',
            31 => 'metatag',
            32 => 'name',
            33 => 'node_reference',
            34 => 'number',
            35 => 'page_manager',
            36 => 'path_breadcrumbs',
            37 => 'picture',
            38 => 'search_api_solr',
            39 => 'select_or_other',
            40 => 'shs',
            41 => 'strongarm',
            42 => 'tagged_text',
            43 => 'taxonomy_display',
            44 => 'telephone',
            45 => 'text',
            46 => 'user_reference',
            47 => 'video_embed_field',
            48 => 'views_accordion',
            49 => 'views_content',
            50 => 'views_content_cache',
            51 => 'views_data_export',
            52 => 'views_litepager',
            53 => 'views_responsive_grid',
            54 => 'views_rss',
            55 => 'views_wookmark',
            56 => 'workflow_access',
            57 => 'workflow_rules',
          ),
          'features' => 
          array (
            'breakpoint_group' => 
            array (
              0 => 'gleanernow',
              1 => 'gn_article_gallery',
              2 => 'gn_author_cover_feature',
              3 => 'gn_author_photo',
              4 => 'gn_feature_hero',
              5 => 'gn_feature_hero_text',
              6 => 'gn_gallery_teaser',
              7 => 'gn_home_images',
              8 => 'gn_issue_feature',
              9 => 'gn_issue_teaser',
              10 => 'gn_section_feature',
              11 => 'gn_section_teaser',
              12 => 'gn_slideshow',
              13 => 'gn_slideshow_thumbs',
            ),
            'breakpoints' => 
            array (
              0 => 'breakpoints.theme.gleanernow.mobile',
              1 => 'breakpoints.theme.gleanernow.narrow',
              2 => 'breakpoints.theme.gleanernow.wide',
            ),
            'ckeditor_profile' => 
            array (
              0 => 'Advanced',
              1 => 'Full',
              2 => 'Print_Body',
              3 => 'Simple',
            ),
            'conditional_fields' => 
            array (
              0 => 'node:photo_contest_entry',
            ),
            'ctools' => 
            array (
              0 => 'breakpoints:default_breakpoint_group:1',
              1 => 'breakpoints:default_breakpoints:1',
              2 => 'ds:ds:1',
              3 => 'ds_extras:ds_extras:1',
              4 => 'elysia_cron:default_elysia_cron_rules:1',
              5 => 'environment_indicator:default_environment_indicator_environments:1',
              6 => 'facetapi:facetapi_defaults:1',
              7 => 'field_group:field_group:1',
              8 => 'file_entity:file_default_displays:1',
              9 => 'page_manager:pages_default:1',
              10 => 'path_breadcrumbs:path_breadcrumbs:1',
              11 => 'picture:default_picture_mapping:1',
              12 => 'strongarm:strongarm:1',
              13 => 'taxonomy_display:taxonomy_display:1',
              14 => 'video_embed_field:default_video_embed_styles:1',
              15 => 'views:views_default:3.0',
            ),
            'custom_pub' => 
            array (
              0 => 'print',
              1 => 'web',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|authors-page|default',
              1 => 'ds_views|columns-page|default',
              2 => 'ds_views|columns_content-page|default',
              3 => 'ds_views|events_calendar-page_1|default',
              4 => 'ds_views|events_calendar-page_4-fields|default',
              5 => 'ds_views|events_calendar-page_4|default',
              6 => 'ds_views|events_calendar-page|default',
              7 => 'ds_views|family-page_births|default',
              8 => 'ds_views|features-page|default',
              9 => 'ds_views|gn_gallery-page|default',
              10 => 'ds_views|home-page_1|default',
              11 => 'ds_views|home-page|default',
              12 => 'ds_views|inprint-page|default',
              13 => 'ds_views|media-page|default',
              14 => 'ds_views|news-page|default',
              15 => 'ds_views|quotes-page|default',
              16 => 'ds_views|taxonomy_term-page|default',
              17 => 'node|article|cover',
              18 => 'node|article|default',
              19 => 'node|article|full',
              20 => 'node|article|preview',
              21 => 'node|article|search_result',
              22 => 'node|article|section_feature',
              23 => 'node|article|tagged_text',
              24 => 'node|article|teaser',
              25 => 'node|author_profile|author_title',
              26 => 'node|author_profile|cover_feature',
              27 => 'node|author_profile|default',
              28 => 'node|author_profile|full',
              29 => 'node|author_profile|preview',
              30 => 'node|author_profile|search_result',
              31 => 'node|calendar_event|default',
              32 => 'node|featured_content|cover_feature',
              33 => 'node|featured_content|default',
              34 => 'node|feature|cover',
              35 => 'node|feature|cover_feature',
              36 => 'node|feature|default',
              37 => 'node|feature|full',
              38 => 'node|feature|search_result',
              39 => 'node|feature|section_feature',
              40 => 'node|feature|teaser',
              41 => 'node|image_gallery|default',
              42 => 'node|image_gallery|search_result',
              43 => 'node|image_gallery|teaser',
              44 => 'node|issue_event|default',
              45 => 'node|issue_pdf|default',
              46 => 'node|media|cover',
              47 => 'node|media|full',
              48 => 'node|media|rss',
              49 => 'node|media|teaser',
              50 => 'node|page|cover',
              51 => 'node|page|default',
              52 => 'node|photo_feature|cover',
              53 => 'node|photo_feature|default',
              54 => 'node|post|cover',
              55 => 'node|post|full',
              56 => 'node|printcontent|default',
              57 => 'node|printcontent|tagged_text',
              58 => 'node|quote|cover',
              59 => 'node|quote|full',
              60 => 'node|webform|default',
              61 => 'node|ws_anniversary|default',
              62 => 'node|ws_birthday|default',
              63 => 'taxonomy_term|issue|default',
              64 => 'taxonomy_term|issue|full',
              65 => 'taxonomy_term|issue|inprint_cover',
              66 => 'taxonomy_term|issue|inside_inprint',
              67 => 'taxonomy_term|issue|issue_name',
              68 => 'user|user|full',
            ),
            'ds_fields' => 
            array (
              0 => 'author_profile_linked_title',
              1 => 'content_by_author',
              2 => 'gleaner_address',
              3 => 'gn_addthis',
              4 => 'gn_author_follow',
              5 => 'gn_comments',
              6 => 'gn_issue_date',
              7 => 'gn_issue_date_link',
              8 => 'gn_issue_description_title',
              9 => 'gn_issue_embed',
              10 => 'gn_issue_features',
              11 => 'gn_issue_image',
              12 => 'gn_issue_menu',
              13 => 'gn_issue_title',
              14 => 'gn_issue_viewer_link',
              15 => 'gn_messages',
              16 => 'gn_mini_calendar',
              17 => 'gn_more_events',
              18 => 'gn_node_meta',
              19 => 'gn_node_meta_author',
              20 => 'gn_node_meta_date',
              21 => 'gn_quote_citation',
              22 => 'gn_quote_citation_home',
              23 => 'gn_search_link',
              24 => 'gn_section_readmore',
              25 => 'gn_section_teaser_image',
              26 => 'gn_tabs',
              27 => 'photo_of_the_week_title',
              28 => 'staff_list',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|authors-page|default',
              1 => 'ds_views|columns-page|default',
              2 => 'ds_views|columns_content-page|default',
              3 => 'ds_views|events_calendar-page_1|default',
              4 => 'ds_views|events_calendar-page_4-fields|default',
              5 => 'ds_views|events_calendar-page_4|default',
              6 => 'ds_views|events_calendar-page|default',
              7 => 'ds_views|features-page|default',
              8 => 'ds_views|gn_gallery-page|default',
              9 => 'ds_views|home-page_1|default',
              10 => 'ds_views|home-page|default',
              11 => 'ds_views|inprint-page|default',
              12 => 'ds_views|media-page|default',
              13 => 'ds_views|news-page|default',
              14 => 'ds_views|quotes-page|default',
              15 => 'ds_views|taxonomy_term-page|default',
              16 => 'file|image|preview',
              17 => 'node|article|cover',
              18 => 'node|article|default',
              19 => 'node|article|demo',
              20 => 'node|article|form',
              21 => 'node|article|full',
              22 => 'node|article|preview',
              23 => 'node|article|print_version',
              24 => 'node|article|search_result',
              25 => 'node|article|section_feature',
              26 => 'node|article|tagged_text',
              27 => 'node|article|teaser',
              28 => 'node|author_profile|author_title',
              29 => 'node|author_profile|cover_feature',
              30 => 'node|author_profile|default',
              31 => 'node|author_profile|form',
              32 => 'node|author_profile|full',
              33 => 'node|author_profile|preview',
              34 => 'node|author_profile|search_result',
              35 => 'node|calendar_event|default',
              36 => 'node|featured_content|cover_feature',
              37 => 'node|featured_content|default',
              38 => 'node|feature|cover',
              39 => 'node|feature|cover_feature',
              40 => 'node|feature|default',
              41 => 'node|feature|form',
              42 => 'node|feature|full',
              43 => 'node|feature|inprint_cover',
              44 => 'node|feature|search_result',
              45 => 'node|feature|section_feature',
              46 => 'node|feature|teaser',
              47 => 'node|image_gallery|default',
              48 => 'node|image_gallery|search_result',
              49 => 'node|image_gallery|teaser',
              50 => 'node|issue_event|default',
              51 => 'node|issue_pdf|default',
              52 => 'node|media|cover',
              53 => 'node|media|full',
              54 => 'node|media|rss',
              55 => 'node|media|teaser',
              56 => 'node|page|cover',
              57 => 'node|page|default',
              58 => 'node|photo_feature|cover',
              59 => 'node|photo_feature|default',
              60 => 'node|post|cover',
              61 => 'node|post|full',
              62 => 'node|printcontent|default',
              63 => 'node|printcontent|form',
              64 => 'node|printcontent|tagged_text',
              65 => 'node|quote|cover',
              66 => 'node|quote|full',
              67 => 'node|quote|teaser',
              68 => 'node|sidebar|default',
              69 => 'node|sidebar|form',
              70 => 'node|webform|default',
              71 => 'node|ws_anniversary|form',
              72 => 'taxonomy_term|issue|default',
              73 => 'taxonomy_term|issue|full',
              74 => 'taxonomy_term|issue|inprint_cover',
              75 => 'taxonomy_term|issue|inside_inprint',
              76 => 'taxonomy_term|issue|issue_name',
              77 => 'user|user|full',
            ),
            'ds_vd' => 
            array (
              0 => 'authors-page',
              1 => 'columns-page',
              2 => 'columns_content-page',
              3 => 'events_calendar-page',
              4 => 'events_calendar-page_1',
              5 => 'events_calendar-page_4',
              6 => 'events_calendar-page_4-fields',
              7 => 'family-page_births',
              8 => 'features-page',
              9 => 'gn_gallery-page',
              10 => 'home-page',
              11 => 'home-page_1',
              12 => 'inprint-page',
              13 => 'media-page',
              14 => 'news-page',
              15 => 'quotes-page',
              16 => 'taxonomy_term-page',
            ),
            'ds_view_modes' => 
            array (
              0 => 'author_feature',
              1 => 'author_title',
              2 => 'cover',
              3 => 'cover_feature',
              4 => 'diff_standard',
              5 => 'inprint_cover',
              6 => 'inside_inprint',
              7 => 'issue_name',
              8 => 'preview',
              9 => 'print_version',
              10 => 'section_feature',
              11 => 'social_updates',
            ),
            'elysia_cron' => 
            array (
              0 => ':default',
              1 => 'acquia_agent_cron',
              2 => 'acquia_spi_cron',
              3 => 'advagg_cron',
              4 => 'backup_migrate_cron',
              5 => 'ctools_cron',
              6 => 'dblog_cron',
              7 => 'field_cron',
              8 => 'googleanalytics_cron',
              9 => 'mailchimp_lists_cron',
              10 => 'masquerade_cron',
              11 => 'node_cron',
              12 => 'redirect_cron',
              13 => 'rules_cron',
              14 => 'rules_scheduler_cron',
              15 => 'scheduler_cron',
              16 => 'search_cron',
              17 => 'system_cron',
              18 => 'themekey_cron',
              19 => 'trigger_cron',
              20 => 'update_cron',
              21 => 'user_import_cron',
              22 => 'views_bulk_operations_cron',
              23 => 'workflow_cron',
              24 => 'xmlsitemap_cron',
              25 => 'xmlsitemap_engines_cron',
              26 => 'xmlsitemap_menu_cron',
              27 => 'xmlsitemap_node_cron',
              28 => 'xmlsitemap_taxonomy_cron',
            ),
            'environment_indicator_environment' => 
            array (
              0 => 'dev_gleanernow_com',
              1 => 'local_development',
              2 => 'test_gleanernow_com',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'body',
              1 => 'field_ad_close_date',
              2 => 'field_anniversary_number',
              3 => 'field_author',
              4 => 'field_author_legacy',
              5 => 'field_baby_father',
              6 => 'field_baby_mother',
              7 => 'field_birth_date',
              8 => 'field_birth_location',
              9 => 'field_birthday_number',
              10 => 'field_body_print',
              11 => 'field_brides_name',
              12 => 'field_caption',
              13 => 'field_celebration_date',
              14 => 'field_celebration_location',
              15 => 'field_celebration_type',
              16 => 'field_compiled_annoucement',
              17 => 'field_couples_name',
              18 => 'field_couples_new_location',
              19 => 'field_credit',
              20 => 'field_date_birth',
              21 => 'field_date_death',
              22 => 'field_date_month',
              23 => 'field_death_age',
              24 => 'field_death_location',
              25 => 'field_description',
              26 => 'field_design_notes',
              27 => 'field_dropbox_link',
              28 => 'field_email',
              29 => 'field_facebook',
              30 => 'field_facebook_social',
              31 => 'field_family',
              32 => 'field_file',
              33 => 'field_full_name',
              34 => 'field_gender',
              35 => 'field_google_social',
              36 => 'field_gplus',
              37 => 'field_grandchildren',
              38 => 'field_great_grandchildren',
              39 => 'field_great_great_grandchildren',
              40 => 'field_grooms_father',
              41 => 'field_grooms_mother',
              42 => 'field_grooms_name',
              43 => 'field_grooms_stepfather',
              44 => 'field_grooms_stepmother',
              45 => 'field_hero_image',
              46 => 'field_hero_title_image',
              47 => 'field_image',
              48 => 'field_image_location',
              49 => 'field_issue',
              50 => 'field_latlon',
              51 => 'field_life_summary',
              52 => 'field_link',
              53 => 'field_link_to',
              54 => 'field_location',
              55 => 'field_maiden_name',
              56 => 'field_married',
              57 => 'field_married_bride',
              58 => 'field_married_groom',
              59 => 'field_mini_summary',
              60 => 'field_name',
              61 => 'field_news_section',
              62 => 'field_notes',
              63 => 'field_number',
              64 => 'field_organization',
              65 => 'field_pdf',
              66 => 'field_phone',
              67 => 'field_photog_location',
              68 => 'field_previous_married_name',
              69 => 'field_priority',
              70 => 'field_published',
              71 => 'field_pullquote',
              72 => 'field_quote_type',
              73 => 'field_real_name',
              74 => 'field_references',
              75 => 'field_related_content',
              76 => 'field_section',
              77 => 'field_section_editor',
              78 => 'field_share',
              79 => 'field_short_title',
              80 => 'field_sidebar',
              81 => 'field_speaker',
              82 => 'field_step_grandchildren',
              83 => 'field_step_great_grandchildren',
              84 => 'field_submission_type',
              85 => 'field_subtitle',
              86 => 'field_taxo_tags',
              87 => 'field_title',
              88 => 'field_title_or_relationship',
              89 => 'field_twitter',
              90 => 'field_twitter_social',
              91 => 'field_user_account',
              92 => 'field_video_embed',
              93 => 'field_volume_id',
              94 => 'field_website',
              95 => 'field_wedding_date',
              96 => 'field_wedding_location',
              97 => 'field_ws_bride_father',
              98 => 'field_ws_bride_mother',
              99 => 'field_ws_bride_stepfather',
              100 => 'field_ws_bride_stepmother',
              101 => 'field_ws_name',
              102 => 'field_ws_photo',
            ),
            'field_group' => 
            array (
              0 => 'group_author_meta|node|author_profile|full',
              1 => 'group_brides_parents|node|ws_wedding|form',
              2 => 'group_content|node|featured_content|form',
              3 => 'group_family_information|node|ws_anniversary|form',
              4 => 'group_family_information|node|ws_birthday|form',
              5 => 'group_family|node|ws_obituary|default',
              6 => 'group_grooms_parents|node|ws_wedding|form',
              7 => 'group_header|node|feature|default',
              8 => 'group_hero_graphics|node|feature|form',
              9 => 'group_image_gallery|node|article|full',
              10 => 'group_image_gallery|node|feature|default',
              11 => 'group_image_gallery|node|feature|full',
              12 => 'group_image_gallery|node|post|full',
              13 => 'group_imagegallery_slideshow|node|image_gallery|default',
              14 => 'group_meta|node|article|form',
              15 => 'group_meta|node|feature|form',
              16 => 'group_number_of|node|ws_anniversary|form',
              17 => 'group_number_of|node|ws_birthday|form',
              18 => 'group_number_of|node|ws_obituary|default',
              19 => 'group_number_of|node|ws_obituary|form',
              20 => 'group_parents|node|ws_wedding|form',
              21 => 'group_photog_info|node|photo_contest_entry|form',
              22 => 'group_printfields|node|article|form',
              23 => 'group_pub_settings_2|node|article|form',
              24 => 'group_pub_settings_2|node|feature|form',
              25 => 'group_pub_settings|node|article|form',
              26 => 'group_pub_settings|node|feature|form',
              27 => 'group_publication|node|article|form',
              28 => 'group_publication|node|feature|form',
              29 => 'group_quote|node|quote|cover',
              30 => 'group_quote|node|quote|full',
              31 => 'group_quote|node|quote|teaser',
              32 => 'group_socialupdates|node|post|form',
              33 => 'group_submitted_by|node|ws_anniversary|form',
              34 => 'group_submitted_by|node|ws_birthday|form',
              35 => 'group_submitted_by|node|ws_birth|form',
              36 => 'group_submitted_by|node|ws_obituary|default',
              37 => 'group_submitted_by|node|ws_obituary|form',
              38 => 'group_submitted_by|node|ws_wedding|form',
              39 => 'group_survivors|node|ws_obituary|form',
            ),
            'field_instance' => 
            array (
              0 => 'file-image-field_caption',
              1 => 'file-image-field_credit',
              2 => 'file-image-field_priority',
              3 => 'file-image-field_published',
              4 => 'node-ad_reference_form-body',
              5 => 'node-article-body',
              6 => 'node-article-field_author',
              7 => 'node-article-field_author_legacy',
              8 => 'node-article-field_body_print',
              9 => 'node-article-field_design_notes',
              10 => 'node-article-field_image',
              11 => 'node-article-field_issue',
              12 => 'node-article-field_latlon',
              13 => 'node-article-field_location',
              14 => 'node-article-field_mini_summary',
              15 => 'node-article-field_news_section',
              16 => 'node-article-field_priority',
              17 => 'node-article-field_pullquote',
              18 => 'node-article-field_related_content',
              19 => 'node-article-field_section',
              20 => 'node-article-field_share',
              21 => 'node-article-field_short_title',
              22 => 'node-article-field_taxo_tags',
              23 => 'node-author_profile-body',
              24 => 'node-author_profile-field_facebook',
              25 => 'node-author_profile-field_gplus',
              26 => 'node-author_profile-field_image',
              27 => 'node-author_profile-field_name',
              28 => 'node-author_profile-field_organization',
              29 => 'node-author_profile-field_title',
              30 => 'node-author_profile-field_twitter',
              31 => 'node-author_profile-field_user_account',
              32 => 'node-author_profile-field_website',
              33 => 'node-feature-body',
              34 => 'node-feature-field_author',
              35 => 'node-feature-field_author_legacy',
              36 => 'node-feature-field_body_print',
              37 => 'node-feature-field_design_notes',
              38 => 'node-feature-field_hero_image',
              39 => 'node-feature-field_hero_title_image',
              40 => 'node-feature-field_image',
              41 => 'node-feature-field_issue',
              42 => 'node-feature-field_latlon',
              43 => 'node-feature-field_location',
              44 => 'node-feature-field_mini_summary',
              45 => 'node-feature-field_news_section',
              46 => 'node-feature-field_priority',
              47 => 'node-feature-field_pullquote',
              48 => 'node-feature-field_related_content',
              49 => 'node-feature-field_section',
              50 => 'node-feature-field_short_title',
              51 => 'node-feature-field_sidebar',
              52 => 'node-feature-field_subtitle',
              53 => 'node-feature-field_taxo_tags',
              54 => 'node-featured_content-field_author',
              55 => 'node-featured_content-field_hero_image',
              56 => 'node-featured_content-field_link',
              57 => 'node-featured_content-field_link_to',
              58 => 'node-image_gallery-body',
              59 => 'node-image_gallery-field_image',
              60 => 'node-image_gallery-field_news_section',
              61 => 'node-image_gallery-field_taxo_tags',
              62 => 'node-issue_pdf-body',
              63 => 'node-issue_pdf-field_issue',
              64 => 'node-issue_pdf-field_link',
              65 => 'node-media-body',
              66 => 'node-media-field_author',
              67 => 'node-media-field_related_content',
              68 => 'node-media-field_taxo_tags',
              69 => 'node-media-field_video_embed',
              70 => 'node-page-body',
              71 => 'node-page-field_file',
              72 => 'node-photo_contest_entry-field_dropbox_link',
              73 => 'node-photo_contest_entry-field_email',
              74 => 'node-photo_contest_entry-field_image',
              75 => 'node-photo_contest_entry-field_name',
              76 => 'node-photo_contest_entry-field_phone',
              77 => 'node-photo_contest_entry-field_photog_location',
              78 => 'node-photo_contest_entry-field_submission_type',
              79 => 'node-photo_contest_entry-field_website',
              80 => 'node-photo_feature-field_image',
              81 => 'node-photo_feature-field_link',
              82 => 'node-post-body',
              83 => 'node-post-field_author',
              84 => 'node-post-field_body_print',
              85 => 'node-post-field_facebook_social',
              86 => 'node-post-field_google_social',
              87 => 'node-post-field_image',
              88 => 'node-post-field_issue',
              89 => 'node-post-field_taxo_tags',
              90 => 'node-post-field_twitter_social',
              91 => 'node-printcontent-body',
              92 => 'node-printcontent-field_body_print',
              93 => 'node-printcontent-field_design_notes',
              94 => 'node-printcontent-field_image',
              95 => 'node-printcontent-field_issue',
              96 => 'node-printcontent-field_priority',
              97 => 'node-printcontent-field_references',
              98 => 'node-printcontent-field_section',
              99 => 'node-quote-body',
              100 => 'node-quote-field_description',
              101 => 'node-quote-field_quote_type',
              102 => 'node-quote-field_speaker',
              103 => 'node-quote-field_taxo_tags',
              104 => 'node-quote-field_website',
              105 => 'node-sidebar-body',
              106 => 'node-sidebar-field_body_print',
              107 => 'node-sidebar-field_image',
              108 => 'node-sidebar-field_priority',
              109 => 'node-sidebar-field_subtitle',
              110 => 'node-webform-body',
              111 => 'node-ws_anniversary-field_anniversary_number',
              112 => 'node-ws_anniversary-field_brides_name',
              113 => 'node-ws_anniversary-field_celebration_date',
              114 => 'node-ws_anniversary-field_celebration_location',
              115 => 'node-ws_anniversary-field_celebration_type',
              116 => 'node-ws_anniversary-field_compiled_annoucement',
              117 => 'node-ws_anniversary-field_couples_name',
              118 => 'node-ws_anniversary-field_email',
              119 => 'node-ws_anniversary-field_family',
              120 => 'node-ws_anniversary-field_grandchildren',
              121 => 'node-ws_anniversary-field_great_grandchildren',
              122 => 'node-ws_anniversary-field_great_great_grandchildren',
              123 => 'node-ws_anniversary-field_grooms_name',
              124 => 'node-ws_anniversary-field_issue',
              125 => 'node-ws_anniversary-field_life_summary',
              126 => 'node-ws_anniversary-field_notes',
              127 => 'node-ws_anniversary-field_phone',
              128 => 'node-ws_anniversary-field_step_grandchildren',
              129 => 'node-ws_anniversary-field_step_great_grandchildren',
              130 => 'node-ws_anniversary-field_title_or_relationship',
              131 => 'node-ws_anniversary-field_wedding_date',
              132 => 'node-ws_anniversary-field_wedding_location',
              133 => 'node-ws_anniversary-field_ws_name',
              134 => 'node-ws_anniversary-field_ws_photo',
              135 => 'node-ws_birth-field_baby_father',
              136 => 'node-ws_birth-field_baby_mother',
              137 => 'node-ws_birth-field_birth_location',
              138 => 'node-ws_birth-field_compiled_annoucement',
              139 => 'node-ws_birth-field_date_birth',
              140 => 'node-ws_birth-field_email',
              141 => 'node-ws_birth-field_full_name',
              142 => 'node-ws_birth-field_issue',
              143 => 'node-ws_birth-field_married',
              144 => 'node-ws_birth-field_notes',
              145 => 'node-ws_birth-field_phone',
              146 => 'node-ws_birth-field_title_or_relationship',
              147 => 'node-ws_birth-field_ws_name',
              148 => 'node-ws_birthday-field_birth_date',
              149 => 'node-ws_birthday-field_birth_location',
              150 => 'node-ws_birthday-field_birthday_number',
              151 => 'node-ws_birthday-field_celebration_date',
              152 => 'node-ws_birthday-field_celebration_location',
              153 => 'node-ws_birthday-field_celebration_type',
              154 => 'node-ws_birthday-field_compiled_annoucement',
              155 => 'node-ws_birthday-field_email',
              156 => 'node-ws_birthday-field_family',
              157 => 'node-ws_birthday-field_full_name',
              158 => 'node-ws_birthday-field_grandchildren',
              159 => 'node-ws_birthday-field_great_grandchildren',
              160 => 'node-ws_birthday-field_great_great_grandchildren',
              161 => 'node-ws_birthday-field_issue',
              162 => 'node-ws_birthday-field_life_summary',
              163 => 'node-ws_birthday-field_maiden_name',
              164 => 'node-ws_birthday-field_notes',
              165 => 'node-ws_birthday-field_phone',
              166 => 'node-ws_birthday-field_previous_married_name',
              167 => 'node-ws_birthday-field_step_grandchildren',
              168 => 'node-ws_birthday-field_step_great_grandchildren',
              169 => 'node-ws_birthday-field_title_or_relationship',
              170 => 'node-ws_birthday-field_ws_name',
              171 => 'node-ws_birthday-field_ws_photo',
              172 => 'node-ws_obituary-field_birth_date',
              173 => 'node-ws_obituary-field_birth_location',
              174 => 'node-ws_obituary-field_compiled_annoucement',
              175 => 'node-ws_obituary-field_date_death',
              176 => 'node-ws_obituary-field_death_age',
              177 => 'node-ws_obituary-field_death_location',
              178 => 'node-ws_obituary-field_email',
              179 => 'node-ws_obituary-field_family',
              180 => 'node-ws_obituary-field_full_name',
              181 => 'node-ws_obituary-field_gender',
              182 => 'node-ws_obituary-field_grandchildren',
              183 => 'node-ws_obituary-field_great_grandchildren',
              184 => 'node-ws_obituary-field_great_great_grandchildren',
              185 => 'node-ws_obituary-field_issue',
              186 => 'node-ws_obituary-field_maiden_name',
              187 => 'node-ws_obituary-field_notes',
              188 => 'node-ws_obituary-field_phone',
              189 => 'node-ws_obituary-field_previous_married_name',
              190 => 'node-ws_obituary-field_step_grandchildren',
              191 => 'node-ws_obituary-field_step_great_grandchildren',
              192 => 'node-ws_obituary-field_title_or_relationship',
              193 => 'node-ws_obituary-field_ws_name',
              194 => 'node-ws_wedding-field_brides_name',
              195 => 'node-ws_wedding-field_compiled_annoucement',
              196 => 'node-ws_wedding-field_couples_new_location',
              197 => 'node-ws_wedding-field_email',
              198 => 'node-ws_wedding-field_grooms_father',
              199 => 'node-ws_wedding-field_grooms_mother',
              200 => 'node-ws_wedding-field_grooms_name',
              201 => 'node-ws_wedding-field_grooms_stepfather',
              202 => 'node-ws_wedding-field_grooms_stepmother',
              203 => 'node-ws_wedding-field_issue',
              204 => 'node-ws_wedding-field_married_bride',
              205 => 'node-ws_wedding-field_married_groom',
              206 => 'node-ws_wedding-field_phone',
              207 => 'node-ws_wedding-field_previous_married_name',
              208 => 'node-ws_wedding-field_title_or_relationship',
              209 => 'node-ws_wedding-field_wedding_date',
              210 => 'node-ws_wedding-field_wedding_location',
              211 => 'node-ws_wedding-field_ws_bride_father',
              212 => 'node-ws_wedding-field_ws_bride_mother',
              213 => 'node-ws_wedding-field_ws_bride_stepfather',
              214 => 'node-ws_wedding-field_ws_bride_stepmother',
              215 => 'node-ws_wedding-field_ws_name',
              216 => 'taxonomy_term-issue-field_ad_close_date',
              217 => 'taxonomy_term-issue-field_date_month',
              218 => 'taxonomy_term-issue-field_image',
              219 => 'taxonomy_term-issue-field_number',
              220 => 'taxonomy_term-issue-field_pdf',
              221 => 'taxonomy_term-issue-field_published',
              222 => 'taxonomy_term-issue-field_taxo_tags',
              223 => 'taxonomy_term-issue-field_volume_id',
              224 => 'user-user-field_real_name',
              225 => 'user-user-field_section_editor',
            ),
            'file_display' => 
            array (
              0 => 'image__default__file_field_file_rendered',
              1 => 'image__default__file_field_file_table',
              2 => 'image__default__file_field_file_url_plain',
              3 => 'image__default__file_field_image',
              4 => 'image__default__file_image',
              5 => 'image__full__file_image',
              6 => 'image__preview__file_field_image',
              7 => 'image__preview__file_image',
              8 => 'image__teaser__file_field_image',
              9 => 'image__teaser__file_image',
            ),
            'image' => 
            array (
              0 => 'gn_2_col',
              1 => 'gn_3_col',
              2 => 'gn_4_col',
              3 => 'gn_5_col',
              4 => 'gn_6_col',
              5 => 'gn_7_col',
              6 => 'gn_article_gallery',
              7 => 'gn_article_gallery_lg',
              8 => 'gn_article_gallery_sm',
              9 => 'gn_article_gallery_sq',
              10 => 'gn_article_gallery_thumb',
              11 => 'gn_author_feature',
              12 => 'gn_author_photo',
              13 => 'gn_author_photo_mobile',
              14 => 'gn_feature_hero',
              15 => 'gn_feature_hero_crop',
              16 => 'gn_feature_hero_text_crop',
              17 => 'gn_home_teaser',
              18 => 'gn_issue_feature',
              19 => 'gn_issue_feature_mobile',
              20 => 'gn_issue_teaser',
              21 => 'gn_media_teaser',
              22 => 'gn_media_teaser_mobile',
              23 => 'gn_section_feature',
              24 => 'gn_section_teaser',
              25 => 'social_image',
              26 => 'square_thumbnail',
            ),
            'mandrill_template_map' => 
            array (
              0 => 'gn_default_emails',
            ),
            'menu_custom' => 
            array (
              0 => 'main-menu',
              1 => 'menu-issue-extras',
              2 => 'menu-secondary-menu',
              3 => 'menu-social-links',
              4 => 'user-menu',
            ),
            'metatag' => 
            array (
              0 => 'global',
              1 => 'global:frontpage',
              2 => 'node',
              3 => 'node:article',
              4 => 'node:feature',
              5 => 'node:featured_content',
              6 => 'taxonomy_term:issue',
            ),
            'node' => 
            array (
              0 => 'article',
              1 => 'author_profile',
              2 => 'feature',
              3 => 'featured_content',
              4 => 'image_gallery',
              5 => 'issue_pdf',
              6 => 'media',
              7 => 'page',
              8 => 'photo_contest_entry',
              9 => 'photo_feature',
              10 => 'printcontent',
              11 => 'quote',
              12 => 'webform',
              13 => 'ws_anniversary',
              14 => 'ws_birth',
              15 => 'ws_birthday',
              16 => 'ws_obituary',
              17 => 'ws_wedding',
            ),
            'page_manager_handlers' => 
            array (
              0 => 'myworkbench_panel_context',
              1 => 'myworkbench_panel_context_2',
              2 => 'myworkbench_panel_context_3',
              3 => 'myworkbench_panel_context_4',
              4 => 'myworkbench_panel_context_5',
              5 => 'myworkbench_panel_context_6',
              6 => 'myworkbench_panel_context_7',
            ),
            'page_manager_pages' => 
            array (
              0 => 'classifieds',
              1 => 'family',
              2 => 'nw_roundtable',
            ),
            'path_breadcrumbs' => 
            array (
              0 => 'authors',
              1 => 'classified_breadcrumbs',
              2 => 'event',
              3 => 'events_view',
              4 => 'events_view_month',
              5 => 'events_view_year',
              6 => 'family_breadcrumbs',
              7 => 'features',
              8 => 'features_view',
              9 => 'gallery',
              10 => 'gallery_view',
              11 => 'inprint',
              12 => 'inprint_issue',
              13 => 'inprint_view',
              14 => 'media',
              15 => 'media_view',
              16 => 'news',
              17 => 'news_view',
              18 => 'news_view_clone',
              19 => 'worksheet_add',
            ),
            'picture_mapping' => 
            array (
              0 => 'custom_group',
              1 => 'feature_hero',
              2 => 'gn_article_gallery',
              3 => 'gn_author_cover_feature',
              4 => 'gn_author_photo',
              5 => 'gn_feature_hero',
              6 => 'gn_feature_hero_crop',
              7 => 'gn_feature_hero_text',
              8 => 'gn_gallery',
              9 => 'gn_gallery_teaser',
              10 => 'gn_home_images',
              11 => 'gn_issue_bug',
              12 => 'gn_issue_feature',
              13 => 'gn_issue_teaser',
              14 => 'gn_media',
              15 => 'gn_section_feature',
              16 => 'gn_section_teaser',
              17 => 'gn_slideshow',
              18 => 'gn_slideshow_thumbs',
            ),
            'rules_config' => 
            array (
              0 => 'gleaner_features_publish_and_set_date',
              1 => 'gleaner_features_workflow_set_web_publish_state',
              2 => 'rules_publish_issue_rule',
              3 => 'rules_unpublish_issue_rule',
              4 => 'rules_vbo_set_published',
              5 => 'rules_workflow_conference_notification',
              6 => 'rules_workflow_copy_edit',
              7 => 'rules_workflow_editorial_review',
              8 => 'rules_workflow_initial_review',
              9 => 'rules_workflow_stage_1_review',
              10 => 'rules_workflow_stage_2_review',
              11 => 'rules_workflow_stage_3_review',
              12 => 'rules_workflow_stage_4_review',
              13 => 'rules_workflow_stage_5_review',
              14 => 'rules_workflow_web_publish',
              15 => 'rules_worksheets_post_submit',
            ),
            'taxonomy' => 
            array (
              0 => 'classified_categories',
              1 => 'issue',
              2 => 'news_sections',
              3 => 'organization',
              4 => 'quote_type',
              5 => 'section_categories',
              6 => 'tags',
            ),
            'user_role' => 
            array (
              0 => 'administrator',
              1 => 'advertiser',
              2 => 'content author',
              3 => 'copy editor',
              4 => 'designer',
              5 => 'developer',
              6 => 'editor',
              7 => 'news editor',
              8 => 'production',
              9 => 'staff',
              10 => 'web editor',
            ),
            'variable' => 
            array (
              0 => 'auto_entitylabel_node_article',
              1 => 'auto_entitylabel_node_author_profile',
              2 => 'auto_entitylabel_node_featured_content',
              3 => 'auto_entitylabel_node_issue_pdf',
              4 => 'auto_entitylabel_node_photo_feature',
              5 => 'auto_entitylabel_node_printcontent',
              6 => 'auto_entitylabel_node_quote',
              7 => 'auto_entitylabel_node_ws_anniversary',
              8 => 'auto_entitylabel_node_ws_birth',
              9 => 'auto_entitylabel_node_ws_birthday',
              10 => 'auto_entitylabel_node_ws_obituary',
              11 => 'auto_entitylabel_node_ws_wedding',
              12 => 'auto_entitylabel_pattern_node_article',
              13 => 'auto_entitylabel_pattern_node_author_profile',
              14 => 'auto_entitylabel_pattern_node_featured_content',
              15 => 'auto_entitylabel_pattern_node_issue_pdf',
              16 => 'auto_entitylabel_pattern_node_photo_feature',
              17 => 'auto_entitylabel_pattern_node_printcontent',
              18 => 'auto_entitylabel_pattern_node_quote',
              19 => 'auto_entitylabel_pattern_node_ws_anniversary',
              20 => 'auto_entitylabel_pattern_node_ws_birth',
              21 => 'auto_entitylabel_pattern_node_ws_birthday',
              22 => 'auto_entitylabel_pattern_node_ws_obituary',
              23 => 'auto_entitylabel_pattern_node_ws_wedding',
              24 => 'auto_entitylabel_php_node_article',
              25 => 'auto_entitylabel_php_node_author_profile',
              26 => 'auto_entitylabel_php_node_featured_content',
              27 => 'auto_entitylabel_php_node_issue_pdf',
              28 => 'auto_entitylabel_php_node_photo_feature',
              29 => 'auto_entitylabel_php_node_printcontent',
              30 => 'auto_entitylabel_php_node_quote',
              31 => 'auto_entitylabel_php_node_ws_anniversary',
              32 => 'auto_entitylabel_php_node_ws_birth',
              33 => 'auto_entitylabel_php_node_ws_birthday',
              34 => 'auto_entitylabel_php_node_ws_obituary',
              35 => 'auto_entitylabel_php_node_ws_wedding',
              36 => 'field_bundle_settings_node__article',
              37 => 'field_bundle_settings_node__author_profile',
              38 => 'field_bundle_settings_node__feature',
              39 => 'field_bundle_settings_node__featured_content',
              40 => 'field_bundle_settings_node__image_gallery',
              41 => 'field_bundle_settings_node__issue_pdf',
              42 => 'field_bundle_settings_node__media',
              43 => 'field_bundle_settings_node__page',
              44 => 'field_bundle_settings_node__photo_feature',
              45 => 'field_bundle_settings_node__post',
              46 => 'field_bundle_settings_node__printcontent',
              47 => 'field_bundle_settings_node__quote',
              48 => 'field_bundle_settings_node__sidebar',
              49 => 'field_bundle_settings_node__webform',
              50 => 'field_bundle_settings_node__ws_anniversary',
              51 => 'field_bundle_settings_node__ws_birth',
              52 => 'field_bundle_settings_node__ws_birthday',
              53 => 'field_bundle_settings_node__ws_obituary',
              54 => 'field_bundle_settings_node__ws_wedding',
              55 => 'menu_options_ad_reference_form',
              56 => 'menu_options_article',
              57 => 'menu_options_author_profile',
              58 => 'menu_options_feature',
              59 => 'menu_options_featured_content',
              60 => 'menu_options_image_gallery',
              61 => 'menu_options_issue_event',
              62 => 'menu_options_issue_pdf',
              63 => 'menu_options_media',
              64 => 'menu_options_page',
              65 => 'menu_options_photo_feature',
              66 => 'menu_options_post',
              67 => 'menu_options_printcontent',
              68 => 'menu_options_quote',
              69 => 'menu_options_sidebar',
              70 => 'menu_options_webform',
              71 => 'menu_options_ws_anniversary',
              72 => 'menu_options_ws_birth',
              73 => 'menu_options_ws_birthday',
              74 => 'menu_options_ws_obituary',
              75 => 'menu_options_ws_wedding',
              76 => 'menu_parent_ad_reference_form',
              77 => 'menu_parent_article',
              78 => 'menu_parent_author_profile',
              79 => 'menu_parent_feature',
              80 => 'menu_parent_featured_content',
              81 => 'menu_parent_image_gallery',
              82 => 'menu_parent_issue_event',
              83 => 'menu_parent_issue_pdf',
              84 => 'menu_parent_media',
              85 => 'menu_parent_page',
              86 => 'menu_parent_photo_feature',
              87 => 'menu_parent_post',
              88 => 'menu_parent_printcontent',
              89 => 'menu_parent_quote',
              90 => 'menu_parent_sidebar',
              91 => 'menu_parent_webform',
              92 => 'menu_parent_ws_anniversary',
              93 => 'menu_parent_ws_birth',
              94 => 'menu_parent_ws_birthday',
              95 => 'menu_parent_ws_obituary',
              96 => 'menu_parent_ws_wedding',
              97 => 'node_options_ad_reference_form',
              98 => 'node_options_article',
              99 => 'node_options_author_profile',
              100 => 'node_options_feature',
              101 => 'node_options_featured_content',
              102 => 'node_options_image_gallery',
              103 => 'node_options_issue_event',
              104 => 'node_options_issue_pdf',
              105 => 'node_options_media',
              106 => 'node_options_page',
              107 => 'node_options_photo_feature',
              108 => 'node_options_post',
              109 => 'node_options_printcontent',
              110 => 'node_options_quote',
              111 => 'node_options_sidebar',
              112 => 'node_options_webform',
              113 => 'node_options_ws_anniversary',
              114 => 'node_options_ws_birth',
              115 => 'node_options_ws_birthday',
              116 => 'node_options_ws_obituary',
              117 => 'node_options_ws_wedding',
              118 => 'node_preview_ad_reference_form',
              119 => 'node_preview_article',
              120 => 'node_preview_author_profile',
              121 => 'node_preview_feature',
              122 => 'node_preview_featured_content',
              123 => 'node_preview_image_gallery',
              124 => 'node_preview_issue_pdf',
              125 => 'node_preview_media',
              126 => 'node_preview_page',
              127 => 'node_preview_photo_feature',
              128 => 'node_preview_post',
              129 => 'node_preview_printcontent',
              130 => 'node_preview_quote',
              131 => 'node_preview_sidebar',
              132 => 'node_preview_webform',
              133 => 'node_preview_ws_anniversary',
              134 => 'node_preview_ws_birth',
              135 => 'node_preview_ws_birthday',
              136 => 'node_preview_ws_obituary',
              137 => 'node_preview_ws_wedding',
              138 => 'node_submitted_ad_reference_form',
              139 => 'node_submitted_article',
              140 => 'node_submitted_author_profile',
              141 => 'node_submitted_feature',
              142 => 'node_submitted_featured_content',
              143 => 'node_submitted_image_gallery',
              144 => 'node_submitted_issue_event',
              145 => 'node_submitted_issue_pdf',
              146 => 'node_submitted_media',
              147 => 'node_submitted_page',
              148 => 'node_submitted_photo_feature',
              149 => 'node_submitted_post',
              150 => 'node_submitted_printcontent',
              151 => 'node_submitted_quote',
              152 => 'node_submitted_sidebar',
              153 => 'node_submitted_webform',
              154 => 'node_submitted_ws_anniversary',
              155 => 'node_submitted_ws_birth',
              156 => 'node_submitted_ws_birthday',
              157 => 'node_submitted_ws_obituary',
              158 => 'node_submitted_ws_wedding',
            ),
          ),
          'features_exclude' => 
          array (
            'field' => 
            array (
              'node-article-body' => 'node-article-body',
              'node-article-field_author' => 'node-article-field_author',
              'node-article-field_section' => 'node-article-field_section',
              'node-article-field_issue' => 'node-article-field_issue',
              'node-article-field_body_print' => 'node-article-field_body_print',
              'node-article-field_design_notes' => 'node-article-field_design_notes',
              'node-article-field_related_content' => 'node-article-field_related_content',
              'node-article-field_priority' => 'node-article-field_priority',
              'node-article-field_taxo_tags' => 'node-article-field_taxo_tags',
              'node-article-field_location' => 'node-article-field_location',
              'node-article-field_latlon' => 'node-article-field_latlon',
              'node-article-field_short_title' => 'node-article-field_short_title',
              'node-article-field_print_title' => 'node-article-field_print_title',
              'node-article-field_print_subtitle' => 'node-article-field_print_subtitle',
              'node-article-field_pullquote' => 'node-article-field_pullquote',
              'node-article-field_mini_summary' => 'node-article-field_mini_summary',
              'node-article-field_print_workflow' => 'node-article-field_print_workflow',
              'node-article-field_sample_image' => 'node-article-field_sample_image',
              'node-article-field_pictures' => 'node-article-field_pictures',
              'field_collection_item-field_pictures-field_image' => 'field_collection_item-field_pictures-field_image',
              'field_collection_item-field_pictures-field_caption' => 'field_collection_item-field_pictures-field_caption',
              'field_collection_item-field_pictures-field_credit' => 'field_collection_item-field_pictures-field_credit',
              'field_collection_item-field_pictures-field_web_print' => 'field_collection_item-field_pictures-field_web_print',
              'taxonomy_term-issue-field_volume_id' => 'taxonomy_term-issue-field_volume_id',
              'taxonomy_term-issue-field_number' => 'taxonomy_term-issue-field_number',
              'taxonomy_term-issue-field_pdf' => 'taxonomy_term-issue-field_pdf',
              'taxonomy_term-issue-field_taxo_tags' => 'taxonomy_term-issue-field_taxo_tags',
              'taxonomy_term-issue-field_image_hero' => 'taxonomy_term-issue-field_image_hero',
              'taxonomy_term-issue-field_date_month' => 'taxonomy_term-issue-field_date_month',
              'node-article-field_image' => 'node-article-field_image',
              'taxonomy_term-issue-field_image' => 'taxonomy_term-issue-field_image',
            ),
            'dependencies' => 
            array (
              'media' => 'media',
              'entity' => 'entity',
              'date_views' => 'date_views',
              'ds' => 'ds',
              'views' => 'views',
              'ctools' => 'ctools',
              'date' => 'date',
              'file' => 'file',
              'file_entity' => 'file_entity',
              'image' => 'image',
              'options' => 'options',
              'rules' => 'rules',
              'taxonomy' => 'taxonomy',
              'conditional_fields' => 'conditional_fields',
              'exif_custom' => 'exif_custom',
              'html5_tools' => 'html5_tools',
            ),
            'field_base' => 
            array (
              'field_image_hero' => 'field_image_hero',
            ),
            'variable' => 
            array (
              'auto_entitylabel_node_photo_contest_entry' => 'auto_entitylabel_node_photo_contest_entry',
              'auto_entitylabel_pattern_node_photo_contest_entry' => 'auto_entitylabel_pattern_node_photo_contest_entry',
              'auto_entitylabel_php_node_photo_contest_entry' => 'auto_entitylabel_php_node_photo_contest_entry',
              'field_bundle_settings_node__photo_contest_entry' => 'field_bundle_settings_node__photo_contest_entry',
              'menu_options_photo_contest_entry' => 'menu_options_photo_contest_entry',
              'menu_parent_photo_contest_entry' => 'menu_parent_photo_contest_entry',
              'node_options_photo_contest_entry' => 'node_options_photo_contest_entry',
              'node_preview_photo_contest_entry' => 'node_preview_photo_contest_entry',
              'node_submitted_photo_contest_entry' => 'node_submitted_photo_contest_entry',
            ),
          ),
          'mtime' => '1424913747',
          'no autodetect' => '1',
          'description' => '',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.12',
      ),
      'gleaner_photo_contest' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_photo_contest/gleaner_photo_contest.module',
        'basename' => 'gleaner_photo_contest.module',
        'name' => 'gleaner_photo_contest',
        'info' => 
        array (
          'name' => 'Gleaner Photo Contest',
          'description' => 'Gleaner Photo Contest views and styles.',
          'core' => '7.x',
          'package' => 'Gleaner',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'flag',
            2 => 'rules',
            3 => 'rules_conditional',
            4 => 'session_api',
            5 => 'views',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_custom.module',
        'basename' => 'gleaner_custom.module',
        'name' => 'gleaner_custom',
        'info' => 
        array (
          'name' => 'Gleaner Custom',
          'description' => 'A custom module to provide custom functions and integration for the Gleaner',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'commerce_wordcounter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/commerce_wordcounter/commerce_wordcounter.module',
        'basename' => 'commerce_wordcounter.module',
        'name' => 'commerce_wordcounter',
        'info' => 
        array (
          'name' => 'Commerce Wordcounter',
          'description' => 'Contains rules for calculating product price adjustments based on entity word counts.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_node_checkout',
            1 => 'rules',
            2 => 'wordcounter',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'plupload' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/plupload/plupload.module',
        'basename' => 'plupload.module',
        'name' => 'plupload',
        'info' => 
        array (
          'name' => 'Plupload integration module',
          'description' => 'Provides a plupload element.',
          'files' => 
          array (
            0 => 'plupload.module',
          ),
          'core' => '7.x',
          'package' => 'Media',
          'version' => '7.x-1.7',
          'project' => 'plupload',
          'datestamp' => '1415390716',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
    ),
    'themes' => 
    array (
      'seven' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/themes/seven/seven.info',
        'basename' => 'seven.info',
        'name' => 'Seven',
        'info' => 
        array (
          'name' => 'Seven',
          'description' => 'A simple one-column, tableless, fluid width administration theme.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'reset.css',
              1 => 'style.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
        ),
        'version' => '7.35',
      ),
      'garland' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/themes/garland/garland.info',
        'basename' => 'garland.info',
        'name' => 'Garland',
        'info' => 
        array (
          'name' => 'Garland',
          'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'style.css',
            ),
            'print' => 
            array (
              0 => 'print.css',
            ),
          ),
          'settings' => 
          array (
            'garland_width' => 'fluid',
          ),
        ),
        'version' => '7.35',
      ),
      'bartik' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/themes/bartik/bartik.info',
        'basename' => 'bartik.info',
        'name' => 'Bartik',
        'info' => 
        array (
          'name' => 'Bartik',
          'description' => 'A flexible, recolorable theme with many regions.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/layout.css',
              1 => 'css/style.css',
              2 => 'css/colors.css',
            ),
            'print' => 
            array (
              0 => 'css/print.css',
            ),
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'highlighted' => 'Highlighted',
            'featured' => 'Featured',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'triptych_first' => 'Triptych first',
            'triptych_middle' => 'Triptych middle',
            'triptych_last' => 'Triptych last',
            'footer_firstcolumn' => 'Footer first column',
            'footer_secondcolumn' => 'Footer second column',
            'footer_thirdcolumn' => 'Footer third column',
            'footer_fourthcolumn' => 'Footer fourth column',
            'footer' => 'Footer',
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '0',
          ),
        ),
        'version' => '7.35',
      ),
      'stark' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/themes/stark/stark.info',
        'basename' => 'stark.info',
        'name' => 'Stark',
        'info' => 
        array (
          'name' => 'Stark',
          'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
          'package' => 'Core',
          'version' => '7.35',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'layout.css',
            ),
          ),
        ),
        'version' => '7.35',
      ),
      'ember' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/contrib/ember/ember.info',
        'basename' => 'ember.info',
        'name' => 'Ember',
        'info' => 
        array (
          'name' => 'Ember',
          'description' => 'A responsive administration theme.',
          'package' => 'Core',
          'version' => '7.x-2.0-alpha2+53-dev',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'styles/style.css',
              1 => 'styles/system.base.css',
              2 => 'styles/system.menus.css',
              3 => 'styles/system.theme.css',
              4 => 'styles/system.messages.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'messages' => 'Messages',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
          'project' => 'ember',
          'datestamp' => '1424961485',
        ),
        'version' => '7.x-2.0-alpha2+53-dev',
      ),
      'aurora' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/contrib/aurora/aurora.info',
        'basename' => 'aurora.info',
        'name' => 'Aurora',
        'info' => 
        array (
          'name' => 'Aurora',
          'description' => 'Lightweight Responsive Ready HTML5 base theme',
          'core' => '7.x',
          'regions' => 
          array (
            'header' => 'Header',
            'main_prefix' => 'Main Region Prefix',
            'content' => 'Main',
            'main_suffix' => 'Main Region Suffix',
            'footer' => 'Footer',
          ),
          'plugins' => 
          array (
            'panels' => 
            array (
              'layouts' => 'layouts',
            ),
          ),
          'settings' => 
          array (
            'aurora_min_ie_support' => '9',
            'aurora_html_tags' => '1',
            'aurora_typekit_id' => '; Information added by Drupal.org packaging script on 2015-02-11',
          ),
          'version' => '7.x-3.5',
          'project' => 'aurora',
          'datestamp' => '1423684983',
        ),
        'version' => '7.x-3.5',
      ),
      'gleanernow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/gleanernow/gleanernow.info',
        'basename' => 'gleanernow.info',
        'name' => 'gleanernow',
        'info' => 
        array (
          'name' => 'gleanernow',
          'description' => 'gleanernow theme based on the Aurora base theme.',
          'core' => '7.x',
          'base theme' => 'aurora',
          'regions' => 
          array (
            'header' => 'Header',
            'header_suffix' => 'Header - Suffix',
            'menu' => 'Main Menu',
            'main_prefix' => 'Main Region Prefix',
            'content' => 'Main Region',
            'main_suffix' => 'Main Region Suffix',
            'footer' => 'Footer',
            'footer_social' => 'Footer - Social',
            'footer_sub' => 'Footer - Sub',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'stylesheets/style.css',
            ),
            'print' => 
            array (
              0 => 'stylesheets/print.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'javascripts/imagesloaded.pkgd.min.js',
            1 => 'javascripts/responsive-nav.min.js',
            2 => 'javascripts/jquery.sly.min.js',
            3 => 'javascripts/jquery.fitvids.js',
            4 => 'javascripts/scripts.js',
          ),
          'modernizr' => 
          array (
            'tests' => 
            array (
              0 => 'css_boxsizing',
            ),
          ),
          'settings' => 
          array (
            'toggle_node_user_picture' => '1',
            'toggle_comment_user_picture' => '1',
            'toggle_comment_user_verification' => '1',
            'default_logo' => '1',
            'logo_path' => '',
            'logo_upload' => '',
            'default_favicon' => '1',
            'favicon_path' => '',
            'favicon_upload' => '',
            'hide_recomended_modules' => '0',
            'aurora_enable_chrome_frame' => '1',
            'aurora_min_ie_support' => '10',
            'aurora_html_tags' => '1',
            'aurora_typekit_id' => '0',
            'aurora_livereload' => '0',
            'magic_embedded_mqs' => '0',
            'magic_css_excludes' => '',
            'magic_footer_js' => '0',
            'magic_library_head' => '0',
            'magic_experimental_js' => '0',
            'magic_js_excludes' => '',
            'magic_rebuild_registry' => '0',
            'magic_viewport_indicator' => '0',
            'magic_modernizr_debug' => '0',
            'magic_performance__active_tab' => 'edit-dev',
            'magic_css_excludes_regex' => '',
            'magic_js_excludes_regex' => '',
          ),
          'breakpoints' => 
          array (
            'wide' => '(min-width: 50em)',
            'narrow' => '(min-width: 30em)',
            'mobile' => '(min-width: 0px)',
          ),
          'plugins' => 
          array (
            'panels' => 
            array (
              'layouts' => 'layouts',
            ),
          ),
          'version' => 'unknown',
          'project' => 'gleanernow',
          'datestamp' => '1383156312',
        ),
        'version' => 'unknown',
      ),
      'cinder' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/cinder/cinder.info',
        'basename' => 'cinder.info',
        'name' => 'Cinder',
        'info' => 
        array (
          'name' => 'Cinder',
          'description' => 'Cinder in a cleaned up version of Ember 2.x. Hopefully, much of this code can be removed at Ember stableizes.',
          'core' => '7.x',
          'base theme' => 'ember',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'stylesheets/screen.css',
            ),
            'print' => 
            array (
              0 => 'stylesheets/print.css',
            ),
          ),
        ),
        'version' => NULL,
      ),
      'weebpal_backend' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/weebpal_backend/weebpal_backend.info',
        'basename' => 'weebpal_backend.info',
        'name' => 'WeebPal Backend',
        'info' => 
        array (
          'name' => 'WeebPal Backend',
          'description' => 'Backend theme from WeebPal',
          'version' => '7.35',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'fonts/font-awesome/css/font-awesome.css',
              1 => 'css/theme.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'js/weebpal_backend.js',
          ),
          'features' => 
          array (
            0 => 'logo',
            1 => 'name',
            2 => 'slogan',
            3 => 'node_user_picture',
            4 => 'comment_user_picture',
            5 => 'comment_user_verification',
            6 => 'favicon',
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'highlighted' => 'Highlighted',
            'help' => 'Help',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'footer' => 'Footer',
          ),
        ),
        'owner' => '/var/aegir/platforms/gleaner_cms_1.6.1/themes/engines/phptemplate/phptemplate.engine',
        'prefix' => 'phptemplate',
        'template' => true,
        'version' => '7.35',
      ),
    ),
    'platforms' => 
    array (
      'drupal' => 
      array (
        'short_name' => 'drupal',
        'version' => '7.35',
        'description' => 'This platform is running Drupal 7.35',
      ),
    ),
    'profiles' => 
    array (
      'minimal' => 
      array (
        'name' => 'minimal',
        'filename' => './profiles/minimal/minimal.profile',
        'info' => 
        array (
          'name' => 'Minimal',
          'description' => 'Start with only a few modules enabled.',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'dblog',
          ),
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.35',
      ),
      'standard' => 
      array (
        'name' => 'standard',
        'filename' => './profiles/standard/standard.profile',
        'info' => 
        array (
          'name' => 'Standard',
          'description' => 'Install with commonly used features pre-configured.',
          'version' => '7.35',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'color',
            2 => 'comment',
            3 => 'contextual',
            4 => 'dashboard',
            5 => 'help',
            6 => 'image',
            7 => 'list',
            8 => 'menu',
            9 => 'number',
            10 => 'options',
            11 => 'path',
            12 => 'taxonomy',
            13 => 'dblog',
            14 => 'search',
            15 => 'shortcut',
            16 => 'toolbar',
            17 => 'overlay',
            18 => 'field_ui',
            19 => 'file',
            20 => 'rdf',
          ),
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
          'old_short_name' => 'default',
        ),
        'version' => '7.35',
      ),
    ),
  ),
  'sites-all' => 
  array (
    'modules' => 
    array (
      'views_infinite_scroll' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_infinite_scroll/views_infinite_scroll.module',
        'basename' => 'views_infinite_scroll.module',
        'name' => 'views_infinite_scroll',
        'info' => 
        array (
          'name' => 'Views Infinite Scroll',
          'description' => 'Provides an Infinite Scrolling pager for Views',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_infinite_scroll.module',
            1 => 'views_infinite_scroll.views.inc',
            2 => 'views_plugin_pager_infinite_scroll.inc',
            3 => 'drush/views_infinite_scroll.drush.inc',
            4 => 'theme/views_infinite_scroll_theme.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'views_infinite_scroll',
          'datestamp' => '1335182196',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'robotstxt' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/robotstxt/robotstxt.module',
        'basename' => 'robotstxt.module',
        'name' => 'robotstxt',
        'info' => 
        array (
          'name' => 'robots.txt',
          'description' => 'Generates the robots.txt file dynamically and gives you the chance to edit it, on a per-site basis, from the web UI.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'robotstxt.module',
            1 => 'robotstxt.admin.inc',
            2 => 'robotstxt.install',
          ),
          'configure' => 'admin/config/search/robotstxt',
          'version' => '7.x-1.3',
          'project' => 'robotstxt',
          'datestamp' => '1419385385',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.3',
      ),
      'colorbox' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/colorbox/colorbox.module',
        'basename' => 'colorbox.module',
        'name' => 'colorbox',
        'info' => 
        array (
          'name' => 'Colorbox',
          'description' => 'A light-weight, customizable lightbox plugin for jQuery 1.4.3+.',
          'dependencies' => 
          array (
            0 => 'libraries (>=2.x)',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/media/colorbox',
          'files' => 
          array (
            0 => 'views/colorbox_handler_field_colorbox.inc',
          ),
          'version' => '7.x-2.9',
          'project' => 'colorbox',
          'datestamp' => '1434934994',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.9',
      ),
      'field_default_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_default_token/field_default_token.module',
        'basename' => 'field_default_token.module',
        'name' => 'field_default_token',
        'info' => 
        array (
          'name' => 'Field default token',
          'description' => 'Enables to use tokens as field default values.',
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'field_default_token',
          'datestamp' => '1390208305',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.2',
      ),
      'geocoder' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geocoder/geocoder.module',
        'basename' => 'geocoder.module',
        'name' => 'geocoder',
        'info' => 
        array (
          'name' => 'Geocoder',
          'description' => 'An API and widget to geocode various known data into other GIS data types.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
            1 => 'ctools',
          ),
          'version' => '7.x-1.2',
          'project' => 'geocoder',
          'datestamp' => '1346083034',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'date_facets' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date_facets/date_facets.module',
        'basename' => 'date_facets.module',
        'name' => 'date_facets',
        'info' => 
        array (
          'name' => 'Date Facets',
          'description' => 'Provides date range facets that are similar to implementations in major search engines.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'lib/Drupal/Apachesolr/Facetapi/QueryType/DateRangeQueryType.php',
            1 => 'lib/Drupal/Facetapi/Widget/DateRangeWidget.php',
            2 => 'lib/Drupal/Search/Facetapi/QueryType/DateRangeQueryType.php',
            3 => 'lib/Drupal/SearchApi/Facetapi/QueryType/DateRangeQueryType.php',
          ),
          'version' => '7.x-1.0-beta2+6-dev',
          'project' => 'date_facets',
          'datestamp' => '1382813050',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2+6-dev',
      ),
      'eva' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/eva/eva.module',
        'basename' => 'eva.module',
        'name' => 'eva',
        'info' => 
        array (
          'name' => 'Eva',
          'description' => 'Provides a Views display type that can be attached to entities.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'package' => 'Views',
          'files' => 
          array (
            0 => 'eva_plugin_display_entity.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'eva',
          'datestamp' => '1343701935',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'rules_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules_forms/rules_forms.module',
        'basename' => 'rules_forms.module',
        'name' => 'rules_forms',
        'info' => 
        array (
          'name' => 'Rules Forms Support',
          'description' => 'Provides events, conditions and actions for rule-based form customization.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/rules_forms.ui.inc',
            1 => 'rules_forms.test',
          ),
          'package' => 'Rules',
          'version' => '7.x-1.0-rc2',
          'project' => 'rules_forms',
          'datestamp' => '1329947457',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc2',
      ),
      'calendar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/calendar/calendar.module',
        'basename' => 'calendar.module',
        'name' => 'calendar',
        'info' => 
        array (
          'name' => 'Calendar',
          'description' => 'Views plugin to display views containing dates as Calendars.',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'date_api',
            2 => 'date_views',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/calendar_multiday.css',
            ),
          ),
          'files' => 
          array (
            0 => 'calendar.install',
            1 => 'calendar.module',
            2 => 'includes/calendar.views.inc',
            3 => 'includes/calendar_plugin_style.inc',
            4 => 'includes/calendar_plugin_row.inc',
            5 => 'includes/calendar.views_template.inc',
            6 => 'theme/theme.inc',
            7 => 'theme/calendar-style.tpl.php',
          ),
          'version' => '7.x-3.5',
          'project' => 'calendar',
          'datestamp' => '1413299943',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-3.5',
      ),
      'module_filter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/module_filter/module_filter.module',
        'basename' => 'module_filter.module',
        'name' => 'module_filter',
        'info' => 
        array (
          'name' => 'Module filter',
          'description' => 'Filter the modules list.',
          'core' => '7.x',
          'package' => 'Administration',
          'files' => 
          array (
            0 => 'module_filter.install',
            1 => 'module_filter.js',
            2 => 'module_filter.module',
            3 => 'module_filter.admin.inc',
            4 => 'module_filter.theme.inc',
            5 => 'css/module_filter.css',
            6 => 'css/module_filter_tab.css',
            7 => 'js/module_filter.js',
            8 => 'js/module_filter_tab.js',
          ),
          'configure' => 'admin/config/user-interface/modulefilter',
          'version' => '7.x-2.0+9-dev',
          'project' => 'module_filter',
          'datestamp' => '1426208586',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0+9-dev',
      ),
      'modernizr' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/modernizr/modernizr.module',
        'basename' => 'modernizr.module',
        'name' => 'modernizr',
        'info' => 
        array (
          'name' => 'Modernizr',
          'description' => 'Modernizr integration for Drupal. Extends the library by providing two APIs: (1) for test management (2) for exposing Modernizr.load() to Drupal',
          'core' => '7.x',
          'package' => 'Frontend',
          'configure' => 'admin/config/development/modernizr',
          'files' => 
          array (
            0 => 'modernizr.module',
            1 => 'modernizr.install',
            2 => 'modernizr.admin.inc',
            3 => 'modernizr.args.inc',
            4 => 'tests/modernizr.test',
          ),
          'version' => '7.x-3.4',
          'project' => 'modernizr',
          'datestamp' => '1420636982',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.4',
      ),
      'better_exposed_filters' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/better_exposed_filters/better_exposed_filters.module',
        'basename' => 'better_exposed_filters.module',
        'name' => 'better_exposed_filters',
        'info' => 
        array (
          'name' => 'Better Exposed Filters',
          'description' => 'Allow the use of checkboxes or radio buttons for exposed Views filters',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'better_exposed_filters_exposed_form_plugin.inc',
            1 => 'tests/better_exposed_filters.test',
          ),
          'version' => '7.x-3.0',
          'project' => 'better_exposed_filters',
          'datestamp' => '1418734694',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0',
      ),
      'field_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_permissions/field_permissions.module',
        'basename' => 'field_permissions.module',
        'name' => 'field_permissions',
        'info' => 
        array (
          'name' => 'Field Permissions',
          'description' => 'Set field-level permissions to create, update or view fields.',
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_permissions.module',
            1 => 'field_permissions.admin.inc',
            2 => 'field_permissions.test',
          ),
          'configure' => 'admin/reports/fields/permissions',
          'version' => '7.x-1.0-beta2',
          'project' => 'field_permissions',
          'datestamp' => '1327510549',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-beta2',
      ),
      'livefyre' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/livefyre/livefyre.module',
        'basename' => 'livefyre.module',
        'name' => 'livefyre',
        'info' => 
        array (
          'name' => 'Livefyre Comments',
          'description' => 'Embed Livefyre comments via javascript on your Drupal site.',
          'core' => '7.x',
          'version' => '7.x-1.3',
          'project' => 'livefyre',
          'datestamp' => '1360872158',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'addthis_displays' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addthis/addthis_displays/addthis_displays.module',
        'basename' => 'addthis_displays.module',
        'name' => 'addthis_displays',
        'info' => 
        array (
          'name' => 'AddThis Displays',
          'description' => 'Adds the AddThis display types to render several basic types of AddThis buttons.',
          'core' => '7.x',
          'package' => 'Sharing',
          'dependencies' => 
          array (
            0 => 'addthis',
          ),
          'files' => 
          array (
            0 => 'addthis_displays.field.inc',
          ),
          'version' => '7.x-4.0-alpha4',
          'project' => 'addthis',
          'datestamp' => '1390384409',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-4.0-alpha4',
      ),
      'addthis' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addthis/addthis.module',
        'basename' => 'addthis.module',
        'name' => 'addthis',
        'info' => 
        array (
          'name' => 'AddThis',
          'description' => 'AddThis.com provides an easy way to share your content across the web.',
          'core' => '7.x',
          'package' => 'Sharing',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'addthis.test',
            1 => 'classes/AddThis.php',
            2 => 'classes/AddThisJson.php',
          ),
          'configure' => 'admin/config/user-interface/addthis',
          'version' => '7.x-4.0-alpha4',
          'project' => 'addthis',
          'datestamp' => '1390384409',
          'php' => '5.2.4',
        ),
        'schema_version' => '7401',
        'version' => '7.x-4.0-alpha4',
      ),
      'geofield_map' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geofield/modules/geofield_map/geofield_map.module',
        'basename' => 'geofield_map.module',
        'name' => 'geofield_map',
        'info' => 
        array (
          'name' => 'Geofield Map',
          'description' => 'Provides a basic mapping interface for Geofield.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geofield',
          ),
          'files' => 
          array (
            0 => 'includes/geofield_map.views.inc',
            1 => 'includes/geofield_map_plugin_style_map.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'geofield',
          'datestamp' => '1372735859',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'geofield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geofield/geofield.module',
        'basename' => 'geofield.module',
        'name' => 'geofield',
        'info' => 
        array (
          'name' => 'Geofield',
          'description' => 'Stores geographic and location data (points, lines, and polygons).',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'geophp',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'geofield.module',
            1 => 'geofield.install',
            2 => 'geofield.widgets.inc',
            3 => 'geofield.formatters.inc',
            4 => 'geofield.openlayers.inc',
            5 => 'geofield.feeds.inc',
            6 => 'geofield.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'geofield',
          'datestamp' => '1372735859',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.2',
      ),
      'honeypot' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/honeypot/honeypot.module',
        'basename' => 'honeypot.module',
        'name' => 'honeypot',
        'info' => 
        array (
          'name' => 'Honeypot',
          'description' => 'Mitigates spam form submissions using the honeypot method.',
          'core' => '7.x',
          'configure' => 'admin/config/content/honeypot',
          'package' => 'Spam control',
          'files' => 
          array (
            0 => 'honeypot.test',
          ),
          'version' => '7.x-1.21',
          'project' => 'honeypot',
          'datestamp' => '1441334340',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.x-1.21',
      ),
      'webform' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/webform/webform.module',
        'basename' => 'webform.module',
        'name' => 'webform',
        'info' => 
        array (
          'name' => 'Webform',
          'description' => 'Enables the creation of forms and questionnaires.',
          'core' => '7.x',
          'package' => 'Webform',
          'configure' => 'admin/config/content/webform',
          'php' => '5.3',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'files' => 
          array (
            0 => 'includes/exporters/webform_exporter_delimited.inc',
            1 => 'includes/exporters/webform_exporter_excel_delimited.inc',
            2 => 'includes/exporters/webform_exporter_excel_xlsx.inc',
            3 => 'includes/exporters/webform_exporter.inc',
            4 => 'views/webform_handler_field_form_body.inc',
            5 => 'views/webform_handler_field_is_draft.inc',
            6 => 'views/webform_handler_field_node_link_edit.inc',
            7 => 'views/webform_handler_field_node_link_results.inc',
            8 => 'views/webform_handler_field_submission_count.inc',
            9 => 'views/webform_handler_field_submission_data.inc',
            10 => 'views/webform_handler_field_submission_link.inc',
            11 => 'views/webform_handler_field_webform_status.inc',
            12 => 'views/webform_handler_filter_is_draft.inc',
            13 => 'views/webform_handler_filter_submission_data.inc',
            14 => 'views/webform_handler_filter_webform_status.inc',
            15 => 'views/webform_handler_area_result_pager.inc',
            16 => 'views/webform_plugin_row_submission_view.inc',
            17 => 'views/webform_handler_relationship_submission_data.inc',
            18 => 'views/webform.views.inc',
            19 => 'tests/components.test',
            20 => 'tests/conditionals.test',
            21 => 'tests/permissions.test',
            22 => 'tests/submission.test',
            23 => 'tests/webform.test',
          ),
          'version' => '7.x-4.5',
          'project' => 'webform',
          'datestamp' => '1426611481',
        ),
        'schema_version' => '7422',
        'version' => '7.x-4.5',
      ),
      'search_api_facetapi' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/contrib/search_api_facetapi/search_api_facetapi.module',
        'basename' => 'search_api_facetapi.module',
        'name' => 'search_api_facetapi',
        'info' => 
        array (
          'name' => 'Search facets',
          'description' => 'Integrate the Search API with the Facet API to provide facetted searches.',
          'dependencies' => 
          array (
            0 => 'search_api',
            1 => 'facetapi',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'plugins/facetapi/adapter.inc',
            1 => 'plugins/facetapi/query_type_term.inc',
            2 => 'plugins/facetapi/query_type_date.inc',
          ),
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'search_api_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/contrib/search_api_views/search_api_views.module',
        'basename' => 'search_api_views.module',
        'name' => 'search_api_views',
        'info' => 
        array (
          'name' => 'Search views',
          'description' => 'Integrates the Search API with Views, enabling users to create views with searches as filters or arguments.',
          'dependencies' => 
          array (
            0 => 'search_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/display_facet_block.inc',
            1 => 'includes/handler_argument.inc',
            2 => 'includes/handler_argument_fulltext.inc',
            3 => 'includes/handler_argument_more_like_this.inc',
            4 => 'includes/handler_argument_string.inc',
            5 => 'includes/handler_argument_date.inc',
            6 => 'includes/handler_argument_taxonomy_term.inc',
            7 => 'includes/handler_filter.inc',
            8 => 'includes/handler_filter_boolean.inc',
            9 => 'includes/handler_filter_date.inc',
            10 => 'includes/handler_filter_entity.inc',
            11 => 'includes/handler_filter_fulltext.inc',
            12 => 'includes/handler_filter_language.inc',
            13 => 'includes/handler_filter_options.inc',
            14 => 'includes/handler_filter_taxonomy_term.inc',
            15 => 'includes/handler_filter_text.inc',
            16 => 'includes/handler_filter_user.inc',
            17 => 'includes/handler_sort.inc',
            18 => 'includes/plugin_cache.inc',
            19 => 'includes/query.inc',
          ),
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.14',
      ),
      'search_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api/search_api.module',
        'basename' => 'search_api.module',
        'name' => 'search_api',
        'info' => 
        array (
          'name' => 'Search API',
          'description' => 'Provides a generic API for modules offering search capabilites.',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'search_api.test',
            1 => 'includes/callback.inc',
            2 => 'includes/callback_add_aggregation.inc',
            3 => 'includes/callback_add_hierarchy.inc',
            4 => 'includes/callback_add_url.inc',
            5 => 'includes/callback_add_viewed_entity.inc',
            6 => 'includes/callback_bundle_filter.inc',
            7 => 'includes/callback_comment_access.inc',
            8 => 'includes/callback_language_control.inc',
            9 => 'includes/callback_node_access.inc',
            10 => 'includes/callback_node_status.inc',
            11 => 'includes/callback_role_filter.inc',
            12 => 'includes/datasource.inc',
            13 => 'includes/datasource_entity.inc',
            14 => 'includes/datasource_external.inc',
            15 => 'includes/exception.inc',
            16 => 'includes/index_entity.inc',
            17 => 'includes/processor.inc',
            18 => 'includes/processor_highlight.inc',
            19 => 'includes/processor_html_filter.inc',
            20 => 'includes/processor_ignore_case.inc',
            21 => 'includes/processor_stopwords.inc',
            22 => 'includes/processor_tokenizer.inc',
            23 => 'includes/processor_transliteration.inc',
            24 => 'includes/query.inc',
            25 => 'includes/server_entity.inc',
            26 => 'includes/service.inc',
          ),
          'configure' => 'admin/config/search/search_api',
          'version' => '7.x-1.14',
          'project' => 'search_api',
          'datestamp' => '1419580682',
          'php' => '5.2.4',
        ),
        'schema_version' => '7117',
        'version' => '7.x-1.14',
      ),
      'email_registration' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/email_registration/email_registration.module',
        'basename' => 'email_registration.module',
        'name' => 'email_registration',
        'info' => 
        array (
          'name' => 'Email Registration',
          'description' => 'Allows users to register with an e-mail address as their username.',
          'files' => 
          array (
            0 => 'email_registration.test',
          ),
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'email_registration',
          'datestamp' => '1398265775',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'shs' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/shs/shs.module',
        'basename' => 'shs.module',
        'name' => 'shs',
        'info' => 
        array (
          'name' => 'Simple hierarchical select',
          'core' => '7.x',
          'description' => 'Creates a simple hierarchical select widget for taxonomy fields.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'files' => 
          array (
            0 => 'includes/handlers/shs_handler_filter_term_node_tid.inc',
            1 => 'includes/handlers/shs_handler_filter_term_node_tid_depth.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'shs',
          'datestamp' => '1363529732',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.6',
      ),
      'conditional_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/conditional_fields/conditional_fields.module',
        'basename' => 'conditional_fields.module',
        'name' => 'conditional_fields',
        'info' => 
        array (
          'name' => 'Conditional Fields',
          'description' => 'Define dependencies between fields based on their states and values.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'configure' => 'admin/structure/dependencies',
          'version' => '7.x-3.0-alpha1',
          'project' => 'conditional_fields',
          'datestamp' => '1384798705',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-3.0-alpha1',
      ),
      'borealis_ri' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis_ri/borealis_ri.module',
        'basename' => 'borealis_ri.module',
        'name' => 'borealis_ri',
        'info' => 
        array (
          'name' => 'Borealis Responsive Images',
          'description' => 'Responsive Image Suite to create responsive image sets for any Image preset',
          'core' => '7.x',
          'package' => 'Borealis',
          'dependencies' => 
          array (
            0 => 'borealis',
            1 => 'field_formatter_settings',
            2 => 'image',
          ),
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-2.2',
      ),
      'borealis_sb' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis_sb/borealis_sb.module',
        'basename' => 'borealis_sb.module',
        'name' => 'borealis_sb',
        'info' => 
        array (
          'name' => 'Borealis Semantic Blocks',
          'description' => 'Allows blocks to use semantic templates',
          'core' => '7.x',
          'package' => 'Borealis',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'borealis',
            2 => 'ctools',
          ),
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-2.2',
      ),
      'borealis' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/borealis/borealis.module',
        'basename' => 'borealis.module',
        'name' => 'borealis',
        'info' => 
        array (
          'name' => 'Borealis',
          'description' => 'Shared items between the Borealis submodules',
          'core' => '7.x',
          'package' => 'Borealis',
          'version' => '7.x-2.2',
          'project' => 'borealis',
          'datestamp' => '1358534712',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'filefield_sources' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_sources/filefield_sources.module',
        'basename' => 'filefield_sources.module',
        'name' => 'filefield_sources',
        'info' => 
        array (
          'name' => 'File Field Sources',
          'description' => 'Extends File fields to allow referencing of existing files, remote files, and server files.',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.9',
          'project' => 'filefield_sources',
          'datestamp' => '1383155428',
          'php' => '5.2.4',
        ),
        'schema_version' => '6001',
        'version' => '7.x-1.9',
      ),
      'entity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity/entity.module',
        'basename' => 'entity.module',
        'name' => 'entity',
        'info' => 
        array (
          'name' => 'Entity API',
          'description' => 'Enables modules to work with any entity type and to provide entities.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity.features.inc',
            1 => 'entity.i18n.inc',
            2 => 'entity.info.inc',
            3 => 'entity.rules.inc',
            4 => 'entity.test',
            5 => 'includes/entity.inc',
            6 => 'includes/entity.controller.inc',
            7 => 'includes/entity.ui.inc',
            8 => 'includes/entity.wrapper.inc',
            9 => 'views/entity.views.inc',
            10 => 'views/handlers/entity_views_field_handler_helper.inc',
            11 => 'views/handlers/entity_views_handler_area_entity.inc',
            12 => 'views/handlers/entity_views_handler_field_boolean.inc',
            13 => 'views/handlers/entity_views_handler_field_date.inc',
            14 => 'views/handlers/entity_views_handler_field_duration.inc',
            15 => 'views/handlers/entity_views_handler_field_entity.inc',
            16 => 'views/handlers/entity_views_handler_field_field.inc',
            17 => 'views/handlers/entity_views_handler_field_numeric.inc',
            18 => 'views/handlers/entity_views_handler_field_options.inc',
            19 => 'views/handlers/entity_views_handler_field_text.inc',
            20 => 'views/handlers/entity_views_handler_field_uri.inc',
            21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
            22 => 'views/handlers/entity_views_handler_relationship.inc',
            23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'entity',
          'datestamp' => '1424876582',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.x-1.6',
      ),
      'entity_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity/entity_token.module',
        'basename' => 'entity_token.module',
        'name' => 'entity_token',
        'info' => 
        array (
          'name' => 'Entity tokens',
          'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_token.tokens.inc',
            1 => 'entity_token.module',
          ),
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.6',
          'project' => 'entity',
          'datestamp' => '1424876582',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.6',
      ),
      'readonlymode' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/readonlymode/readonlymode.module',
        'basename' => 'readonlymode.module',
        'name' => 'readonlymode',
        'info' => 
        array (
          'name' => 'Read Only Mode',
          'description' => 'This module will lock your site for any form postings.',
          'core' => '7.x',
          'package' => 'Administration',
          'configure' => 'admin/config/development/maintenance',
          'files' => 
          array (
            0 => 'readonlymode.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'readonlymode',
          'datestamp' => '1402574630',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.2',
      ),
      'session_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/session_api/session_api.module',
        'basename' => 'session_api.module',
        'name' => 'session_api',
        'info' => 
        array (
          'name' => 'Session API',
          'description' => 'Provides an interface for storing session information.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'session_api.module',
            1 => 'session_api.admin.inc',
            2 => 'session_api.install',
            3 => 'session_api.test',
          ),
          'configure' => 'admin/config/development/session-api',
          'version' => '7.x-1.0-rc1',
          'project' => 'session_api',
          'datestamp' => '1354234727',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc1',
      ),
      'maxlength' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/maxlength/maxlength.module',
        'basename' => 'maxlength.module',
        'name' => 'maxlength',
        'info' => 
        array (
          'name' => 'Maxlength',
          'description' => 'Limit the number of characters in textfields and textareas and shows the amount of characters left.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'maxlength.test',
          ),
          'version' => '7.x-3.0',
          'project' => 'maxlength',
          'datestamp' => '1413456249',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7302',
        'version' => '7.x-3.0',
      ),
      'views_bulk_operations' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_bulk_operations/views_bulk_operations.module',
        'basename' => 'views_bulk_operations.module',
        'name' => 'views_bulk_operations',
        'info' => 
        array (
          'name' => 'Views Bulk Operations',
          'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'views',
          ),
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2.9',
          'files' => 
          array (
            0 => 'plugins/operation_types/base.class.php',
            1 => 'views/views_bulk_operations_handler_field_operations.inc',
          ),
          'version' => '7.x-3.2',
          'project' => 'views_bulk_operations',
          'datestamp' => '1387798183',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'actions_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_bulk_operations/actions_permissions.module',
        'basename' => 'actions_permissions.module',
        'name' => 'actions_permissions',
        'info' => 
        array (
          'name' => 'Actions permissions (VBO)',
          'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-3.2',
          'project' => 'views_bulk_operations',
          'datestamp' => '1387798183',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'libraries' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/libraries/libraries.module',
        'basename' => 'libraries.module',
        'name' => 'libraries',
        'info' => 
        array (
          'name' => 'Libraries',
          'description' => 'Allows version-dependent and shared usage of external libraries.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'system (>=7.11)',
          ),
          'files' => 
          array (
            0 => 'tests/libraries.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'libraries',
          'datestamp' => '1391965716',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'views_rss_media' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss_media/views_rss_media.module',
        'basename' => 'views_rss_media.module',
        'name' => 'views_rss_media',
        'info' => 
        array (
          'name' => 'Views RSS: Media (MRSS) Elements',
          'description' => 'Provides Media (MRSS) element set for Views RSS module',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
            1 => 'views_rss_core',
          ),
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'views_rss_media',
          'datestamp' => '1413385143',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'transliteration' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/transliteration/transliteration.module',
        'basename' => 'transliteration.module',
        'name' => 'transliteration',
        'info' => 
        array (
          'name' => 'Transliteration',
          'description' => 'Converts non-latin text to US-ASCII and sanitizes file names.',
          'core' => '7.x',
          'configure' => 'admin/config/media/file-system',
          'version' => '7.x-3.2',
          'project' => 'transliteration',
          'datestamp' => '1395079444',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.2',
      ),
      'menu_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_views/menu_views.module',
        'basename' => 'menu_views.module',
        'name' => 'menu_views',
        'info' => 
        array (
          'name' => 'Menu Views',
          'description' => 'Allows menu items to render views instead of links. This is useful for creating mega menus.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'menu',
            1 => 'views',
          ),
          'version' => '7.x-2.2',
          'project' => 'menu_views',
          'datestamp' => '1371136255',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'magic_dev' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/magic/magic_dev/magic_dev.module',
        'basename' => 'magic_dev.module',
        'name' => 'magic_dev',
        'info' => 
        array (
          'name' => 'Magic Development',
          'description' => 'Development options for Magic',
          'core' => '7.x',
          'package' => 'Frontend',
          'dependencies' => 
          array (
            0 => 'magic',
          ),
          'version' => '7.x-2.2',
          'project' => 'magic',
          'datestamp' => '1423685005',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'magic' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/magic/magic.module',
        'basename' => 'magic.module',
        'name' => 'magic',
        'info' => 
        array (
          'name' => 'Magic',
          'description' => 'Perform many essential frontend tasks with JavaScript, CSS, etc.',
          'core' => '7.x',
          'package' => 'Frontend',
          'testing_api' => '2.x',
          'files' => 
          array (
            0 => 'magic.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'magic',
          'datestamp' => '1423685005',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'jquery_update' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/jquery_update/jquery_update.module',
        'basename' => 'jquery_update.module',
        'name' => 'jquery_update',
        'info' => 
        array (
          'name' => 'jQuery Update',
          'description' => 'Update jQuery and jQuery UI to a more recent version.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'jquery_update.module',
            1 => 'jquery_update.install',
          ),
          'configure' => 'admin/config/development/jquery_update',
          'version' => '7.x-2.5',
          'project' => 'jquery_update',
          'datestamp' => '1422221882',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.5',
      ),
      'seckit' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/seckit/seckit.module',
        'basename' => 'seckit.module',
        'name' => 'seckit',
        'info' => 
        array (
          'name' => 'Security Kit',
          'description' => 'Enhance security of your Drupal website.',
          'package' => 'Security',
          'core' => '7.x',
          'configure' => 'admin/config/system/seckit',
          'files' => 
          array (
            0 => 'seckit.install',
            1 => 'seckit.module',
            2 => 'seckit.test',
            3 => 'includes/seckit.form.inc',
          ),
          'version' => '7.x-1.9',
          'project' => 'seckit',
          'datestamp' => '1399337028',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.9',
      ),
      'search_api_index_status' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_index_status/search_api_index_status.module',
        'basename' => 'search_api_index_status.module',
        'name' => 'search_api_index_status',
        'info' => 
        array (
          'name' => 'Search API index status',
          'description' => 'Adds a page that shows the status of each Search API index.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta1',
          'project' => 'search_api_index_status',
          'datestamp' => '1377861449',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'filefield_paths' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_paths/filefield_paths.module',
        'basename' => 'filefield_paths.module',
        'name' => 'filefield_paths',
        'info' => 
        array (
          'name' => 'File (Field) Paths',
          'description' => 'Adds improved Token based file sorting and renaming functionalities.',
          'dependencies' => 
          array (
            0 => 'token',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filefield_paths.install',
            1 => 'filefield_paths.module',
            2 => 'modules/features.inc',
            3 => 'modules/file.inc',
            4 => 'modules/filefield_paths.drush.inc',
            5 => 'modules/filefield_paths.inc',
            6 => 'modules/image.inc',
            7 => 'modules/token.inc',
            8 => 'modules/video.inc',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'filefield_paths',
          'datestamp' => '1366871711',
          'php' => '5.2.4',
        ),
        'schema_version' => '7107',
        'version' => '7.x-1.0-beta4',
      ),
      'views_load_more' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_load_more/views_load_more.module',
        'basename' => 'views_load_more.module',
        'name' => 'views_load_more',
        'info' => 
        array (
          'name' => 'Views Load More',
          'description' => 'A pager plugin for views. Gives users the option to add a \'more\' button to a view and have the results appended to existing results being displayed.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_load_more.views.inc',
            1 => 'views_plugin_pager_load_more.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'views_load_more',
          'datestamp' => '1412631229',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'better_formats' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/better_formats/better_formats.module',
        'basename' => 'better_formats.module',
        'name' => 'better_formats',
        'info' => 
        array (
          'name' => 'Better Formats',
          'description' => 'Enhances the core input format system by managing input format defaults and settings.',
          'core' => '7.x',
          'configure' => 'admin/config/content/formats',
          'version' => '7.x-1.0-beta1',
          'project' => 'better_formats',
          'datestamp' => '1343262404',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'themekey_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_example/themekey_example.module',
        'basename' => 'themekey_example.module',
        'name' => 'themekey_example',
        'info' => 
        array (
          'name' => 'ThemeKey Example',
          'description' => 'Implements parts of the ThemeKey API as an example for Developers.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'Example modules',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_redirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_redirect/themekey_redirect.module',
        'basename' => 'themekey_redirect.module',
        'name' => 'themekey_redirect',
        'info' => 
        array (
          'name' => 'ThemeKey Redirect',
          'description' => 'Provides an additional rule chain to define rules to redirect the user.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/redirects',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.3',
      ),
      'themekey_css' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_css/themekey_css.module',
        'basename' => 'themekey_css.module',
        'name' => 'themekey_css',
        'info' => 
        array (
          'name' => 'ThemeKey CSS (Experimental)',
          'description' => 'Define rules to dynamically add CSS files to a page. Experimental!',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/css',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/tests/themekey_simpletest.module',
        'basename' => 'themekey_simpletest.module',
        'name' => 'themekey_simpletest',
        'info' => 
        array (
          'name' => 'ThemeKey Testing',
          'description' => 'Just a fake module for testing ThemeKey plugins.',
          'core' => '7.x',
          'package' => 'Development',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_compat' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_compat.module',
        'basename' => 'themekey_compat.module',
        'name' => 'themekey_compat',
        'info' => 
        array (
          'name' => 'ThemeKey Compatibility',
          'description' => 'Optionally disable the theme switching capability of core and additional modules. Their theme switching capabilities will become part of the ThemeKey rule chain instead and therefor configurable.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/compat',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_ui.module',
        'basename' => 'themekey_ui.module',
        'name' => 'themekey_ui',
        'info' => 
        array (
          'name' => 'ThemeKey UI',
          'description' => 'Integrates ThemeKey with Drupal administration forms.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/ui',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.3',
      ),
      'themekey' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey.module',
        'basename' => 'themekey.module',
        'name' => 'themekey',
        'info' => 
        array (
          'name' => 'ThemeKey',
          'description' => 'Map themes to Drupal paths or object properties.',
          'core' => '7.x',
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings',
          'dependencies' => 
          array (
            0 => 'system (>=7.22)',
          ),
          'files' => 
          array (
            0 => 'ThemeKeyEntityFieldQuery.php',
            1 => 'modules/themekey_browser_detection.php',
            2 => 'tests/themekey.test',
            3 => 'tests/ThemekeyDrupalPropertiesTestCase.test',
            4 => 'tests/ThemekeyModulePluginsTestCase.test',
            5 => 'tests/ThemekeyNodePropertiesTestCase.test',
            6 => 'tests/ThemekeyRuleChainTestCase.test',
            7 => 'tests/ThemekeySystemPropertiesTestCase.test',
            8 => 'tests/ThemekeyUITestCase.test',
          ),
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.3',
      ),
      'themekey_user_profile' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_user_profile.module',
        'basename' => 'themekey_user_profile.module',
        'name' => 'themekey_user_profile',
        'info' => 
        array (
          'name' => 'ThemeKey User Profile',
          'description' => 'Allows users to select their own theme for this site. Replaces the corresponding feature that existed in Drupal 6 Core.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
            1 => 'themekey_ui',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/ui',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_features.module',
        'basename' => 'themekey_features.module',
        'name' => 'themekey_features',
        'info' => 
        array (
          'name' => 'ThemeKey Features (Beta)',
          'description' => 'Export single rules with Features.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
            1 => 'features',
          ),
          'package' => 'ThemeKey',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'themekey_debug' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/themekey/themekey_debug.module',
        'basename' => 'themekey_debug.module',
        'name' => 'themekey_debug',
        'info' => 
        array (
          'name' => 'ThemeKey Debug',
          'description' => 'Debug ThemeKey, see which rules are applied and the current values of all properties.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'themekey',
          ),
          'package' => 'ThemeKey',
          'configure' => 'admin/config/user-interface/themekey/settings/debug',
          'version' => '7.x-3.3',
          'project' => 'themekey',
          'datestamp' => '1422966217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.3',
      ),
      'elements' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/elements/elements.module',
        'basename' => 'elements.module',
        'name' => 'elements',
        'info' => 
        array (
          'name' => 'Elements',
          'description' => 'Provides a library of Form API elements.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'elements.module',
            1 => 'elements.theme.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'elements',
          'datestamp' => '1370667652',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'node_convert' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/node_convert/node_convert.module',
        'basename' => 'node_convert.module',
        'name' => 'node_convert',
        'info' => 
        array (
          'name' => 'Node Convert',
          'description' => 'Converts one or more nodes between different node types.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'node_convert.install',
            1 => 'node_convert.module',
            2 => 'node_convert.rules.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'node_convert',
          'datestamp' => '1402559034',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.2',
      ),
      'token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/token/token.module',
        'basename' => 'token.module',
        'name' => 'token',
        'info' => 
        array (
          'name' => 'Token',
          'description' => 'Provides a user interface for the Token API and some missing core tokens.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'token.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'token',
          'datestamp' => '1361665026',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.5',
      ),
      'auto_entitylabel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/auto_entitylabel/auto_entitylabel.module',
        'basename' => 'auto_entitylabel.module',
        'name' => 'auto_entitylabel',
        'info' => 
        array (
          'name' => 'Automatic Entity Labels',
          'description' => 'Allows hiding of entity label fields and automatic label creation.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.3',
          'project' => 'auto_entitylabel',
          'datestamp' => '1419756181',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'workbench' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workbench/workbench.module',
        'basename' => 'workbench.module',
        'name' => 'workbench',
        'info' => 
        array (
          'name' => 'Workbench',
          'description' => 'Workbench editorial suite.',
          'package' => 'Workbench',
          'core' => '7.x',
          'configure' => 'admin/config/workbench/settings',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => '7.x-1.2',
          'project' => 'workbench',
          'datestamp' => '1358534592',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'search_api_solr' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_solr/search_api_solr.module',
        'basename' => 'search_api_solr.module',
        'name' => 'search_api_solr',
        'info' => 
        array (
          'name' => 'Solr search',
          'description' => 'Offers an implementation of the Search API that uses an Apache Solr server for indexing content.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/document.inc',
            1 => 'includes/service.inc',
            2 => 'includes/solr_connection.inc',
            3 => 'includes/solr_connection.interface.inc',
            4 => 'includes/solr_field.inc',
            5 => 'includes/spellcheck.inc',
          ),
          'version' => '7.x-1.6',
          'project' => 'search_api_solr',
          'datestamp' => '1410186051',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.6',
      ),
      'name' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/name/name.module',
        'basename' => 'name.module',
        'name' => 'name',
        'info' => 
        array (
          'name' => 'Name Field',
          'description' => 'Defines a persons name field type.',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'package' => 'Fields',
          'version' => '7.x-1.9',
          'core' => '7.x',
          'recommends' => 
          array (
            0 => 'namedb',
          ),
          'configure' => 'admin/config/regional/name',
          'files' => 
          array (
            0 => 'tests/name.test',
            1 => 'includes/name_handler_filter_name_fulltext.inc',
            2 => 'name.migrate.inc',
          ),
          'project' => 'name',
          'datestamp' => '1368191420',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.9',
      ),
      'fences' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/fences/fences.module',
        'basename' => 'fences.module',
        'name' => 'fences',
        'info' => 
        array (
          'name' => 'Fences',
          'description' => 'Configurable field wrappers',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'configure' => 'admin/config/content/fences',
          'version' => '7.x-1.0',
          'project' => 'fences',
          'datestamp' => '1335373578',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.0',
      ),
      'file_entity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/file_entity/file_entity.module',
        'basename' => 'file_entity.module',
        'name' => 'file_entity',
        'info' => 
        array (
          'name' => 'File Entity',
          'description' => 'Extends Drupal file entities to be fieldable and viewable.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'file',
            2 => 'ctools',
            3 => 'system (>=7.9)',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'files' => 
          array (
            0 => 'views/views_handler_argument_file_type.inc',
            1 => 'views/views_handler_field_file_rendered.inc',
            2 => 'views/views_handler_field_file_type.inc',
            3 => 'views/views_handler_filter_file_type.inc',
            4 => 'views/views_handler_filter_schema_type.inc',
            5 => 'views/views_handler_field_file_filename.inc',
            6 => 'views/views_handler_field_file_link.inc',
            7 => 'views/views_handler_field_file_link_edit.inc',
            8 => 'views/views_handler_field_file_link_delete.inc',
            9 => 'views/views_handler_field_file_link_download.inc',
            10 => 'views/views_handler_field_file_link_usage.inc',
            11 => 'views/views_plugin_row_file_rss.inc',
            12 => 'views/views_plugin_row_file_view.inc',
            13 => 'file_entity.test',
          ),
          'configure' => 'admin/config/media/file-settings',
          'version' => '7.x-2.0-beta2',
          'project' => 'file_entity',
          'datestamp' => '1436896443',
          'php' => '5.2.4',
        ),
        'schema_version' => '7216',
        'version' => '7.x-2.0-beta2',
      ),
      'features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/features/features.module',
        'basename' => 'features.module',
        'name' => 'features',
        'info' => 
        array (
          'name' => 'Features',
          'description' => 'Provides feature management for Drupal.',
          'core' => '7.x',
          'package' => 'Features',
          'files' => 
          array (
            0 => 'tests/features.test',
          ),
          'configure' => 'admin/structure/features/settings',
          'version' => '7.x-2.4',
          'project' => 'features',
          'datestamp' => '1425501344',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'version' => '7.x-2.4',
      ),
      'pushbullet' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/pushbullet/pushbullet.module',
        'basename' => 'pushbullet.module',
        'name' => 'pushbullet',
        'info' => 
        array (
          'name' => 'Pushbullet API',
          'description' => 'Provide Pushbullet integration.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/Pushbullet.class.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'pushbullet',
          'datestamp' => '1428403438',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'chosen' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/chosen/chosen.module',
        'basename' => 'chosen.module',
        'name' => 'chosen',
        'info' => 
        array (
          'name' => 'Chosen',
          'description' => 'Makes select elements more user-friendly using <a href="http://harvesthq.github.com/chosen/">Chosen</a>.',
          'package' => 'User interface',
          'configure' => 'admin/config/user-interface/chosen',
          'core' => '7.x',
          'version' => '7.x-2.0-beta4',
          'project' => 'chosen',
          'datestamp' => '1394256505',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => '7.x-2.0-beta4',
      ),
      'elysia_cron' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/elysia_cron/elysia_cron.module',
        'basename' => 'elysia_cron.module',
        'name' => 'elysia_cron',
        'info' => 
        array (
          'name' => 'Elysia Cron',
          'description' => 'Extended cron support with crontab-like scheduling and other features.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'elysia_cron_update.php',
            1 => 'elysia_drupalconv.php',
          ),
          'configure' => 'admin/config/system/cron',
          'version' => '7.x-2.1',
          'project' => 'elysia_cron',
          'datestamp' => '1331658045',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '1',
        'version' => '7.x-2.1',
      ),
      'views_wookmark' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_wookmark/views_wookmark.module',
        'basename' => 'views_wookmark.module',
        'name' => 'views_wookmark',
        'info' => 
        array (
          'name' => 'Views Wookmark',
          'description' => 'Exposes a views style plugin to render items on a fluid grid.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
            1 => 'libraries',
          ),
          'files' => 
          array (
            0 => 'views_wookmark.module',
            1 => 'theme/theme.inc',
            2 => 'views/views_wookmark.views.inc',
            3 => 'views/views_wookmark_plugin_style.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'views_wookmark',
          'datestamp' => '1380681754',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'shield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/shield/shield.module',
        'basename' => 'shield.module',
        'name' => 'shield',
        'info' => 
        array (
          'name' => 'PHP Authentication shield',
          'description' => 'Creates a general shield for the site',
          'core' => '7.x',
          'configure' => 'admin/config/system/shield',
          'version' => '7.x-1.2',
          'project' => 'shield',
          'datestamp' => '1335436884',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'addressfield' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/addressfield/addressfield.module',
        'basename' => 'addressfield.module',
        'name' => 'addressfield',
        'info' => 
        array (
          'name' => 'Address Field',
          'description' => 'Manage a flexible address field, implementing the xNAL standard.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'addressfield.migrate.inc',
            1 => 'views/addressfield_views_handler_field_country.inc',
            2 => 'views/addressfield_views_handler_filter_country.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'addressfield',
          'datestamp' => '1421426885',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0',
      ),
      'views_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views_export/views_export.module',
        'basename' => 'views_export.module',
        'name' => 'views_export',
        'info' => 
        array (
          'dependencies' => 
          array (
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'views_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views_ui.module',
        'basename' => 'views_ui.module',
        'name' => 'views_ui',
        'info' => 
        array (
          'name' => 'Views UI',
          'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
          'package' => 'Views',
          'core' => '7.x',
          'configure' => 'admin/structure/views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_ui.module',
            1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
          ),
          'version' => '7.x-3.10',
          'project' => 'views',
          'datestamp' => '1423648085',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.10',
      ),
      'views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views/views.module',
        'basename' => 'views.module',
        'name' => 'views',
        'info' => 
        array (
          'name' => 'Views',
          'description' => 'Create customized lists and queries from your database.',
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/views.css',
            ),
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'handlers/views_handler_area.inc',
            1 => 'handlers/views_handler_area_messages.inc',
            2 => 'handlers/views_handler_area_result.inc',
            3 => 'handlers/views_handler_area_text.inc',
            4 => 'handlers/views_handler_area_text_custom.inc',
            5 => 'handlers/views_handler_area_view.inc',
            6 => 'handlers/views_handler_argument.inc',
            7 => 'handlers/views_handler_argument_date.inc',
            8 => 'handlers/views_handler_argument_formula.inc',
            9 => 'handlers/views_handler_argument_many_to_one.inc',
            10 => 'handlers/views_handler_argument_null.inc',
            11 => 'handlers/views_handler_argument_numeric.inc',
            12 => 'handlers/views_handler_argument_string.inc',
            13 => 'handlers/views_handler_argument_group_by_numeric.inc',
            14 => 'handlers/views_handler_field.inc',
            15 => 'handlers/views_handler_field_counter.inc',
            16 => 'handlers/views_handler_field_boolean.inc',
            17 => 'handlers/views_handler_field_contextual_links.inc',
            18 => 'handlers/views_handler_field_custom.inc',
            19 => 'handlers/views_handler_field_date.inc',
            20 => 'handlers/views_handler_field_entity.inc',
            21 => 'handlers/views_handler_field_markup.inc',
            22 => 'handlers/views_handler_field_math.inc',
            23 => 'handlers/views_handler_field_numeric.inc',
            24 => 'handlers/views_handler_field_prerender_list.inc',
            25 => 'handlers/views_handler_field_time_interval.inc',
            26 => 'handlers/views_handler_field_serialized.inc',
            27 => 'handlers/views_handler_field_machine_name.inc',
            28 => 'handlers/views_handler_field_url.inc',
            29 => 'handlers/views_handler_filter.inc',
            30 => 'handlers/views_handler_filter_boolean_operator.inc',
            31 => 'handlers/views_handler_filter_boolean_operator_string.inc',
            32 => 'handlers/views_handler_filter_combine.inc',
            33 => 'handlers/views_handler_filter_date.inc',
            34 => 'handlers/views_handler_filter_equality.inc',
            35 => 'handlers/views_handler_filter_entity_bundle.inc',
            36 => 'handlers/views_handler_filter_group_by_numeric.inc',
            37 => 'handlers/views_handler_filter_in_operator.inc',
            38 => 'handlers/views_handler_filter_many_to_one.inc',
            39 => 'handlers/views_handler_filter_numeric.inc',
            40 => 'handlers/views_handler_filter_string.inc',
            41 => 'handlers/views_handler_filter_fields_compare.inc',
            42 => 'handlers/views_handler_relationship.inc',
            43 => 'handlers/views_handler_relationship_groupwise_max.inc',
            44 => 'handlers/views_handler_sort.inc',
            45 => 'handlers/views_handler_sort_date.inc',
            46 => 'handlers/views_handler_sort_formula.inc',
            47 => 'handlers/views_handler_sort_group_by_numeric.inc',
            48 => 'handlers/views_handler_sort_menu_hierarchy.inc',
            49 => 'handlers/views_handler_sort_random.inc',
            50 => 'includes/base.inc',
            51 => 'includes/handlers.inc',
            52 => 'includes/plugins.inc',
            53 => 'includes/view.inc',
            54 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
            55 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
            56 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
            57 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
            58 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
            59 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
            60 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
            61 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
            62 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
            63 => 'modules/book/views_plugin_argument_default_book_root.inc',
            64 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
            65 => 'modules/comment/views_handler_field_comment.inc',
            66 => 'modules/comment/views_handler_field_comment_depth.inc',
            67 => 'modules/comment/views_handler_field_comment_link.inc',
            68 => 'modules/comment/views_handler_field_comment_link_approve.inc',
            69 => 'modules/comment/views_handler_field_comment_link_delete.inc',
            70 => 'modules/comment/views_handler_field_comment_link_edit.inc',
            71 => 'modules/comment/views_handler_field_comment_link_reply.inc',
            72 => 'modules/comment/views_handler_field_comment_node_link.inc',
            73 => 'modules/comment/views_handler_field_comment_username.inc',
            74 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
            75 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
            76 => 'modules/comment/views_handler_field_node_comment.inc',
            77 => 'modules/comment/views_handler_field_node_new_comments.inc',
            78 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
            79 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
            80 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
            81 => 'modules/comment/views_handler_filter_node_comment.inc',
            82 => 'modules/comment/views_handler_sort_comment_thread.inc',
            83 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
            84 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
            85 => 'modules/comment/views_plugin_row_comment_rss.inc',
            86 => 'modules/comment/views_plugin_row_comment_view.inc',
            87 => 'modules/contact/views_handler_field_contact_link.inc',
            88 => 'modules/field/views_handler_field_field.inc',
            89 => 'modules/field/views_handler_relationship_entity_reverse.inc',
            90 => 'modules/field/views_handler_argument_field_list.inc',
            91 => 'modules/field/views_handler_filter_field_list_boolean.inc',
            92 => 'modules/field/views_handler_argument_field_list_string.inc',
            93 => 'modules/field/views_handler_filter_field_list.inc',
            94 => 'modules/filter/views_handler_field_filter_format_name.inc',
            95 => 'modules/locale/views_handler_field_node_language.inc',
            96 => 'modules/locale/views_handler_filter_node_language.inc',
            97 => 'modules/locale/views_handler_argument_locale_group.inc',
            98 => 'modules/locale/views_handler_argument_locale_language.inc',
            99 => 'modules/locale/views_handler_field_locale_group.inc',
            100 => 'modules/locale/views_handler_field_locale_language.inc',
            101 => 'modules/locale/views_handler_field_locale_link_edit.inc',
            102 => 'modules/locale/views_handler_filter_locale_group.inc',
            103 => 'modules/locale/views_handler_filter_locale_language.inc',
            104 => 'modules/locale/views_handler_filter_locale_version.inc',
            105 => 'modules/node/views_handler_argument_dates_various.inc',
            106 => 'modules/node/views_handler_argument_node_language.inc',
            107 => 'modules/node/views_handler_argument_node_nid.inc',
            108 => 'modules/node/views_handler_argument_node_type.inc',
            109 => 'modules/node/views_handler_argument_node_vid.inc',
            110 => 'modules/node/views_handler_argument_node_uid_revision.inc',
            111 => 'modules/node/views_handler_field_history_user_timestamp.inc',
            112 => 'modules/node/views_handler_field_node.inc',
            113 => 'modules/node/views_handler_field_node_link.inc',
            114 => 'modules/node/views_handler_field_node_link_delete.inc',
            115 => 'modules/node/views_handler_field_node_link_edit.inc',
            116 => 'modules/node/views_handler_field_node_revision.inc',
            117 => 'modules/node/views_handler_field_node_revision_link.inc',
            118 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
            119 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
            120 => 'modules/node/views_handler_field_node_path.inc',
            121 => 'modules/node/views_handler_field_node_type.inc',
            122 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
            123 => 'modules/node/views_handler_filter_node_access.inc',
            124 => 'modules/node/views_handler_filter_node_status.inc',
            125 => 'modules/node/views_handler_filter_node_type.inc',
            126 => 'modules/node/views_handler_filter_node_uid_revision.inc',
            127 => 'modules/node/views_plugin_argument_default_node.inc',
            128 => 'modules/node/views_plugin_argument_validate_node.inc',
            129 => 'modules/node/views_plugin_row_node_rss.inc',
            130 => 'modules/node/views_plugin_row_node_view.inc',
            131 => 'modules/profile/views_handler_field_profile_date.inc',
            132 => 'modules/profile/views_handler_field_profile_list.inc',
            133 => 'modules/profile/views_handler_filter_profile_selection.inc',
            134 => 'modules/search/views_handler_argument_search.inc',
            135 => 'modules/search/views_handler_field_search_score.inc',
            136 => 'modules/search/views_handler_filter_search.inc',
            137 => 'modules/search/views_handler_sort_search_score.inc',
            138 => 'modules/search/views_plugin_row_search_view.inc',
            139 => 'modules/statistics/views_handler_field_accesslog_path.inc',
            140 => 'modules/system/views_handler_argument_file_fid.inc',
            141 => 'modules/system/views_handler_field_file.inc',
            142 => 'modules/system/views_handler_field_file_extension.inc',
            143 => 'modules/system/views_handler_field_file_filemime.inc',
            144 => 'modules/system/views_handler_field_file_uri.inc',
            145 => 'modules/system/views_handler_field_file_status.inc',
            146 => 'modules/system/views_handler_filter_file_status.inc',
            147 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
            148 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
            149 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
            150 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
            151 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
            152 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
            153 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
            154 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
            155 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
            156 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
            157 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
            158 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
            159 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
            160 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
            161 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
            162 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
            163 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
            164 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
            165 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
            166 => 'modules/system/views_handler_filter_system_type.inc',
            167 => 'modules/translation/views_handler_argument_node_tnid.inc',
            168 => 'modules/translation/views_handler_field_node_link_translate.inc',
            169 => 'modules/translation/views_handler_field_node_translation_link.inc',
            170 => 'modules/translation/views_handler_filter_node_tnid.inc',
            171 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
            172 => 'modules/translation/views_handler_relationship_translation.inc',
            173 => 'modules/user/views_handler_argument_user_uid.inc',
            174 => 'modules/user/views_handler_argument_users_roles_rid.inc',
            175 => 'modules/user/views_handler_field_user.inc',
            176 => 'modules/user/views_handler_field_user_language.inc',
            177 => 'modules/user/views_handler_field_user_link.inc',
            178 => 'modules/user/views_handler_field_user_link_cancel.inc',
            179 => 'modules/user/views_handler_field_user_link_edit.inc',
            180 => 'modules/user/views_handler_field_user_mail.inc',
            181 => 'modules/user/views_handler_field_user_name.inc',
            182 => 'modules/user/views_handler_field_user_permissions.inc',
            183 => 'modules/user/views_handler_field_user_picture.inc',
            184 => 'modules/user/views_handler_field_user_roles.inc',
            185 => 'modules/user/views_handler_filter_user_current.inc',
            186 => 'modules/user/views_handler_filter_user_name.inc',
            187 => 'modules/user/views_handler_filter_user_permissions.inc',
            188 => 'modules/user/views_handler_filter_user_roles.inc',
            189 => 'modules/user/views_plugin_argument_default_current_user.inc',
            190 => 'modules/user/views_plugin_argument_default_user.inc',
            191 => 'modules/user/views_plugin_argument_validate_user.inc',
            192 => 'modules/user/views_plugin_row_user_view.inc',
            193 => 'plugins/views_plugin_access.inc',
            194 => 'plugins/views_plugin_access_none.inc',
            195 => 'plugins/views_plugin_access_perm.inc',
            196 => 'plugins/views_plugin_access_role.inc',
            197 => 'plugins/views_plugin_argument_default.inc',
            198 => 'plugins/views_plugin_argument_default_php.inc',
            199 => 'plugins/views_plugin_argument_default_fixed.inc',
            200 => 'plugins/views_plugin_argument_default_raw.inc',
            201 => 'plugins/views_plugin_argument_validate.inc',
            202 => 'plugins/views_plugin_argument_validate_numeric.inc',
            203 => 'plugins/views_plugin_argument_validate_php.inc',
            204 => 'plugins/views_plugin_cache.inc',
            205 => 'plugins/views_plugin_cache_none.inc',
            206 => 'plugins/views_plugin_cache_time.inc',
            207 => 'plugins/views_plugin_display.inc',
            208 => 'plugins/views_plugin_display_attachment.inc',
            209 => 'plugins/views_plugin_display_block.inc',
            210 => 'plugins/views_plugin_display_default.inc',
            211 => 'plugins/views_plugin_display_embed.inc',
            212 => 'plugins/views_plugin_display_extender.inc',
            213 => 'plugins/views_plugin_display_feed.inc',
            214 => 'plugins/views_plugin_display_page.inc',
            215 => 'plugins/views_plugin_exposed_form_basic.inc',
            216 => 'plugins/views_plugin_exposed_form.inc',
            217 => 'plugins/views_plugin_exposed_form_input_required.inc',
            218 => 'plugins/views_plugin_localization_core.inc',
            219 => 'plugins/views_plugin_localization.inc',
            220 => 'plugins/views_plugin_localization_none.inc',
            221 => 'plugins/views_plugin_pager.inc',
            222 => 'plugins/views_plugin_pager_full.inc',
            223 => 'plugins/views_plugin_pager_mini.inc',
            224 => 'plugins/views_plugin_pager_none.inc',
            225 => 'plugins/views_plugin_pager_some.inc',
            226 => 'plugins/views_plugin_query.inc',
            227 => 'plugins/views_plugin_query_default.inc',
            228 => 'plugins/views_plugin_row.inc',
            229 => 'plugins/views_plugin_row_fields.inc',
            230 => 'plugins/views_plugin_row_rss_fields.inc',
            231 => 'plugins/views_plugin_style.inc',
            232 => 'plugins/views_plugin_style_default.inc',
            233 => 'plugins/views_plugin_style_grid.inc',
            234 => 'plugins/views_plugin_style_list.inc',
            235 => 'plugins/views_plugin_style_jump_menu.inc',
            236 => 'plugins/views_plugin_style_mapping.inc',
            237 => 'plugins/views_plugin_style_rss.inc',
            238 => 'plugins/views_plugin_style_summary.inc',
            239 => 'plugins/views_plugin_style_summary_jump_menu.inc',
            240 => 'plugins/views_plugin_style_summary_unformatted.inc',
            241 => 'plugins/views_plugin_style_table.inc',
            242 => 'tests/handlers/views_handlers.test',
            243 => 'tests/handlers/views_handler_area_text.test',
            244 => 'tests/handlers/views_handler_argument_null.test',
            245 => 'tests/handlers/views_handler_argument_string.test',
            246 => 'tests/handlers/views_handler_field.test',
            247 => 'tests/handlers/views_handler_field_boolean.test',
            248 => 'tests/handlers/views_handler_field_custom.test',
            249 => 'tests/handlers/views_handler_field_counter.test',
            250 => 'tests/handlers/views_handler_field_date.test',
            251 => 'tests/handlers/views_handler_field_file_extension.test',
            252 => 'tests/handlers/views_handler_field_file_size.test',
            253 => 'tests/handlers/views_handler_field_math.test',
            254 => 'tests/handlers/views_handler_field_url.test',
            255 => 'tests/handlers/views_handler_field_xss.test',
            256 => 'tests/handlers/views_handler_filter_combine.test',
            257 => 'tests/handlers/views_handler_filter_date.test',
            258 => 'tests/handlers/views_handler_filter_equality.test',
            259 => 'tests/handlers/views_handler_filter_in_operator.test',
            260 => 'tests/handlers/views_handler_filter_numeric.test',
            261 => 'tests/handlers/views_handler_filter_string.test',
            262 => 'tests/handlers/views_handler_sort_random.test',
            263 => 'tests/handlers/views_handler_sort_date.test',
            264 => 'tests/handlers/views_handler_sort.test',
            265 => 'tests/test_handlers/views_test_area_access.inc',
            266 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
            267 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
            268 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
            269 => 'tests/plugins/views_plugin_display.test',
            270 => 'tests/styles/views_plugin_style_jump_menu.test',
            271 => 'tests/styles/views_plugin_style.test',
            272 => 'tests/styles/views_plugin_style_base.test',
            273 => 'tests/styles/views_plugin_style_mapping.test',
            274 => 'tests/styles/views_plugin_style_unformatted.test',
            275 => 'tests/views_access.test',
            276 => 'tests/views_analyze.test',
            277 => 'tests/views_basic.test',
            278 => 'tests/views_argument_default.test',
            279 => 'tests/views_argument_validator.test',
            280 => 'tests/views_exposed_form.test',
            281 => 'tests/field/views_fieldapi.test',
            282 => 'tests/views_glossary.test',
            283 => 'tests/views_groupby.test',
            284 => 'tests/views_handlers.test',
            285 => 'tests/views_module.test',
            286 => 'tests/views_pager.test',
            287 => 'tests/views_plugin_localization_test.inc',
            288 => 'tests/views_translatable.test',
            289 => 'tests/views_query.test',
            290 => 'tests/views_upgrade.test',
            291 => 'tests/views_test.views_default.inc',
            292 => 'tests/comment/views_handler_argument_comment_user_uid.test',
            293 => 'tests/comment/views_handler_filter_comment_user_uid.test',
            294 => 'tests/node/views_node_revision_relations.test',
            295 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
            296 => 'tests/user/views_handler_field_user_name.test',
            297 => 'tests/user/views_user_argument_default.test',
            298 => 'tests/user/views_user_argument_validate.test',
            299 => 'tests/user/views_user.test',
            300 => 'tests/views_cache.test',
            301 => 'tests/views_view.test',
            302 => 'tests/views_ui.test',
          ),
          'version' => '7.x-3.10',
          'project' => 'views',
          'datestamp' => '1423648085',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.10',
      ),
      'node_view_permissions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/node_view_permissions/node_view_permissions.module',
        'basename' => 'node_view_permissions.module',
        'name' => 'node_view_permissions',
        'info' => 
        array (
          'name' => 'Node view permissions',
          'description' => 'Enables permissions "View own content" and "View any content" for each content type.',
          'package' => 'Access control',
          'configure' => 'admin/config/content/node-view-permissions',
          'core' => '7.x',
          'version' => '7.x-1.5',
          'project' => 'node_view_permissions',
          'datestamp' => '1404800028',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'password_policy_password_tab' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/password_policy/contrib/password_tab/password_policy_password_tab.module',
        'basename' => 'password_policy_password_tab.module',
        'name' => 'password_policy_password_tab',
        'info' => 
        array (
          'name' => 'Password change tab',
          'description' => 'Implements a separate password change tab.',
          'package' => 'Other',
          'dependencies' => 
          array (
            0 => 'password_policy',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'password_policy_password_tab.module',
            1 => 'password_policy_password_tab.install',
            2 => 'password_policy_password_tab.admin.inc',
            3 => 'password_policy_password_tab.pages.inc',
            4 => 'password_policy_password_tab.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'password_policy',
          'datestamp' => '1376497420',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'password_policy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/password_policy/password_policy.module',
        'basename' => 'password_policy.module',
        'name' => 'password_policy',
        'info' => 
        array (
          'name' => 'Password policy',
          'description' => 'The password policy module allows you to enforce a specific level of password complexity for the user passwords on the system.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'password_policy.module',
            1 => 'password_policy.install',
            2 => 'password_policy.admin.inc',
            3 => 'password_policy.theme.inc',
            4 => 'tests/password_policy.test',
            5 => 'tests/password_policy_expiration.test',
          ),
          'configure' => 'admin/config/people/password_policy',
          'version' => '7.x-1.5',
          'project' => 'password_policy',
          'datestamp' => '1376497420',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.5',
      ),
      'paymentform' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentform/paymentform.module',
        'basename' => 'paymentform.module',
        'name' => 'paymentform',
        'info' => 
        array (
          'name' => 'Payment Form Field',
          'description' => 'Provides a field to add payment forms to entities.',
          'dependencies' => 
          array (
            0 => 'currency_api',
            1 => 'payment',
          ),
          'package' => 'Payment',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/paymentform.test',
            1 => 'tests/PaymentformWebTestCase.test',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'paymentmethodbasic' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentmethodbasic/paymentmethodbasic.module',
        'basename' => 'paymentmethodbasic.module',
        'name' => 'paymentmethodbasic',
        'info' => 
        array (
          'name' => 'Basic Payment Method',
          'description' => 'A \'dumb\' payment method type that always successfully executes payments, but never actually transfers money. It can be useful for <em>collect on delivery</em>, for instance.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'payment',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'paymentmethodbasic.module',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.14',
      ),
      'paymentreference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/modules/paymentreference/paymentreference.module',
        'basename' => 'paymentreference.module',
        'name' => 'paymentreference',
        'info' => 
        array (
          'name' => 'Payment Reference Field',
          'description' => 'Provides a field to add payments to entities.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'currency_api',
            1 => 'field',
            2 => 'payment',
          ),
          'package' => 'Payment',
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.14',
      ),
      'payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment/payment.module',
        'basename' => 'payment.module',
        'name' => 'payment',
        'info' => 
        array (
          'name' => 'Payment',
          'description' => 'Allows payments to be made using any of the available payment methods.',
          'core' => '7.x',
          'configure' => 'admin/config/services/payment',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'payment.classes.inc',
            1 => 'payment.generate.inc',
            2 => 'views/PaymentViewsHandlerAccessOwnPaymentOverview.inc',
            3 => 'views/PaymentViewsHandlerFieldPaymentMethodControllerTitle.inc',
            4 => 'views/PaymentViewsHandlerFieldPaymentMethodControllerDescription.inc',
            5 => 'views/PaymentViewsHandlerFieldPaymentMethodEnabledMachineName.inc',
            6 => 'views/PaymentViewsHandlerFieldPaymentMethodOperations.inc',
            7 => 'views/PaymentViewsHandlerFieldPaymentStatusTitle.inc',
            8 => 'views/PaymentViewsHandlerFieldPaymentOperations.inc',
            9 => 'views/PaymentViewsHandlerFieldTranslatableString.inc',
            10 => 'views/PaymentViewsHandlerFilterPaymentMethodControllerTitle.inc',
            11 => 'views/PaymentViewsHandlerFilterPaymentMethodTitle.inc',
            12 => 'views/PaymentViewsHandlerFilterPaymentStatusItemStatus.inc',
            13 => 'tests/PaymentWebTestCase.test',
          ),
          'version' => '7.x-1.14',
          'project' => 'payment',
          'datestamp' => '1425414489',
          'php' => '5.2.4',
        ),
        'schema_version' => '7105',
        'version' => '7.x-1.14',
      ),
      'strongarm' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/strongarm/strongarm.module',
        'basename' => 'strongarm.module',
        'name' => 'strongarm',
        'info' => 
        array (
          'name' => 'Strongarm',
          'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'strongarm.admin.inc',
            1 => 'strongarm.install',
            2 => 'strongarm.module',
          ),
          'version' => '7.x-2.0',
          'project' => 'strongarm',
          'datestamp' => '1339604214',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0',
      ),
      'commerce_discount_date' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/modules/commerce_discount_date/commerce_discount_date.module',
        'basename' => 'commerce_discount_date.module',
        'name' => 'commerce_discount_date',
        'info' => 
        array (
          'name' => 'Commerce Discount Date',
          'description' => 'Provides date fields for the Commerce discount entity.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_discount',
            1 => 'date',
            2 => 'date_popup',
          ),
          'files' => 
          array (
            0 => 'commerce_discount_date.test',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_discount_usage' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/modules/commerce_discount_usage/commerce_discount_usage.module',
        'basename' => 'commerce_discount_usage.module',
        'name' => 'commerce_discount_usage',
        'info' => 
        array (
          'name' => 'Commerce Discount Usage',
          'description' => 'Provide usage tracking and limits for Commerce Discounts',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_discount',
          ),
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_discount_usage_handler_field_commerce_discount_analytics.inc',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_discount' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_discount/commerce_discount.module',
        'basename' => 'commerce_discount.module',
        'name' => 'commerce_discount',
        'info' => 
        array (
          'name' => 'Commerce Discount',
          'description' => 'Provides common functionality and a UI for managing discounts.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'number',
            1 => 'commerce',
            2 => 'commerce_price',
            3 => 'commerce_line_item',
            4 => 'commerce_product_reference',
            5 => 'entity',
            6 => 'entityreference',
            7 => 'inline_entity_form (>=1.5)',
            8 => 'views',
            9 => 'inline_conditions',
          ),
          'files' => 
          array (
            0 => 'commerce_discount.info.inc',
            1 => 'includes/commerce_discount.admin.inc',
            2 => 'includes/commerce_discount.class.inc',
            3 => 'includes/commerce_discount_offer.class.inc',
            4 => 'includes/commerce_discount_offer.inline_entity_form.inc',
            5 => 'includes/commerce_discount.controller.inc',
            6 => 'includes/views/commerce_discount.views.inc',
            7 => 'includes/views/handlers/commerce_discount_handler_field_operations_dropbutton.inc',
            8 => 'commerce_discount.test',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'commerce_discount',
          'datestamp' => '1405675128',
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.0-alpha4',
      ),
      'commerce_custom_product' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_custom_product/commerce_custom_product.module',
        'basename' => 'commerce_custom_product.module',
        'name' => 'commerce_custom_product',
        'info' => 
        array (
          'name' => 'Customizable Products',
          'description' => 'Adds features to support the creation of customizable products.',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_line_item',
            1 => 'commerce_line_item_ui',
            2 => 'commerce_product',
            3 => 'commerce_product_reference',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta2',
          'project' => 'commerce_custom_product',
          'datestamp' => '1347049622',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'custom_meta' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/custom_meta/custom_meta.module',
        'basename' => 'custom_meta.module',
        'name' => 'custom_meta',
        'info' => 
        array (
          'name' => 'Custom Meta',
          'description' => 'Adds user defined custom meta tags.',
          'package' => 'Third Party Integration',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'custom_meta.module',
          ),
          'version' => '7.x-1.4',
          'project' => 'custom_meta',
          'datestamp' => '1405783728',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'navbar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/navbar/navbar.module',
        'basename' => 'navbar.module',
        'name' => 'navbar',
        'info' => 
        array (
          'name' => 'Mobile Friendly Navigation Toolbar',
          'description' => 'A very simple mobile friendly toolbar that lets you switch between frontend and backend.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'libraries (>=2.0)',
          ),
          'version' => '7.x-1.5',
          'project' => 'navbar',
          'datestamp' => '1419363481',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.5',
      ),
      'commerce_product_attributes' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_product_attributes/commerce_product_attributes.module',
        'basename' => 'commerce_product_attributes.module',
        'name' => 'commerce_product_attributes',
        'info' => 
        array (
          'name' => 'Product Attributes',
          'description' => 'Enables the display of product attributes in the shopping cart view.',
          'package' => 'Commerce Product Enhancements',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_product_attributes_handler_field_attributes.inc',
          ),
          'version' => '7.x-1.0-beta3',
          'project' => 'commerce_product_attributes',
          'datestamp' => '1316355701',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta3',
      ),
      'file_entity_inline' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/file_entity_inline/file_entity_inline.module',
        'basename' => 'file_entity_inline.module',
        'name' => 'file_entity_inline',
        'info' => 
        array (
          'name' => 'File entity inline',
          'description' => 'Makes field entities editable within other entities.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file_entity',
            1 => 'field',
            2 => 'ctools',
          ),
          'configure' => 'admin/structure/types',
          'version' => '7.x-1.0-beta1',
          'project' => 'file_entity_inline',
          'datestamp' => '1348910169',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'menu_position' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_position/menu_position.module',
        'basename' => 'menu_position.module',
        'name' => 'menu_position',
        'info' => 
        array (
          'name' => 'Menu position',
          'description' => 'Customize menu position of nodes depending on their content type, associated terms and others conditions.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'menu (>7.11)',
          ),
          'files' => 
          array (
            0 => 'menu_position.module',
            1 => 'menu_position.admin.inc',
            2 => 'menu_position.install',
            3 => 'menu_position.node_type.inc',
          ),
          'configure' => 'admin/structure/menu-position',
          'version' => '7.x-1.1',
          'project' => 'menu_position',
          'datestamp' => '1329911144',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.1',
      ),
      'html5_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/html5_tools/html5_tools.module',
        'basename' => 'html5_tools.module',
        'name' => 'html5_tools',
        'info' => 
        array (
          'name' => 'HTML5 Tools',
          'description' => 'Provides a set of tools to allow sites to be built using HTML5.',
          'core' => '7.x',
          'php' => '5',
          'package' => 'Markup',
          'dependencies' => 
          array (
            0 => 'elements',
            1 => 'field',
          ),
          'configure' => 'admin/config/development/html5-tools',
          'version' => '7.x-1.2',
          'project' => 'html5_tools',
          'datestamp' => '1336411555',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'computed_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/computed_field/computed_field.module',
        'basename' => 'computed_field.module',
        'name' => 'computed_field',
        'info' => 
        array (
          'name' => 'Computed Field',
          'description' => 'Defines a field type that allows values to be "computed" via PHP code.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'computed_field.install',
            1 => 'computed_field.module',
          ),
          'version' => '7.x-1.0',
          'project' => 'computed_field',
          'datestamp' => '1386094705',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'stage_file_proxy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/stage_file_proxy/stage_file_proxy.module',
        'basename' => 'stage_file_proxy.module',
        'name' => 'stage_file_proxy',
        'info' => 
        array (
          'name' => 'Stage File Proxy',
          'description' => 'Proxies files from production server so you don\'t have to transfer them manually',
          'core' => '7.x',
          'configure' => 'admin/config/system/stage_file_proxy',
          'version' => '7.x-1.7',
          'project' => 'stage_file_proxy',
          'datestamp' => '1428604383',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'imagecache_coloractions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/coloractions/imagecache_coloractions.module',
        'basename' => 'imagecache_coloractions.module',
        'name' => 'imagecache_coloractions',
        'info' => 
        array (
          'name' => 'Imagecache Color Actions',
          'description' => 'Provides image effects color-shifting, invert colors, brightness, posterize and alpha transparency effects. Also provides a change image format effect.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'files' => 
          array (
            0 => 'imagecache_coloractions.install',
            1 => 'imagecache_coloractions.module',
            2 => 'transparency.inc',
            3 => 'tests/green.imagecache_preset.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_styles_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_styles_admin/image_styles_admin.module',
        'basename' => 'image_styles_admin.module',
        'name' => 'image_styles_admin',
        'info' => 
        array (
          'name' => 'Image styles admin',
          'description' => 'Provides additional administrative functionality to duplicate, export or import image styles.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_effects_text_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text_test/image_effects_text_test.module',
        'basename' => 'image_effects_text_test.module',
        'name' => 'image_effects_text_test',
        'info' => 
        array (
          'name' => 'Image Effects Text test',
          'description' => 'Defines image styles that test the text effect.',
          'core' => '7.x',
          'package' => 'Media',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'image_effects_text',
            2 => 'imagecache_canvasactions',
            3 => 'system_stream_wrapper',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'image_effects_text' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text.module',
        'basename' => 'image_effects_text.module',
        'name' => 'image_effects_text',
        'info' => 
        array (
          'name' => 'Image Effects Text',
          'description' => 'Provides an image effect to overlay text captions on images.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'imagecache_actions',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_autorotate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/autorotate/imagecache_autorotate.module',
        'basename' => 'imagecache_autorotate.module',
        'name' => 'imagecache_autorotate',
        'info' => 
        array (
          'name' => 'Imagecache Autorotate',
          'description' => 'Provides an image effect to autorotate an image based on EXIF data.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_customactions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/customactions/imagecache_customactions.module',
        'basename' => 'imagecache_customactions.module',
        'name' => 'imagecache_customactions',
        'info' => 
        array (
          'name' => 'Imagecache Custom Actions',
          'description' => 'Provides the custom and subroutine image effects.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.5',
      ),
      'imagecache_testsuite' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/tests/imagecache_testsuite.module',
        'basename' => 'imagecache_testsuite.module',
        'name' => 'imagecache_testsuite',
        'info' => 
        array (
          'name' => 'Imagecache_actions Test Suite',
          'description' => 'Displays a collection of demo image styles.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'system_stream_wrapper',
          ),
          'features' => 
          array (
            'image' => 
            array (
              0 => 'corners_combo',
            ),
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_canvasactions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/canvasactions/imagecache_canvasactions.module',
        'basename' => 'imagecache_canvasactions.module',
        'name' => 'imagecache_canvasactions',
        'info' => 
        array (
          'name' => 'Imagecache Canvas Actions',
          'description' => 'Provides image effects for manipulating image canvases: define canvas, image mask, watermark, underlay background image, rounded corners, composite source to image and resize by percent effect. Also provides an aspect switcher (portrait/landscape).',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'imagecache_actions',
            1 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'imagecache_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_actions/imagecache_actions.module',
        'basename' => 'imagecache_actions.module',
        'name' => 'imagecache_actions',
        'info' => 
        array (
          'name' => 'Imagecache Actions',
          'description' => 'Provides utility code for a number of additional image effects that can be found in the sub modules.',
          'package' => 'Media',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'version' => '7.x-1.5',
          'project' => 'imagecache_actions',
          'datestamp' => '1417256282',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.5',
      ),
      'current_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi/contrib/current_search/current_search.module',
        'basename' => 'current_search.module',
        'name' => 'current_search',
        'info' => 
        array (
          'name' => 'Current Search Blocks',
          'description' => 'Provides an interface for creating blocks containing information about the current search.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/current_search/item.inc',
            1 => 'plugins/current_search/item_active.inc',
            2 => 'plugins/current_search/item_group.inc',
            3 => 'plugins/current_search/item_text.inc',
            4 => 'tests/current_search.test',
          ),
          'configure' => 'admin/config/search/current_search',
          'version' => '7.x-1.5',
          'project' => 'facetapi',
          'datestamp' => '1405685332',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.5',
      ),
      'facetapi' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi/facetapi.module',
        'basename' => 'facetapi.module',
        'name' => 'facetapi',
        'info' => 
        array (
          'name' => 'Facet API',
          'description' => 'An abstracted facet API that can be used by various search backends.',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Search Toolkit',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/facetapi/adapter.inc',
            1 => 'plugins/facetapi/dependency.inc',
            2 => 'plugins/facetapi/dependency_bundle.inc',
            3 => 'plugins/facetapi/dependency_role.inc',
            4 => 'plugins/facetapi/empty_behavior.inc',
            5 => 'plugins/facetapi/empty_behavior_text.inc',
            6 => 'plugins/facetapi/filter.inc',
            7 => 'plugins/facetapi/query_type.inc',
            8 => 'plugins/facetapi/url_processor.inc',
            9 => 'plugins/facetapi/url_processor_standard.inc',
            10 => 'plugins/facetapi/widget.inc',
            11 => 'plugins/facetapi/widget_links.inc',
            12 => 'tests/facetapi_test.plugins.inc',
            13 => 'tests/facetapi.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'facetapi',
          'datestamp' => '1405685332',
          'php' => '5.2.4',
        ),
        'schema_version' => '7103',
        'version' => '7.x-1.5',
      ),
      'backup_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/backup_migrate/backup_migrate.module',
        'basename' => 'backup_migrate.module',
        'name' => 'backup_migrate',
        'info' => 
        array (
          'name' => 'Backup and Migrate',
          'description' => 'Backup or migrate the Drupal Database quickly and without unnecessary data.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'backup_migrate.module',
            1 => 'backup_migrate.install',
            2 => 'includes/destinations.inc',
            3 => 'includes/profiles.inc',
            4 => 'includes/schedules.inc',
          ),
          'configure' => 'admin/config/system/backup_migrate',
          'version' => '7.x-2.8',
          'project' => 'backup_migrate',
          'datestamp' => '1383686305',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7205',
        'version' => '7.x-2.8',
      ),
      'custom_pub' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/custom_pub/custom_pub.module',
        'basename' => 'custom_pub.module',
        'name' => 'custom_pub',
        'info' => 
        array (
          'name' => 'Custom Publishing Options',
          'description' => 'Adds the ability to add Custom publishing options to the node Add/Edit forms.',
          'core' => '7.x',
          'package' => 'Other',
          'suggests' => 
          array (
            0 => 'override_node_options',
            1 => 'views',
          ),
          'version' => '7.x-1.3',
          'project' => 'custom_pub',
          'datestamp' => '1370879152',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.3',
      ),
      'taxonomy_display' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/taxonomy_display/taxonomy_display.module',
        'basename' => 'taxonomy_display.module',
        'name' => 'taxonomy_display',
        'info' => 
        array (
          'name' => 'Taxonomy display',
          'description' => 'Modify the display of taxonomy term pages per vocabulary.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'taxonomy_display.module',
            1 => 'classes/associated_display.inc',
            2 => 'classes/breadcrumb_display.inc',
            3 => 'classes/handler_form.inc',
            4 => 'classes/term_display.inc',
            5 => 'handlers/term/core.inc',
            6 => 'handlers/term/hidden.inc',
            7 => 'handlers/associated/core.inc',
            8 => 'handlers/associated/hidden.inc',
            9 => 'handlers/associated/views.inc',
            10 => 'handlers/breadcrumb/core.inc',
            11 => 'handlers/breadcrumb/hidden.inc',
            12 => 'handlers/breadcrumb/ignore.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'taxonomy_display',
          'datestamp' => '1351568833',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.x-1.1',
      ),
      'responsive_favicons' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/responsive_favicons/responsive_favicons.module',
        'basename' => 'responsive_favicons.module',
        'name' => 'responsive_favicons',
        'info' => 
        array (
          'name' => 'Responsive Favicons',
          'description' => 'Add responsive favicons to your site based on the code from http://realfavicongenerator.net/',
          'core' => '7.x',
          'configure' => 'admin/config/user-interface/responsive_favicons',
          'version' => '7.x-1.2',
          'project' => 'responsive_favicons',
          'datestamp' => '1442890144',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.2',
      ),
      'devel_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel_generate/devel_generate.module',
        'basename' => 'devel_generate.module',
        'name' => 'devel_generate',
        'info' => 
        array (
          'name' => 'Devel generate',
          'description' => 'Generate dummy users, nodes, and taxonomy terms.',
          'package' => 'Development',
          'core' => '7.x',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'configure' => 'admin/config/development/generate',
          'files' => 
          array (
            0 => 'devel_generate.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel.module',
        'basename' => 'devel.module',
        'name' => 'devel',
        'info' => 
        array (
          'name' => 'Devel',
          'description' => 'Various blocks, pages, and functions for developers.',
          'package' => 'Development',
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'files' => 
          array (
            0 => 'devel.test',
            1 => 'devel.mail.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7006',
        'version' => '7.x-1.5',
      ),
      'devel_node_access' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/devel/devel_node_access.module',
        'basename' => 'devel_node_access.module',
        'name' => 'devel_node_access',
        'info' => 
        array (
          'name' => 'Devel node access',
          'description' => 'Developer blocks and page illustrating relevant node_access records.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'version' => '7.x-1.5',
          'project' => 'devel',
          'datestamp' => '1398963366',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'telephone' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/telephone/telephone.module',
        'basename' => 'telephone.module',
        'name' => 'telephone',
        'info' => 
        array (
          'name' => 'Telephone',
          'description' => 'Defines a field type for telephone numbers.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'telephone.migrate.inc',
          ),
          'version' => '7.x-1.0-alpha1',
          'project' => 'telephone',
          'datestamp' => '1389736105',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha1',
      ),
      'workflow_extensions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow_extensions/workflow_extensions.module',
        'basename' => 'workflow_extensions.module',
        'name' => 'workflow_extensions',
        'info' => 
        array (
          'name' => 'Workflow extensions',
          'description' => 'Various UI improvements for the Workflow module. In particular the replacement of the workflow radio buttons by either a drop-down or more intuitive single-action buttons with configurable labels.',
          'core' => '7.x',
          'package' => 'Workflow',
          'configure' => 'admin/config/workflow/workflow_extensions',
          'files' => 
          array (
            0 => 'views/workflow_extensions_handler_field_state_change_form.inc',
            1 => 'views/workflow_extensions_handler_field_workflow_comment_link_edit.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'workflow_extensions',
          'datestamp' => '1374541185',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'xmlsitemap_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_custom/xmlsitemap_custom.module',
        'basename' => 'xmlsitemap_custom.module',
        'name' => 'xmlsitemap_custom',
        'info' => 
        array (
          'name' => 'XML sitemap custom',
          'description' => 'Adds user configurable links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_custom.module',
            1 => 'xmlsitemap_custom.admin.inc',
            2 => 'xmlsitemap_custom.install',
            3 => 'xmlsitemap_custom.test',
          ),
          'configure' => 'admin/config/search/xmlsitemap/custom',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_i18n/xmlsitemap_i18n.module',
        'basename' => 'xmlsitemap_i18n.module',
        'name' => 'xmlsitemap_i18n',
        'info' => 
        array (
          'name' => 'XML sitemap internationalization',
          'description' => 'Enables multilingual XML sitemaps.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'i18n',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_i18n.module',
            1 => 'xmlsitemap_i18n.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_taxonomy/xmlsitemap_taxonomy.module',
        'basename' => 'xmlsitemap_taxonomy.module',
        'name' => 'xmlsitemap_taxonomy',
        'info' => 
        array (
          'name' => 'XML sitemap taxonomy',
          'description' => 'Add taxonomy term links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'taxonomy',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_taxonomy.module',
            1 => 'xmlsitemap_taxonomy.install',
            2 => 'xmlsitemap_taxonomy.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_menu' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_menu/xmlsitemap_menu.module',
        'basename' => 'xmlsitemap_menu.module',
        'name' => 'xmlsitemap_menu',
        'info' => 
        array (
          'name' => 'XML sitemap menu',
          'description' => 'Adds menu item links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
            1 => 'menu',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_menu.module',
            1 => 'xmlsitemap_menu.install',
            2 => 'xmlsitemap_menu.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6201',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_node' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_node/xmlsitemap_node.module',
        'basename' => 'xmlsitemap_node.module',
        'name' => 'xmlsitemap_node',
        'info' => 
        array (
          'name' => 'XML sitemap node',
          'description' => 'Adds content links to the sitemap.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_node.module',
            1 => 'xmlsitemap_node.install',
            2 => 'xmlsitemap_node.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6201',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_user' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_user/xmlsitemap_user.module',
        'basename' => 'xmlsitemap_user.module',
        'name' => 'xmlsitemap_user',
        'info' => 
        array (
          'name' => 'XML sitemap user',
          'description' => 'Adds user profile links to the sitemap.',
          'package' => 'XML sitemap',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'xmlsitemap_user.module',
            1 => 'xmlsitemap_user.install',
            2 => 'xmlsitemap_user.test',
          ),
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.2',
      ),
      'xmlsitemap_engines' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap_engines/xmlsitemap_engines.module',
        'basename' => 'xmlsitemap_engines.module',
        'name' => 'xmlsitemap_engines',
        'info' => 
        array (
          'name' => 'XML sitemap engines',
          'description' => 'Submit the sitemap to search engines.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'xmlsitemap',
          ),
          'files' => 
          array (
            0 => 'xmlsitemap_engines.module',
            1 => 'xmlsitemap_engines.admin.inc',
            2 => 'xmlsitemap_engines.install',
            3 => 'tests/xmlsitemap_engines.test',
          ),
          'recommends' => 
          array (
            0 => 'site_verify',
          ),
          'configure' => 'admin/config/search/xmlsitemap/engines',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'php' => '5.2.4',
        ),
        'schema_version' => '6202',
        'version' => '7.x-2.2',
      ),
      'xmlsitemap' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/xmlsitemap/xmlsitemap.module',
        'basename' => 'xmlsitemap.module',
        'name' => 'xmlsitemap',
        'info' => 
        array (
          'name' => 'XML sitemap',
          'description' => 'Creates an XML sitemap conforming to the <a href="http://sitemaps.org/">sitemaps.org protocol</a>.',
          'package' => 'XML sitemap',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'xmlsitemap.module',
            1 => 'xmlsitemap.inc',
            2 => 'xmlsitemap.admin.inc',
            3 => 'xmlsitemap.drush.inc',
            4 => 'xmlsitemap.generate.inc',
            5 => 'xmlsitemap.xmlsitemap.inc',
            6 => 'xmlsitemap.pages.inc',
            7 => 'xmlsitemap.install',
            8 => 'xmlsitemap.test',
          ),
          'recommends' => 
          array (
            0 => 'robotstxt',
          ),
          'configure' => 'admin/config/search/xmlsitemap',
          'version' => '7.x-2.2',
          'project' => 'xmlsitemap',
          'datestamp' => '1422607989',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7203',
        'version' => '7.x-2.2',
      ),
      'masquerade' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/masquerade/masquerade.module',
        'basename' => 'masquerade.module',
        'name' => 'masquerade',
        'info' => 
        array (
          'name' => 'Masquerade',
          'description' => 'This module allows permitted users to masquerade as other users.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'masquerade.test',
          ),
          'configure' => 'admin/config/people/masquerade',
          'version' => '7.x-1.0-rc7',
          'project' => 'masquerade',
          'datestamp' => '1394333639',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-rc7',
      ),
      'corresponding_node_references' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/cnr/corresponding_node_references.module',
        'basename' => 'corresponding_node_references.module',
        'name' => 'corresponding_node_references',
        'info' => 
        array (
          'name' => 'Corresponding Node References',
          'description' => 'Syncs the node reference between two node types which have a nodereference to each other.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'node_reference',
            1 => 'ctools',
          ),
          'version' => '7.x-4.22',
          'project' => 'cnr',
          'datestamp' => '1321480838',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-4.22',
      ),
      'views_litepager' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_litepager/views_litepager.module',
        'basename' => 'views_litepager.module',
        'name' => 'views_litepager',
        'info' => 
        array (
          'name' => 'Views Litepager',
          'description' => 'Provide a pager plugin for Views that doesn\'t require an expensive count query',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'core' => '7.x',
          'package' => 'Views',
          'files' => 
          array (
            0 => 'views_litepager_plugin_pager_lite.inc',
          ),
          'version' => '7.x-3.0',
          'project' => 'views_litepager',
          'datestamp' => '1335573365',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.0',
      ),
      'mailchimp_activity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_activity/mailchimp_activity.module',
        'basename' => 'mailchimp_activity.module',
        'name' => 'mailchimp_activity',
        'info' => 
        array (
          'name' => 'MailChimp Activity',
          'description' => 'View activity for an email address associated with any entity.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'mailchimp_activity.entity.inc',
            1 => 'mailchimp_activity.ui_controller.inc',
          ),
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.12',
      ),
      'mailchimp_campaign' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_campaign/mailchimp_campaign.module',
        'basename' => 'mailchimp_campaign.module',
        'name' => 'mailchimp_campaign',
        'info' => 
        array (
          'name' => 'MailChimp Campaigns',
          'description' => 'Create, send and import MailChimp campaigns.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp_lists',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'mailchimp_campaign.admin.inc',
            1 => 'mailchimp_campaign.entity.inc',
          ),
          'configure' => 'admin/config/services/mailchimp/campaigns',
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.12',
      ),
      'mailchimp_lists' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/modules/mailchimp_lists/mailchimp_lists.module',
        'basename' => 'mailchimp_lists.module',
        'name' => 'mailchimp_lists',
        'info' => 
        array (
          'name' => 'MailChimp Lists',
          'description' => 'Manage and integrate MailChimp lists.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mailchimp',
            1 => 'entity',
            2 => 'entity_token',
          ),
          'configure' => 'admin/config/services/mailchimp/lists',
          'files' => 
          array (
            0 => 'lib/mailchimp_lists.entity.inc',
            1 => 'tests/mailchimp_lists.test',
          ),
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7206',
        'version' => '7.x-2.12',
      ),
      'mailchimp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailchimp/mailchimp.module',
        'basename' => 'mailchimp.module',
        'name' => 'mailchimp',
        'info' => 
        array (
          'name' => 'MailChimp',
          'description' => 'MailChimp email service integration.',
          'package' => 'MailChimp',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/mailchimp.inc',
            1 => 'tests/mailchimp_tests.inc',
          ),
          'dependencies' => 
          array (
            0 => 'libraries (>=2)',
          ),
          'configure' => 'admin/config/services/mailchimp',
          'version' => '7.x-2.12',
          'project' => 'mailchimp',
          'datestamp' => '1374771079',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.12',
      ),
      'semantic_ds' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/semantic_ds/semantic_ds.module',
        'basename' => 'semantic_ds.module',
        'name' => 'semantic_ds',
        'info' => 
        array (
          'name' => 'Semantic Display Suite',
          'description' => 'Semantic field options for Display Suite',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-1.x-dev',
          'project' => 'semantic_ds',
          'datestamp' => '1380627143',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'node_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/node_reference/node_reference.module',
        'basename' => 'node_reference.module',
        'name' => 'node_reference',
        'info' => 
        array (
          'name' => 'Node Reference',
          'description' => 'Defines a field type for referencing one node from another.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'references',
            2 => 'options',
          ),
          'files' => 
          array (
            0 => 'node_reference.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-2.1',
      ),
      'user_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/user_reference/user_reference.module',
        'basename' => 'user_reference.module',
        'name' => 'user_reference',
        'info' => 
        array (
          'name' => 'User Reference',
          'description' => 'Defines a field type for referencing a user from a node.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'references',
            2 => 'options',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'references' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references/references.module',
        'basename' => 'references.module',
        'name' => 'references',
        'info' => 
        array (
          'name' => 'References',
          'description' => 'Defines common base features for the various reference field types.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'views/references_handler_relationship.inc',
            1 => 'views/references_handler_argument.inc',
            2 => 'views/references_plugin_display.inc',
            3 => 'views/references_plugin_style.inc',
            4 => 'views/references_plugin_row_fields.inc',
          ),
          'version' => '7.x-2.1',
          'project' => 'references',
          'datestamp' => '1360265821',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'video_embed_facebook' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_facebook/video_embed_facebook.module',
        'basename' => 'video_embed_facebook.module',
        'name' => 'video_embed_facebook',
        'info' => 
        array (
          'name' => 'Video Embed Facebook',
          'description' => 'Provides Facebook handler for Video Embed Fields.  This module also serves as an example of how to add handlers to video embed field.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'dependencies' => 
          array (
            0 => 'video_embed_field',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta8',
      ),
      'video_embed_brightcove' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_brightcove/video_embed_brightcove.module',
        'basename' => 'video_embed_brightcove.module',
        'name' => 'video_embed_brightcove',
        'info' => 
        array (
          'name' => 'Video Embed Brightcove',
          'description' => 'Provides Brightcove handler for Video Embed Fields.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'dependencies' => 
          array (
            0 => 'video_embed_field',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta8',
      ),
      'video_embed_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/video_embed_field/video_embed_field.module',
        'basename' => 'video_embed_field.module',
        'name' => 'video_embed_field',
        'info' => 
        array (
          'name' => 'Video Embed Field',
          'description' => 'Expose a field type for embedding videos from youtube or vimeo.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/vef_video_styles',
          'files' => 
          array (
            0 => 'video_embed_field.migrate.inc',
            1 => 'views/handlers/views_embed_field_views_handler_field_thumbnail_path.inc',
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'image',
          ),
          'version' => '7.x-2.0-beta8',
          'project' => 'video_embed_field',
          'datestamp' => '1408177433',
          'php' => '5.2.4',
        ),
        'schema_version' => '7008',
        'version' => '7.x-2.0-beta8',
      ),
      'rules_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_i18n/rules_i18n.module',
        'basename' => 'rules_i18n.module',
        'name' => 'rules_i18n',
        'info' => 
        array (
          'name' => 'Rules translation',
          'description' => 'Allows translating rules.',
          'dependencies' => 
          array (
            0 => 'rules',
            1 => 'i18n_string',
          ),
          'package' => 'Multilingual - Internationalization',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_i18n.i18n.inc',
            1 => 'rules_i18n.rules.inc',
            2 => 'rules_i18n.test',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.9',
      ),
      'rules_scheduler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_scheduler/rules_scheduler.module',
        'basename' => 'rules_scheduler.module',
        'name' => 'rules_scheduler',
        'info' => 
        array (
          'name' => 'Rules Scheduler',
          'description' => 'Schedule the execution of Rules components using actions.',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_scheduler.admin.inc',
            1 => 'rules_scheduler.module',
            2 => 'rules_scheduler.install',
            3 => 'rules_scheduler.rules.inc',
            4 => 'rules_scheduler.test',
            5 => 'includes/rules_scheduler.handler.inc',
            6 => 'includes/rules_scheduler.views_default.inc',
            7 => 'includes/rules_scheduler.views.inc',
            8 => 'includes/rules_scheduler_views_filter.inc',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => '7204',
        'version' => '7.x-2.9',
      ),
      'rules_admin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules_admin/rules_admin.module',
        'basename' => 'rules_admin.module',
        'name' => 'rules_admin',
        'info' => 
        array (
          'name' => 'Rules UI',
          'description' => 'Administrative interface for managing rules.',
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules_admin.module',
            1 => 'rules_admin.inc',
          ),
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.9',
      ),
      'rules' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules/rules.module',
        'basename' => 'rules.module',
        'name' => 'rules',
        'info' => 
        array (
          'name' => 'Rules',
          'description' => 'React on events and conditionally evaluate actions.',
          'package' => 'Rules',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rules.features.inc',
            1 => 'tests/rules.test',
            2 => 'includes/faces.inc',
            3 => 'includes/rules.core.inc',
            4 => 'includes/rules.event.inc',
            5 => 'includes/rules.processor.inc',
            6 => 'includes/rules.plugins.inc',
            7 => 'includes/rules.state.inc',
            8 => 'includes/rules.dispatcher.inc',
            9 => 'modules/node.eval.inc',
            10 => 'modules/php.eval.inc',
            11 => 'modules/rules_core.eval.inc',
            12 => 'modules/system.eval.inc',
            13 => 'ui/ui.controller.inc',
            14 => 'ui/ui.core.inc',
            15 => 'ui/ui.data.inc',
            16 => 'ui/ui.plugins.inc',
          ),
          'dependencies' => 
          array (
            0 => 'entity_token',
            1 => 'entity',
          ),
          'version' => '7.x-2.9',
          'project' => 'rules',
          'datestamp' => '1426527210',
          'php' => '5.2.4',
        ),
        'schema_version' => '7214',
        'version' => '7.x-2.9',
      ),
      'date_repeat_field' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.module',
        'basename' => 'date_repeat_field.module',
        'name' => 'date_repeat_field',
        'info' => 
        array (
          'name' => 'Date Repeat Field',
          'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
            2 => 'date_repeat',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date_repeat_field.css',
            ),
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_popup' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_popup/date_popup.module',
        'basename' => 'date_popup.module',
        'name' => 'date_popup',
        'info' => 
        array (
          'name' => 'Date Popup',
          'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/date_popup',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'themes/datepicker.1.7.css',
            ),
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_context' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_context/date_context.module',
        'basename' => 'date_context.module',
        'name' => 'date_context',
        'info' => 
        array (
          'name' => 'Date Context',
          'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'context',
          ),
          'files' => 
          array (
            0 => 'date_context.module',
            1 => 'plugins/date_context_date_condition.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_all_day' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_all_day/date_all_day.module',
        'basename' => 'date_all_day.module',
        'name' => 'date_all_day',
        'info' => 
        array (
          'name' => 'Date All Day',
          'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_migrate_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
        'basename' => 'date_migrate_example.module',
        'name' => 'date_migrate_example',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'date_repeat',
            2 => 'date_repeat_field',
            3 => 'features',
            4 => 'migrate',
          ),
          'description' => 'Examples of migrating with the Date module',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-date_migrate_example-body',
              1 => 'node-date_migrate_example-field_date',
              2 => 'node-date_migrate_example-field_date_range',
              3 => 'node-date_migrate_example-field_date_repeat',
              4 => 'node-date_migrate_example-field_datestamp',
              5 => 'node-date_migrate_example-field_datestamp_range',
              6 => 'node-date_migrate_example-field_datetime',
              7 => 'node-date_migrate_example-field_datetime_range',
            ),
            'node' => 
            array (
              0 => 'date_migrate_example',
            ),
          ),
          'files' => 
          array (
            0 => 'date_migrate_example.migrate.inc',
          ),
          'name' => 'Date Migration Example',
          'package' => 'Features',
          'project' => 'date',
          'version' => '7.x-2.8',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_tools/date_tools.module',
        'basename' => 'date_tools.module',
        'name' => 'date_tools',
        'info' => 
        array (
          'name' => 'Date Tools',
          'description' => 'Tools to import and auto-create dates and calendars.',
          'dependencies' => 
          array (
            0 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/tools',
          'files' => 
          array (
            0 => 'tests/date_tools.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_api/date_api.module',
        'basename' => 'date_api.module',
        'name' => 'date_api',
        'info' => 
        array (
          'name' => 'Date API',
          'description' => 'A Date API that can be used by other modules.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date.css',
            ),
          ),
          'files' => 
          array (
            0 => 'date_api.module',
            1 => 'date_api_sql.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.8',
      ),
      'date_repeat' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
        'basename' => 'date_repeat.module',
        'name' => 'date_repeat',
        'info' => 
        array (
          'name' => 'Date Repeat API',
          'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_repeat.test',
            1 => 'tests/date_repeat_form.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date_views/date_views.module',
        'basename' => 'date_views.module',
        'name' => 'date_views',
        'info' => 
        array (
          'name' => 'Date Views',
          'description' => 'Views integration for date fields and date functionality.',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'includes/date_views_argument_handler.inc',
            1 => 'includes/date_views_argument_handler_simple.inc',
            2 => 'includes/date_views_filter_handler.inc',
            3 => 'includes/date_views_filter_handler_simple.inc',
            4 => 'includes/date_views.views.inc',
            5 => 'includes/date_views_plugin_pager.inc',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.8',
      ),
      'date' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date/date.module',
        'basename' => 'date.module',
        'name' => 'date',
        'info' => 
        array (
          'name' => 'Date',
          'description' => 'Makes date/time fields available.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'date.migrate.inc',
            1 => 'tests/date_api.test',
            2 => 'tests/date.test',
            3 => 'tests/date_field.test',
            4 => 'tests/date_migrate.test',
            5 => 'tests/date_validation.test',
            6 => 'tests/date_timezone.test',
          ),
          'version' => '7.x-2.8',
          'project' => 'date',
          'datestamp' => '1406653438',
        ),
        'schema_version' => '7004',
        'version' => '7.x-2.8',
      ),
      'nodequeue_service' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/addons/nodequeue_service/nodequeue_service.module',
        'basename' => 'nodequeue_service.module',
        'name' => 'nodequeue_service',
        'info' => 
        array (
          'name' => 'Nodequeue Service',
          'description' => 'Provides a nodequeue service.',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'services',
            1 => 'nodequeue',
          ),
          'files' => 
          array (
            0 => 'nodequeue_service.inc',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta1',
      ),
      'smartqueue' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/smartqueue.module',
        'basename' => 'smartqueue.module',
        'name' => 'smartqueue',
        'info' => 
        array (
          'name' => 'Smartqueue taxonomy',
          'description' => 'Creates a node queue for each taxonomy vocabulary',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'nodequeue',
            1 => 'taxonomy',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.0-beta1',
      ),
      'nodequeue' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/nodequeue.module',
        'basename' => 'nodequeue.module',
        'name' => 'nodequeue',
        'info' => 
        array (
          'name' => 'Nodequeue',
          'description' => 'Create queues which can contain nodes in arbitrary order',
          'package' => 'Nodequeue',
          'core' => '7.x',
          'configure' => 'admin/structure/nodequeue/settings',
          'files' => 
          array (
            0 => 'includes/views/nodequeue.views.inc',
            1 => 'includes/nodequeue.actions.inc',
            2 => 'includes/views/nodequeue_handler_argument_subqueue_qid.inc',
            3 => 'includes/views/nodequeue_handler_field_all_queues.inc',
            4 => 'includes/views/nodequeue_handler_field_all_subqueues.inc',
            5 => 'includes/views/nodequeue_handler_field_links.inc',
            6 => 'includes/views/nodequeue_handler_field_queue_tab.inc',
            7 => 'includes/views/nodequeue_handler_filter_in_queue.inc',
            8 => 'includes/views/nodequeue_handler_relationship_nodequeue.inc',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0-beta1',
      ),
      'nodequeue_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/nodequeue/nodequeue_generate.module',
        'basename' => 'nodequeue_generate.module',
        'name' => 'nodequeue_generate',
        'info' => 
        array (
          'name' => 'Nodequeue generate',
          'description' => 'Bulk assign nodes into queues for quickly populating a site.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'nodequeue',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'nodequeue_generate.module',
          ),
          'version' => '7.x-2.0-beta1',
          'project' => 'nodequeue',
          'datestamp' => '1316558104',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta1',
      ),
      'content_lock_timeout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/content_lock/modules/content_lock_timeout/content_lock_timeout.module',
        'basename' => 'content_lock_timeout.module',
        'name' => 'content_lock_timeout',
        'info' => 
        array (
          'name' => 'Content locking (edit lock) timeouts',
          'description' => 'Provides mechanisms for automatically unlocking nodes that have been locked for a certain length of time.',
          'core' => '7.x',
          'project' => 'content_lock',
          'dependencies' => 
          array (
            0 => 'content_lock',
          ),
          'files' => 
          array (
            0 => 'content_lock_timeout.install',
            1 => 'content_lock_timeout.module',
          ),
          'configure' => 'admin/settings/content_lock',
          'version' => '7.x-2.0',
          'datestamp' => '1410528829',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0',
      ),
      'content_lock' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/content_lock/content_lock.module',
        'basename' => 'content_lock.module',
        'name' => 'content_lock',
        'info' => 
        array (
          'name' => 'Content locking (edit lock)',
          'description' => 'Prevents multiple users from trying to edit a single node simultaneously to prevent edit conflicts.',
          'core' => '7.x',
          'project' => 'content_lock',
          'dependencies' => 
          array (
            0 => 'filter',
            1 => 'node',
          ),
          'files' => 
          array (
            0 => 'content_lock.install',
            1 => 'content_lock.module',
            2 => 'content_lock.admin.inc',
            3 => 'views/content_lock.views',
            4 => 'views/views_handler_field_is_locked.inc',
            5 => 'views/views_handler_filter_is_locked.inc',
            6 => 'views/views_handler_sort_is_locked.inc',
          ),
          'configure' => 'admin/settings/content_lock',
          'version' => '7.x-2.0',
          'datestamp' => '1410528829',
          'php' => '5.2.4',
        ),
        'schema_version' => '6200',
        'version' => '7.x-2.0',
      ),
      'ember_support' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ember_support/ember_support.module',
        'basename' => 'ember_support.module',
        'name' => 'ember_support',
        'info' => 
        array (
          'name' => 'Ember Support',
          'description' => 'Additional functionality and unified interfaces for the Ember administration theme.',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/ember-contextual.css',
              1 => 'css/ember-ctools.css',
              2 => 'css/ember-media.css',
              3 => 'css/ember-modal.css',
              4 => 'css/ember-panels-ipe.css',
              5 => 'css/ember-purr.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'js/ember-support.js',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'ember_support',
          'datestamp' => '1421949706',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'ds_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_forms/ds_forms.module',
        'basename' => 'ds_forms.module',
        'name' => 'ds_forms',
        'info' => 
        array (
          'name' => 'Display Suite Forms',
          'description' => 'Manage the layout of forms in Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_ui/ds_ui.module',
        'basename' => 'ds_ui.module',
        'name' => 'ds_ui',
        'info' => 
        array (
          'name' => 'Display Suite UI',
          'description' => 'User interface for managing fields, view modes and classes.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_format' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_format/ds_format.module',
        'basename' => 'ds_format.module',
        'name' => 'ds_format',
        'info' => 
        array (
          'name' => 'Display Suite Format',
          'description' => 'Provides the Display Suite Code format filter.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_devel/ds_devel.module',
        'basename' => 'ds_devel.module',
        'name' => 'ds_devel',
        'info' => 
        array (
          'name' => 'Display Suite Devel',
          'description' => 'Development functionality for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
            1 => 'devel',
          ),
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_search/ds_search.module',
        'basename' => 'ds_search.module',
        'name' => 'ds_search',
        'info' => 
        array (
          'name' => 'Display Suite Search',
          'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/search',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.7',
      ),
      'ds_extras' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/modules/ds_extras/ds_extras.module',
        'basename' => 'ds_extras.module',
        'name' => 'ds_extras',
        'info' => 
        array (
          'name' => 'Display Suite Extras',
          'description' => 'Contains additional features for Display Suite.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/list/extras',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.7',
      ),
      'ds' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ds/ds.module',
        'basename' => 'ds.module',
        'name' => 'ds',
        'info' => 
        array (
          'name' => 'Display Suite',
          'description' => 'Extend the display options for every entity type.',
          'core' => '7.x',
          'package' => 'Display Suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'views/views_plugin_ds_entity_view.inc',
            1 => 'views/views_plugin_ds_fields_view.inc',
            2 => 'tests/ds.base.test',
            3 => 'tests/ds.search.test',
            4 => 'tests/ds.entities.test',
            5 => 'tests/ds.exportables.test',
            6 => 'tests/ds.views.test',
            7 => 'tests/ds.forms.test',
          ),
          'configure' => 'admin/structure/ds',
          'version' => '7.x-2.7',
          'project' => 'ds',
          'datestamp' => '1414483431',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.7',
      ),
      'commerce_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_features/commerce_features.module',
        'basename' => 'commerce_features.module',
        'name' => 'commerce_features',
        'info' => 
        array (
          'name' => 'Commerce Features',
          'description' => 'Features integration for Drupal Commerce module',
          'package' => 'Features',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'commerce',
          ),
          'version' => '7.x-1.0',
          'project' => 'commerce_features',
          'datestamp' => '1385479047',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'redirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/redirect/redirect.module',
        'basename' => 'redirect.module',
        'name' => 'redirect',
        'info' => 
        array (
          'name' => 'Redirect',
          'description' => 'Allows users to redirect from old URLs to new URLs.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'redirect.controller.inc',
            1 => 'redirect.test',
            2 => 'views/redirect.views.inc',
            3 => 'views/redirect_handler_filter_redirect_type.inc',
            4 => 'views/redirect_handler_field_redirect_source.inc',
            5 => 'views/redirect_handler_field_redirect_redirect.inc',
            6 => 'views/redirect_handler_field_redirect_operations.inc',
            7 => 'views/redirect_handler_field_redirect_link_edit.inc',
            8 => 'views/redirect_handler_field_redirect_link_delete.inc',
          ),
          'configure' => 'admin/config/search/redirect/settings',
          'version' => '7.x-1.0-rc3',
          'project' => 'redirect',
          'datestamp' => '1436393342',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.0-rc3',
      ),
      'panels_mini' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_mini/panels_mini.module',
        'basename' => 'panels_mini.module',
        'name' => 'panels_mini',
        'info' => 
        array (
          'name' => 'Mini panels',
          'description' => 'Create mini panels that can be used as blocks by Drupal and panes by other panel modules.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/export_ui/panels_mini_ui.class.php',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'panels_ipe' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_ipe/panels_ipe.module',
        'basename' => 'panels_ipe.module',
        'name' => 'panels_ipe',
        'info' => 
        array (
          'name' => 'Panels In-Place Editor',
          'description' => 'Provide a UI for managing some Panels directly on the frontend, instead of having to use the backend.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'core' => '7.x',
          'configure' => 'admin/structure/panels',
          'files' => 
          array (
            0 => 'panels_ipe.module',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'i18n_panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/i18n_panels/i18n_panels.module',
        'basename' => 'i18n_panels.module',
        'name' => 'i18n_panels',
        'info' => 
        array (
          'name' => 'Panels translation',
          'description' => 'Supports translatable panels items.',
          'dependencies' => 
          array (
            0 => 'i18n',
            1 => 'panels',
            2 => 'i18n_string',
            3 => 'i18n_translation',
          ),
          'package' => 'Multilingual - Internationalization',
          'core' => '7.x',
          'version' => '7.x-3.5',
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.5',
      ),
      'panels_node' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels_node/panels_node.module',
        'basename' => 'panels_node.module',
        'name' => 'panels_node',
        'info' => 
        array (
          'name' => 'Panel nodes',
          'description' => 'Create nodes that are divided into areas with selectable content.',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'dependencies' => 
          array (
            0 => 'panels',
          ),
          'configure' => 'admin/structure/panels',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'panels_node.module',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => '6001',
        'version' => '7.x-3.5',
      ),
      'panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/panels/panels.module',
        'basename' => 'panels.module',
        'name' => 'panels',
        'info' => 
        array (
          'name' => 'Panels',
          'description' => 'Core Panels display functions; provides no external UI, at least one other Panels module should be enabled.',
          'core' => '7.x',
          'package' => 'Panels',
          'version' => '7.x-3.5',
          'configure' => 'admin/structure/panels',
          'dependencies' => 
          array (
            0 => 'ctools (>1.5)',
          ),
          'files' => 
          array (
            0 => 'panels.module',
            1 => 'includes/common.inc',
            2 => 'includes/legacy.inc',
            3 => 'includes/plugins.inc',
            4 => 'plugins/views/panels_views_plugin_row_fields.inc',
          ),
          'project' => 'panels',
          'datestamp' => '1422472985',
          'php' => '5.2.4',
        ),
        'schema_version' => '7303',
        'version' => '7.x-3.5',
      ),
      'blockify' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/blockify/blockify.module',
        'basename' => 'blockify.module',
        'name' => 'blockify',
        'info' => 
        array (
          'name' => 'Blockify',
          'description' => 'Exposes a number of core Drupal elements as blocks.',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'blockify',
          'datestamp' => '1303344135',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'breakpoints' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/breakpoints/breakpoints.module',
        'basename' => 'breakpoints.module',
        'name' => 'breakpoints',
        'info' => 
        array (
          'name' => 'Breakpoints',
          'description' => 'Manage breakpoints',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'breakpoints.module',
            1 => 'breakpoints.test',
          ),
          'configure' => 'admin/config/media/breakpoints',
          'version' => '7.x-1.3',
          'project' => 'breakpoints',
          'datestamp' => '1407507528',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.3',
      ),
      'globalredirect' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/globalredirect/globalredirect.module',
        'basename' => 'globalredirect.module',
        'name' => 'globalredirect',
        'info' => 
        array (
          'name' => 'Global Redirect',
          'description' => 'Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.',
          'package' => 'Path management',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'globalredirect.test',
          ),
          'configure' => 'admin/config/system/globalredirect',
          'version' => '7.x-1.5',
          'project' => 'globalredirect',
          'datestamp' => '1339748779',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'version' => '7.x-1.5',
      ),
      'mimemail_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_example/mimemail_example.module',
        'basename' => 'mimemail_example.module',
        'name' => 'mimemail_example',
        'info' => 
        array (
          'name' => 'Mime Mail Example',
          'description' => 'Example of how to use the Mime Mail module.',
          'dependencies' => 
          array (
            0 => 'mimemail',
          ),
          'package' => 'Example modules',
          'core' => '7.x',
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail_action' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_action/mimemail_action.module',
        'basename' => 'mimemail_action.module',
        'name' => 'mimemail_action',
        'info' => 
        array (
          'name' => 'Mime Mail Action',
          'description' => 'Provide actions for Mime Mail.',
          'package' => 'Mail',
          'dependencies' => 
          array (
            0 => 'mimemail',
            1 => 'trigger',
          ),
          'core' => '7.x',
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail_compress' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/modules/mimemail_compress/mimemail_compress.module',
        'basename' => 'mimemail_compress.module',
        'name' => 'mimemail_compress',
        'info' => 
        array (
          'name' => 'Mime Mail CSS Compressor',
          'description' => 'Converts CSS to inline styles in an HTML message. (Requires the PHP DOM extension.)',
          'package' => 'Mail',
          'dependencies' => 
          array (
            0 => 'mimemail',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'mimemail_compress.inc',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta4',
      ),
      'mimemail' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mimemail/mimemail.module',
        'basename' => 'mimemail.module',
        'name' => 'mimemail',
        'info' => 
        array (
          'name' => 'Mime Mail',
          'description' => 'Send MIME-encoded emails with embedded images and attachments.',
          'dependencies' => 
          array (
            0 => 'mailsystem',
            1 => 'system (>=7.24)',
          ),
          'package' => 'Mail',
          'core' => '7.x',
          'configure' => 'admin/config/system/mimemail',
          'files' => 
          array (
            0 => 'includes/mimemail.mail.inc',
            1 => 'tests/mimemail.test',
            2 => 'tests/mimemail_rules.test',
            3 => 'tests/mimemail_compress.test',
          ),
          'version' => '7.x-1.0-beta4',
          'project' => 'mimemail',
          'datestamp' => '1438530555',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.0-beta4',
      ),
      'facetapi_pretty_paths' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi_pretty_paths/facetapi_pretty_paths.module',
        'basename' => 'facetapi_pretty_paths.module',
        'name' => 'facetapi_pretty_paths',
        'info' => 
        array (
          'name' => 'Facet API Pretty Paths',
          'core' => '7.x',
          'description' => 'Enables pretty paths for searches with Facet API.',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'package' => 'Search Toolkit',
          'files' => 
          array (
            0 => 'plugins/facetapi/url_processor_pretty_paths.inc',
            1 => 'plugins/base_path_provider/facetapi_pretty_paths_base_path_provider.inc',
            2 => 'plugins/base_path_provider/facetapi_pretty_paths_adapter_base_path_provider.inc',
            3 => 'plugins/base_path_provider/facetapi_pretty_paths_default_base_path_provider.inc',
            4 => 'plugins/coders/facetapi_pretty_paths_coder_default.inc',
            5 => 'plugins/coders/facetapi_pretty_paths_coder_taxonomy.inc',
            6 => 'plugins/coders/facetapi_pretty_paths_coder_taxonomy_pathauto.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'facetapi_pretty_paths',
          'datestamp' => '1422829982',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license_role' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/modules/commerce_license_role/commerce_license_role.module',
        'basename' => 'commerce_license_role.module',
        'name' => 'commerce_license_role',
        'info' => 
        array (
          'name' => 'Commerce License Role',
          'description' => 'Provides a license type for selling roles.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/modules/commerce_license_example/commerce_license_example.module',
        'basename' => 'commerce_license_example.module',
        'name' => 'commerce_license_example',
        'info' => 
        array (
          'name' => 'Commerce License Example',
          'description' => 'Provides an example license type for testing and development.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
            1 => 'inline_entity_form',
            2 => 'advancedqueue',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'commerce_license' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_license/commerce_license.module',
        'basename' => 'commerce_license.module',
        'name' => 'commerce_license',
        'info' => 
        array (
          'name' => 'Commerce License',
          'description' => 'Provides a framework for selling access to local or remote resources.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'configure' => 'admin/commerce/config/license',
          'dependencies' => 
          array (
            0 => 'commerce_product',
            1 => 'commerce_product_reference',
            2 => 'commerce_line_item',
            3 => 'commerce_payment',
            4 => 'ctools',
            5 => 'entity',
            6 => 'entity_bundle_plugin',
            7 => 'entityreference',
            8 => 'list',
            9 => 'number',
            10 => 'views_bulk_operations',
          ),
          'files' => 
          array (
            0 => 'commerce_license.info.inc',
            1 => 'includes/commerce_license.controller.inc',
            2 => 'includes/commerce_license.inline_entity_form.inc',
            3 => 'includes/plugins/license_type/base.inc',
            4 => 'includes/views/commerce_license.views_controller.inc',
            5 => 'includes/views/handlers/commerce_license_handler_field_access_details.inc',
            6 => 'includes/views/plugins/commerce_license_plugin_access_sync.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'commerce_license',
          'datestamp' => '1403714930',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.3',
      ),
      'pathauto' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/pathauto/pathauto.module',
        'basename' => 'pathauto.module',
        'name' => 'pathauto',
        'info' => 
        array (
          'name' => 'Pathauto',
          'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
          'dependencies' => 
          array (
            0 => 'path',
            1 => 'token',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'pathauto.test',
          ),
          'configure' => 'admin/config/search/path/patterns',
          'recommends' => 
          array (
            0 => 'redirect',
          ),
          'version' => '7.x-1.2',
          'project' => 'pathauto',
          'datestamp' => '1344525185',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.x-1.2',
      ),
      'ctools_ajax_sample' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
        'basename' => 'ctools_ajax_sample.module',
        'name' => 'ctools_ajax_sample',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) AJAX Example',
          'description' => 'Shows how to use the power of Chaos AJAX.',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'core' => '7.x',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_access_ruleset' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
        'basename' => 'ctools_access_ruleset.module',
        'name' => 'ctools_access_ruleset',
        'info' => 
        array (
          'name' => 'Custom rulesets',
          'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_custom_content' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
        'basename' => 'ctools_custom_content.module',
        'name' => 'ctools_custom_content',
        'info' => 
        array (
          'name' => 'Custom content panes',
          'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'term_depth' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/term_depth/term_depth.module',
        'basename' => 'term_depth.module',
        'name' => 'term_depth',
        'info' => 
        array (
          'name' => 'Term Depth access',
          'description' => 'Controls access to context based upon term depth',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'bulk_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/bulk_export/bulk_export.module',
        'basename' => 'bulk_export.module',
        'name' => 'bulk_export',
        'info' => 
        array (
          'name' => 'Bulk Export',
          'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'views_content' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/views_content/views_content.module',
        'basename' => 'views_content.module',
        'name' => 'views_content',
        'info' => 
        array (
          'name' => 'Views content panes',
          'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'core' => '7.x',
          'version' => '7.x-1.7',
          'files' => 
          array (
            0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
            1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
            2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools_plugin_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
        'basename' => 'ctools_plugin_example.module',
        'name' => 'ctools_plugin_example',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) Plugin Example',
          'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'panels',
            2 => 'page_manager',
            3 => 'advanced_help',
          ),
          'core' => '7.x',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'stylizer' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/stylizer/stylizer.module',
        'basename' => 'stylizer.module',
        'name' => 'stylizer',
        'info' => 
        array (
          'name' => 'Stylizer',
          'description' => 'Create custom styles for applications such as Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'color',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'page_manager' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/page_manager/page_manager.module',
        'basename' => 'page_manager.module',
        'name' => 'page_manager',
        'info' => 
        array (
          'name' => 'Page manager',
          'description' => 'Provides a UI and API to manage pages within the site.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'ctools' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ctools/ctools.module',
        'basename' => 'ctools.module',
        'name' => 'ctools',
        'info' => 
        array (
          'name' => 'Chaos tools',
          'description' => 'A library of helpful tools by Merlin of Chaos.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.7',
          'files' => 
          array (
            0 => 'includes/context.inc',
            1 => 'includes/css-cache.inc',
            2 => 'includes/math-expr.inc',
            3 => 'includes/stylizer.inc',
            4 => 'tests/css_cache.test',
          ),
          'project' => 'ctools',
          'datestamp' => '1426696183',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.7',
      ),
      'payment_commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/payment_commerce/payment_commerce.module',
        'basename' => 'payment_commerce.module',
        'name' => 'payment_commerce',
        'info' => 
        array (
          'name' => 'Payment for Drupal Commerce',
          'description' => 'Allows Drupal Commerce orders to be paid using Payment.',
          'configure' => 'admin/config/services/payment/payment_commerce',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_payment',
            1 => 'payment (>=1.12)',
          ),
          'test_dependencies' => 
          array (
            0 => 'views',
          ),
          'package' => 'Payment',
          'files' => 
          array (
            0 => 'tests/PaymentCommerceCheckoutWebTestCase.test',
            1 => 'tests/PaymentCommerceDeleteOrderWebTestCase.test',
          ),
          'version' => '7.x-1.7',
          'project' => 'payment_commerce',
          'datestamp' => '1414333129',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'facetapi_bonus' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/facetapi_bonus/facetapi_bonus.module',
        'basename' => 'facetapi_bonus.module',
        'name' => 'facetapi_bonus',
        'info' => 
        array (
          'name' => 'Facet API Bonus',
          'package' => 'Search Toolkit',
          'description' => 'Additions to facetapi',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'facetapi',
          ),
          'files' => 
          array (
            0 => 'plugins/facetapi/dependency_facet.inc',
            1 => 'plugins/facetapi/filter_rewrite_items.inc',
            2 => 'plugins/facetapi/filter_exclude_specified_items.inc',
            3 => 'plugins/facetapi/filter_narrow_results.inc',
            4 => 'plugins/facetapi/filter_show_if_minimum_items.inc',
            5 => 'plugins/facetapi/filter_show_deepest_level_items.inc',
            6 => 'plugins/facetapi/current_search/current_search_reset_filters_link.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'facetapi_bonus',
          'datestamp' => '1338012356',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'user_import' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/user_import/user_import.module',
        'basename' => 'user_import.module',
        'name' => 'user_import',
        'info' => 
        array (
          'name' => 'User Import',
          'description' => 'Import users into Drupal from a CSV file.',
          'core' => '7.x',
          'package' => 'Deployment',
          'files' => 
          array (
            0 => 'user_import.module',
            1 => 'user_import.install',
            2 => 'user_import.admin.inc',
            3 => 'user_import.import.inc',
            4 => 'user_import.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'user_import',
          'datestamp' => '1376260694',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'version' => '7.x-2.1',
      ),
      'commerce_node_checkout_expire' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_node_checkout/commerce_node_checkout_expire/commerce_node_checkout_expire.module',
        'basename' => 'commerce_node_checkout_expire.module',
        'name' => 'commerce_node_checkout_expire',
        'info' => 
        array (
          'name' => 'Commerce Node Checkout Expire',
          'description' => 'Automatic expiration of nodes published via purchase',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
            1 => 'commerce_node_checkout',
            2 => 'interval',
            3 => 'date',
            4 => 'date_popup',
          ),
          'package' => 'Commerce (contrib)',
          'files' => 
          array (
            0 => 'commerce_node_checkout_expire.test',
            1 => 'includes/views/handlers/commerce_node_checkout_expire_field_node_relist_form.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_node_checkout',
          'datestamp' => '1400256828',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'commerce_node_checkout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_node_checkout/commerce_node_checkout.module',
        'basename' => 'commerce_node_checkout.module',
        'name' => 'commerce_node_checkout',
        'info' => 
        array (
          'name' => 'Commerce Node Checkout',
          'description' => 'Allows users to pay to publish nodes.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_cart',
            2 => 'commerce_checkout',
            3 => 'commerce_price',
            4 => 'commerce_product_reference',
            5 => 'commerce_product_ui',
            6 => 'entityreference',
            7 => 'rules',
          ),
          'files' => 
          array (
            0 => 'commerce_node_checkout.test',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_node_checkout',
          'datestamp' => '1400256828',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.x-1.0-beta1',
      ),
      'link' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/link/link.module',
        'basename' => 'link.module',
        'name' => 'link',
        'info' => 
        array (
          'name' => 'Link',
          'description' => 'Defines simple link field types.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'link.module',
            1 => 'link.migrate.inc',
            2 => 'tests/link.test',
            3 => 'tests/link.attribute.test',
            4 => 'tests/link.crud.test',
            5 => 'tests/link.crud_browser.test',
            6 => 'tests/link.token.test',
            7 => 'tests/link.validate.test',
            8 => 'views/link_views_handler_argument_target.inc',
            9 => 'views/link_views_handler_filter_protocol.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'link',
          'datestamp' => '1413924830',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.3',
      ),
      'imagecache_token' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imagecache_token/imagecache_token.module',
        'basename' => 'imagecache_token.module',
        'name' => 'imagecache_token',
        'info' => 
        array (
          'name' => 'Imagecache Token',
          'description' => 'Provides additional image tokens for use with ImageCache',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'token',
          ),
          'package' => 'Image',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'imagecache_token',
          'datestamp' => '1414529932',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'inline_entity_form' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/inline_entity_form/inline_entity_form.module',
        'basename' => 'inline_entity_form.module',
        'name' => 'inline_entity_form',
        'info' => 
        array (
          'name' => 'Inline Entity Form',
          'description' => 'Provides a widget for inline management (creation, modification, removal) of referenced entities. ',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'system (>7.14)',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/entity.inline_entity_form.inc',
            1 => 'includes/node.inline_entity_form.inc',
            2 => 'includes/taxonomy_term.inline_entity_form.inc',
            3 => 'includes/commerce_product.inline_entity_form.inc',
            4 => 'includes/commerce_line_item.inline_entity_form.inc',
          ),
          'version' => '7.x-1.5',
          'project' => 'inline_entity_form',
          'datestamp' => '1389971831',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.5',
      ),
      'site_verify' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/site_verify/site_verify.module',
        'basename' => 'site_verify.module',
        'name' => 'site_verify',
        'info' => 
        array (
          'name' => 'Site Verification',
          'description' => 'Verifies ownership of a site for use with search engines.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'site_verify.module',
            1 => 'site_verify.admin.inc',
            2 => 'site_verify.install',
            3 => 'site_verify.test',
          ),
          'configure' => 'admin/config/search/verifications',
          'version' => '7.x-1.1',
          'project' => 'site_verify',
          'datestamp' => '1395656959',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '3',
        'version' => '7.x-1.1',
      ),
      'navigation404' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/navigation404/navigation404.module',
        'basename' => 'navigation404.module',
        'name' => 'navigation404',
        'info' => 
        array (
          'name' => '404 Navigation',
          'description' => 'On a 404 page, ensure navigation links are displayed properly.',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'navigation404',
          'datestamp' => '1294419091',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'flexslider_picture' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/picture/flexslider_picture/flexslider_picture.module',
        'basename' => 'flexslider_picture.module',
        'name' => 'flexslider_picture',
        'info' => 
        array (
          'name' => 'FlexSlider Picture',
          'description' => 'Integrates the Picture module with the FlexSlider module for a truly responsive slider.',
          'package' => 'Picture',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'picture',
            1 => 'flexslider (2.x)',
            2 => 'flexslider_fields (2.x)',
          ),
          'version' => '7.x-1.2',
          'project' => 'picture',
          'datestamp' => '1383130545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'picture' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/picture/picture.module',
        'basename' => 'picture.module',
        'name' => 'picture',
        'info' => 
        array (
          'name' => 'Picture',
          'description' => 'Picture element',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
            1 => 'breakpoints',
          ),
          'configure' => 'admin/config/media/picture',
          'package' => 'Picture',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'picture_wysiwyg.css',
            ),
          ),
          'version' => '7.x-1.2',
          'project' => 'picture',
          'datestamp' => '1383130545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'views_accordion' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_accordion/views_accordion.module',
        'basename' => 'views_accordion.module',
        'name' => 'views_accordion',
        'info' => 
        array (
          'name' => 'Views Accordion',
          'description' => 'Provides an accordion views display plugin.',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_accordion_style_plugin.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'views_accordion',
          'datestamp' => '1422545700',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'commerce_paypal_wps' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/wps/commerce_paypal_wps.module',
        'basename' => 'commerce_paypal_wps.module',
        'name' => 'commerce_paypal_wps',
        'info' => 
        array (
          'name' => 'PayPal WPS',
          'description' => 'Implements PayPal Website Payments Standard in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal_wpp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/wpp/commerce_paypal_wpp.module',
        'basename' => 'commerce_paypal_wpp.module',
        'name' => 'commerce_paypal_wpp',
        'info' => 
        array (
          'name' => 'PayPal WPP',
          'description' => 'Implements PayPal Website Payments Pro in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal_ec' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/ec/commerce_paypal_ec.module',
        'basename' => 'commerce_paypal_ec.module',
        'name' => 'commerce_paypal_ec',
        'info' => 
        array (
          'name' => 'PayPal Express Checkout',
          'description' => 'Implements PayPal Express Checkout in Drupal Commerce checkout.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_payflow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/modules/payflow/commerce_payflow.module',
        'basename' => 'commerce_payflow.module',
        'name' => 'commerce_payflow',
        'info' => 
        array (
          'name' => 'PayPal Payments Advanced and Payflow Link',
          'description' => 'Implements PayPal Payments Advanced (U.S. only) and Payflow Link Hosted Checkout pages and Transparent Redirect.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
            4 => 'commerce_paypal',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'commerce_paypal' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_paypal/commerce_paypal.module',
        'basename' => 'commerce_paypal.module',
        'name' => 'commerce_paypal',
        'info' => 
        array (
          'name' => 'PayPal',
          'description' => 'Implements PayPal payment services for use with Drupal Commerce.',
          'package' => 'Commerce (PayPal)',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_payment',
            3 => 'commerce_order',
          ),
          'core' => '7.x',
          'version' => '7.x-2.3',
          'project' => 'commerce_paypal',
          'datestamp' => '1389740908',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.3',
      ),
      'autocomplete_deluxe' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/autocomplete_deluxe/autocomplete_deluxe.module',
        'basename' => 'autocomplete_deluxe.module',
        'name' => 'autocomplete_deluxe',
        'info' => 
        array (
          'name' => 'Autocomplete Deluxe',
          'description' => 'Enhanced autocomplete using Jquery UI autocomplete.',
          'package' => 'User interface',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'autocomplete_deluxe.module',
          ),
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'autocomplete_deluxe',
          'datestamp' => '1375695669',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-beta3',
      ),
      'googleanalytics' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/google_analytics/googleanalytics.module',
        'basename' => 'googleanalytics.module',
        'name' => 'googleanalytics',
        'info' => 
        array (
          'name' => 'Google Analytics',
          'description' => 'Allows your site to be tracked by Google Analytics by adding a Javascript tracking code to every page.',
          'core' => '7.x',
          'package' => 'Statistics',
          'configure' => 'admin/config/system/googleanalytics',
          'files' => 
          array (
            0 => 'googleanalytics.test',
          ),
          'test_dependencies' => 
          array (
            0 => 'token',
          ),
          'version' => '7.x-1.4',
          'project' => 'google_analytics',
          'datestamp' => '1382021586',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7007',
        'version' => '7.x-1.4',
      ),
      'path_breadcrumbs_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs_i18n/path_breadcrumbs_i18n.module',
        'basename' => 'path_breadcrumbs_i18n.module',
        'name' => 'path_breadcrumbs_i18n',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs translation',
          'description' => 'Allows translating of breadcrumbs.',
          'core' => '7.x',
          'package' => 'Multilingual - Internationalization',
          'files' => 
          array (
            0 => 'path_breadcrumbs_i18n.inc',
          ),
          'dependencies' => 
          array (
            0 => 'path_breadcrumbs',
            1 => 'i18n_string',
            2 => 'i18n',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.2',
      ),
      'path_breadcrumbs_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs_ui/path_breadcrumbs_ui.module',
        'basename' => 'path_breadcrumbs_ui.module',
        'name' => 'path_breadcrumbs_ui',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs UI',
          'description' => 'User interface for Path Breadcrumbs module',
          'core' => '7.x',
          'package' => 'Path Breadcrumbs',
          'configure' => 'admin/structure/path-breadcrumbs',
          'dependencies' => 
          array (
            0 => 'path_breadcrumbs',
          ),
          'files' => 
          array (
            0 => 'includes/path_breadcrumbs_ui.autocomplete.inc',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => '7302',
        'version' => '7.x-3.2',
      ),
      'path_breadcrumbs' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/path_breadcrumbs/path_breadcrumbs.module',
        'basename' => 'path_breadcrumbs.module',
        'name' => 'path_breadcrumbs',
        'info' => 
        array (
          'name' => 'Path Breadcrumbs',
          'description' => 'Allow to create custom breadcrumbs for all pages on the site using contexts.',
          'core' => '7.x',
          'package' => 'Path Breadcrumbs',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'entity',
            2 => 'entity_token',
          ),
          'version' => '7.x-3.2',
          'project' => 'path_breadcrumbs',
          'datestamp' => '1423071784',
          'php' => '5.2.4',
        ),
        'schema_version' => '7304',
        'version' => '7.x-3.2',
      ),
      'views_responsive_grid' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_responsive_grid/views_responsive_grid.module',
        'basename' => 'views_responsive_grid.module',
        'name' => 'views_responsive_grid',
        'info' => 
        array (
          'name' => 'Views Responsive Grid',
          'description' => 'Views plugin for displaying views content in a responsive grid.',
          'core' => '7.x',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_responsive_grid_plugin_style_responsive_grid.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'views_responsive_grid',
          'datestamp' => '1365191423',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'date_ical' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/date_ical/date_ical.module',
        'basename' => 'date_ical.module',
        'name' => 'date_ical',
        'info' => 
        array (
          'name' => 'Date iCal',
          'description' => 'Enables export of iCal feeds using Views, and import of iCal feeds using Feeds.',
          'package' => 'Date/Time',
          'php' => '5.3',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views (>=7.x-3.5)',
            1 => 'date_views',
            2 => 'entity',
            3 => 'libraries (>=7.x-2.0)',
            4 => 'date',
            5 => 'date_api',
          ),
          'files' => 
          array (
            0 => 'includes/date_ical_plugin_row_ical_entity.inc',
            1 => 'includes/date_ical_plugin_row_ical_fields.inc',
            2 => 'includes/date_ical_plugin_style_ical_feed.inc',
            3 => 'includes/DateiCalFeedsParser.inc',
          ),
          'version' => '7.x-3.3',
          'project' => 'date_ical',
          'datestamp' => '1412727230',
        ),
        'schema_version' => '7300',
        'version' => '7.x-3.3',
      ),
      'scheduler' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/scheduler/scheduler.module',
        'basename' => 'scheduler.module',
        'name' => 'scheduler',
        'info' => 
        array (
          'name' => 'Scheduler',
          'description' => 'This module allows nodes to be published and unpublished on specified dates and time.',
          'core' => '7.x',
          'configure' => 'admin/config/content/scheduler',
          'files' => 
          array (
            0 => 'scheduler.install',
            1 => 'scheduler.module',
            2 => 'scheduler.test',
            3 => 'scheduler.views.inc',
            4 => 'scheduler_handler_field_scheduler_countdown.inc',
            5 => 'tests/scheduler_api.test',
          ),
          'test_dependencies' => 
          array (
            0 => 'date',
          ),
          'version' => '7.x-1.3',
          'project' => 'scheduler',
          'datestamp' => '1415728082',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.3',
      ),
      'filefield_sources_plupload' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/filefield_sources_plupload/filefield_sources_plupload.module',
        'basename' => 'filefield_sources_plupload.module',
        'name' => 'filefield_sources_plupload',
        'info' => 
        array (
          'name' => 'FileField Sources Plupload',
          'description' => 'Extends File fields to allow multiple file uploads.',
          'dependencies' => 
          array (
            0 => 'filefield_sources',
            1 => 'plupload',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.1',
          'project' => 'filefield_sources_plupload',
          'datestamp' => '1377635510',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'commerce_product_pricing_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_pricing/commerce_product_pricing_ui.module',
        'basename' => 'commerce_product_pricing_ui.module',
        'name' => 'commerce_product_pricing_ui',
        'info' => 
        array (
          'name' => 'Product Pricing UI',
          'description' => 'Exposes a UI for managing product pricing rules and pre-calculation settings.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'rules_admin',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_price',
            4 => 'commerce_product_pricing',
            5 => 'commerce_product_reference',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/product-pricing',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_product_pricing' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_pricing/commerce_product_pricing.module',
        'basename' => 'commerce_product_pricing.module',
        'name' => 'commerce_product_pricing',
        'info' => 
        array (
          'name' => 'Product Pricing',
          'description' => 'Enables Rules based product sell price calculation for dynamic product pricing.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
            2 => 'commerce_product_reference',
            3 => 'entity',
            4 => 'rules',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_price' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/price/commerce_price.module',
        'basename' => 'commerce_price.module',
        'name' => 'commerce_price',
        'info' => 
        array (
          'name' => 'Price',
          'description' => 'Defines the price field and a price alteration system.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'commerce_price.rules.inc',
            1 => 'includes/views/handlers/commerce_price_handler_field_commerce_price.inc',
            2 => 'includes/views/handlers/commerce_price_handler_filter_commerce_price_amount.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_order_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/order/commerce_order_ui.module',
        'basename' => 'commerce_order_ui.module',
        'name' => 'commerce_order_ui',
        'info' => 
        array (
          'name' => 'Order UI',
          'description' => 'Exposes a default UI for Orders through order edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_line_item',
            4 => 'commerce_order',
            5 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/order',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_order_ui_handler_area_view_order_form.inc',
            1 => 'tests/commerce_order_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_order' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/order/commerce_order.module',
        'basename' => 'commerce_order.module',
        'name' => 'commerce_order',
        'info' => 
        array (
          'name' => 'Order',
          'description' => 'Defines the Order entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_customer',
            2 => 'commerce_line_item',
            3 => 'commerce_price',
            4 => 'entity',
            5 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_order.controller.inc',
            1 => 'includes/views/handlers/commerce_order_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_order_handler_area_order_total.inc',
            3 => 'includes/views/handlers/commerce_order_handler_argument_order_order_id.inc',
            4 => 'includes/views/handlers/commerce_order_handler_field_order.inc',
            5 => 'includes/views/handlers/commerce_order_handler_field_order_status.inc',
            6 => 'includes/views/handlers/commerce_order_handler_field_order_state.inc',
            7 => 'includes/views/handlers/commerce_order_handler_field_order_type.inc',
            8 => 'includes/views/handlers/commerce_order_handler_field_order_link.inc',
            9 => 'includes/views/handlers/commerce_order_handler_field_order_link_delete.inc',
            10 => 'includes/views/handlers/commerce_order_handler_field_order_link_edit.inc',
            11 => 'includes/views/handlers/commerce_order_handler_field_order_mail.inc',
            12 => 'includes/views/handlers/commerce_order_handler_field_order_operations.inc',
            13 => 'includes/views/handlers/commerce_order_handler_filter_order_status.inc',
            14 => 'includes/views/handlers/commerce_order_handler_filter_order_state.inc',
            15 => 'includes/views/handlers/commerce_order_handler_filter_order_type.inc',
            16 => 'includes/views/handlers/commerce_order_plugin_argument_validate_user.inc',
            17 => 'tests/commerce_order.rules.test',
            18 => 'tests/commerce_order.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7109',
        'version' => '7.x-1.11',
      ),
      'commerce_payment_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/modules/commerce_payment_example.module',
        'basename' => 'commerce_payment_example.module',
        'name' => 'commerce_payment_example',
        'info' => 
        array (
          'name' => 'Payment Method Example',
          'description' => 'Provides an example payment method for testing and development.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_payment',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/commerce_payment.module',
        'basename' => 'commerce_payment.module',
        'name' => 'commerce_payment',
        'info' => 
        array (
          'name' => 'Payment',
          'description' => 'Implement core payment features for Drupal commerce.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_order',
            2 => 'entity',
            3 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'commerce_payment.rules.inc',
            1 => 'includes/commerce_payment_transaction.controller.inc',
            2 => 'includes/views/handlers/commerce_payment_handler_area_totals.inc',
            3 => 'includes/views/handlers/commerce_payment_handler_field_amount.inc',
            4 => 'includes/views/handlers/commerce_payment_handler_field_currency_code.inc',
            5 => 'includes/views/handlers/commerce_payment_handler_field_message.inc',
            6 => 'includes/views/handlers/commerce_payment_handler_field_payment_method.inc',
            7 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_link.inc',
            8 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_link_delete.inc',
            9 => 'includes/views/handlers/commerce_payment_handler_field_payment_transaction_operations.inc',
            10 => 'includes/views/handlers/commerce_payment_handler_field_status.inc',
            11 => 'includes/views/handlers/commerce_payment_handler_filter_payment_method.inc',
            12 => 'includes/views/handlers/commerce_payment_handler_filter_payment_transaction_status.inc',
            13 => 'includes/views/handlers/commerce_payment_handler_filter_currency_code.inc',
            14 => 'includes/views/handlers/commerce_payment_handler_field_balance.inc',
            15 => 'tests/commerce_payment.rules.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_payment_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/payment/commerce_payment_ui.module',
        'basename' => 'commerce_payment_ui.module',
        'name' => 'commerce_payment_ui',
        'info' => 
        array (
          'name' => 'Payment UI',
          'description' => 'Exposes a default UI for payment method configuration and payment transaction administration.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'rules_admin',
            1 => 'commerce',
            2 => 'commerce_order',
            3 => 'commerce_order_ui',
            4 => 'commerce_payment',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/payment-methods',
          'files' => 
          array (
            0 => 'tests/commerce_payment_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_tax' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/tax/commerce_tax.module',
        'basename' => 'commerce_tax.module',
        'name' => 'commerce_tax',
        'info' => 
        array (
          'name' => 'Tax',
          'description' => 'Define tax rates and configure tax rules for applicability and display.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'commerce_price',
            3 => 'commerce_product_pricing',
            4 => 'entity',
            5 => 'rules',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_tax_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/tax/commerce_tax_ui.module',
        'basename' => 'commerce_tax_ui.module',
        'name' => 'commerce_tax_ui',
        'info' => 
        array (
          'name' => 'Tax UI',
          'description' => 'Provides a UI for creating simple tax types and rates.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_tax',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/taxes',
          'files' => 
          array (
            0 => 'tests/commerce_tax_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.11',
      ),
      'commerce_checkout' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/checkout/commerce_checkout.module',
        'basename' => 'commerce_checkout.module',
        'name' => 'commerce_checkout',
        'info' => 
        array (
          'name' => 'Checkout',
          'description' => 'Enable checkout as a multi-step form with customizable checkout pages.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_ui',
            2 => 'commerce_order',
            3 => 'entity',
            4 => 'rules',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/checkout',
          'files' => 
          array (
            0 => 'tests/commerce_checkout.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7103',
        'version' => '7.x-1.11',
      ),
      'commerce_product_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product/commerce_product_ui.module',
        'basename' => 'commerce_product_ui.module',
        'name' => 'commerce_product_ui',
        'info' => 
        array (
          'name' => 'Product UI',
          'description' => 'Exposes a default UI for Products through product edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_product',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/products/types',
          'files' => 
          array (
            0 => 'tests/commerce_product_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'version' => '7.x-1.11',
      ),
      'commerce_product' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product/commerce_product.module',
        'basename' => 'commerce_product.module',
        'name' => 'commerce_product',
        'info' => 
        array (
          'name' => 'Product',
          'description' => 'Defines the Product entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_product.controller.inc',
            1 => 'includes/views/handlers/commerce_product_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_product_handler_argument_product_id.inc',
            3 => 'includes/views/handlers/commerce_product_handler_field_product.inc',
            4 => 'includes/views/handlers/commerce_product_handler_field_product_type.inc',
            5 => 'includes/views/handlers/commerce_product_handler_field_product_link.inc',
            6 => 'includes/views/handlers/commerce_product_handler_field_product_link_delete.inc',
            7 => 'includes/views/handlers/commerce_product_handler_field_product_link_edit.inc',
            8 => 'includes/views/handlers/commerce_product_handler_field_product_operations.inc',
            9 => 'includes/views/handlers/commerce_product_handler_filter_product_type.inc',
            10 => 'includes/commerce_product.translation_handler.inc',
            11 => 'tests/commerce_product.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7104',
        'version' => '7.x-1.11',
      ),
      'commerce_line_item_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/line_item/commerce_line_item_ui.module',
        'basename' => 'commerce_line_item_ui.module',
        'name' => 'commerce_line_item_ui',
        'info' => 
        array (
          'name' => 'Line Item UI',
          'description' => 'Exposes a default UI for Line Items through line item type forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_line_item',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/config/line-items',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_line_item' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/line_item/commerce_line_item.module',
        'basename' => 'commerce_line_item.module',
        'name' => 'commerce_line_item',
        'info' => 
        array (
          'name' => 'Line Item',
          'description' => 'Defines the Line Item entity and associated features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_price',
            2 => 'entity',
            3 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_line_item.controller.inc',
            1 => 'includes/views/handlers/commerce_line_item_handler_area_line_item_summary.inc',
            2 => 'includes/views/handlers/commerce_line_item_handler_argument_line_item_line_item_id.inc',
            3 => 'includes/views/handlers/commerce_line_item_handler_field_line_item_title.inc',
            4 => 'includes/views/handlers/commerce_line_item_handler_field_line_item_type.inc',
            5 => 'includes/views/handlers/commerce_line_item_handler_filter_line_item_type.inc',
            6 => 'includes/views/handlers/commerce_line_item_handler_field_edit_quantity.inc',
            7 => 'includes/views/handlers/commerce_line_item_handler_field_edit_delete.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.11',
      ),
      'commerce_customer_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/customer/commerce_customer_ui.module',
        'basename' => 'commerce_customer_ui.module',
        'name' => 'commerce_customer_ui',
        'info' => 
        array (
          'name' => 'Customer UI',
          'description' => 'Exposes a default UI for Customers through profile edit forms and default Views.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'field_ui',
            1 => 'commerce',
            2 => 'commerce_ui',
            3 => 'commerce_customer',
            4 => 'views',
          ),
          'core' => '7.x',
          'configure' => 'admin/commerce/customer-profiles/types',
          'files' => 
          array (
            0 => 'tests/commerce_customer_ui.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_customer' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/customer/commerce_customer.module',
        'basename' => 'commerce_customer.module',
        'name' => 'commerce_customer',
        'info' => 
        array (
          'name' => 'Customer',
          'description' => 'Defines the Customer entity with Address Field integration.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'addressfield',
            1 => 'commerce',
            2 => 'entity',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/commerce_customer_profile.controller.inc',
            1 => 'includes/views/handlers/commerce_customer_handler_area_empty_text.inc',
            2 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile.inc',
            3 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link.inc',
            4 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link_delete.inc',
            5 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_link_edit.inc',
            6 => 'includes/views/handlers/commerce_customer_handler_field_customer_profile_type.inc',
            7 => 'includes/views/handlers/commerce_customer_handler_filter_customer_profile_type.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_product_reference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/product_reference/commerce_product_reference.module',
        'basename' => 'commerce_product_reference.module',
        'name' => 'commerce_product_reference',
        'info' => 
        array (
          'name' => 'Product Reference',
          'description' => 'Defines a product reference field and default display formatters.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'commerce_price',
            3 => 'commerce_product',
            4 => 'entity',
            5 => 'options',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_product_reference_handler_filter_node_is_product_display.inc',
            1 => 'includes/views/handlers/commerce_product_reference_handler_filter_node_type.inc',
            2 => 'includes/views/handlers/commerce_product_reference_handler_filter_product_line_item_type.inc',
            3 => 'tests/commerce_product_reference.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce_cart' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/modules/cart/commerce_cart.module',
        'basename' => 'commerce_cart.module',
        'name' => 'commerce_cart',
        'info' => 
        array (
          'name' => 'Cart',
          'description' => 'Implements the shopping cart system and add to cart features.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_checkout',
            2 => 'commerce_line_item',
            3 => 'commerce_order',
            4 => 'commerce_product',
            5 => 'commerce_product_pricing',
            6 => 'commerce_product_reference',
            7 => 'entity',
            8 => 'rules',
            9 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'includes/views/handlers/commerce_cart_handler_field_add_to_cart_form.inc',
            1 => 'includes/views/handlers/commerce_cart_plugin_argument_default_current_cart_order_id.inc',
            2 => 'includes/views/handlers/commerce_cart_handler_area_empty_text.inc',
            3 => 'tests/commerce_cart.test',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/commerce_ui.module',
        'basename' => 'commerce_ui.module',
        'name' => 'commerce_ui',
        'info' => 
        array (
          'name' => 'Commerce UI',
          'description' => 'Defines menu items common to the various Drupal Commerce UI modules.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'commerce',
          ),
          'core' => '7.x',
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.11',
      ),
      'commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce/commerce.module',
        'basename' => 'commerce.module',
        'name' => 'commerce',
        'info' => 
        array (
          'name' => 'Commerce',
          'description' => 'Defines features and functions common to the Commerce modules. Must be enabled to uninstall other Commerce modules.',
          'package' => 'Commerce',
          'dependencies' => 
          array (
            0 => 'system',
            1 => 'entity',
            2 => 'rules',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/commerce_base.test',
            1 => 'includes/commerce.controller.inc',
          ),
          'version' => '7.x-1.11',
          'project' => 'commerce',
          'datestamp' => '1421426596',
          'php' => '5.2.4',
        ),
        'schema_version' => '7102',
        'version' => '7.x-1.11',
      ),
      'commerce_no_payment' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_no_payment/commerce_no_payment.module',
        'basename' => 'commerce_no_payment.module',
        'name' => 'commerce_no_payment',
        'info' => 
        array (
          'name' => 'No Payment',
          'description' => 'Upgrades No Payment 7.x-1.x to <a href="http://drupal.org/project/payment">Payment\'s</a> Basic payment method.',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'payment_commerce',
            1 => 'paymentmethodbasic',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/CommerceNoPaymentUpgrade.test',
          ),
          'version' => '7.x-2.0',
          'project' => 'commerce_no_payment',
          'datestamp' => '1364753711',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.0',
      ),
      'imce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/imce/imce.module',
        'basename' => 'imce.module',
        'name' => 'imce',
        'info' => 
        array (
          'name' => 'IMCE',
          'description' => 'An image/file uploader and browser supporting personal directories and user quota.',
          'core' => '7.x',
          'package' => 'Media',
          'configure' => 'admin/config/media/imce',
          'version' => '7.x-1.9',
          'project' => 'imce',
          'datestamp' => '1400275428',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.9',
      ),
      'term_merge' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/term_merge/term_merge.module',
        'basename' => 'term_merge.module',
        'name' => 'term_merge',
        'info' => 
        array (
          'name' => 'Term Merge',
          'description' => 'This module allows you to merge multiple terms into one, while updating all fields referring to those terms to refer to the replacement term instead.',
          'package' => 'Taxonomy',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'term_merge.test',
          ),
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'entity',
          ),
          'version' => '7.x-1.2',
          'project' => 'term_merge',
          'datestamp' => '1421194982',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'menu_attributes' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/menu_attributes/menu_attributes.module',
        'basename' => 'menu_attributes.module',
        'name' => 'menu_attributes',
        'info' => 
        array (
          'name' => 'Menu attributes',
          'description' => 'Allows administrators to specify custom attributes for menu items.',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/structure/menu/settings',
          'files' => 
          array (
            0 => 'menu_attributes.test',
          ),
          'version' => '7.x-1.0-rc3',
          'project' => 'menu_attributes',
          'datestamp' => '1413756231',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.0-rc3',
      ),
      'workflow_search_api' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_search_api/workflow_search_api.module',
        'basename' => 'workflow_search_api.module',
        'name' => 'workflow_search_api',
        'info' => 
        array (
          'name' => 'Workflow Search API',
          'description' => 'Adds workflow state information to Search API index',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'entity',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_revert' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_revert/workflow_revert.module',
        'basename' => 'workflow_revert.module',
        'name' => 'workflow_revert',
        'info' => 
        array (
          'name' => 'Workflow Revert',
          'description' => 'Adds an \'Revert\' link to the first workflow history row.',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_access' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_access/workflow_access.module',
        'basename' => 'workflow_access.module',
        'name' => 'workflow_access',
        'info' => 
        array (
          'name' => 'Workflow access',
          'description' => 'Content access control based on workflows and roles.',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_views/workflow_views.module',
        'basename' => 'workflow_views.module',
        'name' => 'workflow_views',
        'info' => 
        array (
          'name' => 'Workflow views',
          'description' => 'Provides views integration for workflows.',
          'package' => 'Workflow',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'includes/workflow_views.views.inc',
            1 => 'includes/workflow_views.views_default.inc',
            2 => 'includes/workflow_views_handler_filter_sid.inc',
            3 => 'includes/workflow_views_handler_field_sid.inc',
            4 => 'includes/workflow_views_handler_field_username.inc',
            5 => 'includes/workflow_views_handler_argument_state.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_rules' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_rules/workflow_rules.module',
        'basename' => 'workflow_rules.module',
        'name' => 'workflow_rules',
        'info' => 
        array (
          'name' => 'Workflow rules',
          'description' => 'Provides rules integration for workflows.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'rules',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_cleanup' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_cleanup/workflow_cleanup.module',
        'basename' => 'workflow_cleanup.module',
        'name' => 'workflow_cleanup',
        'info' => 
        array (
          'name' => 'Workflow Clean Up',
          'description' => 'Cleans up Workflow cruft',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'package' => 'Workflow',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_admin_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_admin_ui/workflow_admin_ui.module',
        'basename' => 'workflow_admin_ui.module',
        'name' => 'workflow_admin_ui',
        'info' => 
        array (
          'name' => 'Workflow UI',
          'description' => 'Provides administrative UI for workflow.',
          'package' => 'Workflow',
          'dependencies' => 
          array (
            0 => 'workflow',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/workflow/workflow',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'workflow_admin_ui.css',
            ),
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.2',
      ),
      'workflow_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_actions/workflow_actions.module',
        'basename' => 'workflow_actions.module',
        'name' => 'workflow_actions',
        'info' => 
        array (
          'name' => 'Workflow actions and triggers',
          'description' => 'Provides actions and triggers for workflows.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'trigger',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow_vbo' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow_vbo/workflow_vbo.module',
        'basename' => 'workflow_vbo.module',
        'name' => 'workflow_vbo',
        'info' => 
        array (
          'name' => 'Workflow VBO',
          'description' => 'Provides workflow actions for VBO.',
          'dependencies' => 
          array (
            0 => 'workflow',
            1 => 'views_bulk_operations',
          ),
          'package' => 'Workflow',
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'workflow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/workflow/workflow.module',
        'basename' => 'workflow.module',
        'name' => 'workflow',
        'info' => 
        array (
          'name' => 'Workflow',
          'description' => 'Allows the creation and assignment of arbitrary workflows to node types.',
          'package' => 'Workflow',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'workflow.pages.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'workflow',
          'datestamp' => '1372980654',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.2',
      ),
      'field_group' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/field_group/field_group.module',
        'basename' => 'field_group.module',
        'name' => 'field_group',
        'info' => 
        array (
          'name' => 'Field Group',
          'description' => 'Provides the ability to group your fields on both form and display.',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'ctools',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/field_group.ui.test',
            1 => 'tests/field_group.display.test',
          ),
          'version' => '7.x-1.4',
          'project' => 'field_group',
          'datestamp' => '1401918529',
          'php' => '5.2.4',
        ),
        'schema_version' => '7007',
        'version' => '7.x-1.4',
      ),
      'geophp' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geophp/geophp.module',
        'basename' => 'geophp.module',
        'name' => 'geophp',
        'info' => 
        array (
          'name' => 'geoPHP',
          'description' => 'Wraps the geoPHP library: advanced geometry operations in PHP',
          'core' => '7.x',
          'version' => '7.x-1.7',
          'project' => 'geophp',
          'datestamp' => '1352084822',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
      'adminrole' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/adminrole/adminrole.module',
        'basename' => 'adminrole.module',
        'name' => 'adminrole',
        'info' => 
        array (
          'name' => 'Admin Role',
          'description' => 'Automatically assign all permissions to an admin role.',
          'package' => 'Administration',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'adminrole',
          'datestamp' => '1351352467',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'customer_profile_type_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/customer_profile_type_ui/customer_profile_type_ui.module',
        'basename' => 'customer_profile_type_ui.module',
        'name' => 'customer_profile_type_ui',
        'info' => 
        array (
          'name' => 'Customer Profile Type UI',
          'description' => 'Provides the ability to dynamically add new customer profile types for the commerce module. Also, defines new rules events and actions to disable customer profile panes.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'commerce_customer_ui',
          ),
          'files' => 
          array (
            0 => 'customer_profile_type_ui.module',
            1 => 'includes/customer_profile_type_ui.ui.inc',
            2 => 'includes/customer_profile_type_ui.checkout_pane.inc',
            3 => 'customer_profile_type_ui.rules.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'customer_profile_type_ui',
          'datestamp' => '1359936335',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'mailsystem' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mailsystem/mailsystem.module',
        'basename' => 'mailsystem.module',
        'name' => 'mailsystem',
        'info' => 
        array (
          'package' => 'Mail',
          'name' => 'Mail System',
          'description' => 'Provides a user interface for per-module and site-wide mail_system selection.',
          'php' => '5.0',
          'core' => '7.x',
          'configure' => 'admin/config/system/mailsystem',
          'dependencies' => 
          array (
            0 => 'filter',
          ),
          'version' => '7.x-2.34',
          'project' => 'mailsystem',
          'datestamp' => '1334082653',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.34',
      ),
      'views_rss_dc' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/modules/views_rss_dc/views_rss_dc.module',
        'basename' => 'views_rss_dc.module',
        'name' => 'views_rss_dc',
        'info' => 
        array (
          'name' => 'Views RSS: DC Elements',
          'description' => 'Provides Dublin Core Metadata element set for Views RSS',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
          ),
          'core' => '7.x',
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-rc4',
      ),
      'views_rss_core' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/modules/views_rss_core/views_rss_core.module',
        'basename' => 'views_rss_core.module',
        'name' => 'views_rss_core',
        'info' => 
        array (
          'name' => 'Views RSS: Core Elements',
          'description' => 'Provides core XML channel and item elements for Views RSS.',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views_rss',
          ),
          'core' => '7.x',
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.0-rc4',
      ),
      'views_rss' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_rss/views_rss.module',
        'basename' => 'views_rss.module',
        'name' => 'views_rss',
        'info' => 
        array (
          'name' => 'Views RSS',
          'description' => 'Provides a views plugin that allows fields to be included in RSS feeds created using the Views module.',
          'package' => 'Views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'views/views_rss_plugin_style_fields.inc',
            1 => 'views/views_rss_handler_field_user_mail.inc',
            2 => 'views/views_rss_handler_field_term_node_tid.inc',
          ),
          'version' => '7.x-2.0-rc4',
          'project' => 'views_rss',
          'datestamp' => '1419363788',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.0-rc4',
      ),
      'views_data_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/views_data_export/views_data_export.module',
        'basename' => 'views_data_export.module',
        'name' => 'views_data_export',
        'info' => 
        array (
          'name' => 'Views Data Export',
          'description' => 'Plugin to export views data into various file formats',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'plugins/views_data_export_plugin_display_export.inc',
            1 => 'plugins/views_data_export_plugin_style_export.inc',
            2 => 'plugins/views_data_export_plugin_style_export_csv.inc',
            3 => 'plugins/views_data_export_plugin_style_export_xml.inc',
            4 => 'tests/base.test',
            5 => 'tests/csv_export.test',
            6 => 'tests/doc_export.test',
            7 => 'tests/txt_export.test',
            8 => 'tests/xls_export.test',
            9 => 'tests/xml_export.test',
          ),
          'version' => '7.x-3.0-beta8',
          'project' => 'views_data_export',
          'datestamp' => '1409118835',
          'php' => '5.2.4',
        ),
        'schema_version' => '7301',
        'version' => '7.x-3.0-beta8',
      ),
      'page_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/page_actions/page_actions.module',
        'basename' => 'page_actions.module',
        'name' => 'page_actions',
        'info' => 
        array (
          'name' => 'Page actions',
          'description' => 'Provides a unified method for making a page\'s form actions more accessible.',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'page_actions',
          'datestamp' => '1406912929',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'email' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/email/email.module',
        'basename' => 'email.module',
        'name' => 'email',
        'info' => 
        array (
          'name' => 'Email',
          'description' => 'Defines an email field type.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'email.migrate.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'email',
          'datestamp' => '1397134155',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'entityreference_behavior_example' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entityreference/examples/entityreference_behavior_example/entityreference_behavior_example.module',
        'basename' => 'entityreference_behavior_example.module',
        'name' => 'entityreference_behavior_example',
        'info' => 
        array (
          'name' => 'Entity Reference Behavior Example',
          'description' => 'Provides some example code for implementing Entity Reference behaviors.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entityreference',
          ),
          'version' => '7.x-1.1',
          'project' => 'entityreference',
          'datestamp' => '1384973110',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.1',
      ),
      'entityreference' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entityreference/entityreference.module',
        'basename' => 'entityreference.module',
        'name' => 'entityreference',
        'info' => 
        array (
          'name' => 'Entity Reference',
          'description' => 'Provides a field that can reference other entities.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'entityreference.migrate.inc',
            1 => 'plugins/selection/abstract.inc',
            2 => 'plugins/selection/views.inc',
            3 => 'plugins/behavior/abstract.inc',
            4 => 'views/entityreference_plugin_display.inc',
            5 => 'views/entityreference_plugin_style.inc',
            6 => 'views/entityreference_plugin_row_fields.inc',
            7 => 'tests/entityreference.handlers.test',
            8 => 'tests/entityreference.taxonomy.test',
            9 => 'tests/entityreference.admin.test',
            10 => 'tests/entityreference.feeds.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'entityreference',
          'datestamp' => '1384973110',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.x-1.1',
      ),
      'inline_conditions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/inline_conditions/inline_conditions.module',
        'basename' => 'inline_conditions.module',
        'name' => 'inline_conditions',
        'info' => 
        array (
          'name' => 'Inline Conditions',
          'description' => 'Manage frequently used conditions through an attached field.',
          'package' => 'Rules',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'version' => '7.x-1.0-alpha4',
          'project' => 'inline_conditions',
          'datestamp' => '1405675129',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha4',
      ),
      'metatag_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_devel/metatag_devel.module',
        'basename' => 'metatag_devel.module',
        'name' => 'metatag_devel',
        'info' => 
        array (
          'name' => 'Metatag:Devel',
          'description' => 'Provides development / debugging functionality for the Metatag module. Integrates with Devel Generate.',
          'package' => 'Development',
          'core' => '7.x',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_context' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_context/metatag_context.module',
        'basename' => 'metatag_context.module',
        'name' => 'metatag_context',
        'info' => 
        array (
          'name' => 'Metatag: Context',
          'description' => 'Assigned Metatag using Context definitions, allowing them to be assigned by path and other criteria.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'context',
            1 => 'metatag',
          ),
          'files' => 
          array (
            0 => 'metatag_context.test',
          ),
          'configure' => 'admin/config/search/metatags/context',
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_views' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_views/metatag_views.module',
        'basename' => 'metatag_views.module',
        'name' => 'metatag_views',
        'info' => 
        array (
          'name' => 'Metatag: Views',
          'description' => 'Provides Metatag integration within the Views interface.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'metatag_views_plugin_display_extender_metatags.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_facebook' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_facebook/metatag_facebook.module',
        'basename' => 'metatag_facebook.module',
        'name' => 'metatag_facebook',
        'info' => 
        array (
          'name' => 'Metatag: Facebook',
          'description' => 'Provides support for Facebook\'s custom meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_google_plus' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_google_plus/metatag_google_plus.module',
        'basename' => 'metatag_google_plus.module',
        'name' => 'metatag_google_plus',
        'info' => 
        array (
          'name' => 'Metatag: Google+',
          'description' => 'Provides support for Google+ \'itemscope\' meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'files' => 
          array (
            0 => 'metatag_google_plus.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_panels' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_panels/metatag_panels.module',
        'basename' => 'metatag_panels.module',
        'name' => 'metatag_panels',
        'info' => 
        array (
          'name' => 'Metatag: Panels',
          'description' => 'Provides Metatag integration within the Panels interface.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'metatag',
            2 => 'panels',
            3 => 'token',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_twitter_cards' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_twitter_cards/metatag_twitter_cards.module',
        'basename' => 'metatag_twitter_cards.module',
        'name' => 'metatag_twitter_cards',
        'info' => 
        array (
          'name' => 'Metatag: Twitter Cards',
          'description' => 'Provides support for Twitter\'s Card meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag_opengraph' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_opengraph/metatag_opengraph.module',
        'basename' => 'metatag_opengraph.module',
        'name' => 'metatag_opengraph',
        'info' => 
        array (
          'name' => 'Metatag:OpenGraph',
          'description' => 'Provides support for Open Graph Protocol meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.4',
      ),
      'metatag_dc' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag_dc/metatag_dc.module',
        'basename' => 'metatag_dc.module',
        'name' => 'metatag_dc',
        'info' => 
        array (
          'name' => 'Metatag: Dublin Core',
          'description' => 'Provides the fifteen <a href="http://dublincore.org/documents/dces/">Dublin Core Metadata Element Set 1.1</a> meta tags from the <a href="http://dublincore.org/">Dublin Core Metadata Institute</a>.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'metatag',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'metatag' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/metatag/metatag.module',
        'basename' => 'metatag.module',
        'name' => 'metatag',
        'info' => 
        array (
          'name' => 'Metatag',
          'description' => 'Adds support and an API to implement meta tags.',
          'package' => 'SEO',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'system (>=7.28)',
            1 => 'ctools',
            2 => 'token',
          ),
          'configure' => 'admin/config/search/metatags',
          'files' => 
          array (
            0 => 'metatag.inc',
            1 => 'metatag.migrate.inc',
            2 => 'metatag.test',
          ),
          'version' => '7.x-1.4',
          'project' => 'metatag',
          'datestamp' => '1412909330',
          'php' => '5.2.4',
        ),
        'schema_version' => '7035',
        'version' => '7.x-1.4',
      ),
      'reroute_email' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/reroute_email/reroute_email.module',
        'basename' => 'reroute_email.module',
        'name' => 'reroute_email',
        'info' => 
        array (
          'name' => 'Reroute emails',
          'description' => 'Reroutes emails send from the site to a predefined email. Useful for test sites.',
          'package' => 'Development',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'reroute_email.test',
          ),
          'configure' => 'admin/config/development/reroute_email',
          'version' => '7.x-1.2',
          'project' => 'reroute_email',
          'datestamp' => '1414831432',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.2',
      ),
      'diff' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/diff/diff.module',
        'basename' => 'diff.module',
        'name' => 'diff',
        'info' => 
        array (
          'name' => 'Diff',
          'description' => 'Show differences between content revisions.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'DiffEngine.php',
          ),
          'version' => '7.x-3.2',
          'project' => 'diff',
          'datestamp' => '1352784357',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7305',
        'version' => '7.x-3.2',
      ),
      'ckeditor' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/ckeditor/ckeditor.module',
        'basename' => 'ckeditor.module',
        'name' => 'ckeditor',
        'info' => 
        array (
          'name' => 'CKEditor',
          'description' => 'Enables CKEditor (WYSIWYG HTML editor) for use instead of plain text fields.',
          'core' => '7.x',
          'package' => 'User interface',
          'configure' => 'admin/config/content/ckeditor',
          'version' => '7.x-1.16',
          'project' => 'ckeditor',
          'datestamp' => '1413311935',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.x-1.16',
      ),
      'flag_bookmark' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag_bookmark/flag_bookmark.module',
        'basename' => 'flag_bookmark.module',
        'name' => 'flag_bookmark',
        'info' => 
        array (
          'name' => 'Flag Bookmark',
          'description' => 'Provides an example bookmark flag and supporting views.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'flag',
          ),
          'package' => 'Flags',
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-3.6',
      ),
      'flag' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag.module',
        'basename' => 'flag.module',
        'name' => 'flag',
        'info' => 
        array (
          'name' => 'Flag',
          'description' => 'Create customized flags that users can set on entities.',
          'core' => '7.x',
          'package' => 'Flags',
          'configure' => 'admin/structure/flags',
          'test_dependencies' => 
          array (
            0 => 'token',
            1 => 'rules',
          ),
          'files' => 
          array (
            0 => 'includes/flag/flag_flag.inc',
            1 => 'includes/flag/flag_entity.inc',
            2 => 'includes/flag/flag_node.inc',
            3 => 'includes/flag/flag_comment.inc',
            4 => 'includes/flag/flag_user.inc',
            5 => 'includes/flag.cookie_storage.inc',
            6 => 'includes/flag.entity.inc',
            7 => 'flag.rules.inc',
            8 => 'includes/views/flag_handler_argument_entity_id.inc',
            9 => 'includes/views/flag_handler_field_ops.inc',
            10 => 'includes/views/flag_handler_field_flagged.inc',
            11 => 'includes/views/flag_handler_filter_flagged.inc',
            12 => 'includes/views/flag_handler_sort_flagged.inc',
            13 => 'includes/views/flag_handler_relationships.inc',
            14 => 'includes/views/flag_plugin_argument_validate_flaggability.inc',
            15 => 'tests/flag.test',
          ),
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7306',
        'version' => '7.x-3.6',
      ),
      'flag_actions' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/flag/flag_actions.module',
        'basename' => 'flag_actions.module',
        'name' => 'flag_actions',
        'info' => 
        array (
          'name' => 'Flag actions',
          'description' => 'Execute actions on Flag events.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'flag',
          ),
          'package' => 'Flags',
          'configure' => 'admin/structure/flags/actions',
          'files' => 
          array (
            0 => 'flag.install',
            1 => 'flag_actions.module',
          ),
          'version' => '7.x-3.6',
          'project' => 'flag',
          'datestamp' => '1425327793',
          'php' => '5.2.4',
        ),
        'schema_version' => '6200',
        'version' => '7.x-3.6',
      ),
      'userreference_url' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/userreference_url/userreference_url.module',
        'basename' => 'userreference_url.module',
        'name' => 'userreference_url',
        'info' => 
        array (
          'name' => 'User Reference URL Widget',
          'description' => 'Adds an additional widget to the User Reference field that prepopulates a reference by the URL.',
          'dependencies' => 
          array (
            0 => 'user_reference',
          ),
          'package' => 'Fields',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'userreference_url',
          'datestamp' => '1334698318',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'simplehtmldom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/simplehtmldom/simplehtmldom.module',
        'basename' => 'simplehtmldom.module',
        'name' => 'simplehtmldom',
        'info' => 
        array (
          'name' => 'simplehtmldom API',
          'description' => 'A wrapper module around the simplehtmldom PHP library at sourceforge.',
          'package' => 'Filter',
          'core' => '7.x',
          'version' => '7.x-1.12',
          'project' => 'simplehtmldom',
          'datestamp' => '1307968619',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.12',
      ),
      'missing_module' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/missing_module/missing_module.module',
        'basename' => 'missing_module.module',
        'name' => 'missing_module',
        'info' => 
        array (
          'name' => 'Missing Module',
          'description' => 'Find modules turned on in the database that aren\'t in file system.',
          'package' => 'Performance',
          'core' => '7.x',
          'version' => '7.x-1.0',
          'project' => 'missing_module',
          'datestamp' => '1375756337',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'mandrill_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_test/mandrill_test.module',
        'basename' => 'mandrill_test.module',
        'name' => 'mandrill_test',
        'info' => 
        array (
          'name' => 'Mandrill Test',
          'description' => 'Used to test Mandrill API hooks. DO NOT ENABLE UNLESS YOU KNOW WHAT YOU ARE DOING. This module is for the purpose of testing Mandrill only.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'dependencies' => 
          array (
            0 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'tests/mandrill_hooks.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_reports' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_reports/mandrill_reports.module',
        'basename' => 'mandrill_reports.module',
        'name' => 'mandrill_reports',
        'info' => 
        array (
          'name' => 'Mandrill Reports',
          'description' => 'Providing reporting on activity through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'dependencies' => 
          array (
            0 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'tests/mandrill_reports.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_activity' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_activity/mandrill_activity.module',
        'basename' => 'mandrill_activity.module',
        'name' => 'mandrill_activity',
        'info' => 
        array (
          'name' => 'Mandrill Activity',
          'description' => 'View activity for an email address associated with any entity.',
          'package' => 'Mandrill',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'mandrill',
            1 => 'entity',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill_activity.entity.inc',
            1 => 'lib/mandrill_activity.ui_controller.inc',
            2 => 'tests/mandrill_activity.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'mandrill_template' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/modules/mandrill_template/mandrill_template.module',
        'basename' => 'mandrill_template.module',
        'name' => 'mandrill_template',
        'info' => 
        array (
          'name' => 'Mandrill Template',
          'description' => 'Use Mandrill templates for messages sent through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'configure' => 'admin/config/services/mandrill/templates',
          'dependencies' => 
          array (
            0 => 'entity (>=1.0)',
            1 => 'mandrill',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill_template_map.entity.inc',
            1 => 'lib/mandrill_template_map.ui_controller.inc',
            2 => 'tests/mandrill_template.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-2.1',
      ),
      'mandrill' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/mandrill/mandrill.module',
        'basename' => 'mandrill.module',
        'name' => 'mandrill',
        'info' => 
        array (
          'name' => 'Mandrill',
          'description' => 'Allow for site emails to be sent through Mandrill.',
          'core' => '7.x',
          'package' => 'Mandrill',
          'configure' => 'admin/config/services/mandrill',
          'dependencies' => 
          array (
            0 => 'mailsystem (>=2.x)',
            1 => 'libraries (>=2)',
          ),
          'files' => 
          array (
            0 => 'lib/mandrill.mail.inc',
            1 => 'lib/mandrill.inc',
            2 => 'tests/includes/mandrill_test.inc',
            3 => 'tests/mandrill.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'mandrill',
          'datestamp' => '1420838882',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.x-2.1',
      ),
      'references_dialog' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/references_dialog/references_dialog.module',
        'basename' => 'references_dialog.module',
        'name' => 'references_dialog',
        'info' => 
        array (
          'name' => 'References dialog',
          'description' => 'Enhances references fields by adding dialogs for adding and creating entities.',
          'package' => 'Fields',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'views',
          ),
          'files' => 
          array (
            0 => 'views/references_dialog_plugin_display.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'references_dialog',
          'datestamp' => '1405042428',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'commerce_fees' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_fees/commerce_fees.module',
        'basename' => 'commerce_fees.module',
        'name' => 'commerce_fees',
        'info' => 
        array (
          'name' => 'Commerce Fees',
          'description' => 'Implement a system for add custom fees to an order.',
          'package' => 'Commerce (contrib)',
          'core' => '7.x',
          'configure' => 'admin/commerce/config/fees',
          'dependencies' => 
          array (
            0 => 'commerce',
            1 => 'commerce_line_item',
            2 => 'rules',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'commerce_fees',
          'datestamp' => '1391006907',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'search404' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search404/search404.module',
        'basename' => 'search404.module',
        'name' => 'search404',
        'info' => 
        array (
          'name' => 'Search 404',
          'description' => 'Automatically search for the keywords in URLs that result in 404 errors and show results instead of Page-Not-Found. ',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search404.module',
            1 => 'search404.install',
          ),
          'configure' => 'admin/config/search/search404',
          'version' => '7.x-1.3',
          'project' => 'search404',
          'datestamp' => '1370809553',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.3',
      ),
      'smartcrop' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/smartcrop/smartcrop.module',
        'basename' => 'smartcrop.module',
        'name' => 'smartcrop',
        'info' => 
        array (
          'name' => 'SmartCrop',
          'description' => 'Crops low-entropy parts of the image.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'image',
          ),
          'package' => 'Image',
          'files' => 
          array (
            0 => 'imageapi_gd.inc',
            1 => 'smartcrop.install',
            2 => 'smartcrop.module',
            3 => 'smartcrop.effects.inc',
            4 => 'tests/smartcrop.test',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'smartcrop',
          'datestamp' => '1291750843',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'geocoder_autocomplete' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/geocoder_autocomplete/geocoder_autocomplete.module',
        'basename' => 'geocoder_autocomplete.module',
        'name' => 'geocoder_autocomplete',
        'info' => 
        array (
          'name' => 'Geocoder Autocomplete',
          'description' => 'Provides a text field widget with autocomplete via the Google Geocoding API.',
          'core' => '7.x',
          'version' => '7.x-1.x-dev',
          'project' => 'geocoder_autocomplete',
          'datestamp' => '1380580681',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.x-dev',
      ),
      'select_or_other' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/select_or_other/select_or_other.module',
        'basename' => 'select_or_other.module',
        'name' => 'select_or_other',
        'info' => 
        array (
          'name' => 'Select (or other)',
          'description' => 'Provides a select box form element with additional option \'Other\' to give a textfield.',
          'core' => '7.x',
          'package' => 'Fields',
          'version' => '7.x-2.20',
          'project' => 'select_or_other',
          'datestamp' => '1378331245',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'version' => '7.x-2.20',
      ),
      'entity_bundle_plugin_test' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity_bundle_plugin/tests/entity_bundle_plugin_test.module',
        'basename' => 'entity_bundle_plugin_test.module',
        'name' => 'entity_bundle_plugin_test',
        'info' => 
        array (
          'name' => 'Entity Bundle Plugin Test',
          'description' => 'Test support and example module for Entity Bundle Plugin.',
          'package' => 'Entity',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity_bundle_plugin',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'entity_bundle_plugin',
          'datestamp' => '1372669557',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'entity_bundle_plugin' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/entity_bundle_plugin/entity_bundle_plugin.module',
        'basename' => 'entity_bundle_plugin.module',
        'name' => 'entity_bundle_plugin',
        'info' => 
        array (
          'name' => 'Entity Bundle Plugin',
          'description' => 'Provide supports for entity types which bundles are plugin classes.',
          'package' => 'Entity',
          'core' => '7.x',
          'php' => '5.3',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'tests/entity_bundle_plugin_merge.test',
            1 => 'entity_bundle_plugin.controller.inc',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'entity_bundle_plugin',
          'datestamp' => '1372669557',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'commerce_rules_extra' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/commerce_rules_extra/commerce_rules_extra.module',
        'basename' => 'commerce_rules_extra.module',
        'name' => 'commerce_rules_extra',
        'info' => 
        array (
          'name' => 'Commerce Rules Extra',
          'description' => 'Extra Commerce Rules.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_line_item',
            1 => 'taxonomy',
            2 => 'rules',
          ),
          'version' => '7.x-2.1',
          'project' => 'commerce_rules_extra',
          'datestamp' => '1417509188',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.1',
      ),
      'search_api_attachments_field_collections' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_attachments/contrib/search_api_attachments_field_collections/search_api_attachments_field_collections.module',
        'basename' => 'search_api_attachments_field_collections.module',
        'name' => 'search_api_attachments_field_collections',
        'info' => 
        array (
          'name' => 'Search API Attachments Field Collections',
          'description' => 'Extends the attachments indexing to file fields in the referenced field collection',
          'dependencies' => 
          array (
            0 => 'field_collection',
            1 => 'search_api_attachments',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'files' => 
          array (
            0 => 'includes/callback_attachments_field_collections_settings.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'search_api_attachments',
          'datestamp' => '1402272545',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.4',
      ),
      'search_api_attachments' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/search_api_attachments/search_api_attachments.module',
        'basename' => 'search_api_attachments.module',
        'name' => 'search_api_attachments',
        'info' => 
        array (
          'name' => 'Search API attachments',
          'description' => 'Provides a generic API for modules offering search capabilites.',
          'dependencies' => 
          array (
            0 => 'search_api',
          ),
          'core' => '7.x',
          'package' => 'Search',
          'configure' => 'admin/config/search/search_api/attachments',
          'files' => 
          array (
            0 => 'includes/callback_attachments_settings.inc',
          ),
          'version' => '7.x-1.4',
          'project' => 'search_api_attachments',
          'datestamp' => '1402272545',
          'php' => '5.2.4',
        ),
        'schema_version' => '7101',
        'version' => '7.x-1.4',
      ),
      'environment_indicator_variable' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/environment_indicator/environment_indicator_variable/environment_indicator_variable.module',
        'basename' => 'environment_indicator_variable.module',
        'name' => 'environment_indicator_variable',
        'info' => 
        array (
          'name' => 'Environment indicator variables',
          'description' => 'Adds a variable a realm for environment.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'variable_realm (7.x-2.x)',
            1 => 'variable_store (7.x-2.x)',
            2 => 'environment_indicator',
          ),
          'files' => 
          array (
            0 => 'class/EnvironmentVariableRealmController.php',
          ),
          'version' => '7.x-2.5',
          'project' => 'environment_indicator',
          'datestamp' => '1403432932',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-2.5',
      ),
      'environment_indicator' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/environment_indicator/environment_indicator.module',
        'basename' => 'environment_indicator.module',
        'name' => 'environment_indicator',
        'info' => 
        array (
          'name' => 'Environment indicator',
          'description' => 'Adds a color indicator for the different environments.',
          'core' => '7.x',
          'configure' => 'admin/config/development/environment-indicator',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'scripts' => 
          array (
            0 => 'tinycon.min.js',
            1 => 'environment_indicator.js',
            2 => 'color.js',
          ),
          'files' => 
          array (
            0 => 'plugins/export_ui/ctools_export_ui_environment_indicator.class.php',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'environment_indicator.css',
            ),
          ),
          'version' => '7.x-2.5',
          'project' => 'environment_indicator',
          'datestamp' => '1403432932',
          'php' => '5.2.4',
        ),
        'schema_version' => '7202',
        'version' => '7.x-2.5',
      ),
      'rules_conditional' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/contrib/rules_conditional/rules_conditional.module',
        'basename' => 'rules_conditional.module',
        'name' => 'rules_conditional',
        'info' => 
        array (
          'name' => 'Conditional Rules',
          'description' => 'Provides conditional control elements within action containers.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules',
          ),
          'package' => 'Rules',
          'files' => 
          array (
            0 => 'includes/rules_conditional.core.inc',
            1 => 'includes/rules_conditional.plugin.inc',
            2 => 'includes/rules_conditional.ui.inc',
            3 => 'tests/rules_conditional.test',
          ),
          'version' => '7.x-1.0-beta2',
          'project' => 'rules_conditional',
          'datestamp' => '1352752992',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta2',
      ),
      'rules_batch' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/rules_batch/rules_batch.module',
        'basename' => 'rules_batch.module',
        'name' => 'rules_batch',
        'info' => 
        array (
          'name' => 'Rules Batch',
          'description' => 'Provides a batched rule loop component to rules.',
          'package' => 'Rules',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rules (2.x)',
          ),
          'files' => 
          array (
            0 => 'rules_batch.rules.inc',
          ),
          'version' => '7.x-1.0-beta1',
          'project' => 'rules_batch',
          'datestamp' => '1355145533',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-beta1',
      ),
      'exif_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/exif_custom/exif_custom.module',
        'basename' => 'exif_custom.module',
        'name' => 'exif_custom',
        'info' => 
        array (
          'name' => 'Custom EXIF mapping',
          'description' => 'Save EXIF data to custom fields',
          'package' => 'Image',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file_entity',
          ),
          'version' => '7.x-1.14',
          'project' => 'exif_custom',
          'datestamp' => '1390318106',
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.x-1.14',
      ),
      'tagged_text' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/tagged_text/tagged_text.module',
        'basename' => 'tagged_text.module',
        'name' => 'tagged_text',
        'info' => 
        array (
          'name' => 'Tagged text',
          'description' => 'Generates tagged text files from content.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'includes/handlers/TaggedTextFieldHandler.inc',
            1 => 'includes/handlers/TaggedTextFieldHandlerMultiple.inc',
            2 => 'includes/handlers/TaggedTextFieldHandlerSingle.inc',
            3 => 'includes/handlers/TaggedTextFieldHandlerText.inc',
            4 => 'includes/handlers/TaggedTextFieldHandlerImage.inc',
            5 => 'includes/handlers/TaggedTextFieldHandlerReferenceNode.inc',
            6 => 'includes/views/tagged_text_handler_field_node_tagged_text_link.inc',
            7 => 'includes/views/tagged_text_plugin_display_archive.inc',
            8 => 'includes/views/tagged_text_plugin_style_archive.inc',
            9 => 'includes/views/tagged_text_plugin_row_node_view.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => NULL,
      ),
      'wordcounter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/wordcounter/wordcounter.module',
        'basename' => 'wordcounter.module',
        'name' => 'wordcounter',
        'info' => 
        array (
          'name' => 'Word Counter',
          'description' => 'Monitor number of words used in an entity and record the value.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => NULL,
      ),
      'gleaner_import' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_import/gleaner_import.module',
        'basename' => 'gleaner_import.module',
        'name' => 'gleaner_import',
        'info' => 
        array (
          'name' => 'Gleaner import',
          'description' => 'Import Gleaner articles.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'aiocalendar' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/aiocalendar/aiocalendar.module',
        'basename' => 'aiocalendar.module',
        'name' => 'aiocalendar',
        'info' => 
        array (
          'name' => 'All-in-One Calendar',
          'description' => 'All-in-one calendar',
          'core' => '7.x',
          'package' => 'All-in-One',
          'version' => '7.x-1.0-alpha2',
          'project' => 'aiocalendar',
          'dependencies' => 
          array (
            0 => 'date_popup',
            1 => 'ds',
            2 => 'filefield_sources',
            3 => 'gleaner_features',
            4 => 'image',
            5 => 'taxonomy',
            6 => 'token',
          ),
          'datestamp' => '1383156310',
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'strongarm:strongarm:1',
              1 => 'views:views_default:3.0',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'field_date',
              1 => 'field_event_location',
              2 => 'field_event_type_event',
            ),
            'field_instance' => 
            array (
              0 => 'node-calendar_event-body',
              1 => 'node-calendar_event-field_date',
              2 => 'node-calendar_event-field_event_location',
              3 => 'node-calendar_event-field_event_type_event',
              4 => 'node-calendar_event-field_image',
            ),
            'node' => 
            array (
              0 => 'calendar_event',
            ),
            'taxonomy' => 
            array (
              0 => 'event_type',
            ),
            'user_permission' => 
            array (
              0 => 'create calendar_event content',
              1 => 'delete any calendar_event content',
              2 => 'delete own calendar_event content',
              3 => 'edit any calendar_event content',
              4 => 'edit own calendar_event content',
            ),
            'variable' => 
            array (
              0 => 'additional_settings__active_tab_calendar_event',
              1 => 'comment_anonymous_calendar_event',
              2 => 'comment_calendar_event',
              3 => 'comment_default_mode_calendar_event',
              4 => 'comment_default_per_page_calendar_event',
              5 => 'comment_form_location_calendar_event',
              6 => 'comment_preview_calendar_event',
              7 => 'comment_subject_field_calendar_event',
              8 => 'date_format_full',
              9 => 'field_bundle_settings_node__calendar_event',
              10 => 'menu_options_calendar_event',
              11 => 'menu_parent_calendar_event',
              12 => 'node_options_calendar_event',
              13 => 'node_preview_calendar_event',
              14 => 'node_submitted_calendar_event',
            ),
            'views_view' => 
            array (
              0 => 'events_calendar',
            ),
          ),
          'features_exclude' => 
          array (
            'taxonomy' => 
            array (
              'events' => 'events',
            ),
            'dependencies' => 
            array (
              'calendar' => 'calendar',
              'date' => 'date',
              'date_ical' => 'date_ical',
              'date_views' => 'date_views',
              'better_exposed_filters' => 'better_exposed_filters',
              'views' => 'views',
            ),
          ),
          'files' => 
          array (
            0 => 'aiocalendar.module',
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha2',
      ),
      'gleaner_license' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_license/gleaner_license.module',
        'basename' => 'gleaner_license.module',
        'name' => 'gleaner_license',
        'info' => 
        array (
          'name' => 'Gleaner License',
          'description' => 'Provices a simple license type for Gleaner subscriptions.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_license',
          ),
          'files' => 
          array (
            0 => 'plugins/license_type/GleanerLicense.class.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'print_export' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/print_export/print_export.module',
        'basename' => 'print_export.module',
        'name' => 'print_export',
        'info' => 
        array (
          'name' => 'Print export',
          'description' => 'Export tagged text and image files from content.',
          'core' => '7.x',
          'configure' => 'admin/config/content/print-export',
          'dependencies' => 
          array (
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'image_archive' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/image_archive/image_archive.module',
        'basename' => 'image_archive.module',
        'name' => 'image_archive',
        'info' => 
        array (
          'name' => 'Image archive',
          'description' => 'Provides an archived download of all images tied to an entity.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'files' => 
          array (
            0 => 'views/image_archive_handler_field_image_archive_link.inc',
            1 => 'views/image_archive_handler_field_image_archive_link_node.inc',
            2 => 'views/image_archive_plugin_display_archive.inc',
            3 => 'views/image_archive_plugin_style_archive.inc',
            4 => 'views/image_archive_plugin_row_node_view.inc',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_search' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_search/gleaner_search.module',
        'basename' => 'gleaner_search.module',
        'name' => 'gleaner_search',
        'info' => 
        array (
          'name' => 'Gleaner Search',
          'description' => 'Gleaner search features',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.0',
          'project' => 'gleaner_search',
          'dependencies' => 
          array (
            0 => 'better_exposed_filters',
            1 => 'current_search',
            2 => 'date_facets',
            3 => 'ds_extras',
            4 => 'elysia_cron',
            5 => 'entity',
            6 => 'facetapi',
            7 => 'facetapi_bonus',
            8 => 'facetapi_pretty_paths',
            9 => 'search_api',
            10 => 'search_api_facetapi',
            11 => 'search_api_index_status',
            12 => 'search_api_solr',
            13 => 'search_api_views',
            14 => 'views',
            15 => 'workflow_search_api',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'current_search:current_search:1',
              1 => 'ds:ds:1',
              2 => 'ds_extras:ds_extras:1',
              3 => 'elysia_cron:default_elysia_cron_rules:1',
              4 => 'facetapi:facetapi_defaults:1',
              5 => 'views:views_default:3.0',
            ),
            'current_search' => 
            array (
              0 => 'columns',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|search-page|default',
            ),
            'ds_fields' => 
            array (
              0 => 'gn_search',
              1 => 'gn_search_author',
              2 => 'gn_search_current',
              3 => 'gn_search_date_published',
              4 => 'gn_search_issue',
              5 => 'gn_search_news_section',
              6 => 'gn_search_node_type',
              7 => 'gn_search_section',
              8 => 'gn_search_tags',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|search-page|default',
            ),
            'ds_vd' => 
            array (
              0 => 'search-page',
            ),
            'ds_view_modes' => 
            array (
              0 => 'search_result',
            ),
            'elysia_cron' => 
            array (
              0 => 'search_api_cron',
              1 => 'search_api_solr_cron',
            ),
            'facetapi' => 
            array (
              0 => 'search_api@node_search::field_section',
              1 => 'search_api@node_search:block:field_news_section',
              2 => 'search_api@node_search:block:field_section',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'search_api_index' => 
            array (
              0 => 'node_search',
            ),
            'search_api_server' => 
            array (
              0 => 'solr_search',
            ),
            'views_view' => 
            array (
              0 => 'columns',
              1 => 'search',
              2 => 'search_more_like_this',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
              'ds' => 'ds',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0',
      ),
      'gn_commerce' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gn_commerce/gn_commerce.module',
        'basename' => 'gn_commerce.module',
        'name' => 'gn_commerce',
        'info' => 
        array (
          'name' => 'Gleaner Commerce',
          'description' => 'Gleaner Commerce features for ads and subscriptions.',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.0-alpha17',
          'project' => 'gn_commerce',
          'dependencies' => 
          array (
            0 => 'commerce_custom_product',
            1 => 'commerce_discount',
            2 => 'commerce_features',
            3 => 'commerce_fees',
            4 => 'commerce_payment_example',
            5 => 'commerce_payment_ui',
            6 => 'commerce_product_attributes',
            7 => 'commerce_product_pricing_ui',
            8 => 'commerce_rules_extra',
            9 => 'commerce_wordcounter',
            10 => 'customer_profile_type_ui',
            11 => 'field_group',
            12 => 'file',
            13 => 'gleaner_features',
            14 => 'gleaner_license',
            15 => 'inline_entity_form',
            16 => 'scheduler',
          ),
          'features' => 
          array (
            'commerce_discount' => 
            array (
              0 => 'discount_npuc_member_discount',
            ),
            'commerce_line_item_type' => 
            array (
              0 => 'custom_ad',
              1 => 'gn_classified',
              2 => 'gn_email_ad',
            ),
            'commerce_product_type' => 
            array (
              0 => 'commerce_node_checkout',
              1 => 'product',
            ),
            'ctools' => 
            array (
              0 => 'ds:ds:1',
              1 => 'ds_extras:ds_extras:1',
              2 => 'field_group:field_group:1',
              3 => 'strongarm:strongarm:1',
              4 => 'views:views_default:3.0',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|classified-page|default',
              1 => 'node|gn_classified_ad|default',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|classified-page|default',
              1 => 'node|gn_classified_ad|default',
            ),
            'ds_vd' => 
            array (
              0 => 'classified-page',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'commerce_customer_address',
              1 => 'commerce_display_path',
              2 => 'commerce_license',
              3 => 'commerce_license_duration',
              4 => 'commerce_license_type',
              5 => 'commerce_price',
              6 => 'commerce_product',
              7 => 'commerce_total',
              8 => 'commerce_unit_price',
              9 => 'field_available_issues',
              10 => 'field_category',
              11 => 'field_design_fee',
              12 => 'field_design_fee2',
              13 => 'field_node_order',
              14 => 'field_online_early',
              15 => 'field_product',
              16 => 'field_special_requests',
              17 => 'field_upload_ad',
              18 => 'field_word_count',
              19 => 'field_word_counter',
            ),
            'field_group' => 
            array (
              0 => 'group_price|node|gn_classified_ad|form',
            ),
            'field_instance' => 
            array (
              0 => 'commerce_customer_profile-gn_mail_add-commerce_customer_address',
              1 => 'commerce_line_item-custom_ad-commerce_display_path',
              2 => 'commerce_line_item-custom_ad-commerce_product',
              3 => 'commerce_line_item-custom_ad-commerce_total',
              4 => 'commerce_line_item-custom_ad-commerce_unit_price',
              5 => 'commerce_line_item-custom_ad-field_available_issues',
              6 => 'commerce_line_item-custom_ad-field_design_fee2',
              7 => 'commerce_line_item-gn_classified-commerce_display_path',
              8 => 'commerce_line_item-gn_classified-commerce_product',
              9 => 'commerce_line_item-gn_classified-commerce_total',
              10 => 'commerce_line_item-gn_classified-commerce_unit_price',
              11 => 'commerce_line_item-gn_email_ad-commerce_display_path',
              12 => 'commerce_line_item-gn_email_ad-commerce_product',
              13 => 'commerce_line_item-gn_email_ad-commerce_total',
              14 => 'commerce_line_item-gn_email_ad-commerce_unit_price',
              15 => 'commerce_line_item-gn_online_ad-commerce_display_path',
              16 => 'commerce_line_item-gn_online_ad-commerce_product',
              17 => 'commerce_line_item-gn_online_ad-commerce_total',
              18 => 'commerce_line_item-gn_online_ad-commerce_unit_price',
              19 => 'commerce_line_item-gn_print_ad-commerce_display_path',
              20 => 'commerce_line_item-gn_print_ad-commerce_product',
              21 => 'commerce_line_item-gn_print_ad-commerce_total',
              22 => 'commerce_line_item-gn_print_ad-commerce_unit_price',
              23 => 'commerce_line_item-gn_print_ad-field_available_issues',
              24 => 'commerce_line_item-gn_print_ad-field_design_fee2',
              25 => 'commerce_line_item-gn_subscription-commerce_display_path',
              26 => 'commerce_line_item-gn_subscription-commerce_product',
              27 => 'commerce_line_item-gn_subscription-commerce_total',
              28 => 'commerce_line_item-gn_subscription-commerce_unit_price',
              29 => 'commerce_product-commerce_node_checkout-commerce_price',
              30 => 'commerce_product-product-commerce_price',
              31 => 'commerce_product-subscription-commerce_price',
              32 => 'node-gn_classified_ad-body',
              33 => 'node-gn_classified_ad-field_available_issues',
              34 => 'node-gn_classified_ad-field_category',
              35 => 'node-gn_classified_ad-field_node_order',
              36 => 'node-gn_classified_ad-field_online_early',
              37 => 'node-gn_classified_ad-field_special_requests',
              38 => 'node-gn_classified_ad-field_word_counter',
              39 => 'node-gn_email_ad-body',
              40 => 'node-gn_email_ad-field_product',
              41 => 'node-gn_online_ad-body',
              42 => 'node-gn_online_ad-field_product',
              43 => 'node-gn_print_ad-body',
              44 => 'node-gn_print_ad-field_product',
              45 => 'node-gn_subscription-body',
              46 => 'node-gn_subscription-field_product',
            ),
            'node' => 
            array (
              0 => 'gn_classified_ad',
            ),
            'rules_config' => 
            array (
              0 => 'rules_add_order_reference',
              1 => 'rules_check_billing_state',
              2 => 'rules_classified_delete_old',
              3 => 'rules_classified_evaluate_last_issue',
              4 => 'rules_classified_publish_check_current_issue',
              5 => 'rules_classified_straight_to_cart',
              6 => 'rules_design_fees',
              7 => 'rules_early_check_publish',
              8 => 'rules_early_check_schedule',
              9 => 'rules_gleaner_online_early_fee',
              10 => 'rules_gn_assign_issue_print_content',
              11 => 'rules_gn_assign_issue_print_content_list_item',
              12 => 'rules_gn_check_node_for_early_fee',
              13 => 'rules_gn_classified_quantity',
              14 => 'rules_gn_combine_classified',
              15 => 'rules_gn_combine_print',
              16 => 'rules_gn_commerce_hide_mailing',
              17 => 'rules_gn_commerce_node_checkout_reference',
              18 => 'rules_gn_commerce_online_early_check',
              19 => 'rules_gn_commerce_publish_classified',
              20 => 'rules_gn_subscription_pane',
              21 => 'rules_issue_based_ads',
              22 => 'rules_worksheets_send_to_print',
            ),
            'variable' => 
            array (
              0 => 'auto_entitylabel_node_gn_classified_ad',
              1 => 'auto_entitylabel_pattern_node_gn_classified_ad',
              2 => 'auto_entitylabel_php_node_gn_classified_ad',
              3 => 'field_bundle_settings_node__gn_classified_ad',
              4 => 'menu_options_gn_classified_ad',
              5 => 'menu_parent_gn_classified_ad',
              6 => 'node_options_gn_classified_ad',
              7 => 'node_preview_gn_classified_ad',
              8 => 'node_submitted_gn_classified_ad',
            ),
            'views_view' => 
            array (
              0 => 'classified',
              1 => 'classified_admin',
              2 => 'classified_customer',
              3 => 'upcoming_issues',
            ),
          ),
          'features_exclude' => 
          array (
            'dependencies' => 
            array (
              'ctools' => 'ctools',
              'number' => 'number',
              'commerce' => 'commerce',
              'commerce_checkout' => 'commerce_checkout',
              'commerce_line_item' => 'commerce_line_item',
              'commerce_node_checkout' => 'commerce_node_checkout',
              'commerce_order' => 'commerce_order',
              'commerce_price' => 'commerce_price',
              'commerce_product_reference' => 'commerce_product_reference',
              'entity' => 'entity',
              'entityreference' => 'entityreference',
              'features' => 'features',
              'options' => 'options',
              'rules' => 'rules',
              'taxonomy' => 'taxonomy',
              'wordcounter' => 'wordcounter',
              'views' => 'views',
            ),
            'field_base' => 
            array (
              'body' => 'body',
            ),
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.0-alpha17',
      ),
      'gleaner_copy' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_copy/gleaner_copy.module',
        'basename' => 'gleaner_copy.module',
        'name' => 'gleaner_copy',
        'info' => 
        array (
          'name' => 'Gleaner copy',
          'description' => 'Provides a copy button above the Print Body field that copies various field values and inserts them in the Print Body field.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_features' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_features/gleaner_features.module',
        'basename' => 'gleaner_features.module',
        'name' => 'gleaner_features',
        'info' => 
        array (
          'name' => 'Gleaner Features',
          'core' => '7.x',
          'package' => 'Gleaner',
          'version' => '7.x-1.12',
          'project' => 'gleaner_features',
          'dependencies' => 
          array (
            0 => 'addressfield',
            1 => 'addthis',
            2 => 'better_exposed_filters',
            3 => 'calendar',
            4 => 'ckeditor',
            5 => 'computed_field',
            6 => 'custom_pub',
            7 => 'date_ical',
            8 => 'ds_extras',
            9 => 'elysia_cron',
            10 => 'email',
            11 => 'environment_indicator',
            12 => 'eva',
            13 => 'facetapi',
            14 => 'features',
            15 => 'field_default_token',
            16 => 'field_group',
            17 => 'file_entity_inline',
            18 => 'filefield_paths',
            19 => 'filefield_sources_plupload',
            20 => 'geocoder',
            21 => 'geocoder_autocomplete',
            22 => 'geofield',
            23 => 'gleaner_copy',
            24 => 'gleaner_custom',
            25 => 'image_archive',
            26 => 'link',
            27 => 'list',
            28 => 'mailchimp_lists',
            29 => 'mandrill_template',
            30 => 'menu',
            31 => 'metatag',
            32 => 'name',
            33 => 'node_reference',
            34 => 'number',
            35 => 'page_manager',
            36 => 'path_breadcrumbs',
            37 => 'picture',
            38 => 'search_api_solr',
            39 => 'select_or_other',
            40 => 'shs',
            41 => 'strongarm',
            42 => 'tagged_text',
            43 => 'taxonomy_display',
            44 => 'telephone',
            45 => 'text',
            46 => 'user_reference',
            47 => 'video_embed_field',
            48 => 'views_accordion',
            49 => 'views_content',
            50 => 'views_content_cache',
            51 => 'views_data_export',
            52 => 'views_litepager',
            53 => 'views_responsive_grid',
            54 => 'views_rss',
            55 => 'views_wookmark',
            56 => 'workflow_access',
            57 => 'workflow_rules',
          ),
          'features' => 
          array (
            'breakpoint_group' => 
            array (
              0 => 'gleanernow',
              1 => 'gn_article_gallery',
              2 => 'gn_author_cover_feature',
              3 => 'gn_author_photo',
              4 => 'gn_feature_hero',
              5 => 'gn_feature_hero_text',
              6 => 'gn_gallery_teaser',
              7 => 'gn_home_images',
              8 => 'gn_issue_feature',
              9 => 'gn_issue_teaser',
              10 => 'gn_section_feature',
              11 => 'gn_section_teaser',
              12 => 'gn_slideshow',
              13 => 'gn_slideshow_thumbs',
            ),
            'breakpoints' => 
            array (
              0 => 'breakpoints.theme.gleanernow.mobile',
              1 => 'breakpoints.theme.gleanernow.narrow',
              2 => 'breakpoints.theme.gleanernow.wide',
            ),
            'ckeditor_profile' => 
            array (
              0 => 'Advanced',
              1 => 'Full',
              2 => 'Print_Body',
              3 => 'Simple',
            ),
            'conditional_fields' => 
            array (
              0 => 'node:photo_contest_entry',
            ),
            'ctools' => 
            array (
              0 => 'breakpoints:default_breakpoint_group:1',
              1 => 'breakpoints:default_breakpoints:1',
              2 => 'ds:ds:1',
              3 => 'ds_extras:ds_extras:1',
              4 => 'elysia_cron:default_elysia_cron_rules:1',
              5 => 'environment_indicator:default_environment_indicator_environments:1',
              6 => 'facetapi:facetapi_defaults:1',
              7 => 'field_group:field_group:1',
              8 => 'file_entity:file_default_displays:1',
              9 => 'page_manager:pages_default:1',
              10 => 'path_breadcrumbs:path_breadcrumbs:1',
              11 => 'picture:default_picture_mapping:1',
              12 => 'strongarm:strongarm:1',
              13 => 'taxonomy_display:taxonomy_display:1',
              14 => 'video_embed_field:default_video_embed_styles:1',
              15 => 'views:views_default:3.0',
            ),
            'custom_pub' => 
            array (
              0 => 'print',
              1 => 'web',
            ),
            'ds_field_settings' => 
            array (
              0 => 'ds_views|authors-page|default',
              1 => 'ds_views|columns-page|default',
              2 => 'ds_views|columns_content-page|default',
              3 => 'ds_views|events_calendar-page_1|default',
              4 => 'ds_views|events_calendar-page_4-fields|default',
              5 => 'ds_views|events_calendar-page_4|default',
              6 => 'ds_views|events_calendar-page|default',
              7 => 'ds_views|family-page_births|default',
              8 => 'ds_views|features-page|default',
              9 => 'ds_views|gn_gallery-page|default',
              10 => 'ds_views|home-page_1|default',
              11 => 'ds_views|home-page|default',
              12 => 'ds_views|inprint-page|default',
              13 => 'ds_views|media-page|default',
              14 => 'ds_views|news-page|default',
              15 => 'ds_views|quotes-page|default',
              16 => 'ds_views|taxonomy_term-page|default',
              17 => 'node|article|cover',
              18 => 'node|article|default',
              19 => 'node|article|full',
              20 => 'node|article|preview',
              21 => 'node|article|search_result',
              22 => 'node|article|section_feature',
              23 => 'node|article|tagged_text',
              24 => 'node|article|teaser',
              25 => 'node|author_profile|author_title',
              26 => 'node|author_profile|cover_feature',
              27 => 'node|author_profile|default',
              28 => 'node|author_profile|full',
              29 => 'node|author_profile|preview',
              30 => 'node|author_profile|search_result',
              31 => 'node|calendar_event|default',
              32 => 'node|featured_content|cover_feature',
              33 => 'node|featured_content|default',
              34 => 'node|feature|cover',
              35 => 'node|feature|cover_feature',
              36 => 'node|feature|default',
              37 => 'node|feature|full',
              38 => 'node|feature|search_result',
              39 => 'node|feature|section_feature',
              40 => 'node|feature|teaser',
              41 => 'node|image_gallery|default',
              42 => 'node|image_gallery|search_result',
              43 => 'node|image_gallery|teaser',
              44 => 'node|issue_event|default',
              45 => 'node|issue_pdf|default',
              46 => 'node|media|cover',
              47 => 'node|media|full',
              48 => 'node|media|rss',
              49 => 'node|media|teaser',
              50 => 'node|page|cover',
              51 => 'node|page|default',
              52 => 'node|photo_feature|cover',
              53 => 'node|photo_feature|default',
              54 => 'node|post|cover',
              55 => 'node|post|full',
              56 => 'node|printcontent|default',
              57 => 'node|printcontent|tagged_text',
              58 => 'node|quote|cover',
              59 => 'node|quote|full',
              60 => 'node|webform|default',
              61 => 'node|ws_anniversary|default',
              62 => 'node|ws_birthday|default',
              63 => 'taxonomy_term|issue|default',
              64 => 'taxonomy_term|issue|full',
              65 => 'taxonomy_term|issue|inprint_cover',
              66 => 'taxonomy_term|issue|inside_inprint',
              67 => 'taxonomy_term|issue|issue_name',
              68 => 'user|user|full',
            ),
            'ds_fields' => 
            array (
              0 => 'author_profile_linked_title',
              1 => 'content_by_author',
              2 => 'gleaner_address',
              3 => 'gn_addthis',
              4 => 'gn_author_follow',
              5 => 'gn_comments',
              6 => 'gn_issue_date',
              7 => 'gn_issue_date_link',
              8 => 'gn_issue_description_title',
              9 => 'gn_issue_embed',
              10 => 'gn_issue_features',
              11 => 'gn_issue_image',
              12 => 'gn_issue_menu',
              13 => 'gn_issue_title',
              14 => 'gn_issue_viewer_link',
              15 => 'gn_messages',
              16 => 'gn_mini_calendar',
              17 => 'gn_more_events',
              18 => 'gn_node_meta',
              19 => 'gn_node_meta_author',
              20 => 'gn_node_meta_date',
              21 => 'gn_quote_citation',
              22 => 'gn_quote_citation_home',
              23 => 'gn_search_link',
              24 => 'gn_section_readmore',
              25 => 'gn_section_teaser_image',
              26 => 'gn_tabs',
              27 => 'photo_of_the_week_title',
              28 => 'staff_list',
            ),
            'ds_layout_settings' => 
            array (
              0 => 'ds_views|authors-page|default',
              1 => 'ds_views|columns-page|default',
              2 => 'ds_views|columns_content-page|default',
              3 => 'ds_views|events_calendar-page_1|default',
              4 => 'ds_views|events_calendar-page_4-fields|default',
              5 => 'ds_views|events_calendar-page_4|default',
              6 => 'ds_views|events_calendar-page|default',
              7 => 'ds_views|features-page|default',
              8 => 'ds_views|gn_gallery-page|default',
              9 => 'ds_views|home-page_1|default',
              10 => 'ds_views|home-page|default',
              11 => 'ds_views|inprint-page|default',
              12 => 'ds_views|media-page|default',
              13 => 'ds_views|news-page|default',
              14 => 'ds_views|quotes-page|default',
              15 => 'ds_views|taxonomy_term-page|default',
              16 => 'file|image|preview',
              17 => 'node|article|cover',
              18 => 'node|article|default',
              19 => 'node|article|demo',
              20 => 'node|article|form',
              21 => 'node|article|full',
              22 => 'node|article|preview',
              23 => 'node|article|print_version',
              24 => 'node|article|search_result',
              25 => 'node|article|section_feature',
              26 => 'node|article|tagged_text',
              27 => 'node|article|teaser',
              28 => 'node|author_profile|author_title',
              29 => 'node|author_profile|cover_feature',
              30 => 'node|author_profile|default',
              31 => 'node|author_profile|form',
              32 => 'node|author_profile|full',
              33 => 'node|author_profile|preview',
              34 => 'node|author_profile|search_result',
              35 => 'node|calendar_event|default',
              36 => 'node|featured_content|cover_feature',
              37 => 'node|featured_content|default',
              38 => 'node|feature|cover',
              39 => 'node|feature|cover_feature',
              40 => 'node|feature|default',
              41 => 'node|feature|form',
              42 => 'node|feature|full',
              43 => 'node|feature|inprint_cover',
              44 => 'node|feature|search_result',
              45 => 'node|feature|section_feature',
              46 => 'node|feature|teaser',
              47 => 'node|image_gallery|default',
              48 => 'node|image_gallery|search_result',
              49 => 'node|image_gallery|teaser',
              50 => 'node|issue_event|default',
              51 => 'node|issue_pdf|default',
              52 => 'node|media|cover',
              53 => 'node|media|full',
              54 => 'node|media|rss',
              55 => 'node|media|teaser',
              56 => 'node|page|cover',
              57 => 'node|page|default',
              58 => 'node|photo_feature|cover',
              59 => 'node|photo_feature|default',
              60 => 'node|post|cover',
              61 => 'node|post|full',
              62 => 'node|printcontent|default',
              63 => 'node|printcontent|form',
              64 => 'node|printcontent|tagged_text',
              65 => 'node|quote|cover',
              66 => 'node|quote|full',
              67 => 'node|quote|teaser',
              68 => 'node|sidebar|default',
              69 => 'node|sidebar|form',
              70 => 'node|webform|default',
              71 => 'node|ws_anniversary|form',
              72 => 'taxonomy_term|issue|default',
              73 => 'taxonomy_term|issue|full',
              74 => 'taxonomy_term|issue|inprint_cover',
              75 => 'taxonomy_term|issue|inside_inprint',
              76 => 'taxonomy_term|issue|issue_name',
              77 => 'user|user|full',
            ),
            'ds_vd' => 
            array (
              0 => 'authors-page',
              1 => 'columns-page',
              2 => 'columns_content-page',
              3 => 'events_calendar-page',
              4 => 'events_calendar-page_1',
              5 => 'events_calendar-page_4',
              6 => 'events_calendar-page_4-fields',
              7 => 'family-page_births',
              8 => 'features-page',
              9 => 'gn_gallery-page',
              10 => 'home-page',
              11 => 'home-page_1',
              12 => 'inprint-page',
              13 => 'media-page',
              14 => 'news-page',
              15 => 'quotes-page',
              16 => 'taxonomy_term-page',
            ),
            'ds_view_modes' => 
            array (
              0 => 'author_feature',
              1 => 'author_title',
              2 => 'cover',
              3 => 'cover_feature',
              4 => 'diff_standard',
              5 => 'inprint_cover',
              6 => 'inside_inprint',
              7 => 'issue_name',
              8 => 'preview',
              9 => 'print_version',
              10 => 'section_feature',
              11 => 'social_updates',
            ),
            'elysia_cron' => 
            array (
              0 => ':default',
              1 => 'acquia_agent_cron',
              2 => 'acquia_spi_cron',
              3 => 'advagg_cron',
              4 => 'backup_migrate_cron',
              5 => 'ctools_cron',
              6 => 'dblog_cron',
              7 => 'field_cron',
              8 => 'googleanalytics_cron',
              9 => 'mailchimp_lists_cron',
              10 => 'masquerade_cron',
              11 => 'node_cron',
              12 => 'redirect_cron',
              13 => 'rules_cron',
              14 => 'rules_scheduler_cron',
              15 => 'scheduler_cron',
              16 => 'search_cron',
              17 => 'system_cron',
              18 => 'themekey_cron',
              19 => 'trigger_cron',
              20 => 'update_cron',
              21 => 'user_import_cron',
              22 => 'views_bulk_operations_cron',
              23 => 'workflow_cron',
              24 => 'xmlsitemap_cron',
              25 => 'xmlsitemap_engines_cron',
              26 => 'xmlsitemap_menu_cron',
              27 => 'xmlsitemap_node_cron',
              28 => 'xmlsitemap_taxonomy_cron',
            ),
            'environment_indicator_environment' => 
            array (
              0 => 'dev_gleanernow_com',
              1 => 'local_development',
              2 => 'test_gleanernow_com',
            ),
            'features_api' => 
            array (
              0 => 'api:2',
            ),
            'field_base' => 
            array (
              0 => 'body',
              1 => 'field_ad_close_date',
              2 => 'field_anniversary_number',
              3 => 'field_author',
              4 => 'field_author_legacy',
              5 => 'field_baby_father',
              6 => 'field_baby_mother',
              7 => 'field_birth_date',
              8 => 'field_birth_location',
              9 => 'field_birthday_number',
              10 => 'field_body_print',
              11 => 'field_brides_name',
              12 => 'field_caption',
              13 => 'field_celebration_date',
              14 => 'field_celebration_location',
              15 => 'field_celebration_type',
              16 => 'field_compiled_annoucement',
              17 => 'field_couples_name',
              18 => 'field_couples_new_location',
              19 => 'field_credit',
              20 => 'field_date_birth',
              21 => 'field_date_death',
              22 => 'field_date_month',
              23 => 'field_death_age',
              24 => 'field_death_location',
              25 => 'field_description',
              26 => 'field_design_notes',
              27 => 'field_dropbox_link',
              28 => 'field_email',
              29 => 'field_facebook',
              30 => 'field_facebook_social',
              31 => 'field_family',
              32 => 'field_file',
              33 => 'field_full_name',
              34 => 'field_gender',
              35 => 'field_google_social',
              36 => 'field_gplus',
              37 => 'field_grandchildren',
              38 => 'field_great_grandchildren',
              39 => 'field_great_great_grandchildren',
              40 => 'field_grooms_father',
              41 => 'field_grooms_mother',
              42 => 'field_grooms_name',
              43 => 'field_grooms_stepfather',
              44 => 'field_grooms_stepmother',
              45 => 'field_hero_image',
              46 => 'field_hero_title_image',
              47 => 'field_image',
              48 => 'field_image_location',
              49 => 'field_issue',
              50 => 'field_latlon',
              51 => 'field_life_summary',
              52 => 'field_link',
              53 => 'field_link_to',
              54 => 'field_location',
              55 => 'field_maiden_name',
              56 => 'field_married',
              57 => 'field_married_bride',
              58 => 'field_married_groom',
              59 => 'field_mini_summary',
              60 => 'field_name',
              61 => 'field_news_section',
              62 => 'field_notes',
              63 => 'field_number',
              64 => 'field_organization',
              65 => 'field_pdf',
              66 => 'field_phone',
              67 => 'field_photog_location',
              68 => 'field_previous_married_name',
              69 => 'field_priority',
              70 => 'field_published',
              71 => 'field_pullquote',
              72 => 'field_quote_type',
              73 => 'field_real_name',
              74 => 'field_references',
              75 => 'field_related_content',
              76 => 'field_section',
              77 => 'field_section_editor',
              78 => 'field_share',
              79 => 'field_short_title',
              80 => 'field_sidebar',
              81 => 'field_speaker',
              82 => 'field_step_grandchildren',
              83 => 'field_step_great_grandchildren',
              84 => 'field_submission_type',
              85 => 'field_subtitle',
              86 => 'field_taxo_tags',
              87 => 'field_title',
              88 => 'field_title_or_relationship',
              89 => 'field_twitter',
              90 => 'field_twitter_social',
              91 => 'field_user_account',
              92 => 'field_video_embed',
              93 => 'field_volume_id',
              94 => 'field_website',
              95 => 'field_wedding_date',
              96 => 'field_wedding_location',
              97 => 'field_ws_bride_father',
              98 => 'field_ws_bride_mother',
              99 => 'field_ws_bride_stepfather',
              100 => 'field_ws_bride_stepmother',
              101 => 'field_ws_name',
              102 => 'field_ws_photo',
            ),
            'field_group' => 
            array (
              0 => 'group_author_meta|node|author_profile|full',
              1 => 'group_brides_parents|node|ws_wedding|form',
              2 => 'group_content|node|featured_content|form',
              3 => 'group_family_information|node|ws_anniversary|form',
              4 => 'group_family_information|node|ws_birthday|form',
              5 => 'group_family|node|ws_obituary|default',
              6 => 'group_grooms_parents|node|ws_wedding|form',
              7 => 'group_header|node|feature|default',
              8 => 'group_hero_graphics|node|feature|form',
              9 => 'group_image_gallery|node|article|full',
              10 => 'group_image_gallery|node|feature|default',
              11 => 'group_image_gallery|node|feature|full',
              12 => 'group_image_gallery|node|post|full',
              13 => 'group_imagegallery_slideshow|node|image_gallery|default',
              14 => 'group_meta|node|article|form',
              15 => 'group_meta|node|feature|form',
              16 => 'group_number_of|node|ws_anniversary|form',
              17 => 'group_number_of|node|ws_birthday|form',
              18 => 'group_number_of|node|ws_obituary|default',
              19 => 'group_number_of|node|ws_obituary|form',
              20 => 'group_parents|node|ws_wedding|form',
              21 => 'group_photog_info|node|photo_contest_entry|form',
              22 => 'group_printfields|node|article|form',
              23 => 'group_pub_settings_2|node|article|form',
              24 => 'group_pub_settings_2|node|feature|form',
              25 => 'group_pub_settings|node|article|form',
              26 => 'group_pub_settings|node|feature|form',
              27 => 'group_publication|node|article|form',
              28 => 'group_publication|node|feature|form',
              29 => 'group_quote|node|quote|cover',
              30 => 'group_quote|node|quote|full',
              31 => 'group_quote|node|quote|teaser',
              32 => 'group_socialupdates|node|post|form',
              33 => 'group_submitted_by|node|ws_anniversary|form',
              34 => 'group_submitted_by|node|ws_birthday|form',
              35 => 'group_submitted_by|node|ws_birth|form',
              36 => 'group_submitted_by|node|ws_obituary|default',
              37 => 'group_submitted_by|node|ws_obituary|form',
              38 => 'group_submitted_by|node|ws_wedding|form',
              39 => 'group_survivors|node|ws_obituary|form',
            ),
            'field_instance' => 
            array (
              0 => 'file-image-field_caption',
              1 => 'file-image-field_credit',
              2 => 'file-image-field_priority',
              3 => 'file-image-field_published',
              4 => 'node-ad_reference_form-body',
              5 => 'node-article-body',
              6 => 'node-article-field_author',
              7 => 'node-article-field_author_legacy',
              8 => 'node-article-field_body_print',
              9 => 'node-article-field_design_notes',
              10 => 'node-article-field_image',
              11 => 'node-article-field_issue',
              12 => 'node-article-field_latlon',
              13 => 'node-article-field_location',
              14 => 'node-article-field_mini_summary',
              15 => 'node-article-field_news_section',
              16 => 'node-article-field_priority',
              17 => 'node-article-field_pullquote',
              18 => 'node-article-field_related_content',
              19 => 'node-article-field_section',
              20 => 'node-article-field_share',
              21 => 'node-article-field_short_title',
              22 => 'node-article-field_taxo_tags',
              23 => 'node-author_profile-body',
              24 => 'node-author_profile-field_facebook',
              25 => 'node-author_profile-field_gplus',
              26 => 'node-author_profile-field_image',
              27 => 'node-author_profile-field_name',
              28 => 'node-author_profile-field_organization',
              29 => 'node-author_profile-field_title',
              30 => 'node-author_profile-field_twitter',
              31 => 'node-author_profile-field_user_account',
              32 => 'node-author_profile-field_website',
              33 => 'node-feature-body',
              34 => 'node-feature-field_author',
              35 => 'node-feature-field_author_legacy',
              36 => 'node-feature-field_body_print',
              37 => 'node-feature-field_design_notes',
              38 => 'node-feature-field_hero_image',
              39 => 'node-feature-field_hero_title_image',
              40 => 'node-feature-field_image',
              41 => 'node-feature-field_issue',
              42 => 'node-feature-field_latlon',
              43 => 'node-feature-field_location',
              44 => 'node-feature-field_mini_summary',
              45 => 'node-feature-field_news_section',
              46 => 'node-feature-field_priority',
              47 => 'node-feature-field_pullquote',
              48 => 'node-feature-field_related_content',
              49 => 'node-feature-field_section',
              50 => 'node-feature-field_short_title',
              51 => 'node-feature-field_sidebar',
              52 => 'node-feature-field_subtitle',
              53 => 'node-feature-field_taxo_tags',
              54 => 'node-featured_content-field_author',
              55 => 'node-featured_content-field_hero_image',
              56 => 'node-featured_content-field_link',
              57 => 'node-featured_content-field_link_to',
              58 => 'node-image_gallery-body',
              59 => 'node-image_gallery-field_image',
              60 => 'node-image_gallery-field_news_section',
              61 => 'node-image_gallery-field_taxo_tags',
              62 => 'node-issue_pdf-body',
              63 => 'node-issue_pdf-field_issue',
              64 => 'node-issue_pdf-field_link',
              65 => 'node-media-body',
              66 => 'node-media-field_author',
              67 => 'node-media-field_related_content',
              68 => 'node-media-field_taxo_tags',
              69 => 'node-media-field_video_embed',
              70 => 'node-page-body',
              71 => 'node-page-field_file',
              72 => 'node-photo_contest_entry-field_dropbox_link',
              73 => 'node-photo_contest_entry-field_email',
              74 => 'node-photo_contest_entry-field_image',
              75 => 'node-photo_contest_entry-field_name',
              76 => 'node-photo_contest_entry-field_phone',
              77 => 'node-photo_contest_entry-field_photog_location',
              78 => 'node-photo_contest_entry-field_submission_type',
              79 => 'node-photo_contest_entry-field_website',
              80 => 'node-photo_feature-field_image',
              81 => 'node-photo_feature-field_link',
              82 => 'node-post-body',
              83 => 'node-post-field_author',
              84 => 'node-post-field_body_print',
              85 => 'node-post-field_facebook_social',
              86 => 'node-post-field_google_social',
              87 => 'node-post-field_image',
              88 => 'node-post-field_issue',
              89 => 'node-post-field_taxo_tags',
              90 => 'node-post-field_twitter_social',
              91 => 'node-printcontent-body',
              92 => 'node-printcontent-field_body_print',
              93 => 'node-printcontent-field_design_notes',
              94 => 'node-printcontent-field_image',
              95 => 'node-printcontent-field_issue',
              96 => 'node-printcontent-field_priority',
              97 => 'node-printcontent-field_references',
              98 => 'node-printcontent-field_section',
              99 => 'node-quote-body',
              100 => 'node-quote-field_description',
              101 => 'node-quote-field_quote_type',
              102 => 'node-quote-field_speaker',
              103 => 'node-quote-field_taxo_tags',
              104 => 'node-quote-field_website',
              105 => 'node-sidebar-body',
              106 => 'node-sidebar-field_body_print',
              107 => 'node-sidebar-field_image',
              108 => 'node-sidebar-field_priority',
              109 => 'node-sidebar-field_subtitle',
              110 => 'node-webform-body',
              111 => 'node-ws_anniversary-field_anniversary_number',
              112 => 'node-ws_anniversary-field_brides_name',
              113 => 'node-ws_anniversary-field_celebration_date',
              114 => 'node-ws_anniversary-field_celebration_location',
              115 => 'node-ws_anniversary-field_celebration_type',
              116 => 'node-ws_anniversary-field_compiled_annoucement',
              117 => 'node-ws_anniversary-field_couples_name',
              118 => 'node-ws_anniversary-field_email',
              119 => 'node-ws_anniversary-field_family',
              120 => 'node-ws_anniversary-field_grandchildren',
              121 => 'node-ws_anniversary-field_great_grandchildren',
              122 => 'node-ws_anniversary-field_great_great_grandchildren',
              123 => 'node-ws_anniversary-field_grooms_name',
              124 => 'node-ws_anniversary-field_issue',
              125 => 'node-ws_anniversary-field_life_summary',
              126 => 'node-ws_anniversary-field_notes',
              127 => 'node-ws_anniversary-field_phone',
              128 => 'node-ws_anniversary-field_step_grandchildren',
              129 => 'node-ws_anniversary-field_step_great_grandchildren',
              130 => 'node-ws_anniversary-field_title_or_relationship',
              131 => 'node-ws_anniversary-field_wedding_date',
              132 => 'node-ws_anniversary-field_wedding_location',
              133 => 'node-ws_anniversary-field_ws_name',
              134 => 'node-ws_anniversary-field_ws_photo',
              135 => 'node-ws_birth-field_baby_father',
              136 => 'node-ws_birth-field_baby_mother',
              137 => 'node-ws_birth-field_birth_location',
              138 => 'node-ws_birth-field_compiled_annoucement',
              139 => 'node-ws_birth-field_date_birth',
              140 => 'node-ws_birth-field_email',
              141 => 'node-ws_birth-field_full_name',
              142 => 'node-ws_birth-field_issue',
              143 => 'node-ws_birth-field_married',
              144 => 'node-ws_birth-field_notes',
              145 => 'node-ws_birth-field_phone',
              146 => 'node-ws_birth-field_title_or_relationship',
              147 => 'node-ws_birth-field_ws_name',
              148 => 'node-ws_birthday-field_birth_date',
              149 => 'node-ws_birthday-field_birth_location',
              150 => 'node-ws_birthday-field_birthday_number',
              151 => 'node-ws_birthday-field_celebration_date',
              152 => 'node-ws_birthday-field_celebration_location',
              153 => 'node-ws_birthday-field_celebration_type',
              154 => 'node-ws_birthday-field_compiled_annoucement',
              155 => 'node-ws_birthday-field_email',
              156 => 'node-ws_birthday-field_family',
              157 => 'node-ws_birthday-field_full_name',
              158 => 'node-ws_birthday-field_grandchildren',
              159 => 'node-ws_birthday-field_great_grandchildren',
              160 => 'node-ws_birthday-field_great_great_grandchildren',
              161 => 'node-ws_birthday-field_issue',
              162 => 'node-ws_birthday-field_life_summary',
              163 => 'node-ws_birthday-field_maiden_name',
              164 => 'node-ws_birthday-field_notes',
              165 => 'node-ws_birthday-field_phone',
              166 => 'node-ws_birthday-field_previous_married_name',
              167 => 'node-ws_birthday-field_step_grandchildren',
              168 => 'node-ws_birthday-field_step_great_grandchildren',
              169 => 'node-ws_birthday-field_title_or_relationship',
              170 => 'node-ws_birthday-field_ws_name',
              171 => 'node-ws_birthday-field_ws_photo',
              172 => 'node-ws_obituary-field_birth_date',
              173 => 'node-ws_obituary-field_birth_location',
              174 => 'node-ws_obituary-field_compiled_annoucement',
              175 => 'node-ws_obituary-field_date_death',
              176 => 'node-ws_obituary-field_death_age',
              177 => 'node-ws_obituary-field_death_location',
              178 => 'node-ws_obituary-field_email',
              179 => 'node-ws_obituary-field_family',
              180 => 'node-ws_obituary-field_full_name',
              181 => 'node-ws_obituary-field_gender',
              182 => 'node-ws_obituary-field_grandchildren',
              183 => 'node-ws_obituary-field_great_grandchildren',
              184 => 'node-ws_obituary-field_great_great_grandchildren',
              185 => 'node-ws_obituary-field_issue',
              186 => 'node-ws_obituary-field_maiden_name',
              187 => 'node-ws_obituary-field_notes',
              188 => 'node-ws_obituary-field_phone',
              189 => 'node-ws_obituary-field_previous_married_name',
              190 => 'node-ws_obituary-field_step_grandchildren',
              191 => 'node-ws_obituary-field_step_great_grandchildren',
              192 => 'node-ws_obituary-field_title_or_relationship',
              193 => 'node-ws_obituary-field_ws_name',
              194 => 'node-ws_wedding-field_brides_name',
              195 => 'node-ws_wedding-field_compiled_annoucement',
              196 => 'node-ws_wedding-field_couples_new_location',
              197 => 'node-ws_wedding-field_email',
              198 => 'node-ws_wedding-field_grooms_father',
              199 => 'node-ws_wedding-field_grooms_mother',
              200 => 'node-ws_wedding-field_grooms_name',
              201 => 'node-ws_wedding-field_grooms_stepfather',
              202 => 'node-ws_wedding-field_grooms_stepmother',
              203 => 'node-ws_wedding-field_issue',
              204 => 'node-ws_wedding-field_married_bride',
              205 => 'node-ws_wedding-field_married_groom',
              206 => 'node-ws_wedding-field_phone',
              207 => 'node-ws_wedding-field_previous_married_name',
              208 => 'node-ws_wedding-field_title_or_relationship',
              209 => 'node-ws_wedding-field_wedding_date',
              210 => 'node-ws_wedding-field_wedding_location',
              211 => 'node-ws_wedding-field_ws_bride_father',
              212 => 'node-ws_wedding-field_ws_bride_mother',
              213 => 'node-ws_wedding-field_ws_bride_stepfather',
              214 => 'node-ws_wedding-field_ws_bride_stepmother',
              215 => 'node-ws_wedding-field_ws_name',
              216 => 'taxonomy_term-issue-field_ad_close_date',
              217 => 'taxonomy_term-issue-field_date_month',
              218 => 'taxonomy_term-issue-field_image',
              219 => 'taxonomy_term-issue-field_number',
              220 => 'taxonomy_term-issue-field_pdf',
              221 => 'taxonomy_term-issue-field_published',
              222 => 'taxonomy_term-issue-field_taxo_tags',
              223 => 'taxonomy_term-issue-field_volume_id',
              224 => 'user-user-field_real_name',
              225 => 'user-user-field_section_editor',
            ),
            'file_display' => 
            array (
              0 => 'image__default__file_field_file_rendered',
              1 => 'image__default__file_field_file_table',
              2 => 'image__default__file_field_file_url_plain',
              3 => 'image__default__file_field_image',
              4 => 'image__default__file_image',
              5 => 'image__full__file_image',
              6 => 'image__preview__file_field_image',
              7 => 'image__preview__file_image',
              8 => 'image__teaser__file_field_image',
              9 => 'image__teaser__file_image',
            ),
            'image' => 
            array (
              0 => 'gn_2_col',
              1 => 'gn_3_col',
              2 => 'gn_4_col',
              3 => 'gn_5_col',
              4 => 'gn_6_col',
              5 => 'gn_7_col',
              6 => 'gn_article_gallery',
              7 => 'gn_article_gallery_lg',
              8 => 'gn_article_gallery_sm',
              9 => 'gn_article_gallery_sq',
              10 => 'gn_article_gallery_thumb',
              11 => 'gn_author_feature',
              12 => 'gn_author_photo',
              13 => 'gn_author_photo_mobile',
              14 => 'gn_feature_hero',
              15 => 'gn_feature_hero_crop',
              16 => 'gn_feature_hero_text_crop',
              17 => 'gn_home_teaser',
              18 => 'gn_issue_feature',
              19 => 'gn_issue_feature_mobile',
              20 => 'gn_issue_teaser',
              21 => 'gn_media_teaser',
              22 => 'gn_media_teaser_mobile',
              23 => 'gn_section_feature',
              24 => 'gn_section_teaser',
              25 => 'social_image',
              26 => 'square_thumbnail',
            ),
            'mandrill_template_map' => 
            array (
              0 => 'gn_default_emails',
            ),
            'menu_custom' => 
            array (
              0 => 'main-menu',
              1 => 'menu-issue-extras',
              2 => 'menu-secondary-menu',
              3 => 'menu-social-links',
              4 => 'user-menu',
            ),
            'metatag' => 
            array (
              0 => 'global',
              1 => 'global:frontpage',
              2 => 'node',
              3 => 'node:article',
              4 => 'node:feature',
              5 => 'node:featured_content',
              6 => 'taxonomy_term:issue',
            ),
            'node' => 
            array (
              0 => 'article',
              1 => 'author_profile',
              2 => 'feature',
              3 => 'featured_content',
              4 => 'image_gallery',
              5 => 'issue_pdf',
              6 => 'media',
              7 => 'page',
              8 => 'photo_contest_entry',
              9 => 'photo_feature',
              10 => 'printcontent',
              11 => 'quote',
              12 => 'webform',
              13 => 'ws_anniversary',
              14 => 'ws_birth',
              15 => 'ws_birthday',
              16 => 'ws_obituary',
              17 => 'ws_wedding',
            ),
            'page_manager_handlers' => 
            array (
              0 => 'myworkbench_panel_context',
              1 => 'myworkbench_panel_context_2',
              2 => 'myworkbench_panel_context_3',
              3 => 'myworkbench_panel_context_4',
              4 => 'myworkbench_panel_context_5',
              5 => 'myworkbench_panel_context_6',
              6 => 'myworkbench_panel_context_7',
            ),
            'page_manager_pages' => 
            array (
              0 => 'classifieds',
              1 => 'family',
              2 => 'nw_roundtable',
            ),
            'path_breadcrumbs' => 
            array (
              0 => 'authors',
              1 => 'classified_breadcrumbs',
              2 => 'event',
              3 => 'events_view',
              4 => 'events_view_month',
              5 => 'events_view_year',
              6 => 'family_breadcrumbs',
              7 => 'features',
              8 => 'features_view',
              9 => 'gallery',
              10 => 'gallery_view',
              11 => 'inprint',
              12 => 'inprint_issue',
              13 => 'inprint_view',
              14 => 'media',
              15 => 'media_view',
              16 => 'news',
              17 => 'news_view',
              18 => 'news_view_clone',
              19 => 'worksheet_add',
            ),
            'picture_mapping' => 
            array (
              0 => 'custom_group',
              1 => 'feature_hero',
              2 => 'gn_article_gallery',
              3 => 'gn_author_cover_feature',
              4 => 'gn_author_photo',
              5 => 'gn_feature_hero',
              6 => 'gn_feature_hero_crop',
              7 => 'gn_feature_hero_text',
              8 => 'gn_gallery',
              9 => 'gn_gallery_teaser',
              10 => 'gn_home_images',
              11 => 'gn_issue_bug',
              12 => 'gn_issue_feature',
              13 => 'gn_issue_teaser',
              14 => 'gn_media',
              15 => 'gn_section_feature',
              16 => 'gn_section_teaser',
              17 => 'gn_slideshow',
              18 => 'gn_slideshow_thumbs',
            ),
            'rules_config' => 
            array (
              0 => 'gleaner_features_publish_and_set_date',
              1 => 'gleaner_features_workflow_set_web_publish_state',
              2 => 'rules_publish_issue_rule',
              3 => 'rules_unpublish_issue_rule',
              4 => 'rules_vbo_set_published',
              5 => 'rules_workflow_conference_notification',
              6 => 'rules_workflow_copy_edit',
              7 => 'rules_workflow_editorial_review',
              8 => 'rules_workflow_initial_review',
              9 => 'rules_workflow_stage_1_review',
              10 => 'rules_workflow_stage_2_review',
              11 => 'rules_workflow_stage_3_review',
              12 => 'rules_workflow_stage_4_review',
              13 => 'rules_workflow_stage_5_review',
              14 => 'rules_workflow_web_publish',
              15 => 'rules_worksheets_post_submit',
            ),
            'taxonomy' => 
            array (
              0 => 'classified_categories',
              1 => 'issue',
              2 => 'news_sections',
              3 => 'organization',
              4 => 'quote_type',
              5 => 'section_categories',
              6 => 'tags',
            ),
            'user_role' => 
            array (
              0 => 'administrator',
              1 => 'advertiser',
              2 => 'content author',
              3 => 'copy editor',
              4 => 'designer',
              5 => 'developer',
              6 => 'editor',
              7 => 'news editor',
              8 => 'production',
              9 => 'staff',
              10 => 'web editor',
            ),
            'variable' => 
            array (
              0 => 'auto_entitylabel_node_article',
              1 => 'auto_entitylabel_node_author_profile',
              2 => 'auto_entitylabel_node_featured_content',
              3 => 'auto_entitylabel_node_issue_pdf',
              4 => 'auto_entitylabel_node_photo_feature',
              5 => 'auto_entitylabel_node_printcontent',
              6 => 'auto_entitylabel_node_quote',
              7 => 'auto_entitylabel_node_ws_anniversary',
              8 => 'auto_entitylabel_node_ws_birth',
              9 => 'auto_entitylabel_node_ws_birthday',
              10 => 'auto_entitylabel_node_ws_obituary',
              11 => 'auto_entitylabel_node_ws_wedding',
              12 => 'auto_entitylabel_pattern_node_article',
              13 => 'auto_entitylabel_pattern_node_author_profile',
              14 => 'auto_entitylabel_pattern_node_featured_content',
              15 => 'auto_entitylabel_pattern_node_issue_pdf',
              16 => 'auto_entitylabel_pattern_node_photo_feature',
              17 => 'auto_entitylabel_pattern_node_printcontent',
              18 => 'auto_entitylabel_pattern_node_quote',
              19 => 'auto_entitylabel_pattern_node_ws_anniversary',
              20 => 'auto_entitylabel_pattern_node_ws_birth',
              21 => 'auto_entitylabel_pattern_node_ws_birthday',
              22 => 'auto_entitylabel_pattern_node_ws_obituary',
              23 => 'auto_entitylabel_pattern_node_ws_wedding',
              24 => 'auto_entitylabel_php_node_article',
              25 => 'auto_entitylabel_php_node_author_profile',
              26 => 'auto_entitylabel_php_node_featured_content',
              27 => 'auto_entitylabel_php_node_issue_pdf',
              28 => 'auto_entitylabel_php_node_photo_feature',
              29 => 'auto_entitylabel_php_node_printcontent',
              30 => 'auto_entitylabel_php_node_quote',
              31 => 'auto_entitylabel_php_node_ws_anniversary',
              32 => 'auto_entitylabel_php_node_ws_birth',
              33 => 'auto_entitylabel_php_node_ws_birthday',
              34 => 'auto_entitylabel_php_node_ws_obituary',
              35 => 'auto_entitylabel_php_node_ws_wedding',
              36 => 'field_bundle_settings_node__article',
              37 => 'field_bundle_settings_node__author_profile',
              38 => 'field_bundle_settings_node__feature',
              39 => 'field_bundle_settings_node__featured_content',
              40 => 'field_bundle_settings_node__image_gallery',
              41 => 'field_bundle_settings_node__issue_pdf',
              42 => 'field_bundle_settings_node__media',
              43 => 'field_bundle_settings_node__page',
              44 => 'field_bundle_settings_node__photo_feature',
              45 => 'field_bundle_settings_node__post',
              46 => 'field_bundle_settings_node__printcontent',
              47 => 'field_bundle_settings_node__quote',
              48 => 'field_bundle_settings_node__sidebar',
              49 => 'field_bundle_settings_node__webform',
              50 => 'field_bundle_settings_node__ws_anniversary',
              51 => 'field_bundle_settings_node__ws_birth',
              52 => 'field_bundle_settings_node__ws_birthday',
              53 => 'field_bundle_settings_node__ws_obituary',
              54 => 'field_bundle_settings_node__ws_wedding',
              55 => 'menu_options_ad_reference_form',
              56 => 'menu_options_article',
              57 => 'menu_options_author_profile',
              58 => 'menu_options_feature',
              59 => 'menu_options_featured_content',
              60 => 'menu_options_image_gallery',
              61 => 'menu_options_issue_event',
              62 => 'menu_options_issue_pdf',
              63 => 'menu_options_media',
              64 => 'menu_options_page',
              65 => 'menu_options_photo_feature',
              66 => 'menu_options_post',
              67 => 'menu_options_printcontent',
              68 => 'menu_options_quote',
              69 => 'menu_options_sidebar',
              70 => 'menu_options_webform',
              71 => 'menu_options_ws_anniversary',
              72 => 'menu_options_ws_birth',
              73 => 'menu_options_ws_birthday',
              74 => 'menu_options_ws_obituary',
              75 => 'menu_options_ws_wedding',
              76 => 'menu_parent_ad_reference_form',
              77 => 'menu_parent_article',
              78 => 'menu_parent_author_profile',
              79 => 'menu_parent_feature',
              80 => 'menu_parent_featured_content',
              81 => 'menu_parent_image_gallery',
              82 => 'menu_parent_issue_event',
              83 => 'menu_parent_issue_pdf',
              84 => 'menu_parent_media',
              85 => 'menu_parent_page',
              86 => 'menu_parent_photo_feature',
              87 => 'menu_parent_post',
              88 => 'menu_parent_printcontent',
              89 => 'menu_parent_quote',
              90 => 'menu_parent_sidebar',
              91 => 'menu_parent_webform',
              92 => 'menu_parent_ws_anniversary',
              93 => 'menu_parent_ws_birth',
              94 => 'menu_parent_ws_birthday',
              95 => 'menu_parent_ws_obituary',
              96 => 'menu_parent_ws_wedding',
              97 => 'node_options_ad_reference_form',
              98 => 'node_options_article',
              99 => 'node_options_author_profile',
              100 => 'node_options_feature',
              101 => 'node_options_featured_content',
              102 => 'node_options_image_gallery',
              103 => 'node_options_issue_event',
              104 => 'node_options_issue_pdf',
              105 => 'node_options_media',
              106 => 'node_options_page',
              107 => 'node_options_photo_feature',
              108 => 'node_options_post',
              109 => 'node_options_printcontent',
              110 => 'node_options_quote',
              111 => 'node_options_sidebar',
              112 => 'node_options_webform',
              113 => 'node_options_ws_anniversary',
              114 => 'node_options_ws_birth',
              115 => 'node_options_ws_birthday',
              116 => 'node_options_ws_obituary',
              117 => 'node_options_ws_wedding',
              118 => 'node_preview_ad_reference_form',
              119 => 'node_preview_article',
              120 => 'node_preview_author_profile',
              121 => 'node_preview_feature',
              122 => 'node_preview_featured_content',
              123 => 'node_preview_image_gallery',
              124 => 'node_preview_issue_pdf',
              125 => 'node_preview_media',
              126 => 'node_preview_page',
              127 => 'node_preview_photo_feature',
              128 => 'node_preview_post',
              129 => 'node_preview_printcontent',
              130 => 'node_preview_quote',
              131 => 'node_preview_sidebar',
              132 => 'node_preview_webform',
              133 => 'node_preview_ws_anniversary',
              134 => 'node_preview_ws_birth',
              135 => 'node_preview_ws_birthday',
              136 => 'node_preview_ws_obituary',
              137 => 'node_preview_ws_wedding',
              138 => 'node_submitted_ad_reference_form',
              139 => 'node_submitted_article',
              140 => 'node_submitted_author_profile',
              141 => 'node_submitted_feature',
              142 => 'node_submitted_featured_content',
              143 => 'node_submitted_image_gallery',
              144 => 'node_submitted_issue_event',
              145 => 'node_submitted_issue_pdf',
              146 => 'node_submitted_media',
              147 => 'node_submitted_page',
              148 => 'node_submitted_photo_feature',
              149 => 'node_submitted_post',
              150 => 'node_submitted_printcontent',
              151 => 'node_submitted_quote',
              152 => 'node_submitted_sidebar',
              153 => 'node_submitted_webform',
              154 => 'node_submitted_ws_anniversary',
              155 => 'node_submitted_ws_birth',
              156 => 'node_submitted_ws_birthday',
              157 => 'node_submitted_ws_obituary',
              158 => 'node_submitted_ws_wedding',
            ),
          ),
          'features_exclude' => 
          array (
            'field' => 
            array (
              'node-article-body' => 'node-article-body',
              'node-article-field_author' => 'node-article-field_author',
              'node-article-field_section' => 'node-article-field_section',
              'node-article-field_issue' => 'node-article-field_issue',
              'node-article-field_body_print' => 'node-article-field_body_print',
              'node-article-field_design_notes' => 'node-article-field_design_notes',
              'node-article-field_related_content' => 'node-article-field_related_content',
              'node-article-field_priority' => 'node-article-field_priority',
              'node-article-field_taxo_tags' => 'node-article-field_taxo_tags',
              'node-article-field_location' => 'node-article-field_location',
              'node-article-field_latlon' => 'node-article-field_latlon',
              'node-article-field_short_title' => 'node-article-field_short_title',
              'node-article-field_print_title' => 'node-article-field_print_title',
              'node-article-field_print_subtitle' => 'node-article-field_print_subtitle',
              'node-article-field_pullquote' => 'node-article-field_pullquote',
              'node-article-field_mini_summary' => 'node-article-field_mini_summary',
              'node-article-field_print_workflow' => 'node-article-field_print_workflow',
              'node-article-field_sample_image' => 'node-article-field_sample_image',
              'node-article-field_pictures' => 'node-article-field_pictures',
              'field_collection_item-field_pictures-field_image' => 'field_collection_item-field_pictures-field_image',
              'field_collection_item-field_pictures-field_caption' => 'field_collection_item-field_pictures-field_caption',
              'field_collection_item-field_pictures-field_credit' => 'field_collection_item-field_pictures-field_credit',
              'field_collection_item-field_pictures-field_web_print' => 'field_collection_item-field_pictures-field_web_print',
              'taxonomy_term-issue-field_volume_id' => 'taxonomy_term-issue-field_volume_id',
              'taxonomy_term-issue-field_number' => 'taxonomy_term-issue-field_number',
              'taxonomy_term-issue-field_pdf' => 'taxonomy_term-issue-field_pdf',
              'taxonomy_term-issue-field_taxo_tags' => 'taxonomy_term-issue-field_taxo_tags',
              'taxonomy_term-issue-field_image_hero' => 'taxonomy_term-issue-field_image_hero',
              'taxonomy_term-issue-field_date_month' => 'taxonomy_term-issue-field_date_month',
              'node-article-field_image' => 'node-article-field_image',
              'taxonomy_term-issue-field_image' => 'taxonomy_term-issue-field_image',
            ),
            'dependencies' => 
            array (
              'media' => 'media',
              'entity' => 'entity',
              'date_views' => 'date_views',
              'ds' => 'ds',
              'views' => 'views',
              'ctools' => 'ctools',
              'date' => 'date',
              'file' => 'file',
              'file_entity' => 'file_entity',
              'image' => 'image',
              'options' => 'options',
              'rules' => 'rules',
              'taxonomy' => 'taxonomy',
              'conditional_fields' => 'conditional_fields',
              'exif_custom' => 'exif_custom',
              'html5_tools' => 'html5_tools',
            ),
            'field_base' => 
            array (
              'field_image_hero' => 'field_image_hero',
            ),
            'variable' => 
            array (
              'auto_entitylabel_node_photo_contest_entry' => 'auto_entitylabel_node_photo_contest_entry',
              'auto_entitylabel_pattern_node_photo_contest_entry' => 'auto_entitylabel_pattern_node_photo_contest_entry',
              'auto_entitylabel_php_node_photo_contest_entry' => 'auto_entitylabel_php_node_photo_contest_entry',
              'field_bundle_settings_node__photo_contest_entry' => 'field_bundle_settings_node__photo_contest_entry',
              'menu_options_photo_contest_entry' => 'menu_options_photo_contest_entry',
              'menu_parent_photo_contest_entry' => 'menu_parent_photo_contest_entry',
              'node_options_photo_contest_entry' => 'node_options_photo_contest_entry',
              'node_preview_photo_contest_entry' => 'node_preview_photo_contest_entry',
              'node_submitted_photo_contest_entry' => 'node_submitted_photo_contest_entry',
            ),
          ),
          'mtime' => '1424913747',
          'no autodetect' => '1',
          'description' => '',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.12',
      ),
      'gleaner_photo_contest' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_photo_contest/gleaner_photo_contest.module',
        'basename' => 'gleaner_photo_contest.module',
        'name' => 'gleaner_photo_contest',
        'info' => 
        array (
          'name' => 'Gleaner Photo Contest',
          'description' => 'Gleaner Photo Contest views and styles.',
          'core' => '7.x',
          'package' => 'Gleaner',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'flag',
            2 => 'rules',
            3 => 'rules_conditional',
            4 => 'session_api',
            5 => 'views',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'gleaner_custom' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/gleaner_custom/gleaner_custom.module',
        'basename' => 'gleaner_custom.module',
        'name' => 'gleaner_custom',
        'info' => 
        array (
          'name' => 'Gleaner Custom',
          'description' => 'A custom module to provide custom functions and integration for the Gleaner',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'commerce_wordcounter' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/custom/commerce_wordcounter/commerce_wordcounter.module',
        'basename' => 'commerce_wordcounter.module',
        'name' => 'commerce_wordcounter',
        'info' => 
        array (
          'name' => 'Commerce Wordcounter',
          'description' => 'Contains rules for calculating product price adjustments based on entity word counts.',
          'core' => '7.x',
          'package' => 'Commerce (contrib)',
          'dependencies' => 
          array (
            0 => 'commerce_node_checkout',
            1 => 'rules',
            2 => 'wordcounter',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => NULL,
      ),
      'plupload' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/modules/plupload/plupload.module',
        'basename' => 'plupload.module',
        'name' => 'plupload',
        'info' => 
        array (
          'name' => 'Plupload integration module',
          'description' => 'Provides a plupload element.',
          'files' => 
          array (
            0 => 'plupload.module',
          ),
          'core' => '7.x',
          'package' => 'Media',
          'version' => '7.x-1.7',
          'project' => 'plupload',
          'datestamp' => '1415390716',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.x-1.7',
      ),
    ),
    'themes' => 
    array (
      'ember' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/contrib/ember/ember.info',
        'basename' => 'ember.info',
        'name' => 'Ember',
        'info' => 
        array (
          'name' => 'Ember',
          'description' => 'A responsive administration theme.',
          'package' => 'Core',
          'version' => '7.x-2.0-alpha2+53-dev',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'styles/style.css',
              1 => 'styles/system.base.css',
              2 => 'styles/system.menus.css',
              3 => 'styles/system.theme.css',
              4 => 'styles/system.messages.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'messages' => 'Messages',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
          'project' => 'ember',
          'datestamp' => '1424961485',
        ),
        'version' => '7.x-2.0-alpha2+53-dev',
      ),
      'aurora' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/contrib/aurora/aurora.info',
        'basename' => 'aurora.info',
        'name' => 'Aurora',
        'info' => 
        array (
          'name' => 'Aurora',
          'description' => 'Lightweight Responsive Ready HTML5 base theme',
          'core' => '7.x',
          'regions' => 
          array (
            'header' => 'Header',
            'main_prefix' => 'Main Region Prefix',
            'content' => 'Main',
            'main_suffix' => 'Main Region Suffix',
            'footer' => 'Footer',
          ),
          'plugins' => 
          array (
            'panels' => 
            array (
              'layouts' => 'layouts',
            ),
          ),
          'settings' => 
          array (
            'aurora_min_ie_support' => '9',
            'aurora_html_tags' => '1',
            'aurora_typekit_id' => '; Information added by Drupal.org packaging script on 2015-02-11',
          ),
          'version' => '7.x-3.5',
          'project' => 'aurora',
          'datestamp' => '1423684983',
        ),
        'version' => '7.x-3.5',
      ),
      'gleanernow' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/gleanernow/gleanernow.info',
        'basename' => 'gleanernow.info',
        'name' => 'gleanernow',
        'info' => 
        array (
          'name' => 'gleanernow',
          'description' => 'gleanernow theme based on the Aurora base theme.',
          'core' => '7.x',
          'base theme' => 'aurora',
          'regions' => 
          array (
            'header' => 'Header',
            'header_suffix' => 'Header - Suffix',
            'menu' => 'Main Menu',
            'main_prefix' => 'Main Region Prefix',
            'content' => 'Main Region',
            'main_suffix' => 'Main Region Suffix',
            'footer' => 'Footer',
            'footer_social' => 'Footer - Social',
            'footer_sub' => 'Footer - Sub',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'stylesheets/style.css',
            ),
            'print' => 
            array (
              0 => 'stylesheets/print.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'javascripts/imagesloaded.pkgd.min.js',
            1 => 'javascripts/responsive-nav.min.js',
            2 => 'javascripts/jquery.sly.min.js',
            3 => 'javascripts/jquery.fitvids.js',
            4 => 'javascripts/scripts.js',
          ),
          'modernizr' => 
          array (
            'tests' => 
            array (
              0 => 'css_boxsizing',
            ),
          ),
          'settings' => 
          array (
            'toggle_node_user_picture' => '1',
            'toggle_comment_user_picture' => '1',
            'toggle_comment_user_verification' => '1',
            'default_logo' => '1',
            'logo_path' => '',
            'logo_upload' => '',
            'default_favicon' => '1',
            'favicon_path' => '',
            'favicon_upload' => '',
            'hide_recomended_modules' => '0',
            'aurora_enable_chrome_frame' => '1',
            'aurora_min_ie_support' => '10',
            'aurora_html_tags' => '1',
            'aurora_typekit_id' => '0',
            'aurora_livereload' => '0',
            'magic_embedded_mqs' => '0',
            'magic_css_excludes' => '',
            'magic_footer_js' => '0',
            'magic_library_head' => '0',
            'magic_experimental_js' => '0',
            'magic_js_excludes' => '',
            'magic_rebuild_registry' => '0',
            'magic_viewport_indicator' => '0',
            'magic_modernizr_debug' => '0',
            'magic_performance__active_tab' => 'edit-dev',
            'magic_css_excludes_regex' => '',
            'magic_js_excludes_regex' => '',
          ),
          'breakpoints' => 
          array (
            'wide' => '(min-width: 50em)',
            'narrow' => '(min-width: 30em)',
            'mobile' => '(min-width: 0px)',
          ),
          'plugins' => 
          array (
            'panels' => 
            array (
              'layouts' => 'layouts',
            ),
          ),
          'version' => 'unknown',
          'project' => 'gleanernow',
          'datestamp' => '1383156312',
        ),
        'version' => 'unknown',
      ),
      'cinder' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/cinder/cinder.info',
        'basename' => 'cinder.info',
        'name' => 'Cinder',
        'info' => 
        array (
          'name' => 'Cinder',
          'description' => 'Cinder in a cleaned up version of Ember 2.x. Hopefully, much of this code can be removed at Ember stableizes.',
          'core' => '7.x',
          'base theme' => 'ember',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'stylesheets/screen.css',
            ),
            'print' => 
            array (
              0 => 'stylesheets/print.css',
            ),
          ),
        ),
        'version' => NULL,
      ),
      'weebpal_backend' => 
      array (
        'filename' => '/var/aegir/platforms/gleaner_cms_1.6.1/sites/all/themes/custom/weebpal_backend/weebpal_backend.info',
        'basename' => 'weebpal_backend.info',
        'name' => 'WeebPal Backend',
        'info' => 
        array (
          'name' => 'WeebPal Backend',
          'description' => 'Backend theme from WeebPal',
          'version' => '7.35',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'fonts/font-awesome/css/font-awesome.css',
              1 => 'css/theme.css',
            ),
          ),
          'scripts' => 
          array (
            0 => 'js/weebpal_backend.js',
          ),
          'features' => 
          array (
            0 => 'logo',
            1 => 'name',
            2 => 'slogan',
            3 => 'node_user_picture',
            4 => 'comment_user_picture',
            5 => 'comment_user_verification',
            6 => 'favicon',
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'highlighted' => 'Highlighted',
            'help' => 'Help',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'footer' => 'Footer',
          ),
        ),
        'version' => '7.35',
      ),
    ),
  ),
  'profiles' => 
  array (
    'minimal' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'standard' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
  ),
);